
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(event){
	$('#site-content').animate({ scrollTop: 0 }, "slow");
	var validated = true;
	debugger;
	var qgs = sm.questionGroups();
	var qgNotFilled = -1;
	for(var k=0;k<qgs.length;k++){
		var qg = qgs[k];
		for (var int = 0; int < qg.questions.length; int++) {
			var question = qg.questions[int];
			if(question.oid ==null && question.atext==null){
				var optionFlag = false;
				for (var int2 = 0; int2 < question.optionDTOs.length; int2++) {
					var option = question.optionDTOs[int2];
					if(option.sel == true){
						optionFlag = false;
						break;
					}else{
						optionFlag = true;
					}
				}
				if(question.optionDTOs.length==0 && question.atext==null){
					validated = false;
					if(qgNotFilled==-1){
						qgNotFilled = k;
					}
					break;
				}
				if(optionFlag == true){
					validated = false;
					if(qgNotFilled==-1){
						qgNotFilled = k;
					}
					break;
				}
			}
		}
	}
	
	if($(this).parent().attr('value') == ($('.licl').length)){
		//Submit
		
		if(validated == false){
			for(var j=($('.licl').length-1);j>qgNotFilled;j--){
				$($("#progressbar li")[j]).removeClass("active");
				$($('.licl')[($('.licl').length-1)]).hide();
				$($('.licl')[($('.licl').length-1)]).css('opacity',0);
				$($('.licl')[qgNotFilled]).css('opacity',1);
				$('.licl').each(function(){
					$(this).css('transform','');
				})
				$($('.licl')[qgNotFilled]).show();
			}
			alert("Please enter all the values before you submit");
			return false;
		}
		if(animating) return false;
		animating = true;
		var data = sm.data;
		data[0].questionGroupDTOs = sm.questionGroups();
		$.LoadingOverlay("show");
		$.ajax({
	         type: "POST",
	         contentType: "application/json",
	         url: appContext+"/proxy/api/survey?_csrf=" + $('#csrf').val(),
	         data: JSON.stringify(data[0]),
	         success: function(result) {
	        	 alert("Survey Posted");
	        	 $('.fbForm').hide();
	        	 $('.fbCompleted').show();
	        	 $.LoadingOverlay("hide");
	         },
	         error: function(){
	        	 alert("There was an error with your previous request.");
	        	 $.LoadingOverlay("hide");
	         },
	         dataType: "text"
	     });
	}

	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	if($(this).parent().attr('role')+1>0){
		$('.previous').show();
	}else{
		$('.previous').hide();
	}
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($(".questions>li").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show();
	if($(this).parent().attr('value') == ($('.licl').length)-1){
		$('.next').last().attr('value','Submit');
	}
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({
        'transform': 'scale('+scale+')',
        'position': 'absolute'
      });
			next_fs.css({'left': "5%", 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			
			animating = false;
			
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});	
	
});

$(".previous").click(function(event){	
	$('#site-content').animate({ scrollTop: 0 }, "slow");
	if(animating) return false;
	animating = true;
	debugger;
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();

	//de-activate current step on progressbar
	$("#progressbar li").eq($(".questions>li").index(current_fs)).removeClass("active");
	if($(this).parent().attr('role')-1>0){
		$('.previous').show();
	}else{
		$('.previous').hide();
	}
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': "5%"});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
	
});

$(".submit").click(function(){
	return false;
})
