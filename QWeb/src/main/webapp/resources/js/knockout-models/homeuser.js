var checkLimit = 20;
function SurveyModel() {
	var self = this;	
	self.data = ko.observableArray([]);;
	self.questionGroups = ko.observableArray([]);
	var sumId = $('#smhdnId').val();
	$.LoadingOverlay("show");
	$.getJSON(appContext+"/proxy/api/surveys?status=ASSIGNED&sumId="+sumId, function(data) {
		self.data = data;
		self.questionGroups(data[0].questionGroupDTOs);
		$.LoadingOverlay("hide");
	});
}

function otherfn(id,ev){
	
	if($(":checkbox:checked" ).length > checkLimit) {
		
		   ev.preventDefault();
		   if(ev.stopPropagation) {
			    // for proper browsers ...
			    ev.stopPropagation();
			  } else {
			      // internet exploder uses cancelBubble ...
			      window.event.cancelBubble = true;
			  }
		   alert("You can only select three options");
	       this.checked = false;
	       return false;
	}
	var indexS = id.replace("othersCheckbox","");
	var textBoxIndex = "othersTextbox"+indexS;
	if($('#'+textBoxIndex).is(":visible")){
		$('#'+textBoxIndex).hide();
	}else{
		$('#'+textBoxIndex).show();
	}
}

function cbChange(id,ev){
	
	if($(":checkbox:checked" ).length > checkLimit) {
		ev.preventDefault();
		if(ev.stopPropagation) {
		    // for proper browsers ...
		    ev.stopPropagation();
		  } else {
		      // internet exploder uses cancelBubble ...
		      window.event.cancelBubble = true;
		  }
		alert("You can only select three options");
		this.checked = false;
		return false;
	       
	}
}