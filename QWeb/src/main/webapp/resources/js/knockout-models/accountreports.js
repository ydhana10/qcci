
$.getJSON(appContext+"/proxy/api/verticals", function(data) {
	$("#verticalSelectId1 option").remove();
	$('<option>').val('0').text('Please Select').appendTo('#verticalSelectId1');
	for (var int = 0; int < data.length; int++) {
		var obj = data[int];
		$('<option>').val(''+obj.verticalId).text(''+obj.verticalName).appendTo('#verticalSelectId1');
	}
});

$('#verticalSelectId1').on('change',function(){
	$("#clientSelectId1 option").remove();
	$('<option>').val('0').text('Please Select').appendTo('#clientSelectId1');
	$.getJSON(appContext+"/proxy/api/clients?verticalId="+$(this).find(":selected").val(), function(data) {
		for (var int = 0; int < data.length; int++) {
			var obj = data[int];
			$('<option>').val(''+obj.clientId).text(''+obj.clientName).appendTo('#clientSelectId1');
		}
	});
});

var clientName;
function clientNameForId(data){
	var self = this;
	var svs = clientName;
	self.data=data;
}

var svsData;
function NPSScore(data) {
	var self = this;
	var svs = svsData;
	self.data=data;

}

function overallAccountScore(data) {
	var self = this;
	self.user12 = ko.observableArray([]);
	self.user12 = data;

}

var svsDat;
function NPSScoreQuestionWise(data) {
	var self = this;
	var svs = svsDat;
	self.data=data;
}


function SurveyDetailsForTopBenefits(data){
	var self = this;
	self.data = ko.observableArray([]);
	self.data = data;
}

function SurveyDetailsForQuestionSet1(dat){
	
	var nonZeroCounter = 0;

	var self = this;
	self.user1 = ko.observableArray([]);
	self.user1 = dat;
	
}

function SurveyDetailsForQuestionSet2(dat){
	var self = this;
	self.user2 = ko.observableArray([]);
	self.user2 = dat;
}

function SurveyDetailsForQuestionSet3(dat){
	var self = this;
	self.user3 = ko.observableArray([]);
	self.user3 = dat;
}

function SurveyDetailsForQuestionSet4(dat){
	var self = this;
	self.user4 = ko.observableArray([]);
	self.user4 = dat;
}
function SurveyDetailsForQuestionSet5(dat){
	var self = this;
	self.user5 = ko.observableArray([]);
	self.user5 = dat;
}




function viewAccountReports(){
	$('#previouspage').show();
	$.LoadingOverlay("show");
	var clientid = document.getElementById('clientSelectId1').value;
	
	$.getJSON(appContext + "/proxy/api/clientName?clientId="+clientid,
			
			function(data){

  		var indexLocation = $(this).attr('id');
		var dmC = new clientNameForId(data);
		ko.applyBindings(dmC, document.getElementById('accountName'));
		$('#accountName').show();
		$.LoadingOverlay("hide");
	});
	
	
  	$.getJSON(appContext + "/proxy/api/accountreports?clientId="+clientid,
			function(data) {
  		var indexLocation = $(this).attr('id');
		var dm1 = new NPSScore(data);
		ko.applyBindings(dm1, document.getElementById('dt1'));
		$('#dt1').show();
		$.LoadingOverlay("hide");
	});
  	
  	$.getJSON(appContext + "/proxy/api/accountnpsreports?clientId="+clientid,
			function(data) {
  		var indexLocation = $(this).attr('id');
		var dm = new overallAccountScore(data);
		ko.applyBindings(dm, document.getElementById('dt'));
		$('#dt').show();
		$.LoadingOverlay("hide");
	});
  	
  	
  	
  	
  	$.getJSON(appContext + "/proxy/api/accounttop3benefits?clientId="+clientid,
			function(data) {
  		var indexLocation = $(this).attr('id');
		var dm2 = new SurveyDetailsForTopBenefits(data);
		ko.applyBindings(dm2, document.getElementById('dt2'));
		$('#dt2').show();
		$.LoadingOverlay("hide");
	});
  	
  	
  	
  	var questionGrpId = 1;+
  	$.getJSON(appContext + "/proxy/api/avgscoresquestionset?clientId="+clientid+"&questionGrpId="+questionGrpId,
			function(data) {
		var dm3 = new SurveyDetailsForQuestionSet1(data);
		ko.applyBindings(dm3, document.getElementById('dt3'));
		$('#dt3').show();
		$.LoadingOverlay("hide");
	});
  	
  	
  	var questionGrpId = 2;
  		$.getJSON(appContext + "/proxy/api/avgscoresquestionset?clientId="+clientid+"&questionGrpId="+questionGrpId,
			function(data) {
		var indexLocation = $(this).attr('id');
		var dm4 = new SurveyDetailsForQuestionSet2(data);
		ko.applyBindings(dm4, document.getElementById('dt4'));
		$('#dt4').show();
		$.LoadingOverlay("hide");
	});
  	
  	var questionGrpId = 3;
  	$.getJSON(appContext + "/proxy/api/avgscoresquestionset?clientId="+clientid+"&questionGrpId="+questionGrpId,
			function(data) {
		var indexLocation = $(this).attr('id');
		var dm5 = new SurveyDetailsForQuestionSet3(data);
		ko.applyBindings(dm5, document.getElementById('dt5'));
		$('#dt5').show();
		$.LoadingOverlay("hide");
	});
  	
  	var questionGrpId = 4;
  	$.getJSON(appContext + "/proxy/api/avgscoresquestionset4?clientId="+clientid+"&questionGrpId="+questionGrpId,
			function(data) {
		var indexLocation = $(this).attr('id');
		var dm8 = new SurveyDetailsForQuestionSet4(data);
		ko.applyBindings(dm8, document.getElementById('dt8'));
		$('#dt8').show();
		$.LoadingOverlay("hide");
	});
  	
  
  	$.getJSON(appContext + "/proxy/api/accountNps?clientId="+clientid,
			function(data) {
		var indexLocation = $(this).attr('id');
		var dm9 = new SurveyDetailsForQuestionSet5(data);
		ko.applyBindings(dm9, document.getElementById('dt9'));
		$('#dt9').show();
		$.LoadingOverlay("hide");
	});
  	
  /*	$.getJSON(appContext + "/proxy/api/npsQuestionWise?clientId="+clientid,
			function(data) {
  		var indexLocation = $(this).attr('id');
		var dm6 = new NPSScoreQuestionWise(data);
		ko.applyBindings(dm6, document.getElementById('dt7'));
		$('#dt7').show();
		$.LoadingOverlay("hide");
	});*/
  	
  	
  	$('#view').show();
  	$('.modal-body').hide();
  	
  	
}

function viewPDF(){
	window.open('/qcsat/views/pages/accountreportpdf.jsp?clientId='+document.getElementById('clientSelectId1').value);
}



