function overAllAverageNps(data){
	
	var self = this;
	self.user1 = ko.observableArray([]);
	self.user1 = data;
	
}

function repondantTop3Benefits(data){
	var self = this;
	self.data = ko.observableArray([]);
	self.data = data;
}

function RespondantSurveyDetailsForQuestionSet1(dat){
	var self = this;
	self.user1 = ko.observableArray([]);
	self.user1 = dat;
}


function RespondantSurveyDetailsForQuestionSet2(dat){
	var self = this;
	self.user2 = ko.observableArray([]);
	self.user2 = dat;
}

function RespondantSurveyDetailsForQuestionSet3(dat){
	var self = this;
	self.user3 = ko.observableArray([]);
	self.user3 = dat;
}

function RespondantNps(dat){
	var self = this;
	self.user4 = ko.observableArray([]);
	self.user4 = dat;
}

function RespondantAnswersForQuestionSet4(dat){
		var self = this;
		self.user5 = ko.observableArray([]);
		self.user5 = dat;
}





$.getJSON(appContext+"/proxy/api/verticals", function(data) {
	$("#verticalSelectId1 option").remove();
	$('<option>').val('0').text('Please Select').appendTo('#verticalSelectId1');
	for (var int = 0; int < data.length; int++) {
		var obj = data[int];
		$('<option>').val(''+obj.verticalId).text(''+obj.verticalName).appendTo('#verticalSelectId1');
	}
});

$('#verticalSelectId1').on('change',function(){
	$("#clientSelectId1 option").remove();
	$('<option>').val('0').text('Please Select').appendTo('#clientSelectId1');
	$.getJSON(appContext+"/proxy/api/clients?verticalId="+$(this).find(":selected").val(), function(data) {
		for (var int = 0; int < data.length; int++) {
			var obj = data[int];
			$('<option>').val(''+obj.clientId).text(''+obj.clientName).appendTo('#clientSelectId1');
		}
	});
});


$('#clientSelectId1').on('change',function(){
	
	$("#respondantId1 option").remove();
	$('<option>').val('0').text('Please Select').appendTo('#respondantId1');
	$.getJSON(appContext+"/proxy/api/respondants?clientId="+$(this).find(":selected").val(),function(data){
		for(var int = 0;int <data.length; int++){
			var obj = data[int];
			
			$('<option>').val(''+obj.id).text(''+obj.firstName+obj.lastName).appendTo('#respondantId1');
		}
	
	});
});




function viewRespondantReports(){
	
	$('#previouspage').show();
	$.LoadingOverlay("show");
		
		var respondantId = document.getElementById('respondantId1').value;
		$.getJSON(appContext + "/proxy/api/respondanttop3benefits?respondantId="+respondantId,
				function(data) {
				var indexLocation = $(this).attr('id');
			var dm1 = new repondantTop3Benefits(data);
			ko.applyBindings(dm1, document.getElementById('dt1'));
			$('#dt1').show();
			$.LoadingOverlay("hide");
		});
		
		var questionGrpId = 1;
	  	$.getJSON(appContext + "/proxy/api/respondantAvgScoresForQuestionSet?respondantId="+respondantId+"&questionGrpId="+questionGrpId,
				function(data) {
			var dm2 = new RespondantSurveyDetailsForQuestionSet1(data);
			ko.applyBindings(dm2, document.getElementById('dt2'));
			$('#dt2').show();
			$.LoadingOverlay("hide");
		});
	  	
	  	var questionGrpId = 2;
  		$.getJSON(appContext + "/proxy/api/respondantAvgScoresForQuestionSet?respondantId="+respondantId+"&questionGrpId="+questionGrpId,
			function(data) {
		var indexLocation = $(this).attr('id');
		var dm3 = new RespondantSurveyDetailsForQuestionSet2(data);
		ko.applyBindings(dm3, document.getElementById('dt3'));
		$('#dt3').show();
		$.LoadingOverlay("hide");
	});
  		
  		var questionGrpId = 3;
  		$.getJSON(appContext + "/proxy/api/respondantAvgScoresForQuestionSet?respondantId="+respondantId+"&questionGrpId="+questionGrpId,
			function(data) {
		var indexLocation = $(this).attr('id');
		var dm4 = new RespondantSurveyDetailsForQuestionSet3(data);
		ko.applyBindings(dm4, document.getElementById('dt4'));
		$('#dt4').show();
		$.LoadingOverlay("hide");
	});
  		
  		$.getJSON(appContext + "/proxy/api/respondantNps?respondantId="+respondantId,
  				function(data) {
  			var indexLocation = $(this).attr('id');
  			var dm5 = new RespondantNps(data);
  			ko.applyBindings(dm5, document.getElementById('dt5'));
  			$('#dt5').show();
  			$.LoadingOverlay("hide");
  		});
  		
  		var questionGrpId = 4;
  	  	$.getJSON(appContext + "/proxy/api/respondantAnswerForQuestionSet4?respondantId="+respondantId+"&questionGrpId="+questionGrpId,
  				function(data) {
  	  		
  			var indexLocation = $(this).attr('id');
  			var dm6 = new RespondantAnswersForQuestionSet4(data);
  			ko.applyBindings(dm6, document.getElementById('dt6'));
  			$('#dt6').show();
  			console.dir(data);
  			$.LoadingOverlay("hide");
  		});
  	  
  	  	$.getJSON(appContext + "/proxy/api/overAllAverageNpsForRespondant?respondantId="+respondantId,
  			function(data) {
  		
  			var indexLocation = $(this).attr('id');
  		var dmForAvg = new overAllAverageNps(data);
  		ko.applyBindings(dmForAvg, document.getElementById('doq'));
  		$('#doq').show();
  	});
  	 
  	  	$('#view').show();
    	$('.modal-body').hide();
    	
  	  	
}

function viewPDF(){
	
	window.open('/qcsat/views/pages/Respondentwisereport.jsp?respondantId='+document.getElementById('respondantId1').value);
}

