var svsData; 
function AllSurveysModel(data) {
	var self = this;
	self.surveys = ko.observableArray([]);
	self.qgs = ko.observableArray([]);
	self.surveys(data);
	self.qgs(data[0]);
}

var accountReports1;
function svsDataForAccountReport1(data){
	var self = this;
	self.surveys = ko.observableArray([]);
	self.qgs = ko.observableArray([]);
	self.surveys(data);
	self.qgs(data[0]);
}

var accountReports2;
function svsDataForAccountReport2(data){
	var self = this;
	self.surveys = ko.observableArray([]);
	self.qgs = ko.observableArray([]);
	self.surveys(data);
	self.qgs(data[0]);
}
function selectAll(btnId){
	if($('#'+btnId).is(":checked")){
		$('.cbCls').each(function(){
			if(!$(this).is(':disabled')){
				$(this).prop("checked", true);
			}
		});
		
	}else{
		$('.cbCls').each(function(){
			if(!$(this).is(':disabled')){
				$(this).prop("checked", false);
			}
		});
	}
}

//function to display the survey results
$.LoadingOverlay("show");
$.getJSON(appContext + "/proxy/api/allsurveys", function(data) {
	//self.surveys(data);
	svsData = data;
	ko.applyBindings(new AllSurveysModel(data), document.getElementById('pr'));
	$('#shid').text('Survey Id:'+data[0].sid+' Survey Name: '+data[0].sname);
	$('#srhid').hide();
	$("#pr").DataTable({"columnDefs": [ {"targets": 6,"orderable": false} ]});
	$.LoadingOverlay("hide");
});


$.getJSON(appContext + "/proxy/api/survey/clients?surveyId=" + 1,
		function(data) {
			for (var int = 0; int < data.length; int++) {
				var obj = data[int];
				$('<option>').val('' + obj.clientId).text('' + obj.clientName)
						.appendTo('#clientSelectId');
			}
		});

$('#exportAllId').on(
		'click',
		function() {
			$.LoadingOverlay("show");
			var surveyId = 1;
			var clientSelectId = $('#clientSelectId').val();

			var startDate = $('#startDate').val();
			var endDate = $('#endDate').val();
			if(startDate == null || startDate == undefined || startDate == ""){
				alert("Please enter start date");
				return false;
			}
			if(endDate == null || endDate == undefined || endDate == ""){
				alert("Please enter end date");
				return false;
			}
			var status = "COMPLETED";
			$.getJSON(appContext + "/proxy/api/file/download?status=" + status
					+ "&startDate=" + startDate + "&endDate=" + endDate
					+ "&surveyId=" + surveyId + "&clientId=" + clientSelectId,
					function(data) {
						$('#did').show();
						$('#did').attr('href', tempAppPath + data.filename);
						$('#did').attr('download', data.filename);

						$('#did').click();
						$.LoadingOverlay("hide");
					});
		});

function SurveyDetailModel(dat) {
	alert(JSON.stringify(dat));
	var self = this;
	var svs = svsData;
	self.qgs = ko.observableArray([]);
	self.qgs(dat);

}

function SurveyDetailModelForAcc(dat) {
	var self = this;
	var svs = svsData;
	self.qgs = ko.observableArray([]);
	alert(self.qgs);
	self.qgs(dat);

}

function sendMail(id) {
	window.event.preventDefault();
	var confirmFlag = confirm("Are you sure, you want to send the notification ?");
	if(confirmFlag){
		$.LoadingOverlay("show");
		$.ajax({
			type : "POST",
			url : appContext + "/proxy/api/notify?_csrf=" + $('#csrf').val()
					+ "&sumId=" + id,
			data : null,
			success : function(result) {
				alert("Notification Sent");
				var count = parseInt($('#'+id).closest('td').prev('td').text());
				$('#'+id).closest('td').prev('td').text(count+1);
				$.LoadingOverlay("hide");
			},
			error: function(){
				$.LoadingOverlay("hide");
				alert('There was an error with your previous request.');
			},
			dataType : "text"
		});
	}

}

function onComplete(id){
	$.LoadingOverlay("show");
	$.getJSON(appContext + "/proxy/api/qgs?sumId="+id,
			function(data) {
		$('#srhid').show();
		var indexLocation = $(this).attr('id');
		var dm = new SurveyDetailModel(data);
		ko.applyBindings(dm, document.getElementById('dt'));
		$('#ptbl').hide();
		$('#dt').show();
		$.LoadingOverlay("hide");
	});
	
}


function sendMailToSel() {
	window.event.preventDefault();
	var confirmFlag = confirm("Are you sure, you want to send the notification ?");
	var id;
	var arr = [];
	
	if(confirmFlag){
		$.LoadingOverlay("show");
		$('#pr input:checked').each(function(){
			
			id = $(this).attr('id');
			if(id == 'sltAllId'){
				return true;
			}
			var count = parseInt($('#'+id).closest('td').prev('td').text());
			$('#'+id).closest('td').prev('td').text(count+1);
			arr.push(id);
		});
		$.ajax({
			type : "POST",
			url : appContext + "/proxy/api/notifyMany?_csrf=" + $('#csrf').val()+"&sumIds="+arr.join(),
			data : null,
			success : function(result) {
				$.LoadingOverlay("hide");
			},
			error: function(){
				$.LoadingOverlay("hide");
			},
			dataType : "text"
		});
		
		
	}

}

$.getJSON(appContext+"/proxy/api/verticals", function(data) {
	$("#verticalSelectId1 option").remove();
	$('<option>').val('0').text('Please Select').appendTo('#verticalSelectId1');
	for (var int = 0; int < data.length; int++) {
		var obj = data[int];
		$('<option>').val(''+obj.verticalId).text(''+obj.verticalName).appendTo('#verticalSelectId1');
	}
});

$('#verticalSelectId1').on('change',function(){
	$("#clientSelectId1 option").remove();
	$('<option>').val('0').text('Please Select').appendTo('#clientSelectId1');
	$.getJSON(appContext+"/proxy/api/clients?verticalId="+$(this).find(":selected").val(), function(data) {
		for (var int = 0; int < data.length; int++) {
			var obj = data[int];
			$('<option>').val(''+obj.clientId).text(''+obj.clientName).appendTo('#clientSelectId1');
		}
	});
});

var clientid = document.getElementById('clientSelectId1').value;
/*function viewAccountReports(){
	alert("here");
	
	alert("client id: "+clientid);
	$.LoadingOverlay("show");
	$.getJSON(appContext + "/proxy/api/accountreports?clientId="+clientid,
			function(data) {
		$('#srhid').show();
		var indexLocation = $(this).attr('id');
		var dm = new SurveyDetailModel(data);
		ko.applyBindings(dm, document.getElementById('dt1'));
		$('#ptbl').hide();
		$('#dt').hide();
		$('#dt1').show();
		//$('#accountsModal').hide();
		$.LoadingOverlay("hide");
	});
	
}*/
function viewAccountReports(){
$.LoadingOverlay("show");
$.getJSON(appContext + "/proxy/api/avgscoresquestionset?clientId="+clientid+"&questionGrpId="+1, function(data) {
	accountReports1 = data;
	ko.applyBindings(new svsDataForAccountReport1(data), document.getElementById('rt1'));
	ko.cleanNode($element[0]);
	$('#srhid').hide();
	$.LoadingOverlay("hide");
});

$.LoadingOverlay("show");
$.getJSON(appContext + "/proxy/api/avgscoresquestionset?clientId="+clientid+"&questionGrpId="+2, function(data) {
	accountReports2 = data;
	ko.applyBindings(new svsDataForAccountReport2(data), document.getElementById('rt2'));
	ko.cleanNode($element[0]);
	$('#srhid').hide();
	$.LoadingOverlay("hide");
});

/*$.LoadingOverlay("show");
$.getJSON(appContext + "/proxy/api/avgscoresquestionset?clientId="+clientid+"&questionGrpId="+3, function(data) {
	svsData = data;
	ko.applyBindings(new svsDataForAccountReport(data), document.getElementById('rt3'));
	ko.cleanNode($element[0]);
	$('#srhid').hide();
	$.LoadingOverlay("hide");
})*/
}







	
