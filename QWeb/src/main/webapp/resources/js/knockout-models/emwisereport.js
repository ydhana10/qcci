
$.getJSON(appContext+"/proxy/api/verticals", function(data) {
	$("#verticalSelectId1 option").remove();
	$('<option>').val('0').text('Please Select').appendTo('#verticalSelectId1');
	for (var int = 0; int < data.length; int++) {
		var obj = data[int];
		$('<option>').val(''+obj.verticalId).text(''+obj.verticalName).appendTo('#verticalSelectId1');
	}
});

/*$('#verticalSelectId1').on('change',function(){
	$("#clientSelectId1 option").remove();
	$('<option>').val('0').text('Please Select').appendTo('#clientSelectId1');
	$.getJSON(appContext+"/proxy/api/clients?verticalId="+$(this).find(":selected").val(), function(data) {
		for (var int = 0; int < data.length; int++) {
			var obj = data[int];
			$('<option>').val(''+obj.clientId).text(''+obj.clientName).appendTo('#clientSelectId1');
		}
	});
});*/

$('#verticalSelectId1').on('change',function(){
	$("#engagementManagerSelectId1 option").remove();
	$('<option>').val('0').text('Please Select').appendTo('#engagementManagerSelectId1');
	$.getJSON(appContext+"/proxy/api/engagementmanager?verticalId="+$(this).find(":selected").val(),function(data){
		for(var int = 0; int < data.length; int++){
			var obj = data[int];
			$('<option>').val(''+obj.engagementManager).text(''+obj.engagementManager).appendTo('#engagementManagerSelectId1');
		}
		
	});
});








function overallEmwiseScore(dat){
	
	var self = this;
	self.data = ko.observableArray([]);
	self.data = dat;
}


function overallNpsEmwiseScore(dat){
	
	var self = this;
	self.user12 = ko.observableArray([]);
	self.user12 = dat;
}

function EmWiseTopBenefits(data){
	var self = this;
	self.data = ko.observableArray([]);
	self.data = data;
}



function EMWiseReportForQuestionGroupAverage(dat){
	;
	var self = this;
	self.user1 = ko.observableArray([]);
	self.user1 = dat;
}

function EMWiseNps(dat){
//	alert(JSON.stringify(dat));
	var self = this;
	self.user2 = ko.observableArray([]);
	self.user2 = dat;
}

function AvgOverallQuestiongrpEmWise(data){
	
	var self = this;
	self.user3 = ko.observableArray([]);
	self.user3 = data;
	
}


function viewEmReports(){
	
	var engagementManager = document.getElementById('engagementManagerSelectId1').value;
	//alert(engagementManager)
	
	
	$.getJSON(appContext + "/proxy/api/emwisenps1?engagementManager="+engagementManager,
			function(data) {
  		
  		var indexLocation = $(this).attr('id');
		var dm = new overallEmwiseScore(data);
		
		ko.applyBindings(dm, document.getElementById('dt'));
		$('#dt').show();
		$.LoadingOverlay("hide");
	});
  	
	$.getJSON(appContext + "/proxy/api/emwiseoverallnps?engagementManager="+engagementManager,
			function(data) {
  		
  		var indexLocation = $(this).attr('id');
		var dm5 = new overallNpsEmwiseScore(data);
		
		ko.applyBindings(dm5, document.getElementById('dt5'));
		$('#dt5').show();
		$.LoadingOverlay("hide");
	});
  	
	
	
	
	$.getJSON(appContext + "/proxy/api/top3BenefitsForEmwise?engagementManager="+engagementManager,
			function(data) {
  		
  		var indexLocation = $(this).attr('id');
		var dm = new EmWiseTopBenefits(data);
		
		ko.applyBindings(dm, document.getElementById('dt6'));
		$('#dt6').show();
		$.LoadingOverlay("hide");
	});
  	
	
	
	
	
	
	
  	$.getJSON(appContext + "/proxy/api/emwiseAvgForQuestionGroup?engagementManager="+engagementManager+"&questionGrpId="+1,
			function(data) {
  		$.LoadingOverlay("show");
  		var indexLocation = $(this).attr('id');
		var dm1 = new EMWiseReportForQuestionGroupAverage(data);
		ko.applyBindings(dm1, document.getElementById('dt1'));
		$('#dt1').show();
		$.LoadingOverlay("hide");
	});
	
	$.getJSON(appContext + "/proxy/api/emwisenps?engagementManager="+engagementManager,
			function(data) {
  		$.LoadingOverlay("show");
  		var indexLocation = $(this).attr('id');
		var dm2 = new EMWiseNps(data);
		ko.applyBindings(dm2, document.getElementById('dt2'));
		$('#dt2').show();
		$.LoadingOverlay("hide");
	});
	
	$.getJSON(appContext + "/proxy/api/avgOverallAverageOfQuestiongrpForEM?engagementManager="+engagementManager,
			function(data) {
  		var indexLocation = $(this).attr('id');
		var dmForAvg = new AvgOverallQuestiongrpEmWise(data);
		ko.applyBindings(dmForAvg, document.getElementById('doq'));
		$('#doq').show();
		
	});
	
	$('#view').show();
	

}

function viewPDF(){
	window.open('/qcsat/views/pages/emwisereportpdf.jsp?engagementManager='+document.getElementById('engagementManagerSelectId1').value);
}