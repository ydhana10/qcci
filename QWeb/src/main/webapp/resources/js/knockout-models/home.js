$(document).ready(function() {
	$(".table").DataTable();
    var file = $('[name="file"]');
    
    $('#btnUpload').on('click', function() {
        var filename = "CSAT"+new Date().valueOf()+".xlsx";
        var fnvd = document.getElementById("fid").files[0].name;
        var extsn = getFileExtension1(fnvd);
        if(extsn == 'xlsx' || extsn == 'xls'){
        	$.LoadingOverlay("show");
            $.ajax({
            	url:appContext+'/proxy/api/file/upload?_csrf=' + $('#csrf').val()+"&filename="+filename+"&surveyId="+$(this).parent().parent().find('#surveyId').text(),
                type: "POST",
                data: new FormData(document.getElementById("msform")),
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false
              }).done(function(data) {
            	  $.LoadingOverlay("hide");
            	  if(("Failure".indexOf(data)) !== -1){
            		  alert('File upload failed ...');
            	  }else{
            		  alert('File upload done ...');
            	  }
            	  
              }).fail(function(jqXHR, textStatus) {
            	  $.LoadingOverlay("hide");
                  alert('File upload failed ...');
              });
        }else{
        	alert("Request Failed, You can upload only Excel Files");
        	return false;
        }  
        
    });
    
    $('#btnClear').on('click', function() {
        file.val('');
    });
});




$.getJSON(appContext+"/proxy/api/verticals", function(data) {
	$("#verticalSelectId option").remove();
	$('<option>').val('0').text('Please Select').appendTo('#verticalSelectId');
	for (var int = 0; int < data.length; int++) {
		var obj = data[int];
		$('<option>').val(''+obj.verticalId).text(''+obj.verticalName).appendTo('#verticalSelectId');
	}
});

$('#verticalSelectId').on('change',function(){
	$("#clientSelectId option").remove();
	$('<option>').val('0').text('Please Select').appendTo('#clientSelectId');
	$.getJSON(appContext+"/proxy/api/clients?verticalId="+$(this).find(":selected").val(), function(data) {
		for (var int = 0; int < data.length; int++) {
			var obj = data[int];
			$('<option>').val(''+obj.clientId).text(''+obj.clientName).appendTo('#clientSelectId');
		}
	});
});

$.getJSON(appContext+"/proxy/api/verticals", function(data) {
	$("#verticalSelectId1 option").remove();
	$('<option>').val('0').text('Please Select').appendTo('#verticalSelectId1');
	for (var int = 0; int < data.length; int++) {
		var obj = data[int];
		$('<option>').val(''+obj.verticalId).text(''+obj.verticalName).appendTo('#verticalSelectId1');
	}
});

$('#verticalSelectId1').on('change',function(){
	alert("here2");
	$("#clientSelectId1 option").remove();
	$('<option>').val('0').text('Please Select').appendTo('#clientSelectId1');
	$.getJSON(appContext+"/proxy/api/clients?verticalId="+$(this).find(":selected").val(), function(data) {
		for (var int = 0; int < data.length; int++) {
			var obj = data[int];
			$('<option>').val(''+obj.clientId).text(''+obj.clientName).appendTo('#clientSelectId1');
		}
	});
});


$('#btn1Id').on('click',function(){
	var obj = {};
	obj.clientId = parseInt($('#clientSelectId').find(":selected").val());
	obj.firstName = $('#firstNameId').val();
	obj.lastName = $('#lastNameId').val();
	obj.email = $('#emailId').val();
	obj.engagementManager = $('#emId').val();
	obj.surveyId = $('#surveyId').text();
	if(obj.clientId==null || obj.clientId==undefined||obj.clientId ==""){
		alert('Please enter Client Id');
		return false;
	}
	if(obj.firstName==null || obj.firstName==undefined||obj.firstName ==""){
		alert('Please enter First Name');
		return false;
	}
	if(obj.lastName==null || obj.lastName==undefined||obj.lastName ==""){
		alert('Please enter Last Name');
		return false;
	}
	if(obj.email==null || obj.email==undefined||obj.email ==""){
		alert('Please enter Email Id');
		return false;
	}
	if(obj.engagementManager==null || obj.engagementManager==undefined||obj.engagementManager ==""){
		alert('Please mention Engagement Manager');
		return false;
	}
	if(!validateEmail(obj.email)){
		alert('Please enter a valid Email Id');
		return false;
	}
	$.LoadingOverlay("show");
	 $.ajax({
         type: "POST",
         contentType: "application/json",
         url: appContext+"/proxy/api/user?_csrf=" + $('#csrf').val(),
         data: JSON.stringify(obj),
         success: function(result) {
        	 alert("Survey Assigned");
        	 $('#btnId').click();
        	 $.LoadingOverlay("hide");
         },
         error: function(){
        	 $.LoadingOverlay("hide");
        	 alert("There was an error with your previous request.");
         },
         dataType: "text"
     });
});


$('#btn2Id').on('click',function(){
	var obj = {};
	obj.clientId = parseInt($('#clientSelectId1').find(":selected").val());
	$.LoadingOverlay("show");
	 $.ajax({
         type: "POST",
         contentType: "application/json",
         url: appContext+"/proxy/api/accountreports?_csrf=" + $('#csrf').val(),
         data: JSON.stringify(obj),
         success: function(result) {
        	 alert("Survey Assigned");
        	 $('#btnId').click();
        	 $.LoadingOverlay("hide");
         },
         error: function(){
        	 $.LoadingOverlay("hide");
        	 alert("There was an error with your previous request.");
         },
         dataType: "text"
     });
});