<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link href="<c:url value='/resources/css/custom_styles.css' />"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="/qcsat/resources/css/report_styles.css" />

<div class="dropdown menus">
	<a style="color: white; font-size: 14px; padding: 5px;"
		href="${pageContext.request.contextPath}/home">Home</a>

	<button class="btn btn-link"
		style="color: white; font-size: 14px; padding: 5px;" type="button"
		data-toggle="dropdown" data-hover="dropdown">
		Reports <span class="caret"></span>
	</button>
	<ul class="dropdown-menu">
		<li><a
			href="${pageContext.request.contextPath}/overallaccountreport">Overall
				Quinnox Report</a></li>
		<li><a href="${pageContext.request.contextPath}/accountreports">Account
				Wise Report</a></li>
		<li><a href="${pageContext.request.contextPath}/emwisereport">Engagement
				Managers Report</a></li>
		<li><a
			href="${pageContext.request.contextPath}/npspromotersreport">Overall
				NPS Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npspromoters">NPS
				Promoters Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npsdetractors">NPS
				Detractors Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npsneutral">NPS
				Passive Report</a></li>
		<li><a
			href="${pageContext.request.contextPath}/respondantwisereport">Repondants
				Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npsquestionwise">NPS
				Question Report</a></li>
	</ul>
</div>
<div class="container pageLayout">
	<form id="msform">
		<div class="form-group">
			<div>
				<h3 id='shid'></h3>

			</div>

			<br clear="all" /> <br clear="all" />
			<div>
				<h2 class="title" id="shid">NPS Question Wise</h2>
			</div>

			<div class="pdflink">
				<a
					href="javascript:window.open('/qcsat/views/pages/npsquestioswisepdf.jsp', 'NPS Question Wise', 'width=200,height=150');">View
					Report</a>
			</div>
			<br> <br> <br>
			<table id="dt1" class="table table-bordered centerTable ">
				<thead class="thead-inverse">
					<tr>
						<th class="darkbgcolor" colspan="5">NPS Data Points</th>
					</tr>
					<tr>
						<td></td>
						<td class="text-left">How willing are you to recommand
							Quinnox to other business units</td>
						<td class="text-left">How willing are you to recommand
							Quinnox to other companies</td>
						<td class="text-left">How willing are you to buy new
							services from Quinnox</td>
						<td class="text-left">How will are you to 'Beta test' with
							few offerings from quinnox in your organisation</td>
					</tr>
					<tr class="highlight-td ">
						<th class="text-center">Name Of Respondant</th>
						<th class="text-center">Q.1</th>
						<th class="text-center">Q.2</th>
						<th class="text-center">Q.3</th>
						<th class="text-center">Q.4</th>
					</tr>
				</thead>
				<tbody>

					<tr class="text-center">
						<td class="lightbgcolor">Count Of Promoters</td>
						<td data-bind="text:user5.NeutralId1"></td>
						<td data-bind="text:user5.NeutralId2"></td>
						<td data-bind="text:user5.NeutralId3"></td>
						<td data-bind="text:user5.NeutralId4"></td>
					</tr>

					<tr class="text-center">
						<td class="lightbgcolor">Count Of Detractors</td>
						<td data-bind="text:user5.DetractorsId1"></td>
						<td data-bind="text:user5.DetractorsId2"></td>
						<td data-bind="text:user5.DetractorsId3"></td>
						<td data-bind="text:user5.DetractorsId4"></td>
					</tr>

					<tr class="text-center">

						<td class="lightbgcolor">Count Of Passive</td>
						<td data-bind="text:user5.PromotersId1"></td>
						<td data-bind="text:user5.PromotersId2"></td>
						<td data-bind="text:user5.PromotersId3"></td>
						<td data-bind="text:user5.PromotersId4"></td>
					</tr>
				<tbody>
			</table>


			<!-- 	<table id="dt2" class ="table table-bordered"
							style="display: none; border: 1px solid #cccccc; width: 71%; table-layout: fixed;margin-left: 159px; " >
							<thead class="thead-inverse">
								<tr>
									<th style="background-color: #cccccc;" colspan="6">CSAT Sent And Recieved</th>
								</tr>
								
							</thead>
							<tbody data-bind="foreach:user5">
							ko foreach: questionGroupDTOs
								
								<tr>
								<td  data-bind="text:$parent.firstName+' '+$parent.lastName "></td>
								ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 })
									<td align="center" data-bind="text:answerDTO.option.otext"></td>
								/ko
									ko if: $parent.npsType == 'detractor'
										<td align="center"><img src="/qcsat/resources/img/detract.jpg" alt="Logo" style="width: 34px;height: 34px;"></td>	
									/ko
									ko if: $parent.npsType == 'promoter'
										<td align="center"><img src="/qcsat/resources/img/promoter.png" alt="Logo" style="width: 52px;height: 48px;"></td>	
									/ko
									ko if: $parent.npsType == 'neutral'
										<td align="center"><img src="/qcsat/resources/img/neutral.jpg" alt="Logo" style="width: 35px;height: 30px;"></td>	
									/ko
									
								</tr>
							/ko
							
							<tbody>
						</table> -->


			<table id="dt4" class="table table-bordered centerTable">
				<thead class="thead-inverse">
					<tr>
						<th class="darkbgcolor" colspan="7">NPS Data Points</th>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td class="text-left">How willing are you to recommend
							Quinnox to other business units</td>
						<td class="text-left">How willing are you to recommend
							Quinnox to other companies</td>
						<td class="text-left">How willing are you to buy new services
							from Quinnox</td>
						<td class="text-left">How willing are you to 'Beta test'
							with few offerings from Quinnox in your organization</td>
						<td></td>
					</tr>

					<tr class="lightbgcolor">
						<th class="text-center">Client Name</th>
						<th class="text-center">Name Of Respondant</th>
						<th class="text-center">Q.1</th>
						<th class="text-center">Q.2</th>
						<th class="text-center">Q.3</th>
						<th class="text-center">Q.4</th>
						<th class="text-center">NPS Type</th>
					</tr>
				</thead>
				<tbody data-bind="foreach:user">
					<!-- ko foreach: questionGroupDTOs -->

					<tr class="text-center">
						<td class="lightbgcolor" data-bind="text:$parent.clientName"></td>
						<td class="lightbgcolor"
							data-bind="text:$parent.firstName+' '+$parent.lastName "></td>
						<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
						<td data-bind="text:answerDTO.option.otext"></td>
						<!-- /ko -->
						<!-- ko if: $parent.npsType == 'detractor' -->
						<td><img src="/qcsat/resources/img/detract.jpg" alt="Logo"
							class="imgCodeDetractor"></td>
						<!-- /ko -->
						<!-- ko if: $parent.npsType == 'promoter' -->
						<td><img src="/qcsat/resources/img/promoter.png" alt="Logo"
							class="imgCode"></td>
						<!-- /ko -->
						<!-- ko if: $parent.npsType == 'neutral' -->
						<td><img src="/qcsat/resources/img/neutral.jpg" alt="Logo"
							class="imgCodeNetural"></td>
						<!-- /ko -->

					</tr>
					<!-- /ko -->
				<tbody>
			</table>
		</div>
	</form>
</div>

<p class="footerCode">&copy; 2017 Quinnox Inc.
<p>
	<script type="text/html" id="nps-data">
	<tr>
		<td><span data-bind="text:data"></span></td>
	</tr>
	

</script>

	<script type="text/javascript"
		src="<c:url value='/resources/js/knockout-models/npsquestionwise.js' />"></script>

	<script type="text/javascript">
		var first = false;
		//var asm = new AllSurveysModel();
		//ko.applyBindings(asm, document.getElementById('pr'));
		// 	$('.btnCls').on('click', function() {
		// 		var indexLocation = $(this).attr('id');
		// 		var dm = new SurveyDetailModel(indexLocation);
		// 		ko.applyBindings(dm, document.getElementById('dt'));
		// 		$('#ptbl').hide();
		// 		$('#dt').show();
		// 	});
		$('.form_date').datetimepicker({
			language : 'fr',
			weekStart : 1,
			todayBtn : 1,
			autoclose : 1,
			todayHighlight : 1,
			startView : 2,
			minView : 2,
			forceParse : 0
		});

		function executeDetailJS() {
		}
</script>