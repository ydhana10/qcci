<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<link href="<c:url value='/resources/css/custom_styles.css' />"
	rel="stylesheet">
 <div class="container" style="padding-top: 81px;">
  		<h1 class="text-center feedback_heading">FEEDBACK SURVEY</h1>
        <form id="msform" class="fbForm">
        <input type="hidden" id="smhdnId" value="${sumId}"/>  
			<!-- progressbar -->
			 <!--  <ul id="progressbar">
				<li class="active"></li>
				<li></li>
				<li></li>
				<li></li>
				<li></li>
			  </ul>
 -->
              <div class="form-group">
              	<ol class="questions" start="1" data-bind="foreach: {data:questionGroups.sort(function (l, r) { return l.grpId > r.grpId ? 1 : -1 }),afterRender:executeJS}">
                	<div class="licl" data-bind="attr:{role:$index(),value:$index()+1}">
                    	<!-- <span data-bind="text: grpDesc"></span> -->
                        <ol data-bind="foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 })" start="1">
                        	<li data-bind="attr:{role:$index(),value:$index()+1}" ><span data-bind="text: qtext"></span>
                                <div class="form-group">
									<span data-bind="if: optionDTOs.length == 0">
	                                    <textarea class="form-control" rows="6" cols="130" data-bind="value:atext"></textarea>
                                    </span>                              
                                	<span data-bind="foreach: optionDTOs.sort(function (l, r) { return l.oid > r.oid ? 1 : -1 })">
                                		
	                                	<!-- If Radiobutton -->
	                                	<span data-bind="if: $parent.questionTypeDTO.qtype == 'radiobutton'">
		                                    <label class="radio-inline">
		                                    	<input type="radio" data-bind="attr:{name:'inlineRadioOptions'+$parent.qid},checked:$parent.oid,value:oid"> <span data-bind="text: otext"></span>
			                                    	<span data-bind="if: ($parents[1].grpId==5)" >
			                                    		<span data-bind="if: ($index()==0)" >
			                                    			(Least Likely)
			                                    		</span>
		                                    		</span>
		                                    	
			                                    	<span data-bind="if: ($parents[1].grpId==5)" >
			                                    		<span data-bind="if: ($index()==$parent.optionDTOs.length-1)">
			                                    			(Most Likely)
			                                    		</span>
		                                    		</span>
		                                    </label>		                                   
	                                    </span>
	                                    
										<!-- If checkbox -->
										
										<span data-bind="if: $parent.questionTypeDTO.qtype == 'checkbox'">
											<span data-bind="if: otext != 'Any Other'">
			                                    <label class="checkbox-inline">
		                                    		<input onclick="cbChange(this.id,event);" type="checkbox" data-bind="attr:{name:'inlineRadioOptions'+$index()},checked:sel,value:oid" ><span data-bind="text: otext"></span>
		                                    	</label>
		                                    </span>
		                                    <span data-bind="if: otext == 'Any Other'">
		                          
		                                    	<label class="checkbox-inline">
		                                    		<input type="checkbox" onclick="otherfn(this.id,event)" data-bind="attr:{name:'inlineRadioOptions'+$index(),id:'othersCheckbox'+$index()+oid},checked:sel,value:oid" ><span data-bind="text: otext"></span>
		                                    		<span>
		                                    			<textarea style="display:none;" class="form-control" rows="6" cols="130" data-bind="attr:{id:'othersTextbox'+$index()+oid},value:$parent.atext"></textarea>
	                                    			</span> 
		                                     </label>
		                                    </span>	
	                                    </span>
	                                    
	                                     
                                    
                                    </span>
		                            
                                </div>
                              </li>
                        </ol>
                        <br clear="all"/>
                        <input type="button" name="previous" class="previous action-button" value="Previous" />
						<input type="button" name="next" class="next action-button" value="Submit" />
               		</div>
         			
                </ol>
            </div>
            <input type="hidden" id="csrf" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
        
        
        <form id="msform" class="fbCompleted" style="display:none;">  
              <div class="form-group">
                    	<h4><br/><br/><br/>Thanks for your participation<br/>
											<br/></h4>
				<a href="http://csat.quinnox.com/qcsat">home</a>
						
            </div>
            <input type="hidden" id="csrf" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
        <p style="color: white;" align="center">&copy; 2017 Quinnox Inc.<p>
</div>
   <script type="text/javascript">
   var once = true;
   function closew(){
	   window.close(); 
	}
   function executeJS() {
// 	   $('.licl').each(function(index){
// 			$(this).attr('value',index+1);
// 	    });
		if(once){
			    $.getScript( appContext+"/resources/js/jquery.easing.min.js", function( data, textStatus, jqxhr ) {
		  		});
				$.getScript( appContext+"/resources/js/progress.js", function( data, textStatus, jqxhr ) {
				});
				once=false;
		}
		if( $($('.licl')[0]).is(":visible")){
			$('.previous').hide();
		}
	}	
    
    </script>
   <script type="text/javascript" src="<c:url value='/resources/js/knockout-models/homeuser.js' />" ></script>
    <script type="text/javascript">	
    var sm = new SurveyModel();
    ko.applyBindings(sm);
    </script>
    
   
   