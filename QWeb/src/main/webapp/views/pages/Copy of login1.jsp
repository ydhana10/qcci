<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
 <link href="<c:url value='/resources/css/custom_styles_login.css' />" rel="stylesheet">
<div class="container">

    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall">
                <img class="profile-img" src="<c:url value='/resources/img/photo.png' />"
                    alt="">
                <form class="form-signin" id="fId" name="f" action="<c:url value='j_spring_security_check'/>"
		method="post">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" id="csrf" />
                <input type="text" class="form-control" id="userName" name='j_username' placeholder="Email" required autofocus>
                <input type="password" name='j_password' id="password" class="form-control" placeholder="Password" required>
                <input type="hidden" name='j_sid' id="sid" class="form-control" placeholder="sid" required/>
                <button class="btnCls btn btn-lg btn-primary btn-block" type="submit">
                    Sign in</button>
              <span class="clearfix"></span>
                </form>
            </div>
            <!-- <a href="#" class="text-center new-account">Create an account </a> -->
        </div>
    </div>
</div>
<script type="text/javascript">
   var username = getParameterByName("_1");
   var password = getParameterByName("_2");
   var sid = getParameterByName("_3");
   if(username!=null && username!=undefined){
		if(password!=null && password!=undefined){
			$('#userName').val(Base64.decode(username));
			$('#password').val((Base64.decode(password)).replace("@123",""));
			$('#sid').val(Base64.decode(sid));
			//document.getElementById('fId').submit();
			$('.btnCls').click();
		}
   }
</script>
<script src="<c:url value='/resources/js/jquery.min.js' />"></script>   
    <script src="<c:url value='/resources/js/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/js/jquery.easing.min.js' />" type="text/javascript"></script>
	<script src="<c:url value='/resources/js/progress.js' />" type="text/javascript"></script>
    <script type="text/javascript">		
    $('.nps_list1 input:checkbox').click(function() {		
    	 $('.nps_list1 input:checkbox').not(this).prop('checked', false);
	});
	$('.nps_list2 input:checkbox').click(function() {		
    	 $('.nps_list2 input:checkbox').not(this).prop('checked', false);
	});
	$('.nps_list3 input:checkbox').click(function() {		
    	 $('.nps_list3 input:checkbox').not(this).prop('checked', false);
	});
	$('.nps_list4 input:checkbox').click(function() {		
    	 $('.nps_list4 input:checkbox').not(this).prop('checked', false);
	});
	$('.others_textbox').hide();
	$('.others').change(function(){
		if($(this). prop("checked") == true){
			$('.others_textbox').show();
		}
		else {
			$('.others_textbox').hide();
		}
	
	});
    </script>.