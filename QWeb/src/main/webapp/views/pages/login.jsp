<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
 <link href="<c:url value='/resources/css/custom_styles_login.css' />" rel="stylesheet">
<div class="container">

    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall">
                <img class="profile-img" src="<c:url value='/resources/img/survey1.png' />"
                    alt="">
                <form class="form-signin" id="fId" name="f" action="<c:url value='j_spring_security_check'/>"
		method="post">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" id="csrf" />
				
                <input type="text" class="form-control" id="userName" name='j_username' placeholder="Email"  required autofocus>
               <!--  <input type="text"  id="password" class="form-control" placeholder="Full Name" required> -->
                <input type="hidden" name='j_password' id="password" class="form-control" placeholder="Full Name" required>
                <input type="hidden" name='j_sid' id="sid" class="form-control" placeholder="sid" required/>
                <button id="submit" class="btnCls btn btn-lg btn-primary btn-block" value="submit">
                    Sign in</button>
              <span class="clearfix"></span>
                </form>
                <p id="errId" style="color:red;display:none;margin:10px;">Sorry, the survey has been closed.</p>
                <p id="errMailId" style="color:red;display:none;margin:10px;">Sorry, Please enter valid email.</p>
            </div>
            
            <!-- <a href="#" class="text-center new-account">Create an account </a> -->
        </div>
    </div>
</div>
<script type="text/javascript">
var username=getParameterByName("_1"),password=getParameterByName("_2"),sid=getParameterByName("_3");null!=username&&void 0!=username&&null!=password&&void 0!=password&&($("#userName").val(Base64.decode(username)),$("#password").val(Base64.decode(password).replace("@123","")),$("#sid").val(Base64.decode(sid)),$(".btnCls").click()),$("#errId").hide();var errorFlag=getParameterByName("errorFlag");(1==errorFlag||"true"==errorFlag)&&$("#errId").show();
$('#submit').click(function(){
	var user = $('#userName').val();
	if(validateEmail(user)){
		var usr = user.split("@");
		$('#password').val(usr[0]);
		$('#fId').submit();
	}else{
		return false;
	}
})

function validateEmail(email){
     var regex = /^([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)@([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)[\\.]([a-zA-Z]{2,9})$/;
     if(!regex.test(email)){
    	 $('#errMailId').show();
    	 $('#errId').hide();
         return false;
     }else{
    	 $('#errMailId').hide();
    	 $('#errId').hide();
    	 return true;
     }
}
</script>

