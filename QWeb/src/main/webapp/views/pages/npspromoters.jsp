<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<style>
@media print {
	#dt4 {
		transform: scale(0.85, .968) translate(-250px, 0px);
	}
	html {
		overflow: visible !important;
		-webkit-scrollbar
		{
		display
		:
		none;
	}
}
}
</style>
<!-- <script src="/resources/js/jsPDF-1.3.2/dist/jspdf.min.js"></script>
<script src="/resources/js/jsPDF-1.3.2/dist/jspdf.debug.js"></script> -->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/MrRio/jsPDF/master/dist/jspdf.min.js"></script> 
<script type="text/javascript" src="//cdn.rawgit.com/niklasvh/html2canvas/0.5.0-alpha2/dist/html2canvas.min.js"></script>
 -->

<link rel="stylesheet" type="text/css"
	href="/qcsat/resources/css/report_styles.css" />

<link href="<c:url value='/resources/css/custom_styles.css' />"
	rel="stylesheet">
<div class="dropdown menus">
	<a style="color: white; font-size: 14px; padding: 5px;"
		href="${pageContext.request.contextPath}/home">Home</a>

	<button class="btn btn-link"
		style="color: white; font-size: 14px; padding: 5px;" type="button"
		data-toggle="dropdown" data-hover="dropdown">
		Reports <span class="caret"></span>
	</button>
	<ul class="dropdown-menu">
		<li><a href="${pageContext.request.contextPath}/overallaccountreport">Overall Quinnox Report</a>
		</li>
		<li> 
		<a href="${pageContext.request.contextPath}/accountreports">Account Wise Report
		</a>
		</li>
		<li><a href="${pageContext.request.contextPath}/emwisereport">Engagement
				Managers Report</a></li>
		<li><a
			href="${pageContext.request.contextPath}/npspromotersreport">Overall
				NPS Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npspromoters">NPS
				Promoters Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npsdetractors">NPS
				Detractors Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npsneutral">NPS
				Passive Report</a></li>
		<li><a
			href="${pageContext.request.contextPath}/respondantwisereport">Respondents
				Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npsquestionwise">NPS
				Question Report</a></li>
	</ul>
</div>
<div class="container pageLayout" id="content">
	<div id="wrapper">
		<form id="msform">

			<!-- <button style = "margin-left:1024px;" id="btn">Generate PDF</button> -->

			<div class="form-group">
				<div>
					<h3 id='shid'></h3>

				</div>
				<br clear="all" /> <br clear="all" />
				<div>

					<h2 class="title" id="shid">CSAT Quinnox Promoters</h2>
				</div>
				<div class="pdflink">
					<a
						href="javascript:window.open('/qcsat/views/pages/promotersreport.jsp', 'PROMOTERS', 'width=200,height=150');">View
						Report
					</a>
				</div>
				<br> <br> <br>
				<table id="dt4" class="table table-bordered centerTable">
					<thead class="thead-inverse">
						<tr>
							<th class="darkbgcolor" colspan="7">NPS Promoters</th>
						</tr>
						<tr height="50">
							<td></td>
							<td></td>
							<td class="text-left">How willing are you to recommend
								Quinnox to other business units
							</td>
							<td class="text-left">How willing are you to recommend
								Quinnox to other companies
							</td>
							<td class="text-left">How willing are you to buy new
								services from Quinnox
							</td>
							<td class="text-left">How willing are you to 'Beta test'
								with few offerings from Quinnox in your organization
							</td>
							<td></td>
						</tr>
						<tr class="lightbgcolor">
							<th>Client Name</th>
							<th>Name Of Respondent</th>
							<th class="text-center">Q.1</th>
							<th class="text-center">Q.2</th>
							<th class="text-center">Q.3</th>
							<th class="text-center">Q.4</th>
							<th class="text-center">Is a Promoter</th>
						</tr>
					</thead>
					<tbody data-bind="foreach:user2">
						<!-- ko foreach: questionGroupDTOs -->
						<!-- ko if : $parent.npsType == 'promoter'  -->

						<tr class="text-center">
							<td class="lightbgcolor" data-bind="text:$parent.clientName"></td>
							<td class="lightbgcolor"
								data-bind="text:$parent.firstName+' '+$parent.lastName  "></td>
							<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
							<td data-bind="text:answerDTO.option.otext"></td>

							<!-- /ko -->
							<td><img src="/qcsat/resources/img/promoter.png" alt="Logo" class="imgCode"></td>

						</tr>
						<!-- /ko -->
						<!-- /ko -->
					<tbody>
				</table>
			</div>
		</form>
	</div>
</div>

<p class="footerCode">&copy; 2017 Quinnox Inc.</p>

<script type="text/html" id="nps-data">
		<tr>
			<td><span data-bind="text:data"></span></td>
		</tr>
</script>

<script type="text/html" id="detail-template">
<thead  class="thead-inverse">
					<tr>
						<th>Question Group</th>
						<th>Question</th>
						<th>Answer</th>
					</tr>
				</thead>
    <tbody data-bind="foreach: questions">
<tr>
 						<!-- ko if: $index()===0 -->
						<td data-bind="attr:{rowspan:$parent.questions.length}" scope="row">
							<span data-bind="text: $parent.grpName"></span>
						</td>
						<td data-bind="text: qtext"></td>
						
						<td>
						<span data-bind="if: questionTypeDTO.qtype == 'radiobutton'">
							<span data-bind="foreach: answerDTOs">
								<span data-bind="text: option.otext"></span>
							</span>
						</span>
						<span data-bind="if: questionTypeDTO.qtype == 'checkbox'">
							<span data-bind="foreach: answerDTOs">
								<span data-bind="text: option.otext">,</span>
							</span>
						</span>
						<span data-bind="if: questionTypeDTO.qtype == 'text'">
							<span data-bind="foreach: answerDTOs">
								<span data-bind="text: atext"></span>
							</span>
						</span>
						</td>
        				<!-- /ko -->
						<!-- ko if: $index()!=0 -->
						
						<td data-bind="text: qtext"></td>
						
						<td>
						<span data-bind="if: questionTypeDTO.qtype == 'radiobutton'">
							<span data-bind="foreach: answerDTOs">
								<span data-bind="text: option.otext"></span>
							</span>
						</span>
						<span data-bind="if: questionTypeDTO.qtype == 'checkbox'">
							<span data-bind="foreach: answerDTOs">
								<span data-bind="text: option.otext"></span><span data-bind="if: atext !=null"><span data-bind="text: ' - '+atext"></span></span><br/>
							</span>
						</span>
						<span data-bind="if: questionTypeDTO.qtype == 'text'">
							<span data-bind="foreach: answerDTOs">
								<span data-bind="text: atext"></span>
							</span>
						</span>
						</td>
        				<!-- /ko -->
</tr>
<tbody>
</script>


<script type="text/javascript"
	src="<c:url value='/resources/js/knockout-models/npspromoters.js' />"></script>
<script type="text/javascript">
	var first = false;
	//var asm = new AllSurveysModel();
	//ko.applyBindings(asm, document.getElementById('pr'));
	// 	$('.btnCls').on('click', function() {
	// 		var indexLocation = $(this).attr('id');
	// 		var dm = new SurveyDetailModel(indexLocation);
	// 		ko.applyBindings(dm, document.getElementById('dt'));
	// 		$('#ptbl').hide();
	// 		$('#dt').show();
	// 	});
	// 		$('.form_date').datetimepicker({
	// 			language : 'fr',
	// 			weekStart : 1,
	// 			todayBtn : 1,
	// 			autoclose : 1,
	// 			todayHighlight : 1,
	// 			startView : 2,
	// 			minView : 2,
	// 			forceParse : 0
	// 		});

	function executeDetailJS() {
	}
</script>
