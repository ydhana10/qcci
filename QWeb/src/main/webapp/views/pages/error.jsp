<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<link href="<c:url value='/resources/css/custom_styles.css' />"
	rel="stylesheet">
 <div class="container" style="padding-top: 81px;">
  		<h1 class="text-center feedback_heading">The operation cannot be completed.<br />An unexpected error occurred.<br clear="all"/>This is logged and the support team will get back to you soon. </h1>
        
</div>
   
    
   
   