<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link href="<c:url value='/resources/css/custom_styles.css' />"
	rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/qcsat/resources/css/report_styles.css" />
	
<div class="dropdown menus">
	<a style="color: white; font-size: 14px; padding: 5px;"
		href="${pageContext.request.contextPath}/home">Home</a>

	<button class="btn btn-link"
		style="color: white; font-size: 14px; padding: 5px;" type="button"
		data-toggle="dropdown" data-hover="dropdown">
		Reports <span class="caret"></span>
	</button>
	<ul class="dropdown-menu">
		<li><a
			href="${pageContext.request.contextPath}/overallaccountreport">Overall
				Quinnox Report</a></li>
		<li><a href="${pageContext.request.contextPath}/accountreports">Account
				Wise Report</a></li>
		<li><a href="${pageContext.request.contextPath}/emwisereport">Engagement
				Managers Report</a></li>
		<li><a
			href="${pageContext.request.contextPath}/npspromotersreport">Overall
				NPS Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npspromoters">NPS
				Promoters Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npsdetractors">NPS
				Detractors Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npsneutral">NPS
				Passive Report</a></li>
		<li><a
			href="${pageContext.request.contextPath}/respondantwisereport">Repondants
				Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npsquestionwise">NPS
				Question Report</a></li>
	</ul>
</div>


<div class="container pageLayout">
	<form id="msform">
		<div class="form-group">
			<div>
				<h3 id='shid'></h3>

			</div>
			<br clear="all" /> <br clear="all" />
			<div>
				<h2 id="shid" class="heading">CSAT
					EM-WISE Report</h2>

				<div id="view" style="display: none">
					<button type="button" class="btn btn-link" onclick="viewPDF()"
						style="margin-left: 1000px; margin-top: -37px;">View
						Report</button>
				</div>
			</div>

		</div>

		<div class="modal-body" id="mb" style="margin-left: 328px;">
			<form style="min-height: 120px !important;">
				<div class="form-group">
					<label for="exampleSelect1">Vertical</label> <select
						class="form-control" id="verticalSelectId1" style="width: 51%">
						<option value="0" selected>Please Select</option>

					</select>
				</div>
				<div class="form-group">
					<label for="exampleSelect1">Engagement Manager</label> <select
						class="form-control" id="engagementManagerSelectId1"
						style="width: 51%">
						<option value="0">Please Select</option>
					</select>
				</div>
				<input type="hidden" id="csrf" name="${_csrf.parameterName}"
					value="${_csrf.token}" />

				<button type="button" id="btn2Id" class="btn btn-primary"
					onclick="viewEmReports()" style="margin-left: 275px;">View
					Report</button>
		</div>
		
		<table id="dt5" class="table table-bordered Tablealign">
			<thead class="thead-inverse">
			</thead>
			<tbody>

				<div>
					<td><b>Overall Score</b></td>
					<td><span data-bind="if: user12.overAllNps<4">
							<div>
								<img src="/qcsat/resources/img/red.jpg"  class="ragscoreImg"  >
							</div>

					</span> <span data-bind="if: user12.overAllNps>=4 && user12.overAllNps<=5">
							<div>
								<img src="/qcsat/resources/img/yellow.jpg" class="ragscoreImg"  >
							</div>
					</span> <span data-bind="if: user12.overAllNps>5">
							<div>
								<img src="/qcsat/resources/img/green.png" class="ragscoreImg"  >
							</div>
					</span></td>
					<td><span><div style="margin-left: 25px;"
								data-bind="text:user12.overAllNps"></div></span></td>
				</div>
				</tr>

			</tbody>
		</table>
		<br> <br> <br>
		<table id="dt" class="table Tablealign">
			<tr>
			<thead class="thead-inverse">

				<tr>
					<th></th>
					<th class="darkbgcolor">NPS Score</th>
					<th class="darkbgcolor">Score(In %)</th>
				</tr>
			<thead>
			<tbody>
				<tr>
					<td class="npscolor-red"></td>
					<td>Detractors</td>
					<td><span data-bind="text:data.Detractors"></span></td>
				</tr>
				<tr>
					<td class="npscolor-yellow"></td>
					<td>Passive</td>
					<td><span data-bind="text:data.Neutral"></span></td>
				</tr>
				<tr>
					<td class="npscolor-green"></td>
					<td>Promoters</td>
					<td><span data-bind="text:data.Promoters"></span></td>
				</tr>
			</tbody>

		</table>

		<table id="dt6" class="table top3Table">
			<thead class="thead-inverse">
				<tr>
					<th class="darkbgcolor">Top 3 Benefits</th>
				</tr>
			</thead>
			<tbody data-bind="foreach:data">
				<tr>
					<td><span data-bind="text : $data"></span></td>
				</tr>
			<tbody>
		</table>


		<br> <br> <br>
		<table id="dt1" class="table table-bordered r-table">
			<thead class="thead-inverse">

				<tr class="lightbgcolor">
					<th></th>
					<th></th>
					<th class="text-center">Quinnox's People Performance</th>
					<th class="text-center">Quinnox's Service Delivery</th>
					<th class="text-center">Quinnox's Performance On Relationship
						Management</th>
					<th></th>
				</tr>
				<tr class="lightbgcolor">
					<th>Client Name</th>
					<th>Name Of Respondent</th>

					<th class="text-center">Q.1</th>

					<th class="text-center">Q.2</th>

					<th class="text-center">Q.3</th>
					
					<th class="text-center">Average</th>

				</tr>
			</thead>
			<tbody data-bind="foreach:user1">

				<tr>
					<!-- <td data-bind="text:clientName"></td> -->
					<td class="lightbgcolor" data-bind="text:clientName "></td>
					<td class="lightbgcolor" data-bind="text:firstName+' '+lastName  "></td>
					<td class="text-center" data-bind="text:avgQg1"></td>
					<td class="text-center" data-bind="text:avgQg2"></td>
					<td class="text-center" data-bind="text:avgQg3"></td>
					<th class="text-center"data-bind="text:avgOfEachUser"></th>
				</tr>
			<tbody>
		</table>
		<br> <br> <br>
		<table id="doq" class=" table table-bordered"style="display:none">
			<thead class="thead-inverse">
				<tr>
					<th class="darkbgcolor" colspan="8">Quinnox
						CSAT 2016-2017</th>
				</tr>
				<tr class="lightbgcolor">
					<th></th>
					<th class="text-center">Quinnox's People Performance</th>
					<th class="text-center">Quinnox's Service Delivery</th>
					<th class="text-center">Quinnox's Performance On Relationship
						Management</th>
				</tr>
				<tr class="lightbgcolor">
					<th></th>

					<th class="text-center">Q.1</th>

					<th class="text-center">Q.2</th>

					<th class="text-center">Q.3</th>
				</tr>

			</thead>
			<tbody>
				<tr>
					<th class="lightbgcolor">Average</th>
					<td class="text-center" data-bind="text:user3.Question1"></td>
					<td class="text-center" data-bind="text:user3.Question2"></td>
					<td class="text-center" data-bind="text:user3.Question3"></td>
				</tr>
			</tbody>
		</table>
		<br> <br> <br>
		<table id="dt2" class="table table-bordered r-table">
			<thead class="thead-inverse">
				<tr>
					<th class="darkbgcolor" colspan="7">NPS Data Points
					</th>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td class="text-left"  >How willing are you to
						recommend Quinnox to other business units</td>
					<td class="text-left"  >How willing are you to
						recommend Quinnox to other companies</td>
					<td class="text-left"  >How willing are you to buy
						new services from Quinnox</td>
					<td class="text-left"  >How willing are you to
						'Beta test' with few offerings from Quinnox in your organization</td>
					<td ></td>
				</tr>
				<tr class="lightbgcolor">
					<th>Client Name</th>
					<th>Name Of Respondent</th>
					<th class="text-center">Q.1</th>
					<th class="text-center">Q.2</th>
					<th class="text-center">Q.3</th>
					<th class="text-center">Q.4</th>
					<th class="text-center">NPS Type</th>
				</tr>
			</thead>
			<tbody data-bind="foreach:user2">
				<!-- ko foreach: questionGroupDTOs -->
			<tr>
					<td class="lightbgcolor" data-bind="text:$parent.clientName"></td>
					<td class="lightbgcolor" data-bind="text:$parent.firstName+' '+$parent.lastName "></td>
					<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
					<!--  ko if: answerDTO.option.otext == " "  -->
					<td class="text-center">NR</td>
					<!-- /ko -->
					<!-- ko if: answerDTO.option.otext !=" " -->
					<td class="text-center" data-bind="text:answerDTO.option.otext"></td>
					<!-- /ko -->
					<!-- /ko -->
					<!-- ko if: $parent.npsType == 'detractor' -->
					<td class="text-center" ><img src="/qcsat/resources/img/detract.jpg"
						alt="Logo" class="imgCodeDetractor"></td>
					<!-- /ko -->
					<!-- ko if: $parent.npsType == 'promoter' -->
					<td class="text-center"><img
						src="/qcsat/resources/img/promoter.png" alt="Logo"
						class="imgCode"></td>
					<!-- /ko -->
					<!-- ko if: $parent.npsType == 'neutral' -->
					<td class="text-center"><img src="/qcsat/resources/img/neutral.jpg"
						alt="Logo" class="imgCodeNetural"></td>
					<!-- /ko -->
			</tr>
				<!-- /ko -->
			<tbody>
		</table>
</div>
</form>
<p class="footerCode">&copy; 2017 Quinnox Inc.
<p>
</div>

<script type="text/javascript"
	src="<c:url value='/resources/js/knockout-models/emwisereport.js' />"></script>

