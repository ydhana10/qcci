<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%@ page isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value='/qcsat/resources/css/custom_styles.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/qcsat/resources/css/report_styles.css' />">
<link rel="stylesheet" type="text/css" href="/qcsat/resources/css/report_styles.css" />

<script src="<c:url value='/resources/js/jquery.min.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/bootstrap.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/knockout-3.4.1.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/knockout.mapping.js' />"></script>

<title>Overall Csat Report</title>

<style>
-webkit-transform :rotate(-90deg);
-moz-transform:rotate(-90deg);
filter
:progid
:DXImageTransform
.Microsoft
.BasicImage
(rotation=3);

@media screen {
	div.divFooter {
		display: none;
	}
}

@media print {
	div.divFooter {
		position: fixed;
		top: 0;
	}
}

@media all {
	.page-break {
		display: none;
	}
}

@media print {
	.page-break {
		display: block;
		page-break-before: always;
	}
}

@page {
	size: landscape;
}
</style>

</head>

<body>
	<div class="contentSection2">
		<img src="/qcsat/resources/img/clientsatisfaction.jpg" alt="Logo"
			class="img-clientsatisfaction">
	</div>

	<div class="voiletbackground"> 
		<p class="ClientSatisfactionSurvey ">2017 Client Satisfaction Survey</p>
	</div>

	<div class="page-break"></div>
	<div class="contentSection1" class="contentSectionAlign">
		<p>
		<p class="ConfidentialityNote">Confidentiality Note</p>
		<br>
		<p class="paragraph">
			This document contains confidential information developed by Quinnox,
			Inc. No part of this document may be reproduced, stored in retrieval
			form, adopted or transmitted in any form or by any means, electronic,
			mechanical, photographic, graphic, optic or otherwise, translated in
			any language or computer language, without prior written permission
			from Quinnox, Inc. <br />
			<br /> This document has been prepared in accordance with the
			accordance with the accepted techniques for software engineering at
			Quinnox, Inc. The representations and related information contained
			in the document reflect our best understanding on the subject of this
			document.<br />
			<br /> However, Quinnox makes no representation or warranties with
			respect to the contents hereof and shall not be responsible for any
			loss or damage caused to the user by the direct or indirect use of
			this document and the accompanying software package. Furthermore
			Quinnox reserves the right to alter, modify or otherwise change in
			any manner the content hereof, without the obligation to notify any
			person of such revision or changes. <br /> <br />
		</p>
		<p class="footerstyle">
			� 2014 Quinnox Inc., all rights reserved.<br /> All registered and
			trademarked names are owned by their respective owners.
		</p>


	</div>
	
	<div class="page-break"></div>
	<br>
	<br>
	<br>
	<h2 class="titlestyle" id="shid">Quinnox CSAT 2016-2017</h2>

	<table id="dt" class="overall">
		<thead class="thead-inverse"></thead>
		<tbody>
			<tr><!-- <th style="padding-left: 359px;background-color:#cccccc">CSAT Report - Virtusa Consulting Services</tr> -->
			<tr>
			<tr>
				<div>
					<th>Overall Score</th>
					<td>
						<span data-bind="if: user1.overAllNps<4">
								<div>
									<img src="/qcsat/resources/img/red.jpg" width="25px"
										height="25px" style="margin-left: 25px;">
								</div>
	
						</span> 
						<span data-bind="if: user1.overAllNps>=4 && user1.overAllNps<=5">
								<div>
									<img src="/qcsat/resources/img/yellow.jpg" width="25px"
										height="25px" style="margin-left: 25px;">
								</div>
						</span> 
						<span data-bind="if: user1.overAllNps>5">
								<div>
									<img src="/qcsat/resources/img/green.png" width="25px"
										height="25px" style="margin-left: 25px;">
								</div>
						</span>
					</td>
					<td><span><div style="margin-left: 25px;" data-bind="text:user1.overAllNps"></div></span></td>
				</div>
			</tr>

		</tbody>
	</table>

	<table id="dt1" class="nps  class=" tabletable-bordered">
		<tr>
		<thead class="thead-inverse">

			<tr class="npstitle">
				<th></th>
				<th>Quinnox Overall NPS Score</th>
				<th>Score(In %)</th>
			</tr>
		<thead>
		<tbody>
			<tr height="54px;">
				<td style="background-color: #fb4e4e;"></td>
				<td class="npsborder">Detractors</td>
				<td class="data-center"><span data-bind="text:data.Ditractors"></span></td>
			</tr>
			<tr height="54px;">
				<td style="background-color: #FFC200"></td>
				<td class="npsborder">Passive</td>
				<td class="data-center"><span data-bind="text:data.Neutral"></span></td>
			</tr>
			<tr height="54px;">
				<td style="background-color: #89d089"></td>
				<td class="npsborder">Promoters</td>
				<td class="data-center"><span data-bind="text:data.Promoters"></span></td>
			</tr>
		</tbody>
	</table>

	<table id="dt2" class="top3" class="table table-bordered">
		<thead class="thead-inverse">
			<tr class="npsborder">
				<th style="background-color: #cccccc; height: 50px;">Top 3
					Benefits</th>
			</tr>
		</thead>
		<tbody data-bind="foreach:data">
			<tr class="npsborder">
				<td class="npsborder" ><span data-bind="text : $data"></span></td>
			</tr>
		<tbody>
	</table>

	<table id="average1" class="tablestyle" class="table table-bordered">
		<thead class="thead-inverse">
			<tr class="question-row">
				<th>Client Name</th>
				<th>Quinnox's People Performance</th>
				<th>Quinnox's Service Delivery</th>
				<th>Quinnox's Performance On Relationship Management</th>
				<th>Average
				</td>
			</tr>
		</thead>
		<tbody>
			<!-- ko foreach: user.sort(function (l, r) { return l.avgOfEachUser < r.avgOfEachUser ? 1 : -1 })-->
			<tr height="50">

				<td class="highlight-td" data-bind="text:clientName"></td>
				<td class="data-center" data-bind="text:avggrp1"></td>
				<td class="data-center" data-bind="text:avggrp2"></td>
				<td class="data-center" data-bind="text:avggrp3"></td>
				<td class="data-center" data-bind="text:avgOfEachUser"></td>
			</tr>
			<!-- /ko -->
		</tbody>
	</table>

	<div class="page-break"></div>
	<table id="doq" class="tablestyle" class="table table-bordered">
		<thead class="thead-inverse">
			<tr class="question-row">
				<th colspan="4">Overall Quinnox Report</th>
			</tr>
			<tr class="question-row">
				<th></th>
				<th>Quinnox's People Performance</th>
				<th>Quinnox's Service Delivery</th>
				<th>Quinnox's Performance On Relationship Management</th>

			</tr>
		</thead>
		<tbody>
			<tr height="50">
				<th class="highlight-td">Average</th>
				<td class="data-center" data-bind="text:user1.Question1"></td>
				<td class="data-center" data-bind="text:user1.Question2"></td>
				<td class="data-center" data-bind="text:user1.Question3"></td>
			</tr>
		</tbody>
	</table>

<script type="text/javascript">		

function NPSScore(data) {
	var svsData;
	var self = this;
	var svs = svsData;
	self.data=data;	
}
							
function OverallAverageOfUser(data){
	var self = this;
	self.user = ko.observableArray([]);
	self.user = data;
}	

function AvgOverallQuestiongrp(data){
	var self = this;
	self.user1 = ko.observableArray([]);
	self.user1 = data;
	
}

function AvgOverallNPS(data){
	var self = this;
	self.user1 = ko.observableArray([]);
	self.user1 = data;
	
}

function overAllTopBenefits(data){
	var self = this;
	self.data = ko.observableArray([]);
	self.data = data;
}

$.getJSON("/qcsat/proxy/api/overallnpsreports",
		function(data) {
	  		var indexLocation = $(this).attr('id');
			var dm1 = new NPSScore(data);
			ko.applyBindings(dm1, document.getElementById('dt1'));
			$('#dt1').show();
	});
  	
$.getJSON("/qcsat/proxy/api/avgOverallAverageOfUser",
		function(data){
			var indexLocation = $(this).attr('id');
			var dat = new OverallAverageOfUser(data);
			ko.applyBindings(dat, document.getElementById('average1'));
			$('#average1').show();		
    });

$.getJSON("/qcsat/proxy/api/avgOverallAverageOfQuestiongrp",
		function(data) {
	  		var indexLocation = $(this).attr('id');
			var dmForAvg = new AvgOverallQuestiongrp(data);
			ko.applyBindings(dmForAvg, document.getElementById('doq'));
			$('#doq').show();
	});
	
$.getJSON("/qcsat/proxy/api/avgOverallAverageOfQuestiongrp",
		function(data) {
	  		var indexLocation = $(this).attr('id');
			var dmForAvg = new AvgOverallNPS(data);
			ko.applyBindings(dmForAvg, document.getElementById('dt'));
			$('#dt').show();
	  });
	
$.getJSON("/qcsat/proxy/api/top3BenefitsForOrganization",
		function(data) {
	  		var indexLocation = $(this).attr('id');
			var dm2 = new overAllTopBenefits(data);
			ko.applyBindings(dm2, document.getElementById('dt2'));
			$('#dt2').show();
	});

</script>

<script type="text/javascript">
    $(function(){
        $('#printOut').click(function(e){
           /*  e.preventDefault(); */
            var w = window.open();
            var printOne = $('.container').html();
            var printTwo = $('.contentSection1').html();
            var printThree = $('.contentSection2').html();
            w.document.write('<html class="page-height"><head><title></title></head><body class="page-height"><h6>1 | CSAT 2017 Promoters</h6><hr />' + printThree ) + '</body></html>';
            w.document.write('<html><head><style>.break { page-break-before: always; }; .page-height{height: 100%};</style><title>Copy Printed</title></head><body><h6 class="break">2 | CSAT 2017 Promoters</h6><hr />' + printTwo ) + '</body></html>';
            w.document.write('<html><head><style>.break { page-break-before: always; }</style><title>Copy Printed</title></head><body><h6 class="break">3 | CSAT 2017 Promoters</h6><hr />' + printOne ) + '</body></html>';
            w.window.print();
            w.document.close();
            return false;
        });
    });
</script>


</body>
</html>