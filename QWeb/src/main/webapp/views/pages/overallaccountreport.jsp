<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link href="<c:url value='/resources/css/custom_styles.css' />" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/qcsat/resources/css/report_styles.css" />
<div class="dropdown menus">
	<a style="color: white; font-size: 14px; padding: 5px;"
		href="${pageContext.request.contextPath}/home">Home
	</a>

	<button class="btn btn-link"
		style="color: white; font-size: 14px; padding: 5px;" type="button"
		data-toggle="dropdown" data-hover="dropdown">
		Reports <span class="caret"></span>
	</button>
	<ul class="dropdown-menu">
		<li><a
			href="${pageContext.request.contextPath}/overallaccountreport">Overall
				Quinnox Report</a></li>
		<li><a href="${pageContext.request.contextPath}/accountreports">Account
				Wise Report</a></li>
		<li><a href="${pageContext.request.contextPath}/emwisereport">Engagement
				Managers Report</a></li>
		<li><a
			href="${pageContext.request.contextPath}/npspromotersreport">Overall
				NPS Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npspromoters">NPS
				Promoters Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npsdetractors">NPS
				Detractors Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npsneutral">NPS
				Passive Report</a></li>
		<li><a
			href="${pageContext.request.contextPath}/respondantwisereport">Respondents
				Report</a></li>
		<li><a href="${pageContext.request.contextPath}/npsquestionwise">NPS
				Question Report</a></li>
	</ul>
</div>
<div class="container pageLayout">
	<form id="msform">
		<div class="form-group">
			<div>
				<h3 id='shid'></h3>
			</div>
			<br clear="all" /> <br clear="all" />
			<div>
				<h2 id="shid" class="heading">QuinnoxCSAT 2016-2017</h2>
			</div>
			<div class="pdflink">
				<a href="javascript:window.open('/qcsat/views/pages/overallaccountreportpdf.jsp', 'ACCOUNT', 'width=200,height=150');">View Report
				</a>
			</div>
			<br> <br> <br>
			<table id="dt" class="table table-bordered Tablealign">
				<thead class="thead-inverse">
				</thead>
				<tbody>
					<tr> 
					<!-- <th style="padding-left: 359px;background-color:#cccccc">CSAT Report - Virtusa Consulting Services</tr> -->
					 <tr>
					 <div>
							<td><b>Overall Score</b></td>
							<td><span data-bind="if: user1.overAllNps<4">
									<div >
										<img src="/qcsat/resources/img/red.jpg"  class="ragscoreImg" >
									</div>

								</span>
								<span data-bind="if: user1.overAllNps>=4 && user1.overAllNps<=5">
								<div>
									<img src="/qcsat/resources/img/yellow.jpg" >
								</div>
								</span> 
								<span data-bind="if: user1.overAllNps>5" class="ragscoreImg">
									<div>
										<img  src="/qcsat/resources/img/green.png" class="ragscoreImg"  >
									</div>
								</span></td>
							<td> <span> <div style="margin-left: 25px;" data-bind="text:user1.overAllNps"></div> </span> </td>
						</div>
					</tr>
				</tbody>
			</table>
			<br> <br> <br>
			<table id="dt1" class="table table-bordered Tablealign">
				<tr>
				<thead class="thead-inverse">
					<tr>
						<th></th>
						<th class="darkbgcolor">Quinnox Overall NPS Score</th>
						<th class="darkbgcolor">Score(In %)</th>
					</tr>
				<thead>
				<tbody>
					<tr>
						<td class="npscolor-red"></td>
						<td>Detractors</td>
						<td><span data-bind="text:data.Ditractors"></span></td>
					</tr>
					<tr>
						<td class="npscolor-yellow"></td>
						<td>Neutral</td>
						<td><span data-bind="text:data.Neutral"></span></td>
					</tr>
					<tr>
						<td class="npscolor-green"></td>
						<td>Promoters</td>
						<td><span data-bind="text:data.Promoters"></span></td>
					</tr>
				</tbody>
			</table>
			<table id="dt2" class="table table-bordered top3Table">
				<thead class="thead-inverse">
					<tr>
						<th class="darkbgcolor">Top 3 Benefits</th>
					</tr>
				</thead>
				<tbody data-bind="foreach:data">
					<tr>
						<td><span data-bind="text : $data"></span></td>
					</tr>
				<tbody>
			</table>
			<br> <br> <br>
			<table id="average1" class="table table-bordered ">
				<thead class="thead-inverse">
					<tr class="lightbgcolor">
						<th></th>
						<!-- <th>Name Of Respondent</th> -->
						<th class="text-center">Quinnox's People Performance</th>
						<th class="text-center">Quinnox's Service Delivery</th>
						<th class="text-center">Quinnox's Performance On Relationship Management</th>
						<th class="text-center"></th>
					</tr>
					<tr class="lightbgcolor">
						<th>Client Name</th>
						<th class="text-center">Q.1</th>
						<th class="text-center">Q.2</th>
						<th class="text-center">Q.3</th>
						<th class="text-center">Average</th>
				</tr>
				</thead>
				<tbody>
					<!-- ko foreach: user.sort(function (l, r) { return l.avgOfEachUser < r.avgOfEachUser ? 1 : -1 })-->
					<tr>
						<td class="lightbgcolor" data-bind="text:clientName"></td>
						<td class="text-center" data-bind="text:avggrp1"></td>
						<td class="text-center" data-bind="text:avggrp2"></td>
						<td class="text-center" data-bind="text:avggrp3"></td>
						<th  class="text-center" data-bind="text:avgOfEachUser"></th>
					</tr>
					<!-- /ko -->
				<tbody>
			</table>
			<br> <br> <br>
			<table id="doq" class="table table-bordered ">
				<thead class="thead-inverse">
					<tr>
						<th class="darkbgcolor" colspan="8">Quinnox CSAT 2016-2017</th>
					</tr>
					<tr class="lightbgcolor">
						<th></th>
						<th>Quinnox's People Performance</th>
						<th>Quinnox's Service Delivery</th>
						<th>Quinnox's Performance On Relationship Management</th>
					</tr>
					<tr class="lightbgcolor">
						<th></th>

						<th class="text-center">Q.1</th>

						<th class="text-center">Q.2</th>

						<th class="text-center">Q.3</th>
					</tr>
				</thead>
				<tbody>
 					<tr>
 						<th>Average</th>
						<th  class="text-center" data-bind="text:user1.Question1"></th>
						<th  class="text-center" data-bind="text:user1.Question2"></th>
						<th  class="text-center" data-bind="text:user1.Question3"></th>
					</tr>
				</tbody>
			</table>
		</div>
	</form>
</div>
<p class="footerCode">&copy; 2017 Quinnox Inc.</p>
	<script type="text/javascript"
		src="<c:url value='/resources/js/knockout-models/overallaccountreport.js' />"></script>