<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<link href="<c:url value='/resources/css/custom_styles.css' />"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="/qcsat/resources/css/report_styles.css" />

<div class="dropdown menus">
<a style="color: white; font-size: 14px; padding: 5px;"
		href="${pageContext.request.contextPath}/home">Home</a>
		
	<button class="btn btn-link"
		style="color: white; font-size: 14px; padding: 5px;" type="button"
		data-toggle="dropdown" data-hover="dropdown">Reports <span class="caret"></span>
	</button>

	<ul class="dropdown-menu">

		<li><a
			href="${pageContext.request.contextPath}/overallaccountreport">Overall
				Quinnox Report</a></li>

		<li><a href="${pageContext.request.contextPath}/accountreports">Account
				Wise Report</a></li>

		<li><a href="${pageContext.request.contextPath}/emwisereport">Engagement
				Managers Report</a></li>

		<li><a
			href="${pageContext.request.contextPath}/npspromotersreport">Overall
				NPS Report</a></li>

		<li><a href="${pageContext.request.contextPath}/npspromoters">NPS
				Promoters Report</a></li>

		<li><a href="${pageContext.request.contextPath}/npsdetractors">NPS
				Detractors Report</a></li>

		<li><a href="${pageContext.request.contextPath}/npsneutral">NPS
				Passive Report</a></li>

		<li><a
			href="${pageContext.request.contextPath}/respondantwisereport">Respondents
				Report</a></li>

		<li><a href="${pageContext.request.contextPath}/npsquestionwise">NPS
				Question Report</a></li>

	</ul>

</div>

<div class="container pageLayout">

	<form id="msform">

		<div class="form-group">
			<div>
				<h3 id='shid'></h3>
			</div>
			<br clear="all" /> <br clear="all" />
			<div>
			<h2 id="shid" class="heading">Respondent Report</h2>
			</div>
			<div id="view" class="reportlink">
				<button type="button" class="btn btn-link" onclick="viewPDF()">View
					Report</button>
			</div>
			<div class="modal-body" id="mb" style="margin-left: 328px;">

				<form style="min-height: 120px !important;">

					<div class="form-group">

						<label for="exampleSelect1">Vertical</label>
					 <select
							class="form-control" id="verticalSelectId1" style="width: 51%">
							<option value="0" selected>Please Select</option>
					</select>
					</div>
					<div class="form-group">

						<label for="exampleSelect1">Client</label> <select
							class="form-control" id="clientSelectId1" style="width: 51%">

							<option value="0">Please Select</option>

						</select>

					</div>
					<div class="form-group">

						<label for="exampleSelect1">Respondent</label>
						 <select
							class="form-control" id="respondantId1" style="width: 51%">
							<option value="0">Please Select</option>
						</select>

					</div>
					<input type="hidden" id="csrf" name="${_csrf.parameterName}"
						value="${_csrf.token}" />

					<button type="button" id="btn2Id" class="btn btn-primary"
						onclick="viewRespondantReports()" style="margin-left: 275px;">View Report
					</button>

				</form>
			</div>
			<br> <br> <br>
			<table id="doq" class="table table-bordered Tablealign">
				<thead class="thead-inverse">
				</thead>
					<tbody>
						<tr>
							<!-- <th style="padding-left: 359px;background-color:#cccccc">CSAT Report - Virtusa Consulting Services</tr> -->
						<tr>
							<td><b>Overall Score</b></td>

							<td><span data-bind="if: user1.RagScore<4">
									<div class="col.md-4">
										<img src="/qcsat/resources/img/red.jpg" class="ragscoreImg">
									</div>
								</span> 
								<span data-bind="if: user1.RagScore>=4 && user1.RagScore<=5">
									<div class="col.md-4">
										<img src="/qcsat/resources/img/yellow.jpg" class="ragscoreImg">
									</div>
								</span> 
								<span data-bind="if: user1.RagScore>5">
									<div class="col.md-4">
										<img src="/qcsat/resources/img/green.png" class="ragscoreImg">
									</div>
								</span>
							</td>
							<td style="margin-left: 25px;"><span><div class="col.md-4" data-bind="text:user1.RagScore"> </div></span></td>
						</tr>
					</tbody>
			</table>
			<table id="dt1" class="table table-bordered r-top3">
				<thead class="thead-inverse">
					<tr>
						<th class="darkbgcolor">Top 3 Benefits</th>
					</tr>
				</thead>
					<tbody data-bind="foreach:data">
						<tr>
							<td><span data-bind="text : $data"></span></td>
						</tr>
					<tbody>
			</table>
			<table id="dt2" class="table table-bordered r-table">
				<thead class="thead-inverse">
				  	 <tr>
					  	<th class="darkbgcolor" colspan="8">Quinnox People Performance</th>
				   	</tr>
					<tr height="100">
						<td></td>
						<td>Understanding of your business</td>
						<td>Technical understanding of your environment</td>
						<td>Flexibility to accommodate your requirements
						</td>
						<td>Responsiveness and attitude</td>
						<td>Timeliness and clarity of communication</td>
						<td>Effectiveness of the Engagement Manager</td>
						<td>Average</td>
					</tr>
					<tr class="lightbgcolor">
						<th class="text-center">Name Of Respondent</th>
						<th class="text-center">Q.1</th>
						<th class="text-center">Q.2</th>
						<th class="text-center">Q.3</th>
						<th class="text-center">Q.4</th>
						<th class="text-center">Q.5</th>
						<th class="text-center">Q.6</th>
						<th class="text-center">Average</th>
					</tr>
				</thead>

					<tbody data-bind="foreach:user1">

						<!-- ko foreach: questionGroupDTOs -->

					<tr>

						<td class="lightbgcolor"
							data-bind="text:$parent.firstName+' '+$parent.lastName"></td>

						<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->

						<td class="text-center"
							data-bind="text:answerDTO.option.otextValue"></td>
						<!-- /ko -->
						<th class="text-center" data-bind="text:$parent.avgNps"></th>
					</tr>
					<!-- /ko -->
				<tbody>
			</table>
			<table id="dt3" class="table table-bordered r-table">

				<thead class="thead-inverse">

					<tr>
						<th class="darkbgcolor" colspan="8">Quinnox's Service Delivery
						</th>

					</tr>
					<tr height="160">

						<td></td>
						<td class="text-left">Prompt fulfillment of resourcing</td>
						<td class="text-left">The delivery teams are have <br>adequate
							skills<br> for my requirements
						</td>
						<td class="text-left">The teams strives hard to Deliver
							Projects/ Services On Time
						</td>
						<td class="text-left">The team Delivers projects/ Services
							with High Quality
						</td>
						<td class="text-left">Prompt escalation of major issues and
							show stoppers
						</td>
						<td class="text-left">Governance reports and data driven
							decision making
						</td>
						<td></td>

					</tr>

					<tr class="lightbgcolor">

						<th class="text-center">Name Of Respondent</th>

						<th class="text-center">Q.1</th>

						<th class="text-center">Q.2</th>

						<th class="text-center">Q.3</th>

						<th class="text-center">Q.4</th>

						<th class="text-center">Q.5</th>

						<th class="text-center">Q.6</th>

						<th class="text-center">Average</th>

					</tr>

				</thead>

				<tbody data-bind="foreach:user2">

					<!-- ko foreach: questionGroupDTOs -->
					<tr>
						<td class="lightbgcolor" class="text-center"
							data-bind="text:$parent.firstName+' '+$parent.lastName  "></td>
						<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
						<td class="text-center"
							data-bind="text:answerDTO.option.otextValue"></td>
						<!-- /ko -->
						<th class="text-center" data-bind="text:$parent.avgNps"></th>

					</tr>
				<!-- /ko -->
				<tbody>
			</table>
			<table id="dt4" class="table table-bordered r-table">
			<thead class="thead-inverse">
					<tr>
					<th class="darkbgcolor" colspan="7">Quinnox's Performance on
							RELATIONSHIP MANAGEMENT
					</th>

					</tr>
					<tr height="160">
					<td></td>
						<td>The account manager is knowledgeable and professional.</td>
						<td>The account manageris making a positive contribution to
							my business.</td>
						<td>The account manager responds to my inquiries in a timely
							manner.</td>
						<td>The account manager works<br>towards achieving High
							Customer Satisfaction
						</td>
						<td>Overall, I am very satisfied with the account manager.</td>
						<td colspan="1"></td>
					</tr>
					<tr class="lightbgcolor">
						<th class="text-center">Name Of Respondent</th>
						<th class="text-center">Q.1</th>
						<th class="text-center">Q.2</th>
						<th class="text-center">Q.3</th>
						<th class="text-center">Q.4</th>
						<th class="text-center">Q.5</th>
						<th class="text-center">Average</th>

					</tr>
			</thead>

				<tbody data-bind="foreach:user3">

					<!-- ko foreach: questionGroupDTOs -->
					<tr>

						<td class="lightbgcolor" class="text-center"
							data-bind="text:$parent.firstName+' '+$parent.lastName  "></td>

						<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->

						<td class="text-center"
							data-bind="text:answerDTO.option.otextValue"></td>

						<!-- /ko -->

						<th class="text-center" data-bind="text:$parent.avgNps"></th>

					</tr>
					<!-- /ko -->
				<tbody>
			</table>
			
			<table id="dt5" class="table table-bordered r-table">

				<thead class="thead-inverse">

					<tr>
					<th class="darkbgcolor" colspan="6">NPS Data Points</th>
					</tr>
					<tr>
						<td></td>
						<td class="text-left">How willing are you to recommend
							Quinnox to other business units
						</td>
						<td class="text-left">How willing are you to recommend
							Quinnox to other companies
						</td>
						<td class="text-left">How willing are you to buy new services
							from Quinnox
						</td>
						<td class="text-left">How will are you to 'Beta test' with
							few offerings from quinnox in your organisation
						</td>
						<td style="padding-top: 72px;" align="center">NPS Type
						</td>

					</tr>
					<tr class="lightbgcolor">

						<th class="text-center">Name Of Respondent</th>
						<th class="text-center">Q.1</th>
						<th class="text-center">Q.2</th>
						<th class="text-center">Q.3</th>
						<th class="text-center">Q.4</th>
						<th class="text-center"></th>

					</tr>
			</thead>
			<tbody data-bind="foreach:user4">

					<!-- ko foreach: questionGroupDTOs -->
					<tr>
						<td class="lightbgcolor"
							data-bind="text:$parent.firstName+' '+$parent.lastName "></td>
						<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
						<td class="text-center" data-bind="text:answerDTO.option.otext"></td>
						<!-- /ko -->
						<!-- ko if: $parent.npsType == 'detractor' -->
						<td class="text-center"><img src="/qcsat/resources/img/detract.jpg" alt="Logo" class="imgCodeDetractor">
						</td>
						<!-- /ko -->
						<!-- ko if: $parent.npsType == 'promoter' -->
						<td class="text-center"><img src="/qcsat/resources/img/promoter.png" alt="Logo" class="imgCode">
						</td>
						<!-- /ko -->
						<!-- ko if: $parent.npsType == 'neutral' -->
						<td align="center"><img src="/qcsat/resources/img/neutral.jpg" alt="Logo" class="imgCodeNetural">
						</td>
						<!-- /ko -->
					</tr>
					<!-- /ko -->
				<tbody>
			</table>
			<table id="dt6" class="table table-bordered r-table">
				<thead class="thead-inverse">
					<tr>
						<th class="darkbgcolor" colspan="4">Quinnox's Overall
							Engagament
						</th>
					</tr>
					<!-- <tr>
								<td></td>
								<td></td>
								<td ID='rotate'>What do we have to do at Quinnox to become your long-term partner of choice.</td>
								<td ID='rotate'>What do we have to improve upon at Quinnox to support you positively.</td>
								</tr> -->
					<tr class="lightbgcolor">
						<th class="text-center" rowspan="2">Name Of Respondent</th>
						<th></th>
						<th class="text-center">Q.1</th>
						<th class="text-center">Q.2</th>
					</tr>
				</thead>
				<tbody data-bind="foreach:user5">
					<!-- ko foreach: questionGroupDTOs -->
					<tr>
						<td class="text-center" class="lightbgcolor"
							data-bind="text:$parent.firstName+' '+$parent.lastName  "></td>
						<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->

						<td class="text-center" data-bind="text:answerDTO.atext"></td>
						<!-- /ko -->
					</tr>
					<!-- /ko -->
					<!-- /ko -->
				<tbody>
			</table>
		</div>
	</form>
</div>
<p class="footerCode">&copy; 2017 Quinnox Inc.
<p>
<script type="text/html" id="nps-data">
<tr>
	<td><span data-bind="text:data"></span></td>
</tr>
</script>
<script type="text/javascript"
		src="<c:url value='/resources/js/knockout-models/respondantwisereport.js' />"></script>
		<script type="text/javascript">
		var first = false;

		//var asm = new AllSurveysModel();
		//ko.applyBindings(asm, document.getElementById('pr'));
		// $('.btnCls').on('click', function() {
		// var indexLocation = $(this).attr('id');
		// var dm = new SurveyDetailModel(indexLocation);
		// ko.applyBindings(dm, document.getElementById('dt'));
		// $('#ptbl').hide();
		// $('#dt').show();
		// });

		$('.form_date').datetimepicker({
			language : 'fr',
			weekStart : 1,
			todayBtn : 1,
			autoclose : 1,
			todayHighlight : 1,
			startView : 2,
			minView : 2,
			forceParse : 0
		});
		function executeDetailJS() {
		}
</script>