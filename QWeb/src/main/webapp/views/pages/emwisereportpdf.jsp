<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value='/qcsat/resources/css/custom_styles.css' />">
<link rel="stylesheet" type="text/css"
	href="/qcsat/resources/css/report_styles.css" />

<script src="<c:url value='/resources/js/jquery.min.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/bootstrap.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/knockout-3.4.1.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/knockout.mapping.js' />"></script>

<title>EM Wise Report</title>
<style>
@media screen {
	div.divFooter {
		display: none;
	}
}

@media print {
	div.divFooter {
		position: fixed;
		bottom: 0;
	}
}

@media all {
	.page-break {
		display: none;
	}
}

@media print {
	.page-break {
		display: block;
		page-break-before: always;
	}
}

@page {
	size: landscape;
}
</style>
</head>
<body>
	<div class="contentSection2">
		<img src="/qcsat/resources/img/clientsatisfaction.jpg" alt="Logo"
			class="img-clientsatisfaction">
	</div>

	<div class="voiletbackground">
		<p class="clientsurvey">2017 Client Satisfaction Survey</p>
	</div>
	<!-- <div class="divFooter"><img src="/qcsat/resources/img/logo.png" alt="Logo" width="60px" height="30px" ></div> -->

	<div class="page-break"></div>
	<div class="contentSection1" class="contentSectionAlign">
		<p>
		<p class="ConfidentialityNote">Confidentiality Note</p>
		<br>
		<p class="paragraph">
			This document contains confidential information developed by Quinnox,
			Inc. No part of this document may be reproduced, stored in retrieval
			form, adopted or transmitted in any form or by any means, electronic,
			mechanical, photographic, graphic, optic or otherwise, translated in
			any language or computer language, without prior written permission
			from Quinnox, Inc. <br />
			<br /> This document has been prepared in accordance with the
			accordance with the accepted techniques for software engineering at
			Quinnox, Inc. The representations and related information contained
			in the document reflect our best understanding on the subject of this
			document.<br />
			<br /> However, Quinnox makes no representation or warranties with
			respect to the contents hereof and shall not be responsible for any
			loss or damage caused to the user by the direct or indirect use of
			this document and the accompanying software package. Furthermore
			Quinnox reserves the right to alter, modify or otherwise change in
			any manner the content hereof, without the obligation to notify any
			person of such revision or changes. <br /> <br />
		</p>
		<p class="footerstyle">
			� 2014 Quinnox Inc., all rights reserved.<br /> All registered and
			trademarked names are owned by their respective owners.
		</p>
	</div>

	<div class="page-break"></div>
	<br>
	<br>
	<br>
	<div>
		<h2 class="titlestyle" id="shid">Satish Sugavanam</h2>
	</div>
	<br>
	<br>
	<br>

	<table id="dt5" class="overall" class="table table-bordered">
		<thead class="thead-inverse">
		</thead>
		<tbody>

			<div>
				<td>Overall Score</td>
				<td>
					<span data-bind="if: user12.overAllNps<4">
							<div>
								<img src="/qcsat/resources/img/red.jpg" class="ragscoreimg">
							</div>
	
					</span> 
					<span data-bind="if: user12.overAllNps>=4 && user12.overAllNps<=5">
							<div>
								<img src="/qcsat/resources/img/yellow.jpg" class="ragscoreimg">
							</div>
					</span> 
					<span data-bind="if: user12.overAllNps>5">
							<div>
								<img src="/qcsat/resources/img/green.png" class="ragscoreimg">
							</div>
					</span>
				</td>
				<td align="center">
					<span><div style="margin-left: 25px;" data-bind="text:user12.overAllNps"></div></span>
				</td>
			</div>
		</tbody>
	</table>

	<table id="dt" class="nps" class="table">
		<thead class="thead-inverse">
			<tr class="npstitle">
				<th></th>
				<th>NPS Score</th>
				<th>Score(In %)</th>
			</tr>
		<thead>
		<tbody>
			<tr>
				<td class="npscolor-red"></td>
				<td class="npsborder">Detractors</td>
				<td class="data-center"><span data-bind="text:data.Detractors"></span></td>
			</tr>
			<tr>
				<td class="npscolor-yellow"></td>
				<td class="npsborder">Passive</td>
				<td class="data-center"><span data-bind="text:data.Neutral"></span></td>
			</tr>
			<tr>
				<td class="npscolor-green"></td>
				<td class="npsborder">Promoters</td>
				<td class="data-center"><span data-bind="text:data.Promoters"></span></td>
			</tr>
		</tbody>
	</table>

	<table id="dt6" class="top3" class="table">
		<thead class="thead-inverse">
			<tr class="npsborder">
				<th style="background-color: #cccccc;">Top 3 Benefits</th>
			</tr>
		</thead>
		<tbody data-bind="foreach:data">
			<tr class="npsborder">
				<td
					style="border-bottom: 2px solid #f3f3f3; border-right: 2px solid #f3f3f3;"><span
					data-bind="text : $data"></span></td>
			</tr>
		<tbody>
	</table>

	<table id="dt1" class="tablestyle" class="table">
		<thead class="thead-inverse">
			<tr class="question-row">
				<th>Client Name</th>
				<th>Name Of Respondent</th>
				<th>Quinnox's People Performance</th>
				<th>Quinnox's Service Delivery</th>
				<th>Quinnox's Performance On Relationship Management</th>
				<th>Average</th>
			</tr>
		</thead>
		<tbody data-bind="foreach:user1">
			<tr>
				<!-- <td data-bind="text:clientName"></td> -->
				<td class="highlight-td" data-bind="text:clientName"></td>
				<td class="highlight-td" data-bind="text:firstName+' '+lastName  "></td>
				<td class="data-center" data-bind="text:avgQg1"></td>
				<td class="data-center" data-bind="text:avgQg2"></td>
				<td class="data-center" data-bind="text:avgQg3"></td>
				<td class="data-center" data-bind="text:avgOfEachUser"></td>
			</tr>
		<tbody>
	</table>

	<table id="doq" class="tablestyle" class="table table-bordered">
		<thead class="thead-inverse">
			<tr class="question-row">
				<th colspan="4">Overall Score</th>
			</tr>
			<tr class="question-row">
				<th></th>
				<th>Quinnox's People Performance</th>
				<th>Quinnox's Service Delivery</th>
				<th>Quinnox's Performance On Relationship Management</th>
			</tr>
		</thead>
		<tbody>
			<tr height="50">
				<th class="highlight-td">Average</th>
				<td class="data-center" data-bind="text:user3.Question1">4.76</td>
				<td class="data-center" data-bind="text:user3.Question2">4.5</td>
				<td class="data-center" data-bind="text:user3.Question3">4.89</td>
			</tr>
		</tbody>
	</table>

	<table id="dt2" class="tablestyle" class="table table-bordered">
		<thead class="thead-inverse">
			<tr class="question-row">
				<th colspan="7">NPS Data Points</th>
			</tr>
			<tr class="question-row">
				<th>Client Name</th>
				<th>Respondent Name</th>
				<th>How willing are you to recommend Quinnox to other business
					units</th>
				<th>How willing are you to recommend Quinnox to other companies</th>
				<th>How willing are you to buy new services from Quinnox</th>
				<th>How willing are you to 'Beta test' with few offerings from
					Quinnox in your organization</th>
				<th>NPS Type</th>
			</tr>
		</thead>
		<tbody data-bind="foreach:user2">
			<!-- ko foreach: questionGroupDTOs -->
			<tr>
				<td class="highlight-td" data-bind="text:$parent.clientName"></td>
				<td class="highlight-td"
					data-bind="text:$parent.firstName+' '+$parent.lastName "></td>
				<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
				<!-- <td  data-bind="text:answerDTO.surveyusermapping.appuser.client.clientName "></td> -->
				<td class="data-center" data-bind="text:answerDTO.option.otext"></td>
				<!-- /ko -->
				<!-- ko if: $parent.npsType == 'detractor' -->
				<td class="data-center"><img
					src="/qcsat/resources/img/detract.jpg" alt="Logo"
					style="width: 34px; height: 34px;"></td>
				<!-- /ko -->
				<!-- ko if: $parent.npsType == 'promoter' -->
				<td class="data-center"><img
					src="/qcsat/resources/img/promoter.png" alt="Logo"
					style="width: 52px; height: 48px;"></td>
				<!-- /ko -->
				<!-- ko if: $parent.npsType == 'neutral' -->
				<td class="data-center"><img
					src="/qcsat/resources/img/neutral.jpg" alt="Logo"
					style="width: 35px; height: 30px;"></td>
				<!-- /ko -->
			</tr>
			<!-- /ko -->
		<tbody>
	</table>

	<script type="text/javascript">
		function overallEmwiseScore(dat) {

			var self = this;
			self.data = ko.observableArray([]);
			self.data = dat;
		}

		function overallNpsEmwiseScore(dat) {

			var self = this;
			self.user12 = ko.observableArray([]);
			self.user12 = dat;
		}

		function EmWiseTopBenefits(data) {
			var self = this;
			self.data = ko.observableArray([]);
			self.data = data;
		}

		function EMWiseReport(dat) {

			var self = this;
			self.user1 = ko.observableArray([]);
			self.user1 = dat;
		}

		function EMWiseNps(dat) {

			var self = this;
			self.user2 = ko.observableArray([]);
			self.user2 = dat;
		}

		function AvgOverallQuestiongrpEmWise(data) {
			var self = this;
			self.user3 = ko.observableArray([]);
			self.user3 = data;

		}

		function getParameterByName(name, url) {
			debugger;
			if (!url) {
				url = window.location.href;
			}
			debugger;
			name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex
					.exec(location.search);
			return results === null ? "" : decodeURIComponent(results[1]
					.replace(/\+/g, " "));
		}

		var engagementManager = getParameterByName("engagementManager");
		$.getJSON("/qcsat/proxy/api/emwisenps1?engagementManager="
				+ engagementManager, function(data) {

			var indexLocation = $(this).attr('id');
			var dm10 = new overallEmwiseScore(data);

			ko.applyBindings(dm10, document.getElementById('dt'));
			$('#dt').show();
			;
		});

		$.getJSON("/qcsat/proxy/api/emwiseoverallnps?engagementManager="
				+ engagementManager, function(data) {

			var indexLocation = $(this).attr('id');
			var dm = new overallNpsEmwiseScore(data);

			ko.applyBindings(dm, document.getElementById('dt5'));
			$('#dt5').show();

		});

		$.getJSON("/qcsat/proxy/api/top3BenefitsForEmwise?engagementManager="
				+ engagementManager, function(data) {

			var indexLocation = $(this).attr('id');
			var dm = new EmWiseTopBenefits(data);

			ko.applyBindings(dm, document.getElementById('dt6'));
			$('#dt6').show();

		});

		$.getJSON(
				"/qcsat/proxy/api/emwiseAvgForQuestionGroup?engagementManager="
						+ engagementManager + "&questionGrpId=" + 1, function(
						data) {

					var indexLocation = $(this).attr('id');
					var dm1 = new EMWiseReport(data);
					ko.applyBindings(dm1, document.getElementById('dt1'));
					$('#dt1').show();

				});

		$.getJSON("/qcsat/proxy/api/emwisenps?engagementManager="
				+ engagementManager, function(data) {

			var indexLocation = $(this).attr('id');
			var dm2 = new EMWiseNps(data);
			ko.applyBindings(dm2, document.getElementById('dt2'));
			$('#dt2').show();

		});

		$.getJSON(
				"/qcsat/proxy/api/avgOverallAverageOfQuestiongrpForEM?engagementManager="
						+ engagementManager, function(data) {
					var indexLocation = $(this).attr('id');
					var dmForAvg = new AvgOverallQuestiongrpEmWise(data);
					ko.applyBindings(dmForAvg, document.getElementById('doq'));
					$('#doq').show();

				});
	</script>

	<script type="text/javascript">
		$(function() {
			$('#printOut').click(function(e) {
								/*  e.preventDefault(); */
								var w = window.open();
								var printOne = $('.container').html();
								var printTwo = $('.contentSection1').html();
								var printThree = $('.contentSection2').html();
								w.document
										.write('<html class="page-height"><head><title></title></head><body class="page-height"><h6>1 | CSAT 2017 Promoters</h6><hr />'
												+ printThree)
										+ '</body></html>';
								w.document
										.write('<html><head><style>.break { page-break-before: always; }; .page-height{height: 100%};</style><title>Copy Printed</title></head><body><h6 class="break">2 | CSAT 2017 Promoters</h6><hr />'
												+ printTwo)
										+ '</body></html>';
								w.document
										.write('<html><head><style>.break { page-break-before: always; }</style><title>Copy Printed</title></head><body><h6 class="break">3 | CSAT 2017 Promoters</h6><hr />'
												+ printOne)
										+ '</body></html>';
								w.window.print();
								w.document.close();
								return false;
						});
		});
	</script>
</body>
</html>