<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link href="<c:url value='/resources/css/custom_styles.css' />" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/qcsat/resources/css/report_styles.css" />

<div class="dropdown menus">
<a style="color:white;font-size:14px;padding:5px;" href="${pageContext.request.contextPath}/home">Home</a>

  <button class="btn btn-link" style="color:white;font-size:14px;padding:5px;" type="button" data-toggle="dropdown" data-hover="dropdown">
  Reports <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="${pageContext.request.contextPath}/overallaccountreport">Overall Quinnox Report</a></li>
    <li><a href="${pageContext.request.contextPath}/accountreports">Account Wise Report</a></li>
    <li><a href="${pageContext.request.contextPath}/emwisereport">Engagement Managers Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npspromotersreport">Overall NPS Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npspromoters">NPS Promoters Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npsdetractors">NPS Detractors Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npsneutral">NPS Passive Report</a></li>
    <li><a href="${pageContext.request.contextPath}/respondantwisereport">Respondents Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npsquestionwise">NPS Question Report</a></li>
  </ul>
</div>
<div class="container pageLayout ">
	<form id="msform">
		<div class="form-group">
			<div>
				<h3 id='shid'></h3>

			</div>
			<br clear="all" /> <br clear="all" />
			<div>
				<h2 class="title" id="shid">CSAT Quinnox Detractors</h2>
			</div>
		<!-- <table id="dt1" class="table" style="display: none;"
				data-bind="template: { name: 'nps-data', foreach: data">

			</table> -->
			<!-- <div style="background-color: f3f3f3; border: 1px solid #cccccc">
				<h2>
					CSAT Report 2016-2017
				</h2>
			</div> -->
		<!-- 	<button type="button" value="Btn1"
								class="btn btn-primary btn-sm btn-responsive"
								data-toggle="modal" data-target="#myModal"
								style="background-color: #428bca;" onClick="viewReports()">View</button> -->
								<div class="pdflink">
								<a  href="javascript:window.open('/qcsat/views/pages/detractorsreport.jsp', 'DETRACTORS', 'width=200,height=150');">View Report</a>
								</div>
						<br><br><br>
						<table  id="dt4" class="table table-bordered centerTable">
							<thead class="thead-inverse">
								<tr>
									<th class="darkbgcolor" colspan="7">NPS Detractors</th>
								</tr>
								<tr height="50">
								<td></td>
								<td></td>
								<td class="text-left" >How willing are you to recommend Quinnox to other business units</td>
								<td class="text-left" >How willing are you to recommend Quinnox to other companies</td>
								<td class="text-left" >How willing are you to buy new services from Quinnox</td>
								<td class="text-left" >How willing are you to 'Beta test' with few offerings from Quinnox in your organisation</td>
								<td></td>
								</tr>
								<tr class="lightbgcolor">
									<th>Client Name</th>
									<th>Name Of Respondent</th>
									<th class="text-center">Q.1</th>
									<th class="text-center">Q.2</th>
									<th class="text-center">Q.3</th>
									<th class="text-center">Q.4</th>
									<th class="text-center" >Is a Detractor</th>
								</tr>
							</thead>
							<tbody data-bind="foreach:user2">
							<!-- ko foreach: questionGroupDTOs -->
							<!-- ko if : $parent.npsType == 'detractor'  -->
								
								<tr>
								<td class="lightbgcolor" data-bind="text:$parent.clientName"></td>
								<td class="lightbgcolor" data-bind="text:$parent.firstName+' '+$parent.lastName "></td>
								<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
								<td class="text-center" data-bind="text:answerDTO.option.otext"></td>
								<!-- /ko -->
								<td class="text-center"><img src="/qcsat/resources/img/detractors.jpg" alt="Logo" class="imgCode"></td>	
								</tr>
								
							<!-- /ko -->
							<!-- /ko -->
							<tbody>
						</table>
				</div>
		</form>
</div>

<p class="footerCode" >&copy; 2017 Quinnox Inc.
<p>

	<script type="text/javascript"
		src="<c:url value='/resources/js/knockout-models/npsdetractors.js' />"></script>

	<script type="text/javascript">
		var first = false;
		//var asm = new AllSurveysModel();
		//ko.applyBindings(asm, document.getElementById('pr'));
		// 	$('.btnCls').on('click', function() {
		// 		var indexLocation = $(this).attr('id');
		// 		var dm = new SurveyDetailModel(indexLocation);
		// 		ko.applyBindings(dm, document.getElementById('dt'));
		// 		$('#ptbl').hide();
		// 		$('#dt').show();
		// 	});
		$('.form_date').datetimepicker({
			language : 'fr',
			weekStart : 1,
			todayBtn : 1,
			autoclose : 1,
			todayHighlight : 1,
			startView : 2,
			minView : 2,
			forceParse : 0
		});

		function executeDetailJS() {
		}
	</script>