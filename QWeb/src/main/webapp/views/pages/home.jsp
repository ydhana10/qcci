<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link href="<c:url value='/resources/css/custom_styles.css' />"
	rel="stylesheet">
<div style="margin-left:107px;margin-top: 15px;" class="dropdown">
<a style="color:white;font-size:14px;padding:5px;" href="${pageContext.request.contextPath}/home">Home</a>

  <button class="btn btn-link" style="color:white;font-size:14px;padding:5px;" type="button" data-toggle="dropdown" data-hover="dropdown">
  Reports <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
   
    <li><a href="${pageContext.request.contextPath}/overallaccountreport">Overall Quinnox Report</a></li>
    <li><a href="${pageContext.request.contextPath}/accountreports">Account Wise Report</a></li>
    <li><a href="${pageContext.request.contextPath}/emwisereport">Engagement Managers Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npspromotersreport">Overall NPS Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npspromoters">NPS Promoters Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npsdetractors">NPS Detractors Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npsneutral">NPS Neutral Report</a></li>
    <li><a href="${pageContext.request.contextPath}/respondantwisereport">Respondents Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npsquestionwise">NPS Question Report</a></li>
  </ul>
</div>
<div class="container"
	style="padding-left: 20px !important; padding-top: 81px;">
	<form id="msform">
		<div class="form-group">
			<h3>List of Surveys</h3>
			<a style="right:0;float:right" href="http://localhost:8234/qcsat/templates/CSAT_UPLOAD_TEMPLATE.xlsx">Download Multiple Users - Upload Template</a>
			<br clear="all" /> <br clear="all" />
			<table class="table">
				<thead class="thead-inverse">
					<tr>
						<th>Survey Id</th>
						<th>Survey Name</th>
						<th>Survey Description</th>
						<th>Assign (Single User)</th>
						<th>Upload File (Assign Multiple Users)</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td id="surveyId" scope="row"><a style="text-decoration: underline;"
		href="${pageContext.request.contextPath}/surveyresults">1</a></td>
						<td>CSAT</td>
						<td>Customer Satisfaction Survey</td>
						<td><button type="button" value="Btn1"
								class="btn btn-primary btn-sm btn-responsive"
								data-toggle="modal" data-target="#myModal"
								style="background-color: #428bca;">Assign</button></td>
						<td>
						
						    <input type="file" id="fid" name="file" style="padding:0px !important;border:0px !important;border-radius:0px !important;margin-bottom:3px !important"/>
						    <input type="button" class="btn btn-primary btn-sm btn-responsive" style="padding:4px !important;color:white;" id="btnUpload" type="button" value="Upload file"/>
						    <input type="button" class="btn btn-primary btn-sm btn-responsive" style="padding:4px !important;color:white;" id="btnClear" value="Clear"/>
						
						</td>
					</tr>
					
					<tr>
						<td id="surveyId" scope="row"><a style="text-decoration: underline;"
		href="${pageContext.request.contextPath}/surveyresults">2</a></td>
						<td>QSurvey</td>
						<td>Questions AMS Survey</td>
						<td><button type="button" value="Btn1"
								class="btn btn-primary btn-sm btn-responsive"
								data-toggle="modal" data-target="#myModal"
								style="background-color: #428bca;">Assign</button></td>
						<td>
						
						    <input type="file" id="fid" name="file" style="padding:0px !important;border:0px !important;border-radius:0px !important;margin-bottom:3px !important"/>
						    <input type="button" class="btn btn-primary btn-sm btn-responsive" style="padding:4px !important;color:white;" id="btnUpload" type="button" value="Upload file"/>
						    <input type="button" class="btn btn-primary btn-sm btn-responsive" style="padding:4px !important;color:white;" id="btnClear" value="Clear"/>
						
						</td>
					</tr>
				</tbody>
			</table>

		</div>
	</form>
	<p style="color: white;" align="center">&copy; 2017 Quinnox Inc.<p>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Assign Survey to User</h4>
			</div>
			<div class="modal-body">
				<form style="min-height:400px !important;">
				<div class="form-group">
						<label for="exampleSelect1">Vertical</label> <select
							class="form-control" id="verticalSelectId">
							<option value="0" selected>Please Select</option>
							
						</select>
					</div>
					<div class="form-group">
						<label for="exampleSelect1">Client</label> <select
							class="form-control" id="clientSelectId">
							<option value="0">Please Select</option>
						</select>
					</div>
					
					
					
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label> <input
							type="email" class="form-control" id="emailId"
							aria-describedby="emailHelp" placeholder="Enter email"> <small
							id="emailHelp" class="form-text text-muted">This will be the username to login into application.</small>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">First Name</label> <input
							type="text" class="form-control" id="firstNameId"
							placeholder="First Name">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Last Name</label> <input
							type="text" class="form-control" id="lastNameId"
							placeholder="Last Name">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Engagement Manager</label> <input
							type="text" class="form-control" id="emId"
							placeholder="Engagement Manager">
					</div>
					
					<input
						type="hidden" id="csrf" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
					<button type="button" id="btn1Id" class="btn btn-primary">Submit</button>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnId" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
	
</div>
<!-- Modal -->
<div id="accountsModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Choose Account</h4>
			</div>
			<div class="modal-body">
				<form style="min-height:120px !important;">
				<div class="form-group">
						<label for="exampleSelect1">Vertical</label> <select
							class="form-control" id="verticalSelectId1">
							<option value="0" selected>Please Select</option>
							
						</select>
					</div>
					<div class="form-group">
						<label for="exampleSelect1">Client</label> <select
							class="form-control" id="clientSelectId1">
							<option value="0">Please Select</option>
						</select>
					</div>
					
					<input
						type="hidden" id="csrf" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
					<button type="button" id="btn2Id" class="btn btn-primary">View Report</button>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnId" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
	
</div>
<script type="text/javascript" src="<c:url value='/resources/js/knockout-models/home.js' />" ></script>