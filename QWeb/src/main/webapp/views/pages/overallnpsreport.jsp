<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value='/qcsat/resources/css/custom_styles.css' />">
<link rel="stylesheet" type="text/css"
	href="/qcsat/resources/css/report_styles.css" />
<script src="<c:url value='/resources/js/jquery.min.js' />"></script>

<script type="text/javascript"
	src="<c:url value='/resources/js/bootstrap.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/knockout-3.4.1.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/knockout.mapping.js' />"></script>

<title>Overall NPS Report</title>
<style>
-webkit-transform
:
 
rotate
(-90deg);


-moz-transform
:rotate(-90deg)
;


filter
:progid
:DXImageTransform
.Microsoft
.BasicImage
(rotation=3);



@media screen {
	div.divFooter {
		display: none;
	}
}

@media print {
	div.divFooter {
		position: fixed;
		bottom: 0;
	}
}

@media all {
	.page-break {
		display: none;
	}
}

@media print {
	.page-break {
		display: block;
		page-break-before: always;
	}
}

@page {
	size: landscape;
}
</style>
</head>
<body>
	<div class="contentSection2">
		<img src="/qcsat/resources/img/clientsatisfaction.jpg" alt="Logo"
			class="img-clientsatisfaction">
	</div>

	<div class="voiletbackground">
		
		<p class="clientsurvey">2017 Client Satisfaction Survey</p>
	</div>

	<div class="page-break"></div>
	<div class="contentSection1" class="contentSectionAlign">
		<p>
		<p class="ConfidentialityNote">Confidentiality Note</p>
		<br>

		<p class="paragraph">
			This document contains confidential information developed by Quinnox,
			Inc. No part of this document may be reproduced, stored in retrieval
			form, adopted or transmitted in any form or by any means, electronic,
			mechanical, photographic, graphic, optic or otherwise, translated in
			any language or computer language, without prior written permission
			from Quinnox, Inc. <br />
			<br /> This document has been prepared in accordance with the
			accordance with the accepted techniques for software engineering at
			Quinnox, Inc. The representations and related information contained
			in the document reflect our best understanding on the subject of this
			document.<br />
			<br /> However, Quinnox makes no representation or warranties with
			respect to the contents hereof and shall not be responsible for any
			loss or damage caused to the user by the direct or indirect use of
			this document and the accompanying software package. Furthermore
			Quinnox reserves the right to alter, modify or otherwise change in
			any manner the content hereof, without the obligation to notify any
			person of such revision or changes. <br /> <br />
		</p>
		<p class="footerstyle">
			� 2014 Quinnox Inc., all rights reserved.<br /> All registered and
			trademarked names are owned by their respective owners.
		</p>


	</div>
	<div class="page-break"></div>
	<div>

		<h2 class="titlestyle" id="shid">Overall NPS</h2>

	</div>


	<br>
	<br>
	<table id="dt3" class="overallnpstable" class="table">
		<tr>
		<thead class="thead-inverse">

			<tr class="npstitle">
				<th></th>
				<th>NPS Score</th>
				<th>Score(In %)</th>
			</tr>
		<thead>
		<tbody>
			<tr>
				<td class="npscolor-red"></td>
				<td class="npsborder">Detractors</td>
				<td class="data-center"><span
					data-bind="text:user1.Ditractors"></span></td>
			</tr>
			<tr>
				<td class="npscolor-yellow"></td>
				<td class="npsborder">Passive</td>
				<td class="data-center"><span data-bind="text:user1.Neutral"></span></td>
			</tr>
			<tr>
				<td class="npscolor-green"></td>
				<td class="npsborder">Promoters</td>
				<td class="data-center"><span data-bind="text:user1.Promoters"></span></td>
			</tr>
		</tbody>

	</table>

	<table id="dt4" class="tablestyle" class="table table-bordered">
		<thead class="thead-inverse">
			<tr class="question-row">
				<th colspan="7">NPS Data Points</th>
			</tr>
			<tr class="question-row">
				<td></td>
				<td></td>
				<td>How willing are you to recommend Quinnox to other business
					units</td>
				<td>How willing are you to recommend Quinnox to other
					companies</td>
				<td>How willing are you to buy new services from Quinnox</td>
				<td>How willing are you to 'Beta test' with few offerings
					from Quinnox in your organization</td>
				<td></td>
			</tr>
			<tr class="question-row">
				<th>Client Name</th>
				<th>Respondent Name</th>
				<th>Q.1</th>
				<th>Q.2</th>
				<th>Q.3</th>
				<th>Q.4</th>
				<th></th>
				</tr>
		</thead>
		<tbody data-bind="foreach:user">
			<!-- ko foreach: questionGroupDTOs -->

			<tr>
			 <td class="highlight-td" data-bind="text:$parent.clientName"></td>
				<td class="highlight-td" data-bind="text:$parent.firstName+' '+$parent.lastName "></td>
				<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
				<td class="data-center"  data-bind="text:answerDTO.option.otext"></td>
				<!-- /ko -->
				<!-- ko if: $parent.npsType == 'detractor' -->
				<td class="data-center"><img
					src="/qcsat/resources/img/detract.jpg" alt="Logo"
					class="imgCodeDetractor"></td>
				<!-- /ko -->
				<!-- ko if: $parent.npsType == 'promoter' -->
				<td class="data-center"><img
					src="/qcsat/resources/img/promoter.png" alt="Logo"
					class="imgCode"></td>
				<!-- /ko -->
				<!-- ko if: $parent.npsType == 'neutral' -->
				<td class="data-center"><img
					src="/qcsat/resources/img/neutral.jpg" alt="Logo"
					class="imgCodeNetural"></td>
				<!-- /ko -->

			</tr>
			<!-- /ko -->
		<tbody>
	</table>

	<!-- </div> -->
	</div>
	</form>


	</div>

	<script type="text/javascript">
function npsPromoters(dat){
	var self = this;
	self.user1 = ko.observableArray([]);
	self.user1 = dat;
}
function npsForEachQuestion(data){
	var self = this;
	self.user = ko.observableArray([]);
	self.user = data;
}

	$.getJSON("/qcsat/proxy/api/npspromoters",
		function(data) {
	var dm3 = new npsPromoters(data);
	ko.applyBindings(dm3, document.getElementById('dt3'));
	$('#dt3').show();
});
	
	$.getJSON("/qcsat/proxy/api/npsoverallreport",
			function(data) {
		var dm = new npsForEachQuestion(data);
		ko.applyBindings(dm, document.getElementById('dt4'));
		$('#dt4').show();
	});
	
</script>

</body>

</html>