<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link href="<c:url value='/resources/css/custom_styles.css' />"
	rel="stylesheet">
<div style="margin-left:107px;margin-top: 15px;" class="dropdown">
<a style="color:white;font-size:14px;padding:5px;" href="${pageContext.request.contextPath}/home">Home</a>

  <button class="btn btn-link" style="color:white;font-size:14px;padding:5px;" type="button" data-toggle="dropdown" data-hover="dropdown">
  Reports <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
     <li><a href="${pageContext.request.contextPath}/overallaccountreport">Overall Quinnox Report</a></li>
    <li><a href="${pageContext.request.contextPath}/accountreports">Account Wise Report</a></li>
    <li><a href="${pageContext.request.contextPath}/emwisereport">Engagement Managers Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npspromotersreport">Overall NPS Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npspromoters">NPS Promoters Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npsdetractors">NPS Detractors Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npsneutral">NPS Neutral Report</a></li>
    <li><a href="${pageContext.request.contextPath}/respondantwisereport">Repondents Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npsquestionwise">NPS Question Report</a></li>
  </ul>
</div>
<div class="container"
	style="padding-left: 20px !important; padding-top: 81px;">
	<form id="msform">
		<div class="form-group">
			<div>
				<h3 id='shid'></h3>
				<button style="float: right; background-color: #428bca;"
					type="button" value="Btn1"
					class="btn btn-primary btn-sm btn-responsive" data-toggle="modal"
					data-target="#myModal">Export</button>
			</div>
			<br clear="all" /> <br clear="all" />
			<div id="ptbl">
			<table id="pr" class="table" style="font-size:15px !important;">
				<thead class="thead-inverse">
					<tr>
						<!-- <th>Survey Id</th> -->
						<th>Survey Name</th>
						<th>Client</th>
						<th>Client Name</th>
						<th>Client Id</th>
						<th>Notification Count</th>
						<th>Status/Actions</th>
						<th>
		                    	<input onclick="selectAll(this.id);" type="checkbox" id="sltAllId" >Select All
		                	
		                </th>
					</tr>
				</thead>
				<tbody
					data-bind="foreach:{data:surveys,afterRender:executeDetailJS}">
					<tr>
						<!-- <th id="surveyId" data-bind="text: sid" scope="row"></th> -->
						<td data-bind="text: sname"></td>
						<td data-bind="text: userDTO.clientName"></td>
						<td data-bind="text: userDTO.firstName+' '+userDTO.lastName"></td>
						<td data-bind="text: userDTO.ssoId"></td>
						<td data-bind="text: notificationCount"></td>
						<!-- ko if: status == 'COMPLETED' -->
						<td>
							<button type="button" value="Btn1"
								class="btnCls btn btn-primary btn-sm btn-responsive"
								style="background-color: #428bca;" onclick="onComplete(this.id);"
								data-bind="attr:{id: sumId}">Completed</button>
						</td>
						<!-- /ko -->
						<!-- ko if: status == 'ASSIGNED' -->
						<td>Assigned
							<button style="padding:5px !important;font-size:12px !important;" data-bind="attr:{id: sumId}" class="sbdNtfn btn btn-large btn-primary"
								data-toggle="confirmation" data-btn-ok-label="Continue"
								data-btn-ok-icon="glyphicon glyphicon-share-alt"
								data-btn-ok-class="btn-success" data-btn-cancel-label="Stoooop!"
								data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
								data-btn-cancel-class="btn-danger" data-title="Is it ok?"
								data-content="This might be dangerous" onclick="sendMail(this.id)">Send Notification</button>
								<!-- ko if: $index() == $parent.surveys().length-1 -->
									
								<!-- /ko -->
						</td>
						<!-- /ko -->
						<td>
							<!-- ko if: status == 'ASSIGNED' -->
								<label class="checkbox-inline">
				                    	<input type="checkbox" class="cbCls" data-bind="attr:{id: sumId}" >
				                </label>
			                <!-- /ko -->
			                <!-- ko if: status == 'COMPLETED' -->
								<label class="checkbox-inline">
				                    	<input type="checkbox" disabled="disabled" class="cbCls" data-bind="attr:{id: sumId}" >
				                </label>
			                <!-- /ko -->
		                </td>
					</tr>
					
				</tbody>
			</table>
			<button type="button" value="Btn1"
								class="btnCls btn btn-primary btn-sm btn-responsive"
								style="background-color: #428bca;left:41%;position:absolute;" onclick="sendMailToSel();"
								>Send Notification to Selected</button>
			</div>
			<table id="dt" class="table" style="display: none;"
				data-bind="template: { name: 'detail-template', foreach: qgs}">


			</table>
			
			<table id="dt1" class="table" style="display: none;"
				data-bind="template: { name: 'detail-report', foreach: qgs}">


			</table>

		</div>
	</form>
	<p style="color: white;" align="center">&copy; 2017 Quinnox Inc.<p>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<input type="hidden" id="srId" />
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Export Survey</h4>
			</div>
			<div class="modal-body">
				<form style="min-height: 400px !important;">
					<div class="form-group">
						<label for="exampleSelect1">Client</label> <select
							class="form-control" id="clientSelectId">
							<option value="0" selected>Please Select</option>

						</select>
					</div>

					<div class="form-group">
						<label for="dtp_input2">Start Date</label><br clear="all" />
						<div class="input-group date form_date col-md-5" data-date=""
							data-date-format="yyyy-mm-dd" data-link-field="dtp_input2"
							data-link-format="yyyy-mm-dd">
							<input id="startDate" class="form-control" type="text" value=""
								readonly> <span class="input-group-addon"><span
								class="glyphicon glyphicon-remove"></span></span> <span
								class="input-group-addon"><span
								class="glyphicon glyphicon-th"></span></span>
						</div>
						<input type="hidden" id="dtp_input2" value="" /><br />
					</div>

					<div class="form-group">
						<label for="dtp_input2">End Date</label><br clear="all" />
						<div class="input-group date form_date col-md-5" data-date=""
							data-date-format="yyyy-mm-dd" data-link-field="dtp_input2"
							data-link-format="yyyy-mm-dd">
							<input id="endDate" class="form-control" type="text" value=""
								readonly> <span class="input-group-addon"><span
								class="glyphicon glyphicon-remove"></span></span> <span
								class="input-group-addon"><span
								class="glyphicon glyphicon-th"></span></span>
						</div>
						<input type="hidden" id="dtp_input2" value="" /><br />
					</div>
					<input type="hidden" id="csrf" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
					<button type="button" id="exportAllId" class="btn btn-primary">Export</button>
					<br clear="all" /> <a href="" style="display: none;" id="did">Please
						click on this link to download the file</a>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnId" class="btn btn-default"
					data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div id="accountsModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Choose Account</h4>
			</div>
			<div class="modal-body">
				<form style="min-height:120px !important;">
				<div class="form-group">
						<label for="exampleSelect1">Vertical</label> <select
							class="form-control" id="verticalSelectId1">
							<option value="0" selected>Please Select</option>
							
						</select>
					</div>
					<div class="form-group">
						<label for="exampleSelect1">Client</label> <select
							class="form-control" id="clientSelectId1">
							<option value="0">Please Select</option>
						</select>
					</div>
					
					<input
						type="hidden" id="csrf" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
					<button type="button" id="btn2Id" class="btn btn-primary" onclick = "viewAccountReports()">View Report</button>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnId" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
	
</div>
<script type="text/html" id="detail-template">
<thead data-bind="if: $index()==0" class="thead-inverse">
					<tr>
						<th>Question Group</th>
						<th>Question</th>
						<th>Answer</th>
					</tr>
				</thead>
    <tbody data-bind="foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 })">
<tr>
 						<!-- ko if: $index()===0 -->
						<td data-bind="attr:{rowspan:$parent.questions.length}" scope="row">
							<span data-bind="text: $parent.grpName"></span>
						</td>
						<td data-bind="text: qtext"></td>
						
						<td>
						<span data-bind="if: questionTypeDTO.qtype == 'radiobutton'">
							<span data-bind="foreach: answerDTOs">
								<span data-bind="text: option.otext"></span>
							</span>
						</span>
						<span data-bind="if: questionTypeDTO.qtype == 'checkbox'">
							<span data-bind="foreach: answerDTOs">
								<span data-bind="text: option.otext">,</span>
							</span>
						</span>
						<span data-bind="if: questionTypeDTO.qtype == 'text'">
							<span data-bind="foreach: answerDTOs">
								<span data-bind="text: atext"></span>
							</span>
						</span>
						</td>
        				<!-- /ko -->


						<!-- ko if: $index()!=0 -->
						
						<td data-bind="text: qtext"></td>
						
						<td>
						<span data-bind="if: questionTypeDTO.qtype == 'radiobutton'">
							<span data-bind="foreach: answerDTOs">
								<span data-bind="text: option.otext"></span>
							</span>
						</span>
						<span data-bind="if: questionTypeDTO.qtype == 'checkbox'">
							<span data-bind="foreach: answerDTOs">
								<span data-bind="text: option.otext"></span><span data-bind="if: atext !=null"><span data-bind="text: ' - '+atext"></span></span><br/>
							</span>
						</span>
						<span data-bind="if: questionTypeDTO.qtype == 'text'">
							<span data-bind="foreach: answerDTOs">
								<span data-bind="text: atext"></span>
							</span>
						</span>
						</td>
        				<!-- /ko -->

						
						
					</tr>
<tbody>
</script>

<script type="text/html" id="detail-report">
<thead data-bind="if: $index()==0" class="thead-inverse">
					<tr>
						<th>Question Group</th>
						<th>OverAll Score</th>
					</tr>


					<tr>
						<th style="font-size:bold">NPS</th>
						<td><img id="myImg" src="/QWeb/src/main/webapp/resources/img/logo.png" alt="Trolltunga, Norway" width="50" height="50"></td>
					</tr>
<tr>
<th>Top 3 Benefits</th>
<td>Flexibility</td>
<tr>
<td></td><td>Skills<td>
</tr>
<tr>
<td></td><td>Domain Knowledge<td>
</tr>





</script>

<script type="text/javascript"
	src="<c:url value='/resources/js/knockout-models/surveyresults.js' />"></script>
<script type="text/javascript">
	var first = false;
	//var asm = new AllSurveysModel();
	//ko.applyBindings(asm, document.getElementById('pr'));
// 	$('.btnCls').on('click', function() {
// 		var indexLocation = $(this).attr('id');
// 		var dm = new SurveyDetailModel(indexLocation);
// 		ko.applyBindings(dm, document.getElementById('dt'));
// 		$('#ptbl').hide();
// 		$('#dt').show();
// 	});
	$('.form_date').datetimepicker({
		language : 'fr',
		weekStart : 1,
		todayBtn : 1,
		autoclose : 1,
		todayHighlight : 1,
		startView : 2,
		minView : 2,
		forceParse : 0
	});

	function executeDetailJS() {
	}
</script>