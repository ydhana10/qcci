<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value='/qcsat/resources/css/custom_styles.css' />">
<link rel="stylesheet" type="text/css"
	href="/qcsat/resources/css/report_styles.css" />

<script src="<c:url value='/resources/js/jquery.min.js' />"></script>

<script type="text/javascript"
	src="<c:url value='/resources/js/bootstrap.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/knockout-3.4.1.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/knockout.mapping.js' />"></script>

<title>Respondent Wise Report</title>
<style>
@media screen {
	div.divFooter {
		display: none;
	}
}

@media print {
	div.divFooter {
		position: fixed;
		bottom: 0;
	}
}

@media all {
	.page-break {
		display: none;
	}
}

@media print {
	.page-break {
		display: block;
		page-break-before: always;
	}
}

@page {
	size: landscape;
}
</style>
</head>

<body>
	<div class="contentSection2">
		<img src="/qcsat/resources/img/clientsatisfaction.jpg" alt="Logo"
			class="img-clientsatisfaction">
	</div>

	<div class="voiletbackground"> 
		<p class="clientsurvey">2017 Client Satisfaction Survey</p>
	</div>
	<!-- <div class="divFooter" style="height: 20px;"><img src="/qcsat/resources/img/logo.png" alt="Logo" width="60px" height="20px" ></div>  -->
	<div class="page-break"></div>
	<div class="contentSection1" class="contentSectionAlign">
		<p>
		<p class="ConfidentialityNote">Confidentiality Note</p>
		<br>

		<p class="paragraph">
			This document contains confidential information developed by Quinnox,
			Inc. No part of this document may be reproduced, stored in retrieval
			form, adopted or transmitted in any form or by any means, electronic,
			mechanical, photographic, graphic, optic or otherwise, translated in
			any language or computer language, without prior written permission
			from Quinnox, Inc. <br />
			<br /> This document has been prepared in accordance with the
			accordance with the accepted techniques for software engineering at
			Quinnox, Inc. The representations and related information contained
			in the document reflect our best understanding on the subject of this
			document.<br />
			<br /> However, Quinnox makes no representation or warranties with
			respect to the contents hereof and shall not be responsible for any
			loss or damage caused to the user by the direct or indirect use of
			this document and the accompanying software package. Furthermore
			Quinnox reserves the right to alter, modify or otherwise change in
			any manner the content hereof, without the obligation to notify any
			person of such revision or changes. <br /> <br />
		</p>
		<p class="footerstyle">
			� 2014 Quinnox Inc., all rights reserved.<br /> All registered and
			trademarked names are owned by their respective owners.
		</p>

	</div>


	<div class="page-break"></div>
	<br>
	<br>
	<br>
	<h2 class="titlestyle" id="shid">Respondent Report</h2>

	</div>




	<table id="doq" class="overall" class="table">
		<thead class="thead-inverse"></thead>
		<tbody>
			<tr>

				<!-- <th style="padding-left: 359px;background-color:#cccccc">CSAT Report - Virtusa Consulting Services</tr> -->
			<tr>
				<td>Overall Score</td>
				<td><span data-bind="if: user1.RagScore<4">

						<div class="col.md-4">
							<img src="/qcsat/resources/img/red.jpg" class="ragscoreimg">
						</div>
				</span> <span data-bind="if: user1.RagScore>=4 && user1.RagScore<=5">

						<div class="col.md-4">
							<img src="/qcsat/resources/img/yellow.jpg" class="ragscoreimg">
						</div>

				</span> <span data-bind="if: user1.RagScore>5">

						<div class="col.md-4">
							<img src="/qcsat/resources/img/green.png" class="ragscoreimg">
						</div>
				</span></td>

				<td style="margin-left: 25px;"><span><div
							class="col.md-4" data-bind="text:user1.RagScore"></div></span></td>

			</tr>

		</tbody>

	</table>

	<table id="dt1" class="respondent-top3 " class="table">

		<thead class="thead-inverse">

			<tr class="npsborder">

				<th style="background-color: #cccccc;">Top 3 Benefits</th>

			</tr>

		</thead>

		<tbody data-bind="foreach:data">

			<tr class="npsborder">

				<td class="npsborder"><span data-bind="text : $data"></span></td>

			</tr>
		<tbody>
	</table>
	<table id="dt2" class="tablestyle" class="table table-bordered">

		<thead class="thead-inverse">

			<tr class="question-row">

				<th colspan="8">Quinnox People Performance</th>

			</tr>

			<tr class="question-row">

				<th></th>

				<th>Understanding of your business</th>

				<th>Technical understanding of your environment</th>

				<th>Flexibility to accommodate your requirements</th>

				<th>Responsiveness and attitude</th>

				<th>Timeliness and clarity of communication</th>

				<th>Effectiveness of the Engagement Manager</th>

				<th></th>

			</tr>
			<tr class="question-row">

				<th>Name Of Respondent</th>

				<th>Q.1</th>

				<th>Q.2</th>

				<th>Q.3</th>

				<th>Q.4</th>

				<th>Q.5</th>

				<th>Q.6</th>

				<th>Average</th>

			</tr>
		</thead>

		<tbody data-bind="foreach:user1">

			<!-- ko foreach: questionGroupDTOs -->

			<tr height="50">

				<td class="highlight-td"
					data-bind="text:$parent.firstName+' '+$parent.lastName"></td>

				<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->

				<td class="data-center" data-bind="text:answerDTO.option.otextValue"></td>

				<!-- /ko -->

				<td class="data-center" data-bind="text:$parent.avgNps"></td>

			</tr>
			<!-- /ko -->
		<tbody>
	</table>
	<br>
	<br>
	<br>

	<table id="dt3" class="tablestyle" class="table table-bordered">

		<thead class="thead-inverse">

			<tr class="question-row">

				<th colspan="8">Quinnox's Service Delivery</th>

			</tr>

			<tr class="question-row">

				<th></th>

				<th>Prompt fulfillment of resourcing</th>

				<th s>The delivery teams are have <br>adequate skills<br>
					for my requirements
				</th>

				<th>The teams strives hard to Deliver Projects/ Services On
					Time</th>

				<th>The team Delivers projects/ Services with High Quality</th>

				<th>Prompt escalation of major issues and show stoppers</th>

				<th>Governance reports and data driven decision making</th>

				<th></th>

			</tr>

			<tr class="question-row">

				<th>Name Of Respondent</th>

				<th>Q.1</th>

				<th>Q.2</th>

				<th>Q.3</th>

				<th>Q.4</th>

				<th>Q.5</th>

				<th>Q.6</th>

				<th>Average</th>

			</tr>

		</thead>

		<tbody data-bind="foreach:user2">

			<!-- ko foreach: questionGroupDTOs -->
			<tr>

				<td class="highlight-td"
					data-bind="text:$parent.firstName+' '+$parent.lastName  "></td>

				<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->

				<td class="data-center" data-bind="text:answerDTO.option.otextValue"></td>

				<!-- /ko -->

				<td class="data-center" data-bind="text:$parent.avgNps"></td>

			</tr>
			<!-- /ko -->
		<tbody>
	</table>

	<table id="dt4" class="tablestyle" class="table table-bordered">

		<thead class="thead-inverse">

			<tr class="question-row">

				<th colspan="7">Performance on RELATIONSHIP MANAGEMENT</th>

			</tr>

			<tr class="question-row">

				<th></th>

				<th>The account manager is knowledgeable and professional.</th>

				<th>The account manageris making a positive contribution to
					my business.</th>

				<th>The account manager responds to my inquiries in a timely
					manner.</th>

				<th>The account manager works<br>towards achieving High
					Customer Satisfaction

				</th>

				<th>Overall, I am very satisfied with the account manager.</th>

				<th></th>

			</tr>

			<tr class="question-row">

				<th>Name Of Respondent</th>

				<th>Q.1</th>

				<th>Q.2</th>

				<th>Q.3</th>

				<th>Q.4</th>

				<th>Q.5</th>

				<th>Average</th>

			</tr>

		</thead>

		<tbody data-bind="foreach:user3">

			<!-- ko foreach: questionGroupDTOs -->
			<tr>

				<td class="highlight-td"
					data-bind="text:$parent.firstName+' '+$parent.lastName  "></td>

				<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->

				<td class="data-center" data-bind="text:answerDTO.option.otextValue"></td>

				<!-- /ko -->

				<td class="data-center" data-bind="text:$parent.avgNps"></td>

			</tr>
			<!-- /ko -->
		<tbody>
	</table>

	<table id="dt5" class="tablestyle" class="table table-bordered">

		<thead class="thead-inverse">

			<tr class="question-row">

				<th colspan="6">NPS Data Points</th>

			</tr>

			<tr class="question-row">

				<th></th>

				<th>How willing are you to recommend Quinnox to other
					business units</th>

				<th>How willing are you to recommend Quinnox to other
					companies</th>

				<th>How willing are you to buy new services from Quinnox</th>

				<th>How will are you to 'Beta test' with few offerings from
					quinnox in your organisation</th>

				<th></th>

			</tr>

			<tr class="question-row">

				<th>Name Of Respondent</th>

				<th>Q.1</th>

				<th>Q.2</th>

				<th>Q.3</th>

				<th>Q.4</th>


				<th>NPS Type</th>

			</tr>

		</thead>

		<tbody data-bind="foreach:user4">

			<!-- ko foreach: questionGroupDTOs -->
			<tr>

				<td class="highlight-td"
					data-bind="text:$parent.firstName+' '+$parent.lastName "></td>

				<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->

				<td class="data-center" data-bind="text:answerDTO.option.otext"></td>

				<!-- /ko -->

				<!-- ko if: $parent.npsType == 'detractor' -->

				<td class="data-center"><img
					src="/qcsat/resources/img/detract.jpg" alt="Logo"
					style="width: 34px; height: 34px;"></td>

				<!-- /ko -->

				<!-- ko if: $parent.npsType == 'promoter' -->

				<td class="data-center"><img
					src="/qcsat/resources/img/promoter.png" alt="Logo"
					style="width: 52px; height: 48px;"></td>

				<!-- /ko -->

				<!-- ko if: $parent.npsType == 'neutral' -->

				<td class="data-center"><img
					src="/qcsat/resources/img/neutral.jpg" alt="Logo"
					style="width: 35px; height: 30px;"></td>

				<!-- /ko -->

			</tr>

			<!-- /ko -->
		<tbody>
	</table>

	<table id="dt6" class="tablestyle" class="table table-bordered">
		<thead class="thead-inverse">
			<tr class="question-row">
				<th colspan="4">Quinnox's Overall Engagament</th>
			</tr>
			<!-- <tr>
								<td></td>
								<td></td>
								<td ID='rotate'>What do we have to do at Quinnox to become your long-term partner of choice.</td>
								<td ID='rotate'>What do we have to improve upon at Quinnox to support you positively.</td>
								</tr> -->
			<tr class="question-row">
				<th>Name Of Respondent</th>
				<th></th>
				<th>Q.1</th>
				<th>Q.2</th>

			</tr>
		</thead>
		<tbody data-bind="foreach:user5">
			<!-- ko foreach: questionGroupDTOs -->
			<tr>
				<td class="highlight-td"
					data-bind="text:$parent.firstName+' '+$parent.lastName  "></td>
				<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
				<td class="data-center" data-bind="text:answerDTO.atext"></td>
				<!-- /ko -->
			</tr>
			<!-- /ko -->
			<!-- /ko -->
		<tbody>
	</table>

	</div>
	</form>

	</div>
	<script type="text/javascript">

function overAllAverageNps(data){
	
	var self = this;
	self.user1 = ko.observableArray([]);
	self.user1 = data;
	
}

function repondantTop3Benefits(data){
	var self = this;
	self.data = ko.observableArray([]);
	self.data = data;
}

function RespondantSurveyDetailsForQuestionSet1(dat){
	var self = this;
	self.user1 = ko.observableArray([]);
	self.user1 = dat;
}


function RespondantSurveyDetailsForQuestionSet2(dat){
	var self = this;
	self.user2 = ko.observableArray([]);
	self.user2 = dat;
}

function RespondantSurveyDetailsForQuestionSet3(dat){
	var self = this;
	self.user3 = ko.observableArray([]);
	self.user3 = dat;
}

function RespondantNps(dat){
	var self = this;
	self.user4 = ko.observableArray([]);
	self.user4 = dat;
}

function RespondantAnswersForQuestionSet4(dat){
		var self = this;
		self.user5 = ko.observableArray([]);
		self.user5 = dat;
}


function getParameterByName(name, url) {
	debugger;

    if (!url) {
      url = window.location.href;
    }
    debugger;
   
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}




		var respondantId = getParameterByName("respondantId");
		$.getJSON("/qcsat/proxy/api/respondanttop3benefits?respondantId="+respondantId,
				function(data) {
				var indexLocation = $(this).attr('id');
			var dm1 = new repondantTop3Benefits(data);
			ko.applyBindings(dm1, document.getElementById('dt1'));
			$('#dt1').show();
			
		});
		
		var questionGrpId = 1;
	  	$.getJSON("/qcsat/proxy/api/respondantAvgScoresForQuestionSet?respondantId="+respondantId+"&questionGrpId="+questionGrpId,
				function(data) {
			var dm2 = new RespondantSurveyDetailsForQuestionSet1(data);
			ko.applyBindings(dm2, document.getElementById('dt2'));
			$('#dt2').show();
			
		});
	  	
	  	var questionGrpId = 2;
  		$.getJSON("/qcsat/proxy/api/respondantAvgScoresForQuestionSet?respondantId="+respondantId+"&questionGrpId="+questionGrpId,
			function(data) {
		var indexLocation = $(this).attr('id');
		var dm3 = new RespondantSurveyDetailsForQuestionSet2(data);
		ko.applyBindings(dm3, document.getElementById('dt3'));
		$('#dt3').show();
		
	});
  		
  		var questionGrpId = 3;
  		$.getJSON("/qcsat/proxy/api/respondantAvgScoresForQuestionSet?respondantId="+respondantId+"&questionGrpId="+questionGrpId,
			function(data) {
		var indexLocation = $(this).attr('id');
		var dm4 = new RespondantSurveyDetailsForQuestionSet3(data);
		ko.applyBindings(dm4, document.getElementById('dt4'));
		$('#dt4').show();
		
	});
  		
  		$.getJSON("/qcsat/proxy/api/respondantNps?respondantId="+respondantId,
  				function(data) {
  			var indexLocation = $(this).attr('id');
  			var dm5 = new RespondantNps(data);
  			ko.applyBindings(dm5, document.getElementById('dt5'));
  			$('#dt5').show();
  			
  		});
  		
  		var questionGrpId = 4;
  	  	$.getJSON( "/qcsat/proxy/api/respondantAnswerForQuestionSet4?respondantId="+respondantId+"&questionGrpId="+questionGrpId,
  				function(data) {
  	  		
  			var indexLocation = $(this).attr('id');
  			var dm6 = new RespondantAnswersForQuestionSet4(data);
  			ko.applyBindings(dm6, document.getElementById('dt6'));
  			$('#dt6').show();
  			console.dir(data);
  			
  		});
  	 
  	  	$.getJSON("/qcsat/proxy/api/overAllAverageNpsForRespondant?respondantId="+respondantId,
  			function(data) {
  		
  			var indexLocation = $(this).attr('id');
  		var dmForAvg = new overAllAverageNps(data);
  		ko.applyBindings(dmForAvg, document.getElementById('doq'));
  		$('#doq').show();
  		
  	});
  	  	
  	  </script>
	<script type="text/javascript">
    $(function(){
        $('#printOut').click(function(e){
           /*  e.preventDefault(); */
            var w = window.open();
            var printOne = $('.container').html();
            var printTwo = $('.contentSection1').html();
            var printThree = $('.contentSection2').html();
            w.document.write('<html class="page-height"><head><title></title></head><body class="page-height"><h6>1 | CSAT 2017 Promoters</h6><hr />' + printThree ) + '</body></html>';
            w.document.write('<html><head><style>.break { page-break-before: always; }; .page-height{height: 100%};</style><title>Copy Printed</title></head><body><h6 class="break">2 | CSAT 2017 Promoters</h6><hr />' + printTwo ) + '</body></html>';
            w.document.write('<html><head><style>.break { page-break-before: always; }</style><title>Copy Printed</title></head><body><h6 class="break">3 | CSAT 2017 Promoters</h6><hr />' + printOne ) + '</body></html>';
            w.window.print();
            w.document.close();
            return false;
        });
    });
</script>