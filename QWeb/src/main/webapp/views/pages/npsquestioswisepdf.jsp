<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value='/qcsat/resources/css/custom_styles.css' />">
<link rel="stylesheet" type="text/css"
	href="/qcsat/resources/css/report_styles.css" />
<script src="<c:url value='/resources/js/jquery.min.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/bootstrap.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/knockout-3.4.1.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/knockout.mapping.js' />"></script>

<title>NPS Questionwise Report</title>
<style>
-webkit-transform
:
 
rotate
(-90deg);


-moz-transform
:rotate(-90deg)
;


filter
:progid
:DXImageTransform
.Microsoft
.BasicImage
(rotation=3);



@media screen {
	div.divFooter {
		display: none;
	}
}

@media print {
	div.divFooter {
		position: fixed;
		bottom: 0;
	}
}

@media all {
	.page-break {
		display: none;
	}
}

@media print {
	.page-break {
		display: block;
		page-break-before: always;
	}
}

@page {
	size: landscape;
}
</style>
</head>


<body>
	<div class="contentSection2">
		<img src="/qcsat/resources/img/clientsatisfaction.jpg" alt="Logo"
			class="img-clientsatisfaction">
	</div>

	<div class="voiletbackground">
		<p class="clientsurvey">2017 Client Satisfaction Survey</p>
	</div>

	<div class="page-break"></div>
	<div class="contentSection1" class="contentSectionAlign">
		<p>
		<p class="ConfidentialityNote">Confidentiality Note</p>
		<br>

		<p class="paragraph">
			This document contains confidential information developed by Quinnox,
			Inc. No part of this document may be reproduced, stored in retrieval
			form, adopted or transmitted in any form or by any means, electronic,
			mechanical, photographic, graphic, optic or otherwise, translated in
			any language or computer language, without prior written permission
			from Quinnox, Inc. <br />
			<br /> This document has been prepared in accordance with the
			accordance with the accepted techniques for software engineering at
			Quinnox, Inc. The representations and related information contained
			in the document reflect our best understanding on the subject of this
			document.<br />
			<br /> However, Quinnox makes no representation or warranties with
			respect to the contents hereof and shall not be responsible for any
			loss or damage caused to the user by the direct or indirect use of
			this document and the accompanying software package. Furthermore
			Quinnox reserves the right to alter, modify or otherwise change in
			any manner the content hereof, without the obligation to notify any
			person of such revision or changes. <br /> <br />
		</p>
		<p class="footerstyle">
			� 2014 Quinnox Inc., all rights reserved.<br /> All registered and
			trademarked names are owned by their respective owners.
		</p>


	</div>
	<!-- <div class="divFooter"><img src="/qcsat/resources/img/logo.png" alt="Logo" width="40px" height="20px"></div> -->
	<div class="page-break"></div>
	<br>
	<br>
	<br>
	<h2 class="titlestyle" id="shid">NPS Questionwise Report</h2>

	<table id="dt1" class="tablestyle" class="table table-bordered">
		<thead class="thead-inverse">
			<tr class="question-row">
				<th colspan="5">NPS Data Points</th>
			</tr>
			<tr class="question-row">
				<th></th>
				<th>How willing are you to recommand Quinnox to other business
					units</th>
				<th>How willing are you to recommand Quinnox to other
					companies</th>
				<th>How willing are you to buy new services from Quinnox</th>
				<th>How will are you to 'Beta test' with few offerings from
					quinnox in your organisation</th>
			</tr>
			<tr class="question-row">
				<th>Name Of Respondant</th>
				<th>Q.1</th>
				<th>Q.2</th>
				<th>Q.3</th>
				<th>Q.4</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="totalcount">Count Of Promoters</td>
				<td class="data-center" data-bind="text:user5.NeutralId1"></td>
				<td class="data-center" data-bind="text:user5.NeutralId2"></td>
				<td class="data-center" data-bind="text:user5.NeutralId3"></td>
				<td class="data-center" data-bind="text:user5.NeutralId4"></td>
			</tr>
			<tr>
				<td class="totalcount">Count Of Detractors</td>
				<td class="data-center" data-bind="text:user5.DetractorsId1"></td>
				<td class="data-center" data-bind="text:user5.DetractorsId2"></td>
				<td class="data-center" data-bind="text:user5.DetractorsId3"></td>
				<td class="data-center" data-bind="text:user5.DetractorsId4"></td>
			</tr>
			<tr>
				<td class="totalcount">Count Of Passive</td>
				<td class="data-center" data-bind="text:user5.PromotersId1"></td>
				<td class="data-center" data-bind="text:user5.PromotersId2"></td>
				<td class="data-center" data-bind="text:user5.PromotersId3"></td>
				<td class="data-center" data-bind="text:user5.PromotersId4"></td>
			</tr>
		<tbody>
	</table>
						
	<table id="dt4" class="tablestyle" class="table table-bordered">
		<thead class="thead-inverse">
			<tr class="question-row">
				<th class="question-row" colspan="7">NPS Data Points</th>
			</tr>
			<tr class="question-row">
				<th></th>
				<th></th>
				<th>How willing are you to recommend Quinnox to other business
					units</th>
				<th>How willing are you to recommend Quinnox to other companies</th>
				<th>How willing are you to buy new services from Quinnox</th>
				<th>How willing are you to 'Beta test' with few offerings from
					Quinnox in your organization</th>
				<th></th>
			</tr>
			<tr class="question-row">
				<th>Client Name</th>
				<th>Respondent Name</th>
				<th>Q.2</th>
				<th>Q.3</th>
				<th>Q.4</th>
				<th>Q.5</th>
				<th>NPS Type</th>
		</thead>
		<tbody data-bind="foreach:user">
			<!-- ko foreach: questionGroupDTOs -->

			<tr height="54px;">
				<td class="highlight-td" data-bind="text:$parent.clientName"></td>
				<td class="highlight-td"
					data-bind="text:$parent.firstName+' '+$parent.lastName "></td>
				<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
				<td class="data-center" data-bind="text:answerDTO.option.otext"></td>
				<!-- /ko -->
				<!-- ko if: $parent.npsType == 'detractor' -->
				<td class="data-center"><img
					src="/qcsat/resources/img/detract.jpg" alt="Logo"
					style="width: 34px; height: 34px;"></td>
				<!-- /ko -->
				<!-- ko if: $parent.npsType == 'promoter' -->
				<td class="data-center"><img
					src="/qcsat/resources/img/promoter.png" alt="Logo"
					style="width: 52px; height: 48px;"></td>
				<!-- /ko -->
				<!-- ko if: $parent.npsType == 'neutral' -->
				<td class="data-center"><img
					src="/qcsat/resources/img/neutral.jpg" alt="Logo"
					style="width: 35px; height: 30px;"></td>
				<!-- /ko -->

			</tr>
			<!-- /ko -->
		<tbody>
	</table>

	<script type="text/javascript">

		function npsQuestionWise(dat){
			
			var self = this;
			self.user5 = ko.observableArray([]);
			self.user5 = dat;
		}
		
		function npsForEachQuestion(data){
			var self = this;
			self.user = ko.observableArray([]);
			self.user = data;
		}
	
	  	$.getJSON("/qcsat/proxy/api/npsquestionwise",
				function(data) {
	  		
			var indexLocation = $(this).attr('id');
			var dm = new npsQuestionWise(data);
			ko.applyBindings(dm, document.getElementById('dt1'));
			$('#dt1').show();
			
		});
	  	
	  	$.getJSON("/qcsat/proxy/api/npsoverallreport",
				function(data) {
				var dm = new npsForEachQuestion(data);
			ko.applyBindings(dm, document.getElementById('dt4'));
			$('#dt4').show();
		
		});

   </script>

</body>
</html>
