<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<style>
@media print {
	#dt4 {
		transform: scale(0.85, .968) translate(-250px, 0px);
	}
	html{
		overflow: visible !important;
		-webkit-scrollbar{
		display:none; }
	}
}
</style>

<link href="<c:url value='/resources/css/custom_styles.css' />"
	rel="stylesheet">
<div style="margin-left:107px;margin-top: 15px;" class="dropdown">
<a style="color:white;font-size:14px;padding:5px;" href="${pageContext.request.contextPath}/home">Home</a>


  <button class="btn btn-link" style="color:white;font-size:14px;padding:5px;" type="button" data-toggle="dropdown" data-hover="dropdown">
  Reports <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
   <li><a href="${pageContext.request.contextPath}/overallaccountreport">Overall Quinnox Report</a></li>
    <li><a href="${pageContext.request.contextPath}/accountreports">Account Wise Report</a></li>
    <li><a href="${pageContext.request.contextPath}/emwisereport">Engagement Managers Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npspromotersreport">Overall NPS Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npspromoters">NPS Promoters Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npsdetractors">NPS Detractors Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npsneutral">NPS Passive Report</a></li>
    <li><a href="${pageContext.request.contextPath}/respondantwisereport">Repondents Report</a></li>
    <li><a href="${pageContext.request.contextPath}/npsquestionwise">NPS Question Report</a></li>
  </ul>
</div>


<div class="container"
	style="padding-left: 20px !important; padding-top: 81px;">
	<form id="msform">
		<div class="form-group">
			<div>
				<h3 id='shid'></h3>

			</div>
			<br clear="all" /> <br clear="all" />
			<div>
				
				<h2 id="shid"
					style="margin-top: -52px; text-align: center; margin-left: -10px; border-bottom: 1px solid #cccccc;color: black;">CSAT
					Account Report   </h2>
					<div id="view" style="display:none">
		<button type="button" class="btn btn-link"
						onclick="viewPDF()" style="margin-left: 1000px;margin-top: -37px;">View
						Report</button></div> 
					
				</div>

			<div class="modal-body" id="mb" style="margin-left: 328px;" >
				<form style="min-height: 120px !important;">
					<div class="form-group">
						<label for="exampleSelect1">Vertical</label> <select
							class="form-control" id="verticalSelectId1" style="width: 51%">
							<option value="0" selected>Please Select</option>

						</select>
					</div>
					<div class="form-group">
						<label for="exampleSelect1">Client</label> <select
							class="form-control" id="clientSelectId1" style="width: 51%">
							<option value="0">Please Select</option>
						</select>
					</div>

					<input type="hidden" id="csrf" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
					<button type="button" id="btn2Id" class="btn btn-primary"
						onclick="viewAccountReports()" style="margin-left: 275px;">View
						Report</button>
				</form>
			</div>
			
			<div class="previouspage" style="display:none; margin-left: 998px;"><button type="button" id="btn2Id" class="btn btn-primary"
						onclick="javascript:window.history.back()" style="margin-left: 275px;">
						Back</button></div>			
						
					<table class="table" id="fulltab"
				style="display: none; border: 1px solid #cccccc;">
				<tbody>
				<tr>
				
		
				
				
			<!-- 	
				<table id="dataTable" class="table"
							style="display: none; width: 35%; align: center; margin-left: 6px; margin-top: 10px;border-top:0px solid #cccccc!important">
							<thead class="thead-inverse">
							</thead>
							<tbody>
								<tr>
									<th style="color:black;">Account Name</th>
									
										<tbody>
											<tr>
												<td></span></td>
								</tr>
							</tbody>		
				</table> -->
				
				
			
			
				<!-- <span id="dataTable" class="table" style="display:none;height:41px;font-style:bold"><div style="background-color:#cccccc;height:41px;padding-top: 10px;padding-left: 386px;font-weight: bold;font-size: 18px;"></div></span> -->
						
						
						<!-- <div id = "accountName" style="color: black;">Account Name:<span data-bind="text :$data"/></div>
			 -->
						<table id="dt" class="table table-bordered" style="display: none; width: 35%; align: center;margin-left: 8px; margin-top: 10px;border-top:0px solid #cccccc!important;">
							<thead class="thead-inverse">
							</thead>
							<tbody>
								<tr>
									<tr>
								<tr>
								<div>
										<td><b>Overall Score</b></td>

							<td> 

								<span data-bind="if: user12.RagScore<4">
									<div class="col-sm-4"><img src="/qcsat/resources/img/red.jpg" width="25px" height="25px" style="margin-left: 25px;"></div>
								</span> 

								<span data-bind="if: user12.RagScore>=4 && user12.RagScore<=5">
									<div><img src="/qcsat/resources/img/yellow.jpg" width="25px" height="25px" style="margin-left: 25px;"></div>
								</span>

								 <span data-bind="if: user12.RagScore>5">
									<div><img src="/qcsat/resources/img/green.png" width="25px" height="25px" style="margin-left: 25px;"></div>
								</span>

							</td> 

							<td>
								<span><div style="margin-left: 25px;"data-bind="text:user12.RagScore"></div></span>
							</td>

					</div>
				</tr>

</tbody>

</table>
<table id="dt1" class="table table-bordered" style="display: none; width: 385px; align: center; margin-left: 6px; border: 1px solid #cccccc;">
										<tr>
							
										<thead class="thead-inverse">
							
								<tr>
									<th></th>
									<th style="background-color: #cccccc;">NPS Score</th>
									<th style="background-color: #cccccc;">Score(In %)</th>
								</tr>
								
							
										<thead>
							
										<tbody>
								<tr>
								<td style="background-color:#fb4e4e"></td>
								<td>Detractors</td>
									<td><span data-bind="text:data.Ditractors"></span></td>
								</tr>
								<tr>
								<td style="background-color:FFC200"></td>
								<td>Passive</td>
									<td><span data-bind="text:data.Neutral"></span></td>
								</tr>
								<tr>
								<td style="background-color:#89d089"></td>
								<td>Promoters</td>
									<td><span data-bind="text:data.Promoters"></span></td>
								</tr>
							</tbody>

						</table>
				

					
						<table id="dt2" class="table"
										style="display: none; width: 385px; align: center; margin-left: 707px; margin-top: -183px; border: 1px solid #cccccc;">
							<thead class="thead-inverse">
								<tr>
									<th style="background-color: #cccccc;">Top 3 Benefits</th>
								</tr>
							</thead>
							<tbody data-bind="foreach:data">
								<tr>
									<td><span data-bind="text : $data"></span></td>
								</tr>
							
										<tbody>
						
									</table>
			
					<table id="dt3" class="table table-bordered"
										style="display: none; border: 1px solid #cccccc; table-layout: fixed; ">
							<thead class="thead-inverse">
								<tr>
									<th style="background-color: #cccccc;" colspan="8">Quinnox
										People Performance</th>
								</tr>
								<tr>
								<td></td>
								<td>Understanding of your business</td>
								<td>Technical understanding of your environment</td>
								<td>Flexibility to accommodate <br>your requirements</th>
								<td>Responsiveness  and attitude</td>
								<td>Timeliness and clarity of communication</td>
								<td>Effectiveness of the Engagement Manager</td>
								<td class="text-center" ></td>
								</tr>
								<tr style="background-color: #f3f3f3;">
									<th class="text-center">Name Of Respondant</th>
									<th class="text-center">Q.1</th>
									<th class="text-center" >Q.2</th>
									<th class="text-center" >Q.3</th>
									<th class="text-center" >Q.4</th>
									<th class="text-center" >Q.5</th>
									<th class="text-center">Q.6</th>
									<th class="text-center">Average</th>
								</tr>
								
							</thead>
							<tbody data-bind="foreach:user1">
							<!-- ko foreach: questionGroupDTOs -->
								
								<tr>
								<td style="background-color:#f3f3f3;" data-bind="text:$parent.firstName+' '+$parent.lastName"></td>
								
								<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
									<td  class="text-center" data-bind="text:answerDTO.option.otextValue">
										
									</td>
								<!-- /ko -->
								<td  style="font-weight:bold;" class="text-center" data-bind="text:$parent.avgNps"></td>
								</tr>
								
								
								
								
							<!-- /ko -->
							
						
							
					<!-- ko if: $index() == $parent.user1.length-1 -->
					<tr>
						<td style="background-color:#f3f3f3; ;font-weight:bold;">Average</td>
									<td style="background-color:#f3f3f3;; font-weight:bold;" class="text-center" data-bind="text:questionavg1"></td>
									<td style="background-color:#f3f3f3;; font-weight:bold; "  class="text-center" data-bind="text:questionavg2"></td>
									<td style="background-color:#f3f3f3;; font-weight:bold;" class="text-center" data-bind="text:questionavg3"></td>
									<td style="background-color:#f3f3f3;; font-weight:bold;" class="text-center" data-bind="text:questionavg4"></td>
									<td style="background-color:#f3f3f3;; font-weight:bold;"  class="text-center" data-bind="text:questionavg5"></td>
									<td style="background-color:#f3f3f3;; font-weight:bold;"  class="text-center" data-bind="text:questionavg6"></td>
									<td style="background-color:#f3f3f3;; font-weight:bold;"  class="text-center" data-bind="text:avgOfEachUser"></td>
						</tr>
						<!-- /ko -->
						
				</tbody>
 								
						</table>
				

						<table id="dt4" class="table table-bordered"
										style="display: none; border: 1px solid #cccccc; table-layout: fixed;">
							<thead class="thead-inverse">
								<tr>
									<th style="background-color: #cccccc;" colspan="8">Quinnox's Service Delivery</th>
								</tr>
								<tr height="50">
								<td></td>
								<td class="text-left">Prompt fulfillment of resourcing</td>
								<td class="text-left">The delivery teams are have <br>adequate skills<br> for my requirements</td>
								<td class="text-left">The teams strives hard to Deliver Projects/ Services On Time</td>
								<td class="text-left">The team Delivers projects/ Services with High Quality</td>
								<td class="text-left">Prompt escalation of major issues and show stoppers</td>
								<td class="text-left">Governance reports and data driven decision making</td>
								<td></td>
								</tr>
								<tr style="background-color: #f3f3f3;">
									<th>Name Of Respondant</th>
									<th class="text-center" >Q.1</th>
									<th class="text-center">Q.2</th>
									<th class="text-center">Q.3</th>
									<th class="text-center" >Q.4</th>
									<th class="text-center">Q.5</th>
									<th class="text-center" >Q.6</th>
									<th class="text-center">Average</th>
								</tr>
							</thead>
							<tbody data-bind="foreach:user2">
							<!-- ko foreach: questionGroupDTOs -->
								
								<tr>
								<td style="background-color:#f3f3f3;" class="text-left" data-bind="text:$parent.firstName+' '+$parent.lastName  "></td>
								<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
									<td class="text-center" data-bind="text:answerDTO.option.otextValue">
										
									</td>
								<!-- /ko -->
								<td style="font-weight:bold;" class="text-center"  data-bind="text:$parent.avgNps"></td>	
								</tr>
								
							<!-- /ko -->
							
						
									<!-- ko if: $index() == $parent.user2.length-1 -->
					<tr>
						<td style="font-weight:bold;background-color:#f3f3f3;">Average</td>
									<td style="font-weight:bold; background-color:#f3f3f3;;" class="text-center" data-bind="text:questionavg1"></td>
									<td style="font-weight:bold; background-color:#f3f3f3;;" class="text-center"  data-bind="text:questionavg2"></td>
									<td style="font-weight:bold; background-color:#f3f3f3;;"  class="text-center" data-bind="text:questionavg3"></td>
									<td style="font-weight:bold; background-color:#f3f3f3;;" class="text-center" data-bind="text:questionavg4"></td>
									<td style="font-weight:bold;background-color:#f3f3f3;; " class="text-center" data-bind="text:questionavg5"></td>
									<td style="font-weight:bold;background-color:#f3f3f3;; " class="text-center" data-bind="text:questionavg6"></td>
									<td style="font-weight:bold;background-color:#f3f3f3;; " class="text-center"  data-bind="text:avgOfEachUser"></td>
									
						</tr>
						<!-- /ko -->
						</tbody>
						
						</table>
					

						<table id="dt5" class="table table-bordered"
										style="display: none; border: 1px solid #cccccc; width: 100%; table-layout: fixed; ">
							<thead class="thead-inverse">
								<tr>
									<th style="background-color: #cccccc;" colspan="7">Quinnox's Performance on RELATIONSHIP MANAGEMENT</th>
								</tr>
								<tr height="160">
								<td></td>
								<td class="text-left" width="100">The account manager is  knowledgeable and professional.</td>
								<td class="text-left" width="100">The account manageris making a positive contribution to my business.</td>
								<td class="text-left" width="100">The account manager responds to my inquiries in a timely manner.
 </td>
								<td class="text-left" width="100">The account manager works<br>towards achieving High Customer Satisfaction
 </td>
								<td class="text-left" width="100">Overall, I am very satisfied with the account manager.</td>
								<td colspan="1"></td>
								
								</tr>
								<tr style="background-color: #f3f3f3;">
									<th>Name Of Respondant</th>
									<th class="text-center" >Q.1</th>
									<th class="text-center" >Q.2</th>
									<th class="text-center" >Q.3</th>
									<th class="text-center" >Q.4</th>
									<th class="text-center" >Q.5</th>
									<th class="text-center" >Average</th>
								</tr>
							</thead>
							<tbody data-bind="foreach:user3">
							<!-- ko foreach: questionGroupDTOs -->
								
								<tr>
								<td style="background-color:#f3f3f3;" data-bind="text:$parent.firstName+' '+$parent.lastName  "></td>
								<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
									<td class="text-center" data-bind="text:answerDTO.option.otextValue">
										
									</td>
								<!-- /ko -->
								<td style="font-weight:bold" class="text-center" data-bind="text:$parent.avgNps"></td>	
								</tr>
								
							<!-- /ko -->
							
								<!-- ko if: $index() == $parent.user3.length-1 -->
					      <tr>
						<td style="font-weight:bold;background-color:#f3f3f3;">Average</td>
									<td style="font-weight:bold; background-color:#f3f3f3;;" class="text-center" data-bind="text:questionavg1"></td>
									<td style="font-weight:bold;background-color:#f3f3f3;;" class="text-center" data-bind="text:questionavg2"></td>
									<td style="font-weight:bold; background-color:#f3f3f3;;" class="text-center" data-bind="text:questionavg3"></td>
									<td style="font-weight:bold;background-color:#f3f3f3;;" class="text-center" data-bind="text:questionavg4"></td>
									<td style="font-weight:bold;background-color:#f3f3f3;; " class="text-center" data-bind="text:questionavg5"></td>
									<td style="font-weight:bold;background-color:#f3f3f3;; " class="text-center" data-bind="text:avgNps1"></td>
						</tr>
						<!-- /ko -->
						
						</tbody>	
						</table>
				
					
					<table id="dt8" class="table table-bordered"
										style="display: none; border: 1px solid #cccccc; width: 100%; table-layout: fixed">
							<thead class="thead-inverse">
								<tr>
									<th style="background-color: #cccccc;" colspan="4">Quinnox's Overall Engagament</th>
								</tr>
								<!-- <tr>
								<td></td>
								<td></td>
								<td ID='rotate'>What do we have to do at Quinnox to become your long-term partner of choice.</td>
								<td ID='rotate'>What do we have to improve upon at Quinnox to support you positively.</td>
								</tr> -->
								<tr style="background-color: #f3f3f3;">
									<th rowspan="2">Name Of Respondant</th>
									<th></th>
									<th class="text-center" >Q.1</th>
									<th class="text-center" >Q.2</th>
									
								</tr>
							</thead>
							<tbody data-bind="foreach:user4">
							<!-- ko foreach: questionGroupDTOs -->
								<tr>
								<td style="background-color:#f3f3f3;" data-bind="text:$parent.firstName+' '+$parent.lastName  "></td>
								<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
									<td class="text-center" data-bind="text:answerDTO.atext"></td>
								<!-- /ko -->
								</tr>
							<!-- /ko -->
						<!-- /ko -->
							
										</tbody>
						
									</table>
					
					
								<tr>
					<table id="dt9" class ="table table-bordered"
							style="display: none; border: 1px solid #cccccc; width: 100%; table-layout: fixed" >
							<thead class="thead-inverse">
								<tr>
									<th style="background-color: #cccccc;" colspan="6">NPS Data Points</th>
								</tr>
								<tr>
								<td></td>
								<td class="text-left" ID='rotate'>How willing are you to recommend Quinnox to other business units</td>
								<td  class="text-left" ID='rotate'>How willing are you to recommend Quinnox to other companies</td>
								<td class="text-left" ID='rotate'>How willing are you to buy new services from Quinnox</td>
								<td class="text-left" ID='rotate' >How willing are you to 'Beta test' with few offerings from quinnox in your organisation</td>
								<td style="padding-top: 72px;" align="center">NPS Type</td>
								</tr>
								<tr style="background-color: #f3f3f3;">
									<th>Name Of Respondant</th>
									<th class="text-center" >Q.1</th>
									<th class="text-center" >Q.2</th>
									<th class="text-center" >Q.3</th>
									<th class="text-center" >Q.4</th>
									
									<th class="text-center" ></th>
								</tr>
							</thead>
							<tbody data-bind="foreach:user5">
							<!-- ko foreach: questionGroupDTOs -->
								
								<tr>
								<td style="background-color:#f3f3f3;"  data-bind="text:$parent.firstName+' '+$parent.lastName "></td>
								<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
									<td align="center" data-bind="text:answerDTO.option.otext"></td>
								<!-- /ko -->
									<!-- ko if: $parent.npsType == 'detractor' -->
										<td align="center"><img src="/qcsat/resources/img/detract.jpg" alt="Logo" style="width: 34px;height: 34px;"></td>	
									<!-- /ko -->
									<!-- ko if: $parent.npsType == 'promoter' -->
										<td align="center"><img src="/qcsat/resources/img/promoter.png" alt="Logo" style="width: 52px;height: 48px;"></td>	
									<!-- /ko -->
									<!-- ko if: $parent.npsType == 'neutral' -->
										<td align="center"><img src="/qcsat/resources/img/neutral.jpg" alt="Logo" style="width: 35px;height: 30px;"></td>	
									<!-- /ko -->
									
								</tr>
							<!-- /ko -->
							
							<tbody>
						</table>
						</tr>
					
										
				</tbody>
			</table>

		</div>
	</form>


</div>

<p style="color: white;" align="center">&copy; 2017 Quinnox Inc.
			
					
			
<p>

	<script type="text/html" id="nps-data">
	
		<tr>
			<td><span data-bind="text:data"></span></td>
		</tr>
	

</script>

	
	<script type="text/javascript"
		src="<c:url value='/resources/js/knockout-models/accountreports.js' />"></script>

	<script type="text/javascript">
		var first = false;
		//var asm = new AllSurveysModel();
		//ko.applyBindings(asm, document.getElementById('pr'));
		// 	$('.btnCls').on('click', function() {
		// 		var indexLocation = $(this).attr('id');
		// 		var dm = new SurveyDetailModel(indexLocation);
		// 		ko.applyBindings(dm, document.getElementById('dt'));
		// 		$('#ptbl').hide();
		// 		$('#dt').show();
		// 	});
		$('.form_date').datetimepicker({
			language : 'fr',
			weekStart : 1,
			todayBtn : 1,
			autoclose : 1,
			todayHighlight : 1,
			startView : 2,
			minView : 2,
			forceParse : 0
		});

		function executeDetailJS() {
		}
	</script>