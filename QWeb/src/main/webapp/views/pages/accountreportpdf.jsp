<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link type="text/css"
	href="<c:url value='/qcsat/resources/css/custom_styles.css' />">
<link rel="stylesheet" type="text/css"
	href="/qcsat/resources/css/report_styles.css" />
<script src="<c:url value='/resources/js/jquery.min.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/bootstrap.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/knockout-3.4.1.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/knockout.mapping.js' />"></script>

<title>Account Report</title>

<style>
@media screen {
	div.divFooter {
		display: none;
	}
}

@media print {
	div.divFooter {
		position: fixed;
		bottom: 0;
	}
}

@media all {
	.page-break {
		display: none;
	}
}

@media print {
	.page-break {
		display: block;
		page-break-before: always;
	}
}

@page {
	size: landscape;
}
</style>
</head>

<body>
	<div class="contentSection2">
		<img src="/qcsat/resources/img/clientsatisfaction.jpg" alt="Logo"
			class="img-clientsatisfaction">
	</div>

	<div class="voiletbackground">
		<p class="clientsurvey">Client Satisfaction Survey</p>
	</div>

	<div class="page-break"></div>
	<div class="contentSection1" class="contentSectionAlign">
		<p>
		<p class="ConfidentialityNote">Confidentiality Note</p>

		<p class="paragraph">
			This document contains confidential information developed by Quinnox,
			Inc. No part of this document may be reproduced, stored in retrieval
			form, adopted or transmitted in any form or by any means, electronic,
			mechanical, photographic, graphic, optic or otherwise, translated in
			any language or computer language, without prior written permission
			from Quinnox, Inc. <br /> <br /> This document has been prepared in
			accordance with the accordance with the accepted techniques for
			software engineering at Quinnox, Inc. The representations and related
			information contained in the document reflect our best understanding
			on the subject of this document.<br /> <br /> However, Quinnox
			makes no representation or warranties with respect to the contents
			hereof and shall not be responsible for any loss or damage caused to
			the user by the direct or indirect use of this document and the
			accompanying software package. Furthermore Quinnox reserves the right
			to alter, modify or otherwise change in any manner the content
			hereof, without the obligation to notify any person of such revision
			or changes. <br /> <br />
		</p>
		<p class="footerstyle">
			� 2014 Quinnox Inc., all rights reserved.<br /> All registered and
			trademarked names are owned by their respective owners.
		</p>

	</div>


	<div class="page-break"></div>
	<br>
	<br>
	<br>
	<h2 class="titlestyle" id="shid">Qualcomm�Technologies�Inc.</h2>

	<div>
		<table id="dt" class="overall">

			<thead class="thead-inverse">
			</thead>
			<tbody>
				<tr>
					<td>OverAll Score</td>

					<td>
						<span data-bind="if: user12.RagScore<4">
								<div>
									<img src="/qcsat/resources/img/red.jpg" class="ragscoreimg">
								</div>
						</span> 
						<span data-bind="if: user12.RagScore>=4 && user12.RagScore<=5">
								<div>
									<img src="/qcsat/resources/img/yellow.jpg" class="ragscoreimg">
								</div>
						</span> 
						<span data-bind="if: user12.RagScore>5">
								<div>
									<img src="/qcsat/resources/img/green.png" class="ragscoreimg">
								</div>
						</span>
					</td>

					<td align="center">
						<span><div style="margin-left: 25px;" data-bind="text:user12.RagScore"></div></span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div>
		<table id="dt1" class="nps " class="table">
			<thead class="thead-inverse">
				<tr class="npstitle">
					<th></th>
					<th>NPS Score</th>
					<th>Score(In %)</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="npscolor-red"></td>
					<td class="npsborder">Detractors</td>
					<td class="data-center"><span data-bind="text:data.Ditractors"></span></td>
				</tr>
				<tr>
					<td class="npscolor-yellow"></td>
					<td class="npsborder">Passive</td>
					<td class="data-center"><span data-bind="text:data.Neutral"></span></td>
				</tr>
				<tr>
					<td class="npscolor-green"></td>
					<td class="npsborder">Promoters</td>
					<td class="data-center"><span data-bind="text:data.Promoters"></span></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div>
		<table id="dt2" class="top3" class="table">
			<thead class="thead-inverse">
				<tr class="npsborder">
					<th style="background-color: #cccccc;">Top 3 Benefits</th>
				</tr>
			</thead>
			<tbody data-bind="foreach:data">
				<tr class="npsborder">
					<td class="npsborder"><span data-bind="text : $data"></span></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div>
		<table id="dt3" class="tablestyle" class="table table-bordered">
			<thead class="thead-inverse">
				<tr class="question-row">
					<th colspan="8">Quinnox People Performance</th>
				</tr>
				<tr class="question-row">
					<th></th>
					<th>Understanding of your business</th>
					<th>Technical understanding of your environment</th>
					<th>Flexibility to accommodate your requirements</th>
					<th>Responsiveness and attitude</th>
					<th>Timeliness and clarity of communication</th>
					<th>Effectiveness of the Engagement Manager</th>
					<th>
					</td>
				</tr>
				<tr class="question-row">
					<th>Name Of Respondent</th>
					<th>Q.1</th>
					<th>Q.2</th>
					<th>Q.3</th>
					<th>Q.4</th>
					<th>Q.5</th>
					<th>Q.6</th>
					<th>Average</th>
				</tr>
			</thead>

			<tbody data-bind="foreach:user1">
				<!-- ko foreach: questionGroupDTOs -->
				<tr>
					<td class="highlight-td"
						data-bind="text:$parent.firstName+' '+$parent.lastName"></td>
					<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
					<td class="data-center"
						data-bind="text:answerDTO.option.otextValue"></td>
					<!-- /ko -->
					<td class="data-center" data-bind="text:$parent.avgNps"></td>
				</tr>
				<!-- /ko -->
				<!-- ko if: $index() == $parent.user1.length-1 -->
				<tr>
					<th class="highlight-td">Average</th>
					<th class="data-center" data-bind="text:questionavg1"></th>
					<th class="data-center" data-bind="text:questionavg2"></th>
					<th class="data-center" data-bind="text:questionavg3"></th>
					<th class="data-center" data-bind="text:questionavg4"></th>
					<th class="data-center" data-bind="text:questionavg5"></th>
					<th class="data-center" data-bind="text:questionavg6"></th>
					<th class="data-center" data-bind="text:avgOfEachUser"></th>
				</tr>
				<!-- /ko -->
			</tbody>
		</table>
	</div>

	<div>
		<table id="dt4" class="tablestyle" class="table table-bordered">
			<thead class="thead-inverse">
				<tr height="54px;">
					<th class="question-row" colspan="8">Quinnox's Service
						Delivery</th>
				</tr>
				<tr class="question-row">
					<th></th>
					<th>Prompt fulfillment of resourcing</th>
					<th>The delivery teams are have <br>adequate skills<br>
						for my requirements
					</th>
					<th>The teams strives hard to Deliver Projects/ Services On
						Time</th>
					<th>The team Delivers projects/ Services with High Quality</th>
					<th>Prompt escalation of major issues and show stoppers</th>
					<th>Governance reports and data driven decision making</th>
					<th></th>
				</tr>
				<tr class="question-row">
					<th>Name Of Respondent</th>
					<th>Q.1</th>
					<th>Q.2</th>
					<th>Q.3</th>
					<th>Q.4</th>
					<th>Q.5</th>
					<th>Q.6</th>
					<th>Average</th>
				</tr>
			</thead>
			<tbody data-bind="foreach:user2">
				<!-- ko foreach: questionGroupDTOs -->
				<tr>
					<td class="highlight-td"
						data-bind="text:$parent.firstName+' '+$parent.lastName  "></td>
					<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
					<td class="data-center"
						data-bind="text:answerDTO.option.otextValue"></td>
					<!-- /ko -->
					<td class="data-center" data-bind="text:$parent.avgNps"></td>
				</tr>
				<!-- /ko -->
				<!-- ko if: $index() == $parent.user2.length-1 -->
				<tr>
					<th class="highlight-td">Average
					</td>
					<th class="data-center" data-bind="text:questionavg1"></th>
					<th class="data-center" data-bind="text:questionavg2"></th>
					<th class="data-center" data-bind="text:questionavg3"></th>
					<th class="data-center" data-bind="text:questionavg4"></th>
					<th class="data-center" data-bind="text:questionavg5"></th>
					<th class="data-center" data-bind="text:questionavg6"></th>
					<th class="data-center" data-bind="text:avgOfEachUser"></th>
				</tr>
				<!-- /ko -->
			</tbody>
		</table>
	</div>

	<div>
		<table id="dt5" class="tablestyle" class="table table-bordered">
			<thead class="thead-inverse">
				<tr class="question-row">
					<th colspan="7">Quinnox's Performance on RELATIONSHIP
						MANAGEMENT</th>
				</tr>
				<tr class="question-row">
					<th></th>
					<th>The account manager is knowledgeable and professional.</th>
					<th>The account manageris making a positive contribution to my
						business.</th>
					<th>The account manager responds to my inquiries in a timely
						manner.</th>
					<th>The account manager works<br>towards achieving High
						Customer Satisfaction
					</th>
					<th>Overall, I am very satisfied with the account manager.</th>
					<th style="" colspan="1"></th>
				</tr>
				<tr class="question-row">
					<th>Name Of Respondent</th>
					<th>Q.1</th>
					<th>Q.2</th>
					<th>Q.3</th>
					<th>Q.4</th>
					<th>Q.5</th>
					<th>Average</th>
				</tr>
			</thead>
			<tbody data-bind="foreach:user3">
				<!-- ko foreach: questionGroupDTOs -->
				<tr>
					<td class="highlight-td"
						data-bind="text:$parent.firstName+' '+$parent.lastName  "></td>
					<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
					<td class="data-center"
						data-bind="text:answerDTO.option.otextValue"></td>
					<!-- /ko -->
					<td class="data-center" data-bind="text:$parent.avgNps"></td>
				</tr>
				<!-- /ko -->
				<!-- ko if: $index() == $parent.user3.length-1 -->
				<tr>
					<td class="highlight-td">Average</td>
					<th class="data-center" data-bind="text:questionavg1"></th>
					<th class="data-center" data-bind="text:questionavg2"></th>
					<th class="data-center" data-bind="text:questionavg3"></th>
					<th class="data-center" data-bind="text:questionavg4"></th>
					<th class="data-center" data-bind="text:questionavg5"></th>
					<th class="data-center" data-bind="text:avgNps1"></th>
				</tr>
				<!-- /ko -->
			</tbody>
		</table>
	</div>

	<div>
		<table id="dt8" class="tablestyle" class="table table-bordered">
			<thead class="thead-inverse">
				<tr class="question-row">
					<th colspan="4">Quinnox's Overall Engagament</th>
				</tr>
				<tr class="question-row">
					<th rowspan="2">Name Of Respondent</th>
					<th></th>
					<th>Q.1</th>
					<th>Q.2</th>
				</tr>
			</thead>
			<tbody data-bind="foreach:user4">
				<!-- ko foreach: questionGroupDTOs -->
				<tr height="54px;">
					<td class="highlight-td"
						data-bind="text:$parent.firstName+' '+$parent.lastName  "></td>
					<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
					<td class="data-center" data-bind="text:answerDTO.atext"></td>
					<!-- /ko -->
				</tr>
				<!-- /ko -->
				<!-- /ko -->
			<tbody>
		</table>
	</div>

	<div>
		<table id="dt9" class="tablestyle" class="table table-bordered">
			<thead class="thead-inverse">
				<tr class="question-row">
					<th colspan="6">NPS Data Points</th>
				</tr>
				<tr class="question-row">
					<th>
					</td>
					<th>How willing are you to recommend Quinnox to other business
						units</th>
					<th>How willing are you to recommend Quinnox to other
						companies</th>
					<th>How willing are you to buy new services from Quinnox</th>
					<th>How willing are you to 'Beta test' with few offerings from
						quinnox in your organisation</th>
					<th></th>
				</tr>
				<tr class="question-row">
					<th>Name Of Respondent</th>
					<th>Q.1</th>
					<th>Q.2</th>
					<th>Q.3</th>
					<th>Q.4</th>
					<th>NPS Type</th>
				</tr>
			</thead>
			<tbody data-bind="foreach:user5">
				<!-- ko foreach: questionGroupDTOs -->
				<tr>
					<td class="highlight-td"
						data-bind="text:$parent.firstName+' '+$parent.lastName "></td>
					<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
					<td class="data-center" data-bind="text:answerDTO.option.otext"></td>
					<!-- /ko -->
					<!-- ko if: $parent.npsType == 'detractor' -->
					<td class="data-center"><img
						src="/qcsat/resources/img/detract.jpg" alt="Logo"
						style="width: 34px; height: 34px;"></td>
					<!-- /ko -->
					<!-- ko if: $parent.npsType == 'promoter' -->
					<td class="data-center"><img
						src="/qcsat/resources/img/promoter.png" alt="Logo"
						style="width: 52px; height: 48px;"></td>
					<!-- /ko -->
					<!-- ko if: $parent.npsType == 'neutral' -->
					<td class="data-center"><img
						src="/qcsat/resources/img/neutral.jpg" alt="Logo"
						style="width: 35px; height: 30px;"></td>
					<!-- /ko -->
				</tr>
				<!-- /ko -->
			<tbody>
		</table>
	</div>

		<script type="text/javascript">
				var svsData;
				function NPSScore(data) {
					var self = this;
					var svs = svsData;
					self.data = data;
		
				}
		
				function overallAccountScore(data) {
					var self = this;
					self.user12 = ko.observableArray([]);
					self.user12 = data;
		
				}
		
				var svsDat;
				function NPSScoreQuestionWise(data) {
					var self = this;
					var svs = svsDat;
					self.data = data;
				}
		
				function SurveyDetailsForTopBenefits(data) {
					var self = this;
					self.data = ko.observableArray([]);
					self.data = data;
				}
		
				function SurveyDetailsForQuestionSet1(dat) {
					var self = this;
					self.user1 = ko.observableArray([]);
					self.user1 = dat;
				}
		
				function SurveyDetailsForQuestionSet2(dat) {
					var self = this;
					self.user2 = ko.observableArray([]);
					self.user2 = dat;
				}
		
				function SurveyDetailsForQuestionSet3(dat) {
					var self = this;
					self.user3 = ko.observableArray([]);
					self.user3 = dat;
				}
		
				function SurveyDetailsForQuestionSet4(dat) {
					var self = this;
					self.user4 = ko.observableArray([]);
					self.user4 = dat;
				}
				function SurveyDetailsForQuestionSet5(dat) {
					var self = this;
					self.user5 = ko.observableArray([]);
					self.user5 = dat;
				}
		
				function clientNameForId(data) {
					var self = this;
					var svs = clientName;
					self.svs = data;
				}
		
				function getParameterByName(name, url) {
					if (!url) {
						url = window.location.href;
					}
					name = name.replace(/[\[\]]/g, "\\$&");
					var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex
							.exec(url);
					if (!results)
						return null;
					if (!results[2])
						return '';
					return decodeURIComponent(results[2].replace(/\+/g, " "));
				}
		
				var clientid = getParameterByName("clientId");
				$.getJSON("/qcsat/proxy/api/accountreports?clientId=" + clientid,
						function(data) {
							var indexLocation = $(this).attr('id');
							var dm1 = new NPSScore(data);
							ko.applyBindings(dm1, document.getElementById('dt1'));
		
							$('#dt1').show();
						});
		
				$.getJSON("/qcsat/proxy/api/accountnpsreports?clientId=" + clientid,
						function(data) {
							var indexLocation = $(this).attr('id');
							var dm = new overallAccountScore(data);
							ko.applyBindings(dm, document.getElementById('dt'));
							$('#dt').show();
						});
		
				$.getJSON("/qcsat/proxy/api/accounttop3benefits?clientId=" + clientid,
						function(data) {
							var indexLocation = $(this).attr('id');
							var dm2 = new SurveyDetailsForTopBenefits(data);
							ko.applyBindings(dm2, document.getElementById('dt2'));
							$('#dt2').show();
						});
		
				var questionGrpId = 1;
				$.getJSON("/qcsat/proxy/api/avgscoresquestionset?clientId=" + clientid
						+ "&questionGrpId=" + questionGrpId, function(data) {
					var dm3 = new SurveyDetailsForQuestionSet1(data);
					debugger;
					ko.applyBindings(dm3, document.getElementById('dt3'));
					$('#dt3').show();
		
				});
		
				var questionGrpId = 2;
				$.getJSON("/qcsat/proxy/api/avgscoresquestionset?clientId=" + clientid
						+ "&questionGrpId=" + questionGrpId, function(data) {
					var indexLocation = $(this).attr('id');
					var dm4 = new SurveyDetailsForQuestionSet2(data);
					ko.applyBindings(dm4, document.getElementById('dt4'));
					$('#dt4').show();
				});
		
				var questionGrpId = 3;
				$.getJSON("/qcsat/proxy/api/avgscoresquestionset?clientId=" + clientid
						+ "&questionGrpId=" + questionGrpId, function(data) {
					var indexLocation = $(this).attr('id');
					var dm5 = new SurveyDetailsForQuestionSet3(data);
					ko.applyBindings(dm5, document.getElementById('dt5'));
					$('#dt5').show();
				});
		
				var questionGrpId = 4;
				$.getJSON("/qcsat/proxy/api/avgscoresquestionset4?clientId=" + clientid
						+ "&questionGrpId=" + questionGrpId, function(data) {
					var indexLocation = $(this).attr('id');
					var dm8 = new SurveyDetailsForQuestionSet4(data);
					ko.applyBindings(dm8, document.getElementById('dt8'));
					$('#dt8').show();
				});
		
				$.getJSON("/qcsat/proxy/api/accountNps?clientId=" + clientid, function(
						data) {
					var indexLocation = $(this).attr('id');
					var dm9 = new SurveyDetailsForQuestionSet5(data);
					ko.applyBindings(dm9, document.getElementById('dt9'));
					$('#dt9').show();
				});
		
				$.getJSON("/qcsat/proxy/api/clientName?clientId=" + clientid, function(
						data) {
		
					var indexLocation = $(this).attr('id');
					var dmC = new clientNameForId(data);
					ko.applyBindings(dmC, document.getElementById('dtC'));
					$('#dtC').show();
				});
		</script>

		<script type="text/javascript">
				$(function() {
					$('#printOut')
							.click(
									function(e) {
										/*  e.preventDefault(); */
										var w = window.open();
										var printOne = $('.container').html();
										var printTwo = $('.contentSection1').html();
										var printThree = $('.contentSection2').html();
										w.document
												.write('<html class="page-height"><head><title></title></head><body class="page-height"><h6>1 | CSAT 2017 Promoters</h6><hr />'
														+ printThree)
												+ '</body></html>';
										w.document
												.write('<html><head><style>.break { page-break-before: always; }; .page-height{height: 100%};</style><title>Copy Printed</title></head><body><h6 class="break">2 | CSAT 2017 Promoters</h6><hr />'
														+ printTwo)
												+ '</body></html>';
										w.document
												.write('<html><head><style>.break { page-break-before: always; }</style><title>Copy Printed</title></head><body><h6 class="break">3 | CSAT 2017 Promoters</h6><hr />'
														+ printOne)
												+ '</body></html>';
										w.window.print();
										w.document.close();
										return false;
									});
				});
		</script>

</body>
</html>
