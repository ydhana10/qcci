<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value='/qcsat/resources/css/custom_styles.css' />">
<link rel="stylesheet" type="text/css"
	href="/qcsat/resources/css/report_styles.css" />
<script src="<c:url value='/resources/js/jquery.min.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/bootstrap.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/knockout-3.4.1.js' />"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/knockout.mapping.js' />"></script>

<title>Passive Report</title>
<style>
-webkit-transform
:
 
rotate
(-90deg);


-moz-transform
:rotate(-90deg)
;


filter
:progid
:DXImageTransform
.Microsoft
.BasicImage
(rotation=3);



@media screen {
	div.divFooter {
		display: none;
	}
}

@media print {
	div.divFooter {
		position: fixed;
		bottom: 0;
	}
}

@media all {
	.page-break {
		display: none;
	}
}

@media print {
	.page-break {
		display: block;
		page-break-before: always;
	}
}

@page {
	size: landscape;
}
</style>
</head>

<body>
	<div class="contentSection2">
		<img src="/qcsat/resources/img/clientsatisfaction.jpg"
			class="img-clientsatisfaction">
	</div>
	<div class="voiletbackground">
		<p class="clientsurvey">2017 Client Satisfaction Survey</p>
	</div>
	<div class="page-break"></div>
	<div class="contentSection1" class="contentSectionAlign">
		<p>
		<p class="ConfidentialityNote">Confidentiality Note</p>
		<br>
		<p class="paragraph">
			This document contains confidential information developed by Quinnox,
			Inc. No part of this document may be reproduced, stored in retrieval
			form, adopted or transmitted in any form or by any means, electronic,
			mechanical, photographic, graphic, optic or otherwise, translated in
			any language or computer language, without prior written permission
			from Quinnox, Inc. <br />
			<br /> This document has been prepared in accordance with the
			accordance with the accepted techniques for software engineering at
			Quinnox, Inc. The representations and related information contained
			in the document reflect our best understanding on the subject of this
			document.<br />
			<br /> However, Quinnox makes no representation or warranties with
			respect to the contents hereof and shall not be responsible for any
			loss or damage caused to the user by the direct or indirect use of
			this document and the accompanying software package. Furthermore
			Quinnox reserves the right to alter, modify or otherwise change in
			any manner the content hereof, without the obligation to notify any
			person of such revision or changes. <br /> <br />
		</p>
		<p class="footerstyle">
			� 2014 Quinnox Inc., all rights reserved.<br /> All registered and
			trademarked names are owned by their respective owners.
		</p>
	</div>

	<div class="page-break"></div>
	<br>
	<br>
	<br>
	<h2 class="titlestyle" id="shid">CSAT Quinnox Passive</h2>

	<table class="tablestyle" id="dt4" class="table table-bordered">
		<thead class="thead-inverse">
			<tr>
				<th class="question-row" colspan="7">NPS Passive</th>
			</tr>
			<tr class="question-row">
				<th></th>
				<th></th>
				<th>How willing are you to recommand Quinnox to other business
					units</th>
				<th>How willing are you to recommand Quinnox to other
					companies</th>
				<th>How willing are you to buy new services from Quinnox</th>
				<th>How will are you to 'Beta test' with few offerings from
					quinnox in your organisation</th>
				<th></th>
			</tr>
			<tr class="question-row">
				<th>Client Name</th>
				<th>Name Of Respondent</th>
				<th>Q.1</th>
				<th>Q.2</th>
				<th>Q.3</th>
				<th>Q.4</th>
				<th>Is Passive</th>
			</tr>
		</thead>
		<tbody data-bind="foreach:user2">
			<!-- ko foreach: questionGroupDTOs -->
			<!-- ko if : $parent.npsType == 'neutral'  -->

			<tr>
				<td class="highlight-td" data-bind="text:$parent.clientName"></td>
				<td class="highlight-td"
					data-bind="text:$parent.firstName+' '+$parent.lastName "></td>
				<!-- ko foreach: questions.sort(function (l, r) { return l.qid > r.qid ? 1 : -1 }) -->
				<td class="data-center" data-bind="text:answerDTO.option.otext">

				</td>
				<!-- /ko -->
				<td class="data-center"><img
					src="/qcsat/resources/img/neutral.jpg" alt="Logo"
					style="width: 38px; height: 34px;"></td>
			</tr>
			<!-- /ko -->
			<!-- /ko -->
		<tbody>
	</table>


	<script type="text/javascript">
		function npsDetractorsList(dat){
			var self = this;
			self.user2 = ko.observableArray([]);
			self.user2 = dat;
		}
		
		$('#dataTable').show();
		$.getJSON("/qcsat/proxy/api/npsdetractorslist",
				function(data) {
			var indexLocation = $(this).attr('id');
			var dm3 = new npsDetractorsList(data);
			ko.applyBindings(dm3, document.getElementById('dt4'));
			$('#dt4').show();
			
		});
   </script>

	<script type="text/javascript">
	    $(function(){
	        $('#printOut').click(function(e){
	           /*  e.preventDefault(); */
	            var w = window.open();
	            var printOne = $('.container').html();
	            var printTwo = $('.contentSection1').html();
	            var printThree = $('.contentSection2').html();
	            w.document.write('<html class="page-height"><head><title></title></head><body class="page-height"><h6>1 | CSAT 2017 Neutral</h6><hr />' + printThree ) + '</body></html>';
	            w.document.write('<html><head><style>.break { page-break-before: always; }; .page-height{height: 100%};</style><title>Copy Printed</title></head><body><h6 class="break">2 | CSAT 2017 Neutral</h6><hr />' + printTwo ) + '</body></html>';
	            w.document.write('<html><head><style>.break { page-break-before: always; }</style><title>Copy Printed</title></head><body><h6 class="break">3 | CSAT 2017 Neutral</h6><hr />' + printOne ) + '</body></html>';
	            w.window.print();
	            w.document.close();
	            return false;
	        });
	    });
   </script>

</body>
</html>