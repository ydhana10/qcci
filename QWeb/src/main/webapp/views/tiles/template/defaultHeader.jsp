<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="authz"
	uri="http://www.springframework.org/security/tags"%>
<div class="text-center header_bg">	
    	 <div class="container">
         	<img src="<c:url value='/resources/img/logo.png' />" alt="Logo" />
         	
         	<authz:authorize ifAllGranted="ADMIN">
         	<a style="float:right;right:0;font-size:14px;font-weight: bold;" href="javascript:lg();"> Logout</a>
         	</authz:authorize>
         	<form:form id="logoutForm" style="display:none;" action="${pageContext.request.contextPath}/j_spring_security_logout" method="POST">
			    <input type="submit" value="Logout" />
			</form:form>
         </div>
    
    </div>
    
    <script type="text/javascript">
    function lg() {
        document.getElementById("logoutForm").submit();
    }
    </script>