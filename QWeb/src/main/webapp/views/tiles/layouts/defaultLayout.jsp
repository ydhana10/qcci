<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html>

 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Feedback form</title>
    <link href="<c:url value='/resources/css/bootstrap.css' />" rel="stylesheet">
    <link href="<c:url value='/resources/css/app.css' />" rel="stylesheet">
    <link href="<c:url value='/resources/css/bootstrap-datetimepicker.min.css' />" rel="stylesheet">
    <script type="text/javascript" src="<c:url value='/resources/js/jquery.min.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/resources/js/respond.min.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/resources/js/bootstrap.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/resources/js/knockout-3.4.1.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/resources/js/knockout.mapping.js' />" ></script>
    
    <script type="text/javascript" src="<c:url value='/resources/js/base64.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/resources/js/bootstrap-datetimepicker.min.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/resources/js/bootstrap-confirmation.min.js' />" ></script>    
    
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/dataTables.bootstrap.min.css' />"/>
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/responsive.bootstrap.min.css' />"/>
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/scroller.bootstrap.min.css' />"/>
<%-- 	 <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/print.min.css' />"/> --%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.dataTables.min.js' />"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/dataTables.bootstrap.min.js' />"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/dataTables.responsive.min.js' />"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/responsive.bootstrap.min.js' />"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/dataTables.scroller.min.js' />"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/loadingoverlay.min.js' />"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/loadingoverlay_progress.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/util.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/resources/js/jspdf.min.js' />" ></script>
    <script type="text/javascript" src="<c:url value='/resources/js/html2canvas.min.js' />" ></script>
    <!-- <script type="text/javascript" src="//cdn.rawgit.com/MrRio/jsPDF/master/dist/jspdf.min.js"></script>  -->
<!-- <script type="text/javascript" src="//cdn.rawgit.com/niklasvh/html2canvas/0.5.0-alpha2/dist/html2canvas.min.js"></script> -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->    
  </head>
 
<body>
		<header id="header">
			<tiles:insertAttribute name="header" />
		</header>
	
		<%-- <section id="sidemenu">
			<tiles:insertAttribute name="menu" />
		</section> --%>
		<div id="ovl">	
		<section id="site-content" style="left:0 !important;">
			<tiles:insertAttribute name="body" />
		</section>
		</div>
<!-- 		<footer id="footer"> -->
<%-- 			<tiles:insertAttribute name="footer" /> --%>
<!-- 		</footer> -->
</body>
</html>