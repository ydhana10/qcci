package com.qweb.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;



public class RESTUtil {

	private static final Logger LOG = LoggerFactory.getLogger(RESTUtil.class);

	public static String qWebWSBaseUrl;
	
	public static String qWebAppBaseUrl;
	
	public static String clientId;
	
	public static String clientSecret;
	
	public static String grantType;

	private RestTemplate restTemplate;

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public static void setqWebWSBaseUrl(String qWebWSBaseUrl) {
		RESTUtil.qWebWSBaseUrl = qWebWSBaseUrl;
	}

	public static void setqWebAppBaseUrl(String qWebAppBaseUrl) {
		RESTUtil.qWebAppBaseUrl = qWebAppBaseUrl;
	}
	
	
	public static void setGrantType(String grantType) {
		RESTUtil.grantType = grantType;
	}

	public static void setClientId(String clientId) {
		RESTUtil.clientId = clientId;
	}

	public static void setClientSecret(String clientSecret) {
		RESTUtil.clientSecret = clientSecret;
	}

	public Object getData(String contextPath, Class<?> clazz) throws Exception {

		String qWebWSURL;

		qWebWSURL = qWebWSBaseUrl + contextPath;

		//LOG.debug("qWebWSURL: " + qWebWSURL);

		Object response;

		response = restTemplate.getForObject(qWebWSURL, clazz);

		return response;
	}

	
	
	public <T> T postData(Object postRequest, String contextPath, Class<T> clazz)
			throws Exception {

		String qWebURL;
		qWebURL = qWebWSBaseUrl + contextPath;
		//LOG.debug("URL: " + qWebURL);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(postRequest,
				headers);
		ResponseEntity<T> responseEntity = restTemplate.exchange(qWebURL,
				HttpMethod.POST, requestEntity, clazz);

		responseEntity.getBody();
		return responseEntity.getBody();
	}
}
