package com.qweb.security;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.qweb.dto.CustomUserBean;
import com.qweb.dto.UserDTO;
import com.qweb.util.RESTUtil;



/**
 * @author Syed Usman The Class TransformAuthenticationProvider.
 */
@SuppressWarnings("all")
class QWebAuthenticationProvider implements AuthenticationProvider {

	/** The login service. */
	

	/** The logger. */
	private static Logger logger = Logger
			.getLogger(QWebAuthenticationProvider.class);
	
	
	private RESTUtil restUtil;
	

	public RESTUtil getRestUtil() {
		return restUtil;
	}

	public void setRestUtil(RESTUtil restUtil) {
		this.restUtil = restUtil;
	}
	
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.authentication.AuthenticationProvider#
	 * authenticate(org.springframework.security.core.Authentication)
	 */
	public CustomAuthenticationToken authenticate(Authentication authentication)
			throws AuthenticationException {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		String userName = authentication.getName().trim();
		String password = authentication.getCredentials().toString().trim();
		CustomAuthenticationToken auth = null;
		UserDTO userDTO = new UserDTO();
		userDTO.setSsoId(userName);
		userDTO.setPassword(password);
		UserDTO userDetails = null;
		
		try {
			if(attr.getRequest().getParameter("j_sid")!=null && !((attr.getRequest().getParameter("j_sid").trim()).isEmpty())){
				userDetails = restUtil.postData(userDTO,"/l/auth?surveyId="+attr.getRequest().getParameter("j_sid"),UserDTO.class);
				userDetails.setSurveyId(Integer.parseInt(attr.getRequest().getParameter("j_sid").trim()+""));
				userDetails.setSumId(Integer.parseInt(attr.getRequest().getParameter("j_sid").trim()+""));
			}else{
				userDetails = restUtil.postData(userDTO,"/l/auth",UserDTO.class);
			}
			
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(
					(userDetails.getRole()).trim());
			authorities.add(0, grantedAuthority);
			CustomUserBean appUser = new CustomUserBean();
			appUser.setUserDTO(userDetails);
			String surveyId = "1";
			String url = "/oauth/token?grant_type="+restUtil.grantType+"&client_id="+restUtil.clientId+"&client_secret="+restUtil.clientSecret+"&username="+userName+"&password="+password;
			if(attr.getRequest().getParameter("j_sid")!=null && !((attr.getRequest().getParameter("j_sid")).isEmpty())){
				surveyId = attr.getRequest().getParameter("j_sid").trim();
				url+="&surveyId="+surveyId;
			}
			
			String json = (String)restUtil.getData(url, String.class);		
			JSONObject jsonObject = new JSONObject(json);
			appUser.setAccessToken(jsonObject.get("value").toString());
			JSONObject _r_token = (JSONObject)jsonObject.get("refreshToken");
			appUser.setRefreshToken(_r_token.get("value").toString());
			if(userDetails!=null){
				auth = new CustomAuthenticationToken(userName, password, authorities, appUser);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return auth;
	}

	/**
	 * Supports.
	 *
	 * @param authentication
	 *            the authentication
	 * @return true, if successful
	 */
	@Override
	public boolean supports(Class<? extends Object> authentication) {
		return (UsernamePasswordAuthenticationToken.class
				.isAssignableFrom(authentication));
	}

}