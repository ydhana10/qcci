package com.qweb.security;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import com.qweb.dto.CustomUserBean;

public class CustomAuthenticationToken extends UsernamePasswordAuthenticationToken {

    private static final long serialVersionUID = 7425814465946838862L;
    private CustomUserBean customUserBean;

    public CustomAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, CustomUserBean customUserBean){

        super(principal, credentials, authorities);
        this.customUserBean = customUserBean;   
    }

	public CustomUserBean getCustomUserBean() {
		return customUserBean;
	}

	public void setCustomUserBean(CustomUserBean customUserBean) {
		this.customUserBean = customUserBean;
	}

    
}
