package com.qweb.proxy.controller;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.qweb.controller.BaseController;
import com.qweb.dto.CustomUserBean;
import com.qweb.dto.FileBucket;
import com.qweb.dto.SurveyDTO;
import com.qweb.dto.UserDTO;
import com.qweb.security.CustomAuthenticationToken;

@Controller
@RequestMapping("/proxy/api")
public class ProxyController extends BaseController{
	
	@RequestMapping(value = "/verticals", method = RequestMethod.GET)
	@ResponseBody
	public String list(Authentication authentication) throws Exception {
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
		String json = (String)restUtil.getData("/verticals?access_token="+user.getAccessToken(), String.class);
		return json;

	}
	
	
	@RequestMapping(value = "/respondants", method = RequestMethod.GET)
	@ResponseBody
	public String listOfRespondants(Authentication authentication,@RequestParam("clientId") Integer clientId) throws Exception {
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
		String json = (String)restUtil.getData("/respondants?access_token="+user.getAccessToken()+"&clientId="+clientId, String.class);
		System.out.println("JSON IN PROXY CONTROLLER: "+json);
		return json;
	}
	
	@RequestMapping(value = "/clients", method = RequestMethod.GET)
	@ResponseBody
	public String listClients(@RequestParam(value="verticalId")Integer verticalId,Authentication authentication) throws Exception {
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
		String json = (String)restUtil.getData("/clients?access_token="+user.getAccessToken()+"&verticalId="+verticalId, String.class);
		return json;

	}
	@RequestMapping(value = "/engagementmanager", method = RequestMethod.GET)
	@ResponseBody
	public String listEngagementManager(@RequestParam(value="verticalId")Integer verticalId,Authentication authentication) throws Exception {
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
		String json = (String)restUtil.getData("/engagementmanager?access_token="+user.getAccessToken()+"&verticalId="+verticalId, String.class);
		return json;

	}
	
	@RequestMapping(value = "/projects", method = RequestMethod.GET)
	@ResponseBody
	public String listProjects(@RequestParam(value="clientId")Integer clientId,Authentication authentication) throws Exception {
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
		String json = (String)restUtil.getData("/projects?access_token="+user.getAccessToken()+"&clientId="+clientId, String.class);
		return json;

	}
	
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	@ResponseBody
	public Boolean insertUser(@RequestBody UserDTO userDTO,Authentication authentication) throws Exception {
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
		Boolean success = (Boolean)restUtil.postData(userDTO,"/user?access_token="+user.getAccessToken(), Boolean.class);
		return success;
	}
	
	
	@RequestMapping(value = "/surveys", method = RequestMethod.GET)
	@ResponseBody
	public String list(@RequestParam(value="userId",required=false)String userId,@RequestParam(value="sumId")Integer sumId,
			@RequestParam(value="status", required=false)String status,Authentication authentication) throws Exception {
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        
		String json = (String)restUtil.getData("/surveys?access_token="+user.getAccessToken()+"&userId="+user.getUserDTO().getSsoId()+"&status="+status+"&sumId="+sumId, String.class);
		return json;

	}
	
	@RequestMapping(value = "/allsurveys", method = RequestMethod.GET)
	@ResponseBody
	public String listSurveys(Authentication authentication) throws Exception {
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
		String json = (String)restUtil.getData("/allsurveys?access_token="+user.getAccessToken(), String.class);
		return json;

	}
	
	@RequestMapping(value = "/qgs", method = RequestMethod.GET)
	@ResponseBody
	public String listQgs(Authentication authentication,@RequestParam("sumId") Integer sumId) throws Exception {
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
		String json = (String)restUtil.getData("/qgs?access_token="+user.getAccessToken()+"&sumId="+sumId, String.class);
		return json;

	}
	
	
	@RequestMapping(value = "/survey", method = RequestMethod.POST)
	@ResponseBody
	public Boolean postSurvey(@RequestBody SurveyDTO surveyDTO,Authentication authentication) throws Exception {
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
		Boolean success = (Boolean)restUtil.postData(surveyDTO,"/survey?access_token="+user.getAccessToken(), Boolean.class);
		return success;
	}
	
	@RequestMapping(value="/file/upload", method = RequestMethod.POST)
    public @ResponseBody String singleFileUpload(MultipartHttpServletRequest request,
			@RequestParam("filename") String filename,@RequestParam("surveyId") Integer surveyId,Authentication authentication) throws Exception {
            System.out.println("Fetching file");
            FileBucket fileBucket = new FileBucket();
            fileBucket.setFile(request.getFile("file"));
            CustomUserBean user = null;
            if(authentication instanceof CustomAuthenticationToken){
                user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
            }
            MultipartFile multipartFile = request.getFile("file");
            String fnvd = multipartFile.getOriginalFilename();
            String extension = FilenameUtils.getExtension(fnvd);
            String assignedName = filename;
            String assExt = FilenameUtils.getExtension(assignedName);
            if(!assExt.equalsIgnoreCase("xlsx") && !assExt.equalsIgnoreCase("xls")){
            	return "Failure, You can upload only Excel Files";
            }
            if(!extension.equalsIgnoreCase("xlsx") && !extension.equalsIgnoreCase("xls")){
            	return "Failure, You can upload only Excel Files";
            }
            Long size = multipartFile.getSize();
            String contentType = multipartFile.getContentType();
            InputStream stream = multipartFile.getInputStream();
            byte[] bytes = IOUtils.toByteArray(stream);
            UserDTO userDTO = user.getUserDTO();
            userDTO.setBase64(new String(Base64Utils.encode(bytes)));
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("fileoriginalsize", size);
            map.put("contenttype", contentType);
            //map.put("base64", );
            map.put("userDTO", user.getUserDTO());
            userDTO.setSurveyId(surveyId);
            //FileCopyUtils.copy(fileBucket.getFile().getBytes(), new File("F:\\AI\\" + fileBucket.getFileName()));          
            String success = (String)restUtil.postData(userDTO,"/file/upload?access_token="+user.getAccessToken()+"&filename="+filename, String.class);
            return success;
    }
	
	@RequestMapping(value="/file/download", method = RequestMethod.GET)
    public @ResponseBody String singleFileUpload(@RequestParam(value="status") String status,
			@RequestParam(value="startDate") String startDate,
			@RequestParam(value="endDate") String endDate,
			@RequestParam(value="clientId",required=false) Integer clientId,@RequestParam("surveyId") Integer surveyId,Authentication authentication) throws Exception {
            System.out.println("Fetching file");
            CustomUserBean user = null;
            if(authentication instanceof CustomAuthenticationToken){
                user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
            }
            
            String success = (String)restUtil.getData("/file/download?access_token="+user.getAccessToken()+"&status="+status+"&startDate="+startDate+"&endDate="+endDate+"&clientId="+clientId+"&surveyId="+surveyId, String.class);
            JSONObject jsonObject = new JSONObject(success);
            
            byte[] buf = new sun.misc.BASE64Decoder().decodeBuffer(jsonObject.get("encodedData").toString());
        	java.io.File file = new File(System.getProperty("catalina.base")+"/webapps/qcsat/temp/" + (jsonObject.get("filename").toString()));
        	java.io.FileOutputStream fop = new java.io.FileOutputStream(file);
        	fop.write(buf);
        	fop.flush();
        	fop.close();
            return success;
    }
	
	@RequestMapping(value = "/survey/clients", method = RequestMethod.GET)
	@ResponseBody
	public String listClientsBySurvey(@RequestParam(value="surveyId")Integer surveyId,Authentication authentication) throws Exception {
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
		String json = (String)restUtil.getData("/survey/clients?access_token="+user.getAccessToken()+"&surveyId="+surveyId, String.class);
		return json;

	}
	
	@RequestMapping(value = "/notify", method = RequestMethod.POST)
	@ResponseBody
	public String notify(@RequestParam(value="sumId")Integer sumId,Authentication authentication) throws Exception {
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
		String success = (String)restUtil.postData(null,"/notify?access_token="+user.getAccessToken()+"&sumId="+sumId, String.class);
		return success;
	}
	
	
	@RequestMapping(value = "/notifyMany", method = RequestMethod.POST)
	@ResponseBody
	public String notifyMany(@RequestParam(value="sumIds")String sumIds,Authentication authentication) throws Exception {
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
		String success = (String)restUtil.postData(null,"/notifyMany?access_token="+user.getAccessToken()+"&sumIds="+sumIds, String.class);
		return success;
	}
	
	
	//method to get reports account wise
	
	
	@RequestMapping(value = "/accountreports", method = RequestMethod.GET)
	@ResponseBody
	public String viewAccountReport(Authentication authentication,@RequestParam("clientId") Integer clientId) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/accountreports?access_token="+user.getAccessToken()+"&clientId="+clientId, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	
	@RequestMapping(value = "/clientName", method = RequestMethod.GET)
	@ResponseBody
	public String clientNameById(Authentication authentication,@RequestParam("clientId") Integer clientId) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/clientName?access_token="+user.getAccessToken()+"&clientId="+clientId, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	@RequestMapping(value = "/accountnpsreports", method = RequestMethod.GET)
	@ResponseBody
	 public String viewAccountReport1(Authentication authentication,@RequestParam("clientId") Integer clientId) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        
        json = (String)restUtil.getData("/accountnpsreports?access_token="+user.getAccessToken()+"&clientId="+clientId, String.class);
        System.out.println("------------------------------------------- TEST -----------------------------");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	@RequestMapping(value = "/accounttop3benefits", method = RequestMethod.GET)
	@ResponseBody
	public String viewAccountTop3Benefits(Authentication authentication,@RequestParam("clientId") Integer clientId) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/top3BenefitsForAccount?access_token="+user.getAccessToken()+"&clientId="+clientId, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	@RequestMapping(value = "/avgscoresquestionset", method = RequestMethod.GET)
	@ResponseBody
	public String avgScoresForQuestionSet1(Authentication authentication,@RequestParam("clientId") Integer clientId,@RequestParam("questionGrpId") Integer questionGrpId) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/avgscoresquestionset?access_token="+user.getAccessToken()+"&clientId="+clientId+"&questionGrpId="+questionGrpId, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	

	
	/*@RequestMapping(value = "/npsQuestionWise", method = RequestMethod.GET)
	@ResponseBody
	public String npsScoreQuestionWise(Authentication authentication,@RequestParam("clientId") Integer clientId) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/npsQuestionWise?access_token="+user.getAccessToken()+"&clientId="+clientId, String.class);
        System.out.println("-------------- JSON --------------------------"+json);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}*/
	@RequestMapping(value = "/avgscoresquestionset4", method = RequestMethod.GET)
	@ResponseBody
	public String avgScoresForQuestionSet4(Authentication authentication,@RequestParam("clientId") Integer clientId,@RequestParam("questionGrpId") Integer questionGrpId) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/avgscoresquestionset4?access_token="+user.getAccessToken()+"&clientId="+clientId+"&questionGrpId="+questionGrpId, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	@RequestMapping(value = "/npspromoters", method = RequestMethod.GET)
	@ResponseBody
	public String npsPromoters(Authentication authentication) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/npspromoters?access_token="+user.getAccessToken(), String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	@RequestMapping(value = "/npsoverallreport", method = RequestMethod.GET)
	@ResponseBody
	public String npsForIndividualQuestions(Authentication authentication) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/npsoverallreport?access_token="+user.getAccessToken(), String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	@RequestMapping(value = "/npspromoterslist", method = RequestMethod.GET)
	@ResponseBody
	public String npsPromotersList(Authentication authentication) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/npspromoterslist?access_token="+user.getAccessToken(), String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	@RequestMapping(value = "/npsdetractorslist", method = RequestMethod.GET)
	@ResponseBody
	public String npsDetractorsList(Authentication authentication) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/npsdetractorslist?access_token="+user.getAccessToken(), String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	@RequestMapping(value = "/npsneutrallist", method = RequestMethod.GET)
	@ResponseBody
	public String npsNeutralList(Authentication authentication) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/npsneutrallist?access_token="+user.getAccessToken(), String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	@RequestMapping(value = "/avgOverallAverageOfUser",method = RequestMethod.GET)
	@ResponseBody
	public String avgForOverallUser(Authentication authentication){
		String json = null;
		try{
			CustomUserBean user = null;
			if(authentication instanceof CustomAuthenticationToken){
				user = ((CustomAuthenticationToken) authentication).getCustomUserBean();
			}
			json = (String)restUtil.getData("/avgOverallAverageOfUser?access_token="+user.getAccessToken(),String.class);
			System.out.println("----------------------------------"+json);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;
		
	}
	
	@RequestMapping(value = "/avgOverallAverageOfQuestiongrp",method = RequestMethod.GET)
	@ResponseBody
	public String avgForOverallQuestiongrp(Authentication authentication){
		String json = null;
		try{
			CustomUserBean user = null;
			if(authentication instanceof CustomAuthenticationToken){
				user = ((CustomAuthenticationToken) authentication).getCustomUserBean();
			}
			json = (String)restUtil.getData("/avgOverallAverageOfQuestiongrp?access_token="+user.getAccessToken(),String.class);
			System.out.println("----------------------------------"+json);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;
		
	}
	
	
	@RequestMapping(value = "/overallNpsForOrganization",method = RequestMethod.GET)
	@ResponseBody
	public String overallNpsForOrganization(Authentication authentication){
		String json = null;
		try{
			CustomUserBean user = null;
			if(authentication instanceof CustomAuthenticationToken){
				user = ((CustomAuthenticationToken) authentication).getCustomUserBean();
			}
			json = (String)restUtil.getData("/overallNpsForOrganization?access_token="+user.getAccessToken(),String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;
		
	}
	
	@RequestMapping(value = "/accountNps",method = RequestMethod.GET)
	@ResponseBody
	public String accountNps(Authentication authentication,@RequestParam("clientId") Integer clientId){
		String json = null;
		try{
			CustomUserBean user = null;
			if(authentication instanceof CustomAuthenticationToken){
				user = ((CustomAuthenticationToken) authentication).getCustomUserBean();
			}
			json = (String)restUtil.getData("/accountNps?access_token="+user.getAccessToken()+"&clientId="+clientId,String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;
		
	}
	
	@RequestMapping(value = "/respondanttop3benefits", method = RequestMethod.GET)
	@ResponseBody
	public String viewRespondantTop3benefits(Authentication authentication,@RequestParam("respondantId") Long respondantId) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/respondanttop3benefits?access_token="+user.getAccessToken()+"&respondantId="+respondantId, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	@RequestMapping(value = "/respondantAvgScoresForQuestionSet", method = RequestMethod.GET)
	@ResponseBody
	public String respondantAvgScoresForQuestionSet(Authentication authentication,@RequestParam("respondantId") Long respondantId,@RequestParam("questionGrpId") Integer questionGrpId) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/respondantAvgScoresForQuestionSet?access_token="+user.getAccessToken()+"&respondantId="+respondantId+"&questionGrpId="+questionGrpId, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	@RequestMapping(value = "/respondantNps",method = RequestMethod.GET)
	@ResponseBody
	public String respondantNps(Authentication authentication,@RequestParam("respondantId") Long respondantId){
		String json = null;
		try{
			CustomUserBean user = null;
			if(authentication instanceof CustomAuthenticationToken){
				user = ((CustomAuthenticationToken) authentication).getCustomUserBean();
			}
			json = (String)restUtil.getData("/respondantNps?access_token="+user.getAccessToken()+"&respondantId="+respondantId,String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;
		
	}
	
	@RequestMapping(value = "/respondantAnswerForQuestionSet4", method = RequestMethod.GET)
	@ResponseBody
	public String avgScoresForQuestionSet4(Authentication authentication,@RequestParam("respondantId") Long respondantId,@RequestParam("questionGrpId") Integer questionGrpId) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/respondantAnswerForQuestionSet4?access_token="+user.getAccessToken()+"&respondantId="+respondantId+"&questionGrpId="+questionGrpId, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	@RequestMapping(value = "/npsquestionwise", method = RequestMethod.GET)
	@ResponseBody
	public String npsScoreQuestionWise(Authentication authentication) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/npsquestionwise?access_token="+user.getAccessToken(), String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	@RequestMapping(value = "/csatreportssentandrecieved", method = RequestMethod.GET)
	@ResponseBody
	public String csatReportsSentAndRecieved(Authentication authentication) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/csatreportssentandrecieved?access_token="+user.getAccessToken(), String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	@RequestMapping(value = "/overallnpsreports",method = RequestMethod.GET)
	@ResponseBody
	public String avgForOverallNps(Authentication authentication){
		String json = null;
		try{
			CustomUserBean user = null;
			if(authentication instanceof CustomAuthenticationToken){
				user = ((CustomAuthenticationToken) authentication).getCustomUserBean();
			}
			json = (String)restUtil.getData("/overallnpsreports?access_token="+user.getAccessToken(),String.class);
			System.out.println("----------------------------------"+json);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;
		
	}
	
	@RequestMapping(value = "/top3BenefitsForOrganization", method = RequestMethod.GET)

	@ResponseBody

	public String viewOrganizationTop3Benefits(Authentication authentication) {

	String json = null;

	try{

	CustomUserBean user = null;

	if(authentication instanceof CustomAuthenticationToken){

	user = ((CustomAuthenticationToken)authentication).getCustomUserBean();

	}

	json = (String)restUtil.getData("/top3BenefitsForOrganization?access_token="+user.getAccessToken(), String.class);

	 

	}catch(Exception e){

	e.printStackTrace();

	}
	return json;
	}
	
	@RequestMapping(value = "/emwiseAvgForQuestionGroup", method = RequestMethod.GET)
	@ResponseBody
	public String engagementManagerWiseReport(Authentication authentication,@RequestParam("engagementManager") String engagementManager) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/emwiseAvgForQuestionGroup?access_token="+user.getAccessToken()+"&engagamentManager="+engagementManager, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	@RequestMapping(value = "/overAllAverageNpsForRespondant", method = RequestMethod.GET)

	@ResponseBody

	public String overAllAverageNpsForRespondant(Authentication authentication,@RequestParam("respondantId") int respondantId) {

	String json = null;

	try{

	CustomUserBean user = null;

	        if(authentication instanceof CustomAuthenticationToken){
	            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
	        }

	        json = (String)restUtil.getData("/overAllAverageNpsForRespondant?access_token="+user.getAccessToken()+"&respondantId="+respondantId, String.class);

	}catch(Exception e){

	e.printStackTrace();

	}

	return json;

	}
	
	@RequestMapping(value = "/emwisenps", method = RequestMethod.GET)
	@ResponseBody
	public String engagementManagerWiseNps(Authentication authentication,@RequestParam("engagementManager") String engagementManager) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/emwisenps?access_token="+user.getAccessToken()+"&engagementManager="+engagementManager, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	@RequestMapping(value = "/avgOverallAverageOfQuestiongrpForEM",method = RequestMethod.GET)
	@ResponseBody
	public String avgForOverallQuestiongrpEmWise(Authentication authentication,@RequestParam("engagementManager") String engagementManager){
		String json = null;
		try{
			CustomUserBean user = null;
			if(authentication instanceof CustomAuthenticationToken){
				user = ((CustomAuthenticationToken) authentication).getCustomUserBean();
			}
			json = (String)restUtil.getData("/avgOverallAverageOfQuestiongrpForEM?access_token="+user.getAccessToken()+"&engagementManager="+engagementManager,String.class);
			System.out.println("----------------------------------"+json);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;
		
	}
	
	@RequestMapping(value = "/emwisenps1", method = RequestMethod.GET)
	@ResponseBody
	 public String viewNpsOfEmWise(Authentication authentication,@RequestParam("engagementManager") String engagementManager) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        
        json = (String)restUtil.getData("/emwisenps1?access_token="+user.getAccessToken()+"&engagementManager="+engagementManager, String.class);
        System.out.println("------------------------------------------- TEST -----------------------------");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	
	@RequestMapping(value = "/emwiseoverallnps", method = RequestMethod.GET)
	@ResponseBody
	 public String viewOverallNpsOfEmWise(Authentication authentication,@RequestParam("engagementManager") String engagementManager) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        
        json = (String)restUtil.getData("/emwiseoverallnps?access_token="+user.getAccessToken()+"&engagementManager="+engagementManager, String.class);
        System.out.println("------------------------------------------- TEST -----------------------------");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	
	@RequestMapping(value = "/top3BenefitsForEmwise", method = RequestMethod.GET)
	@ResponseBody
	public String top3BenefitsForEmWise(Authentication authentication,@RequestParam("engagementManager") String engagementManager) {
		String json = null;
		try{
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        json = (String)restUtil.getData("/top3BenefitsForEmwise?access_token="+user.getAccessToken()+"&engagementManager="+engagementManager, String.class);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return json;

	}
	
	
}
