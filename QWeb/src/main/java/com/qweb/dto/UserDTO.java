package com.qweb.dto;

import java.io.Serializable;

public class UserDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1640873759953203424L;
	private Long id;
	private String ssoId;
	private String role;
	private String firstName;
	private String lastName;
	private String email;
	private String state;
	private String password;
	private Integer clientId;
	private Integer surveyId;
	private Integer sumId;
	private String clientName;
	private String verticalName;
	private String projectName;
	private String engagementManager;
	private Long fileoriginalsize;
	private String contenttype;
	private String base64;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSsoId() {
		return ssoId;
	}
	public void setSsoId(String ssoId) {
		this.ssoId = ssoId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer prId) {
		this.clientId = prId;
	}
	public Integer getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(Integer surveyId) {
		this.surveyId = surveyId;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getVerticalName() {
		return verticalName;
	}
	public void setVerticalName(String verticalName) {
		this.verticalName = verticalName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getEngagementManager() {
		return engagementManager;
	}
	public void setEngagementManager(String engagementManager) {
		this.engagementManager = engagementManager;
	}
	public Long getFileoriginalsize() {
		return fileoriginalsize;
	}
	public void setFileoriginalsize(Long fileoriginalsize) {
		this.fileoriginalsize = fileoriginalsize;
	}
	public String getContenttype() {
		return contenttype;
	}
	public void setContenttype(String contenttype) {
		this.contenttype = contenttype;
	}
	public String getBase64() {
		return base64;
	}
	public void setBase64(String base64) {
		this.base64 = base64;
	}
	public Integer getSumId() {
		return sumId;
	}
	public void setSumId(Integer sumId) {
		this.sumId = sumId;
	}
	
	
	

}
