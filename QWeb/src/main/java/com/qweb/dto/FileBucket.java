package com.qweb.dto;

import javax.mail.Multipart;

import org.springframework.web.multipart.MultipartFile;


public class FileBucket {
	String fileName;

	MultipartFile file;
	
	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
}