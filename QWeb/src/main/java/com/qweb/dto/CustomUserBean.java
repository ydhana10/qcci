package com.qweb.dto;

import java.io.Serializable;

public class CustomUserBean implements Serializable {

    private static final long serialVersionUID = 5047510412099091708L;
    private String accessToken;
    private String refreshToken;
    private UserDTO userDTO;
    
    
	public UserDTO getUserDTO() {
		return userDTO;
	}
	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

   
}
