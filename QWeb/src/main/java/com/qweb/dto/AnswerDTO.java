package com.qweb.dto;


import java.util.Date;

public class AnswerDTO implements java.io.Serializable {

	private Integer aid;
	private OptionDTO optionDTO;
	private String atext;
	private int userId;

	public Integer getAid() {
		return this.aid;
	}

	public void setAid(Integer aid) {
		this.aid = aid;
	}

	public OptionDTO getOption() {
		return this.optionDTO;
	}

	public void setOption(OptionDTO option) {
		this.optionDTO = option;
	}

	public String getAtext() {
		return this.atext;
	}

	public void setAtext(String atext) {
		this.atext = atext;
	}


	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
