package com.qweb.controller;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.qweb.dto.CustomUserBean;
import com.qweb.dto.FileBucket;
import com.qweb.security.CustomAuthenticationToken;



/**
 * Author: Syed Usman
 * The Class AppController.
 */
@Controller
@RequestMapping("/")
public class AppController extends BaseController{
	
	/**
	 * Home page.
	 *
	 * @param model the model
	 * @param authentication the authentication
	 * @return the string
	 */
	//@Retryable(maxAttempts=10,value=HttpClientErrorException.class,backoff = @Backoff(delay = 10000,multiplier=2))
	@RequestMapping(value = { "/home"}, method = RequestMethod.GET)
	public String homePage(ModelMap model,Authentication authentication) {
		CustomUserBean user = null;
        //Get the additional data stored
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        String page = "homeuser";
        if(user.getUserDTO().getSumId()!=null){
        	model.addAttribute("sumId",user.getUserDTO().getSumId());
        }
		Collection<GrantedAuthority> authorities = ((CustomAuthenticationToken)authentication).getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		if(role.equals("1")){
			page = "home";
		}
		return page;
	}
	
	
	@RequestMapping(value = { "/surveyresults"}, method = RequestMethod.GET)
	public String surveyresults(ModelMap model,Authentication authentication) {
		CustomUserBean user = null;
        //Get the additional data stored
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        String page = "homeuser";
        
		Collection<GrantedAuthority> authorities = ((CustomAuthenticationToken)authentication).getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		if(role.equals("ADMIN")){
			page = "surveyresults";
		}
		
		return page;
	}
	
	@RequestMapping(value = { "/accountreports"}, method = RequestMethod.GET)
	public String accountReports(ModelMap model,Authentication authentication) {
		CustomUserBean user = null;
        //Get the additional data stored
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        String page = "homeuser";
        
		Collection<GrantedAuthority> authorities = ((CustomAuthenticationToken)authentication).getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		if(role.equals("ADMIN")){
			page = "accountreports";
		}
		
		return page;
	}
	
	
	@RequestMapping(value = { "/npspromotersreport"}, method = RequestMethod.GET)
	public String npsPromotersReport(ModelMap model,Authentication authentication) {
		CustomUserBean user = null;
        //Get the additional data stored
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        String page = "homeuser";
        
		Collection<GrantedAuthority> authorities = ((CustomAuthenticationToken)authentication).getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		if(role.equals("ADMIN")){
			page = "npspromotersreport";
		}
		
		return page;
	}
	

	@RequestMapping(value = { "/npspromoters"}, method = RequestMethod.GET)
	public String npsPromotersList(ModelMap model,Authentication authentication) {
		CustomUserBean user = null;
        //Get the additional data stored
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        String page = "homeuser";
        
		Collection<GrantedAuthority> authorities = ((CustomAuthenticationToken)authentication).getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		if(role.equals("ADMIN")){
			page = "npspromoters";
		}
		
		return page;
	}
	
	@RequestMapping(value = { "/npsdetractors"}, method = RequestMethod.GET)
	public String npsDetractorsList(ModelMap model,Authentication authentication) {
		CustomUserBean user = null;
        //Get the additional data stored
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        String page = "homeuser";
        
		Collection<GrantedAuthority> authorities = ((CustomAuthenticationToken)authentication).getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		if(role.equals("ADMIN")){
			page = "npsdetractors";
		}
		
		return page;
	}
	@RequestMapping(value = { "/npsneutral"}, method = RequestMethod.GET)
	public String npsNeutralList(ModelMap model,Authentication authentication) {
		CustomUserBean user = null;
        //Get the additional data stored
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        String page = "homeuser";
        
		Collection<GrantedAuthority> authorities = ((CustomAuthenticationToken)authentication).getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		if(role.equals("ADMIN")){
			page = "npsneutral";
		}
		
		return page;
	}
	
	@RequestMapping(value = { "/overallaccountreport"}, method = RequestMethod.GET)
	public String overallAccountReport(ModelMap model,Authentication authentication) {
		CustomUserBean user = null;
        //Get the additional data stored
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        String page = "homeuser";
        
		Collection<GrantedAuthority> authorities = ((CustomAuthenticationToken)authentication).getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		if(role.equals("ADMIN")){
			page = "overallaccountreport";
		}
		
		return page;
	}
	
	@RequestMapping(value = { "/respondantwisereport"}, method = RequestMethod.GET)
	public String respondantwisereport(ModelMap model,Authentication authentication) {
		CustomUserBean user = null;
        //Get the additional data stored
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        String page = "homeuser";
        
		Collection<GrantedAuthority> authorities = ((CustomAuthenticationToken)authentication).getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		if(role.equals("ADMIN")){
			page = "respondantwisereport";
		}
		
		return page;
	}
	
	
	@RequestMapping(value = { "/emwisereport"}, method = RequestMethod.GET)
	public String emwiseReport(ModelMap model,Authentication authentication) {
		CustomUserBean user = null;
        //Get the additional data stored
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        String page = "homeuser";
        
		Collection<GrantedAuthority> authorities = ((CustomAuthenticationToken)authentication).getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		if(role.equals("ADMIN")){
			page = "emwisereport";
		}
		
		return page;
	}
	@RequestMapping(value = { "/npsquestionwise"}, method = RequestMethod.GET)
	public String npsQuestionWise(ModelMap model,Authentication authentication) {
		CustomUserBean user = null;
        //Get the additional data stored
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        String page = "homeuser";
        
		Collection<GrantedAuthority> authorities = ((CustomAuthenticationToken)authentication).getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		if(role.equals("ADMIN")){
			page = "npsquestionwise";
		}
		
		return page;
	}
	
	/*@RequestMapping(value = { "/detractorsreport"}, method = RequestMethod.GET)
	public String detractorsreport(ModelMap model,Authentication authentication) {
		CustomUserBean user = null;
        //Get the additional data stored
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
        String page = "homeuser";
        
		Collection<GrantedAuthority> authorities = ((CustomAuthenticationToken)authentication).getAuthorities();
		String role = authorities.iterator().next().getAuthority();
		if(role.equals("ADMIN")){
			page = "detractorsreport";
		}
		
		return page;
	}*/
	
	
	/**
	 * Login page.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = { "/","/login"}, method = RequestMethod.GET)
	public String loginPage(ModelMap model) {
		return "login";
	}

	/**
	 * Products page.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = { "/products"}, method = RequestMethod.GET)
	public String productsPage(ModelMap model) {
		return "products";
	}

	/**
	 * Contact us page.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = { "/contactus"}, method = RequestMethod.GET)
	public String contactUsPage(ModelMap model) {
		return "contactus";
	}
}