package com.qweb.controller;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;

import com.qweb.dto.CustomUserBean;
import com.qweb.security.CustomAuthenticationToken;
import com.qweb.util.RESTUtil;

/**
 * Author: Syed Usman
 * The Class BaseController.
 */
//@Configuration
//@EnableRetry
@Controller
public class BaseController {
	
	/** The rest util. */
	@Autowired
	public RESTUtil restUtil;
	
	
	/**
	 * Renew token.
	 *
	 * @param authentication the authentication
	 * @param request the request
	 * @return the string
	 */
	@ResponseStatus(value=HttpStatus.UNAUTHORIZED)
	@ExceptionHandler(HttpClientErrorException.class)
	public String renewToken(Authentication authentication,HttpServletRequest request){
		CustomUserBean user = null;
        if(authentication instanceof CustomAuthenticationToken){
            user = ((CustomAuthenticationToken)authentication).getCustomUserBean();
        }
		String url = "/oauth/token?grant_type=refresh_token&client_id="+restUtil.clientId+"&client_secret="+restUtil.clientSecret+"&refresh_token="+user.getRefreshToken();
		String json=null;
		try {
			json = (String)restUtil.getData(url, String.class);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		JSONObject jsonObject = new JSONObject(json);
		user.setAccessToken(jsonObject.get("value").toString());
		JSONObject _r_token = (JSONObject)jsonObject.get("refreshToken");
		user.setRefreshToken(_r_token.get("value").toString());
		String referer = ((request.getRequestURI().toString()).replace(request.getContextPath(),""))  + (request.getQueryString()==null?"":"?"+request.getQueryString());
		return "redirect:"+referer;
	}
	
	/**
	 * On exception.
	 *
	 * @param authentication the authentication
	 * @param request the request
	 * @return the string
	 */
	@ExceptionHandler(Exception.class)
	public String onException(Exception e,Authentication authentication,HttpServletRequest request){
		e.printStackTrace();
		return "error";
	}
	

}
