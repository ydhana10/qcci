package com.qwebservices.webservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qwebservices.business.service.UserService;
import com.qwebservices.infra.dto.UserDTO;

/**
 * @author Nagesh.Chauhan
 *
 */
@Controller
@RequestMapping("/l")
public class LoginAuthController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "/auth", method = RequestMethod.POST)
	@ResponseBody
	public UserDTO list(@RequestBody UserDTO userDTO,@RequestParam(value="surveyId",required=false)String surveyId ) throws Exception {
		if(surveyId==null){
			return userService.authenticateUser(userDTO.getSsoId(),userDTO.getPassword());
		}else{
			return userService.authenticateUserBySurveyId(userDTO.getSsoId(), Integer.parseInt(surveyId.trim()));
		}
	}
}
