package com.qwebservices.webservices.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qwebservices.business.service.AnswerService;
import com.qwebservices.business.service.ClientService;
import com.qwebservices.business.service.ReportsService;
import com.qwebservices.business.service.SurveyService;
import com.qwebservices.business.service.SurveyUserMappingsService;
import com.qwebservices.business.service.UserService;
import com.qwebservices.business.service.VerticalService;
import com.qwebservices.data.domain.AppUser;
import com.qwebservices.data.domain.Surveyusermapping;
import com.qwebservices.infra.dto.ClientDTO;
import com.qwebservices.infra.dto.QuestionGroupDTO;
import com.qwebservices.infra.dto.SurveyDTO;
import com.qwebservices.infra.dto.UserDTO;
import com.qwebservices.infra.dto.VerticalDTO;
import com.qwebservices.infra.util.ConfigValues;


@Controller
public class CommonRESTWS {
	
	private static Logger log = Logger.getLogger(CommonRESTWS.class);
	
	@Autowired
	VerticalService verticalService;
	
	@Autowired
	ClientService clientService;
	

	
	@Autowired
	UserService userService;
	
	@Autowired
	SurveyUserMappingsService surveyUserMappingsService;
	
	@Autowired
	AnswerService answerService;
	
	@Autowired
	SurveyService surveyService;
	
	@Autowired
	ReportsService reportsService;
	
	
	@RequestMapping(value = "/verticals", method = RequestMethod.GET)
	@ResponseBody
	public List<VerticalDTO> list() throws Exception {
		try {
			return verticalService.findVerticals();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/clients", method = RequestMethod.GET)
	@ResponseBody
	public List<ClientDTO> list(@RequestParam(value="verticalId")Integer verticalId) throws Exception {
		try {
			return clientService.listClients(verticalId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/respondants", method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> listOfRespondants(@RequestParam(value="clientId")Integer clientId) throws Exception {
		try {
			System.out.println("************** IN RESPONDANTS RESTWS**************************");
			return verticalService.findRespondants(clientId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@RequestMapping(value = "/engagementmanager", method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> listOfEngagementmanager(@RequestParam(value="verticalId")Integer verticalId) throws Exception {
		try {
			return userService.listEmanager(verticalId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	@ResponseBody
	public Boolean insertUser(@RequestBody UserDTO userDTO) throws Exception {
		try {
			return userService.insertUser(userDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/surveys", method = RequestMethod.GET)
	@ResponseBody
	public List<SurveyDTO> listProjects(@RequestParam(value="userId")String userId,@RequestParam(value="sumId")Integer sumId,
			@RequestParam(value="status", required=false)String status) throws Exception {
		try {
			return surveyUserMappingsService.listSurvey(userId, status,sumId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/survey", method = RequestMethod.POST)
	@ResponseBody
	public Boolean insertUser(@RequestBody SurveyDTO surveyDTO) throws Exception {
		try {
			return answerService.insertAnswers(surveyDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/allsurveys", method = RequestMethod.GET)
	@ResponseBody
	public List<SurveyDTO> allsurveys() throws Exception {
		try {
			return surveyUserMappingsService.listSurveys();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/qgs", method = RequestMethod.GET)
	@ResponseBody
	public List<QuestionGroupDTO> allQgs(@RequestParam("sumId") Integer sumId) throws Exception {
		try {
			return surveyUserMappingsService.listQuestionGrpsBySumId(sumId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@RequestMapping(value="/file/upload", method = RequestMethod.POST)
    public @ResponseBody String singleFileUpload(@RequestBody  UserDTO userDTO,
			@RequestParam("filename") String filename) throws Exception {
            System.out.println("Fetching file");
            String success = "success";
            try {
            	//Base64.decodeBase64(
				//FileCopyUtils.copy(fileBucket.getFile().getBytes(), new File(System.getProperty("catalina.base")+ConfigValues.getConfigValue("UPLOAD_LOC") + (fileBucket.getFileName()).replace("xlsx", "xls")));
            	byte[] buf = new sun.misc.BASE64Decoder().decodeBuffer(userDTO.getBase64());
            	java.io.File file = new File(System.getProperty("catalina.base")+ConfigValues.getConfigValue("UPLOAD_LOC") + (filename));
            	java.io.FileOutputStream fop = new java.io.FileOutputStream(file);
            	fop.write(buf);
            	fop.flush();
            	fop.close();
            	userService.uploadMultipleUsers(userDTO, System.getProperty("catalina.base")+ConfigValues.getConfigValue("UPLOAD_LOC") + (filename));
            	//Files.write(Paths.get(System.getProperty("catalina.base")+ConfigValues.getConfigValue("UPLOAD_LOC") + (fileBucket.getFileName()).replace("xlsx", "xls")), DatatypeConverter.parseBase64Binary(fileBucket.getFile()));
				System.out.println(System.getProperty("catalina.base"));
			} catch (Exception e) {
				success = "error";
				log.error("Exception Occurred",e);
			}          
            return success;
    }
	
	
	@RequestMapping(value="/file/download", method = RequestMethod.GET)
    public @ResponseBody String singleFileDownload(
			@RequestParam(value="status") String status,
			@RequestParam(value="startDate") String startDate,
			@RequestParam(value="endDate") String endDate,
			@RequestParam(value="clientId",required=false) Integer clientId,@RequestParam("surveyId") Integer surveyId) throws Exception {
            System.out.println("Fetching file");
            String success = "success";
            String encodedString = "";
            String filename = "";
            try {
            	//Base64.decodeBase64(
				//FileCopyUtils.copy(fileBucket.getFile().getBytes(), new File(System.getProperty("catalina.base")+ConfigValues.getConfigValue("UPLOAD_LOC") + (fileBucket.getFileName()).replace("xlsx", "xls")));
            	//byte[] buf = new sun.misc.BASE64Decoder().decodeBuffer(userDTO.getBase64());
            	//java.io.File file = new File(System.getProperty("catalina.base")+ConfigValues.getConfigValue("UPLOAD_LOC") + (filename));
            	
            	filename = surveyService.generateExcelReport(surveyId, clientId, startDate, endDate, status);
            	File file = new File(System.getProperty("catalina.base")+ConfigValues.getConfigValue("DOWNLOAD_LOC") + filename);
        		byte[] bytes = loadFile(file);
        		byte[] encoded = Base64.encodeBase64(bytes);
        		encodedString = new String(encoded);


            	//Files.write(Paths.get(System.getProperty("catalina.base")+ConfigValues.getConfigValue("UPLOAD_LOC") + (fileBucket.getFileName()).replace("xlsx", "xls")), DatatypeConverter.parseBase64Binary(fileBucket.getFile()));
				System.out.println(System.getProperty("catalina.base"));
			} catch (Exception e) {
				success = "error";
				log.error("Exception Occurred",e);
			}          
            return "{\"encodedData\":\""+encodedString+"\",\"filename\":\""+filename+"\"}";
    }
	

	private static byte[] loadFile(File file) throws IOException {
	    InputStream is = new FileInputStream(file);

	    long length = file.length();
	    if (length > Integer.MAX_VALUE) {
	        // File is too large
	    }
	    byte[] bytes = new byte[(int)length];
	    
	    int offset = 0;
	    int numRead = 0;
	    while (offset < bytes.length
	           && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
	        offset += numRead;
	    }

	    if (offset < bytes.length) {
	        throw new IOException("Could not completely read file "+file.getName());
	    }

	    is.close();
	    return bytes;
	}
	
	
	@RequestMapping(value="/survey/dropdown", method = RequestMethod.GET)
    public @ResponseBody List<SurveyDTO> singleFileDownload() throws Exception {
            System.out.println("Fetching file");
            List<SurveyDTO> surveyDTOs = null;
            try {
            	surveyDTOs = surveyService.surveyDropdown();
			} catch (Exception e) {
				log.error("Exception Occurred",e);
			}          
            return surveyDTOs;
    }
	
	@RequestMapping(value = "/survey/clients", method = RequestMethod.GET)
	@ResponseBody
	public List<ClientDTO> listClients(@RequestParam("surveyId") Integer surveyId) throws Exception {
		try {
			return surveyUserMappingsService.clientBySurvey(surveyId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/notify", method = RequestMethod.POST)
    public @ResponseBody String sendNotification(
			@RequestParam("sumId") Integer sumId) throws Exception {
            String success = "success";
            try {
            	userService.sendNotification(sumId);
			} catch (Exception e) {
				success = "error";
				log.error("Exception Occurred",e);
			}          
            return success;
    }
	
	
	@RequestMapping(value="/notifyMany", method = RequestMethod.POST)
    public @ResponseBody String sendNotificationToMany(@RequestParam("sumIds") String sumIds) throws Exception {
            String success = "success";
            try {
            	userService.sendNotificationToMany(sumIds);
			} catch (Exception e) {
				success = "error";
				log.error("Exception Occurred",e);
			}          
            return success;
    }
	
	
	
	@RequestMapping(value = "/accountreports", method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String,Float> viewAccountReport(@RequestParam("clientId") Integer clientId) throws Exception {
		try {
			return reportsService.getOverallNpsForAccount(clientId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/top3BenefitsForAccount", method = RequestMethod.GET)
	@ResponseBody
	public List<String> top3BenefitsForAccount(@RequestParam("clientId") Integer clientId) throws Exception {
		try {
			return reportsService.getTop3BenefitsForAccount(clientId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/avgscoresquestionset",method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> avgForQuestionSet1(@RequestParam("clientId") Integer clientId,@RequestParam("questionGrpId") Integer questionGrpId ) throws Exception {
		try {
			return reportsService.getAverageScoresForEachQestionGrpForAccount(clientId,questionGrpId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/avgscoresquestionset4",method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> avgForQuestionSet4(@RequestParam("clientId") Integer clientId,@RequestParam("questionGrpId") Integer questionGrpId ) throws Exception {
		try {
			return reportsService.getScoresForQestionGrp4ForAccount(clientId,questionGrpId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/avgscoresquestionset5",method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> avgForQuestionSet5(@RequestParam("clientId") Integer clientId,@RequestParam("questionGrpId") Integer questionGrpId ) throws Exception {
		try {
			return reportsService.getAverageScoresForEachQestionGrpForAccount(clientId,questionGrpId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@RequestMapping(value = "/clientName", method = RequestMethod.GET)
	@ResponseBody
	public String clientNameByClientId(@RequestParam("clientId") Integer clientId) throws Exception {
		try {
			return reportsService.getClientNameByClientId(clientId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/npspromoters", method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Float> npsPromoters() throws Exception {
		try {
			return reportsService.overallNpsForOrganization();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/npsoverallreport",method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> npsForIndividualQuestions() throws Exception {
		try {
			return reportsService.getListOfPromotersAndDetractorsAndNeutral();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/npspromoterslist",method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> npsPromotersList() throws Exception {
		try {
			return reportsService.getListOfPromotersAndDetractorsAndNeutral();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/npsdetractorslist",method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> npsDetractorsList() throws Exception {
		try {
			return reportsService.getListOfPromotersAndDetractorsAndNeutral();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@RequestMapping(value="/npsneutrallist",method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> npsNeutralList() throws Exception {
		try {
			return reportsService.getListOfPromotersAndDetractorsAndNeutral();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/top3BenefitsForOrganization", method = RequestMethod.GET)
	@ResponseBody
	public List<String> top3BenefitsForOrganization() throws Exception {
		try {
			return reportsService.getTop3BenefitsForOrganization();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/*@RequestMapping(value = "/npsQuestionWise", method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String,Float> npsScoreQuestionWise(@RequestParam("clientId") Integer clientId) throws Exception {
		try {
			return reportsService.npsScoreQuestionWise(clientId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}*/
	@RequestMapping(value="/avgOverallAverageOfUser",method = RequestMethod.GET)
	@ResponseBody
	public List<ClientDTO> avgForOverallUser() throws Exception {
		try {
			return reportsService.getAverageScoresForAllClientOfOrganization();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/overallNpsForOrganization",method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Integer> overallNpsForOrganization() throws Exception {
		try {
			return reportsService.overallCountOfDetractorsPromotersAndNeutral();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@RequestMapping(value = "/avgOverallAverageOfQuestiongrp", method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Double> avgOverallAverageOfQuestiongrp() throws Exception {
		try {
			return reportsService.getOverallScoreOfEachQuestiongrpForOrganization();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/avgOverallAverageOfQuestiongrpForEM", method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Double> avgOverallAverageOfQuestiongrpEmwise(@RequestParam("engagementManager") String engagementManager) throws Exception {
		try {
			return reportsService.getOverallScoreForEmWise(engagementManager);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/accountNps",method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> accountNps(@RequestParam("clientId") Integer clientId) throws Exception {
		try {
			return reportsService.getListOfPromotersAndDetractorsAndNeutralForClientId(clientId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/respondanttop3benefits", method = RequestMethod.GET)
	@ResponseBody
	public List<String> respondanttop3benefits(@RequestParam("respondantId") Long respondantId) throws Exception {
		try {
			return reportsService.respondantTop3Benefits(respondantId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/respondantAvgScoresForQuestionSet",method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> respondantAvgScoresForQuestionSet1(@RequestParam("respondantId") Long respondantId,@RequestParam("questionGrpId") Integer questionGrpId ) throws Exception {
		try {
			return reportsService.respondantAvgScoresForQuestionSet(respondantId,questionGrpId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/respondantNps",method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> respondantNps(@RequestParam("respondantId") Long respondantId) throws Exception {
		try {
			return reportsService.getNpsScoreForRespondant(respondantId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/respondantAnswerForQuestionSet4",method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> avgForQuestionSet4(@RequestParam("respondantId") Long respondantId,@RequestParam("questionGrpId") Integer questionGrpId ) throws Exception {
		try {
			return reportsService.respondantAnswerForQuestionSet4(respondantId,questionGrpId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/npsquestionwise",method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String,Integer> npsScoreQuestionWise() throws Exception {
		try {
			return reportsService.npsValueForEachQuestion();
			} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@RequestMapping(value="/csatreportssentandrecieved",method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> csatReportsSentAndRecieved() throws Exception {
		try {
			return reportsService.getAllDetailsOfSurveysSentAndRecieved();
			} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@RequestMapping(value = "/overallnpsreports", method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String,Float> viewOverAllNps() throws Exception {
		try {
			//System.out.println("COMMON RESTWS");
			return reportsService.overallNpsForOrganization();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value="/emwiseAvgForQuestionGroup",method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> emWiseAverageScoreForQuestionGroups(@RequestParam("engagamentManager") String engagementManager) throws Exception {
		try {
			System.out.println("In engagement manager restws");
			return reportsService.getAverageScoreForEachQuestionGroupsForEmwise(engagementManager);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@RequestMapping(value="/overAllAverageNpsForRespondant",method = RequestMethod.GET)

	@ResponseBody

	public HashMap<String, Double> overAllAverageNpsForRespondant(@RequestParam("respondantId") int respondantId ) throws Exception {

	try {

	return reportsService.overAllAverageNpsForRespondant(respondantId);

	 

	} catch (Exception e) {

	e.printStackTrace();

	}

	return null;

	}
	
	@RequestMapping(value="/emwisenps",method = RequestMethod.GET)
	@ResponseBody
	public List<UserDTO> emWiseNps(@RequestParam("engagementManager") String engagementManager) throws Exception {
		try {
			return reportsService.getNpsPointsForEmWise(engagementManager);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/accountnpsreports", method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String,Double> viewAccountReport1(@RequestParam("clientId") Integer clientId) throws Exception {
		try {
			return reportsService.getOverAllScoreForAccount(clientId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@RequestMapping(value = "/emwisenps1", method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String,Double> viewNpsOfEmWise(@RequestParam("engagementManager") String engagementManager) throws Exception {
		try {
			return reportsService.getOverAllNpsScoreForEmWise(engagementManager);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/emwiseoverallnps", method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Double> viewoverallNpsEmwise(@RequestParam("engagementManager") String engagementManager) throws Exception {
		try {
			return reportsService.getOverallScoreForEmWise(engagementManager);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@RequestMapping(value = "/top3BenefitsForEmwise", method = RequestMethod.GET)
	@ResponseBody
	public List<String> top3BenefitsForEmWise(@RequestParam("engagementManager") String engagementManager) throws Exception {
		try {
			return reportsService.getTop3BenefitsForEmWise(engagementManager);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
