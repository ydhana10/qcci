package com.qwebservices.web.constants;



public interface QWebConstants {
	static interface STATUS_VALUES{
		public static final String ASSIGNED = "ASSIGNED";
		public static final String COMPLETED = "COMPLETED";
	}
}
