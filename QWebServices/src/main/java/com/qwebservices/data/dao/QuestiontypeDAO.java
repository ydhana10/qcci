package com.qwebservices.data.dao;
// default package
// Generated Jan 11, 2017 4:23:49 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qwebservices.data.domain.Questiontype;

/**
 * Home object for domain model class Questiontype.
 * @see .Questiontype
 * @author Hibernate Tools
 */
@Repository("questiontypeDAO")
public class QuestiontypeDAO {

	private static final Log log = LogFactory.getLog(QuestiontypeDAO.class);

	@Autowired
	private SessionFactory sessionFactory;

	public void persist(Questiontype transientInstance) {
		log.debug("persisting Questiontype instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Questiontype instance) {
		log.debug("attaching dirty Questiontype instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Questiontype instance) {
		log.debug("attaching clean Questiontype instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Questiontype persistentInstance) {
		log.debug("deleting Questiontype instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Questiontype merge(Questiontype detachedInstance) {
		log.debug("merging Questiontype instance");
		try {
			Questiontype result = (Questiontype) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Questiontype findById(java.lang.Integer id) {
		log.debug("getting Questiontype instance with id: " + id);
		try {
			Questiontype instance = (Questiontype) sessionFactory
					.getCurrentSession().get("Questiontype", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Questiontype instance) {
		log.debug("finding Questiontype instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("Questiontype")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
