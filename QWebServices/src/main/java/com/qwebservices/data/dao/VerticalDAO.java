package com.qwebservices.data.dao;
// default package
// Generated Jan 11, 2017 4:23:49 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qwebservices.data.domain.AppUser;
import com.qwebservices.data.domain.Vertical;

/**
 * Home object for domain model class Vertical.
 * @see .Vertical
 * @author Hibernate Tools
 */
@Repository("verticalDAO")
public class VerticalDAO {

	private static final Log log = LogFactory.getLog(VerticalDAO.class);

	@Autowired
	private SessionFactory sessionFactory;

	public void persist(Vertical transientInstance) {
		log.debug("persisting Vertical instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}
	
	public Integer save(Vertical transientInstance) {
		log.debug("persisting Vertical instance");
		Integer id = null;
		try {
			id = (Integer)sessionFactory.getCurrentSession().save(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
		return id;
	}

	public void attachDirty(Vertical instance) {
		log.debug("attaching dirty Vertical instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Vertical instance) {
		log.debug("attaching clean Vertical instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Vertical persistentInstance) {
		log.debug("deleting Vertical instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Vertical merge(Vertical detachedInstance) {
		log.debug("merging Vertical instance");
		try {
			Vertical result = (Vertical) sessionFactory.getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Vertical findById(java.lang.Integer id) {
		log.debug("getting Vertical instance with id: " + id);
		try {
			Vertical instance = (Vertical) sessionFactory.getCurrentSession()
					.get("Vertical", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Vertical instance) {
		log.debug("finding Vertical instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("Vertical").add(Example.create(instance))
					.list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	
	public List<Vertical> findActiveVerticals(){
		log.debug("findActiveVerticals starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findActiveVerticals");
		query.setString("activeStatus","Yes");
		List<Vertical> verticals = (List<Vertical>)query.list();
		return verticals;
	}
	
	public Vertical findActiveVerticalByVerticalId(Integer verticalId){
		log.debug("findActiveVerticalByVerticalId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findActiveVerticalByVerticalId");
		query.setInteger("verticalId",verticalId);
		Vertical vertical = (Vertical)query.uniqueResult();
		return vertical;
	}
	
	public List<AppUser> findRespondantsByClientId(Integer clientId){
		log.debug("findRespondantsByClientId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findRespondantsByClientId");
		query.setInteger("clientId",clientId);
		List<AppUser> appUser = query.list();
		return appUser;
	}
	
	public Vertical findActiveVerticalByVerticalName(String verticalName){
		log.debug("findActiveVerticalByVerticalId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findActiveVerticalByVerticalName");
		query.setString("verticalName","%"+verticalName+"%");
		Vertical vertical = (Vertical)query.uniqueResult();
		return vertical;
	}
}
