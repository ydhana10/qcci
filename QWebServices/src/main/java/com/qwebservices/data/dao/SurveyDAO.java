package com.qwebservices.data.dao;
// default package
// Generated Jan 11, 2017 4:23:49 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qwebservices.data.domain.Survey;
import com.qwebservices.data.domain.Surveyusermapping;

/**
 * Home object for domain model class Survey.
 * @see .Survey
 * @author Hibernate Tools
 */
@Repository("surveyDAO")
public class SurveyDAO {

	private static final Log log = LogFactory.getLog(SurveyDAO.class);

	@Autowired
	private SessionFactory sessionFactory;

	public void persist(Survey transientInstance) {
		log.debug("persisting Survey instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Survey instance) {
		log.debug("attaching dirty Survey instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Survey instance) {
		log.debug("attaching clean Survey instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Survey persistentInstance) {
		log.debug("deleting Survey instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Survey merge(Survey detachedInstance) {
		log.debug("merging Survey instance");
		try {
			Survey result = (Survey) sessionFactory.getCurrentSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Survey findById(java.lang.Integer id) {
		log.debug("getting Survey instance with id: " + id);
		try {
			Survey instance = (Survey) sessionFactory.getCurrentSession().get(
					"Survey", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Survey instance) {
		log.debug("finding Survey instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("Survey").add(Example.create(instance))
					.list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	
	public List<Survey> findSurveys(){
		log.debug("findSurveyByStatus starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findSurveysForDropdown");
		List<Survey> surveys = (List<Survey>)query.list();
		return surveys;
	}
}
