package com.qwebservices.data.dao;
// default package
// Generated Jan 11, 2017 4:23:49 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qwebservices.data.domain.Client;

/**
 * Home object for domain model class Client.
 * @see .Client
 * @author Hibernate Tools
 */
@Repository("clientDAO")
public class ClientDAO {

	private static final Log log = LogFactory.getLog(ClientDAO.class);

	@Autowired
	private SessionFactory sessionFactory;

	public void persist(Client transientInstance) {
		log.debug("persisting Client instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}
	
	public Integer save(Client transientInstance) {
		log.debug("persisting Client instance");
		Integer id = null;
		try {
			id = (Integer)sessionFactory.getCurrentSession().save(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
		return id;
	}

	public void attachDirty(Client instance) {
		log.debug("attaching dirty Client instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Client instance) {
		log.debug("attaching clean Client instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Client persistentInstance) {
		log.debug("deleting Client instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Client merge(Client detachedInstance) {
		log.debug("merging Client instance");
		try {
			Client result = (Client) sessionFactory.getCurrentSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Client findById(java.lang.Integer id) {
		log.debug("getting Client instance with id: " + id);
		try {
			Client instance = (Client) sessionFactory.getCurrentSession().get(
					"Client", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Client instance) {
		log.debug("finding Client instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("Client").add(Example.create(instance))
					.list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	
	public List<Client> findClientsByVerticalId(int verticalId){
		log.debug("findClientsByVerticalId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findClientsByVerticalId");
		query.setInteger("verticalId",verticalId);
		List<Client> clients = (List<Client>)query.list();
		return clients;
	}
	
	public Client findClientByClientName(String clientName){
		log.debug("findClientByClientName starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findClientByClientName");
		query.setString("clientName",clientName);
		Client client = (Client)query.uniqueResult();
		return client;
	}
	
	public Client findClientByClientId(Integer clientId){
		log.debug("findClientByClientName starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findClientByClientId");
		query.setInteger("clientId",clientId);
		Client client = (Client)query.uniqueResult();
		return client;
	}
	
	
}
