package com.qwebservices.data.dao;
// default package
// Generated Jan 11, 2017 4:23:49 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qwebservices.data.domain.AppUser;
import com.qwebservices.data.domain.Client;
/**
 * Home object for domain model class AppUser.
 * @see .AppUser
 * @author Hibernate Tools
 */
@Repository("appUserDAO")
public class AppUserDAO {

	private static final Log log = LogFactory.getLog(AppUserDAO.class);

	@Autowired
	private SessionFactory sessionFactory;

	public void persist(AppUser transientInstance) {
		log.debug("persisting AppUser instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(AppUser instance) {
		log.debug("attaching dirty AppUser instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(AppUser instance) {
		log.debug("attaching clean AppUser instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(AppUser persistentInstance) {
		log.debug("deleting AppUser instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public AppUser merge(AppUser detachedInstance) {
		log.debug("merging AppUser instance");
		try {
			AppUser result = (AppUser) sessionFactory.getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public AppUser findById(java.lang.Long id) {
		log.debug("getting AppUser instance with id: " + id);
		try {
			AppUser instance = (AppUser) sessionFactory.getCurrentSession()
					.get("AppUser", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(AppUser instance) {
		log.debug("finding AppUser instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("AppUser").add(Example.create(instance))
					.list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	
	public AppUser findUserBySSOId(String ssoId){
		log.debug("findUserBySSOId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findUserBySSOId");
		query.setString("ssoId",ssoId);
		AppUser appUser = (AppUser)query.uniqueResult();
		return appUser;
	}
	
	public AppUser findUserBySSOIdWithoutState(String ssoId){
		log.debug("findUserBySSOId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findUserBySSOIdWithoutState");
		query.setString("ssoId",ssoId);
		AppUser appUser = (AppUser)query.uniqueResult();
		return appUser;
	}
	
	
	public int deactivetUserBySSOId(String ssoId,Integer surveyId){
		log.debug("deactivetUserBySSOId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("deactivetUserBySSOIdAndSurveyId");
		//query.setString("ssoId",ssoId);
		query.setInteger("surveyId",surveyId);
		int numberOfRowsUpdated = query.executeUpdate();
		return numberOfRowsUpdated;
	}

	public AppUser findUserBySSOIdAndSurveyId(String userName, int surveyId) {
		log.debug("findUserBySSOId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findUserBySSOIdAndSurveyId");
		query.setString("ssoId",userName);
		query.setInteger("surveyId",surveyId);
		AppUser appUser = (AppUser)query.uniqueResult();
		return appUser;
}
	public List<AppUser> findEmanagersByVerticalId(int verticalId) {
		log.debug("findClientsByVerticalId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findEmanagerByVerticalId");
		query.setInteger("verticalId",verticalId);
		List<AppUser> appUsers = (List<AppUser>)query.list();
		return appUsers;
	}
}
