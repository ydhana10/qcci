package com.qwebservices.data.dao;
// default package
// Generated Jan 11, 2017 4:52:30 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qwebservices.data.domain.Client;
import com.qwebservices.data.domain.Surveyusermapping;
import com.qwebservices.data.domain.Vertical;

/**
 * Home object for domain model class Surveyusermapping.
 * @see .Surveyusermapping
 * @author Hibernate Tools
 */
@Repository("surveyusermappingDAO")
public class SurveyusermappingDAO {

	private static final Log log = LogFactory
			.getLog(SurveyusermappingDAO.class);

	@Autowired
	private SessionFactory sessionFactory;

	public void persist(Surveyusermapping transientInstance) {
		log.debug("persisting Surveyusermapping instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Surveyusermapping instance) {
		log.debug("attaching dirty Surveyusermapping instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Surveyusermapping instance) {
		log.debug("attaching clean Surveyusermapping instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Surveyusermapping persistentInstance) {
		log.debug("deleting Surveyusermapping instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Surveyusermapping merge(Surveyusermapping detachedInstance) {
		log.debug("merging Surveyusermapping instance");
		try {
			Surveyusermapping result = (Surveyusermapping) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Surveyusermapping findById(java.lang.Integer id) {
		log.debug("getting Surveyusermapping instance with id: " + id);
		try {
			Surveyusermapping instance = (Surveyusermapping) sessionFactory
					.getCurrentSession().get("Surveyusermapping", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Surveyusermapping instance) {
		log.debug("finding Surveyusermapping instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("Surveyusermapping")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	
	public List<Surveyusermapping> findSurveyByStatus(String status,String userId){
		log.debug("findSurveyByStatus starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findSurveyByStatusAndUserId");
		query.setString("status",status);
		query.setString("userId",userId);
		List<Surveyusermapping> surveyusermappings = (List<Surveyusermapping>)query.list();
		return surveyusermappings;
	}
	
	public List<Surveyusermapping> findSurveys(){
		log.debug("findSurveys starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findSurveys");
		//query.setString("userId",userId);
		List<Surveyusermapping> surveyusermappings = (List<Surveyusermapping>)query.list();
		return surveyusermappings;
	}
	
	
	public int updateSurveyStatusById(String status,Integer sumId){
		log.debug("updateSurveyStatusById starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("updateSurveyStatusById");
		query.setString("status",status);
		query.setInteger("sumId",sumId);
		int numOfRowsUpdated = query.executeUpdate();
		return numOfRowsUpdated;
	}
	
	public List<Surveyusermapping> findSurveysByStatusAndClientIdAndSurveyIdAndCreatedDate(String status,Integer clientId,Integer surveyId,String startDate,String endDate){
		log.debug("findSurveyByStatus starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findSurveysByStatusAndClientIdAndSurveyIdAndCreatedDate");
		query.setString("status",status);
		query.setInteger("clientId",clientId);
		query.setInteger("surveyId",surveyId);
		query.setString("startDate",startDate);
		query.setString("endDate",endDate);
		List<Surveyusermapping> surveyusermappings = (List<Surveyusermapping>)query.list();
		return surveyusermappings;
	}
	
	public List<Surveyusermapping> findSurveysByStatusAndSurveyIdAndCreatedDate(String status,Integer surveyId,String startDate,String endDate){
		log.debug("findSurveyByStatus starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findSurveysByStatusAndSurveyIdAndCreatedDate");
		query.setString("status",status);
		query.setInteger("surveyId",surveyId);
		query.setString("startDate",startDate);
		query.setString("endDate",endDate);
		List<Surveyusermapping> surveyusermappings = (List<Surveyusermapping>)query.list();
		return surveyusermappings;
	}
	
	
	public List<Surveyusermapping> findClientsBySurveyId(Integer surveyId){
		log.debug("findClientsBySurveyId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findClientsBySurveyId");
		query.setInteger("surveyId",surveyId);
		List<Surveyusermapping> surveyusermappings = (List<Surveyusermapping>)query.list();
		return surveyusermappings;
	}
	
	public Surveyusermapping findSurveyUMBySumId(Integer sumId){
		log.debug("findSurveyUMBySumId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findSurveyUMBySumId");
		query.setInteger("sumId",sumId);
		Surveyusermapping surveyusermappings = (Surveyusermapping)query.uniqueResult();
		return surveyusermappings;
	}
	
}
