package com.qwebservices.data.dao;
// default package
// Generated Jan 11, 2017 4:23:49 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qwebservices.data.domain.Questiongroup;
import com.qwebservices.data.domain.Vertical;

/**
 * Home object for domain model class Questiongroup.
 * @see .Questiongroup
 * @author Hibernate Tools
 */
@Repository("questiongroupDAO")
public class QuestiongroupDAO {

	private static final Log log = LogFactory.getLog(QuestiongroupDAO.class);

	@Autowired
	private SessionFactory sessionFactory;

	public void persist(Questiongroup transientInstance) {
		log.debug("persisting Questiongroup instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Questiongroup instance) {
		log.debug("attaching dirty Questiongroup instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Questiongroup instance) {
		log.debug("attaching clean Questiongroup instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Questiongroup persistentInstance) {
		log.debug("deleting Questiongroup instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Questiongroup merge(Questiongroup detachedInstance) {
		log.debug("merging Questiongroup instance");
		try {
			Questiongroup result = (Questiongroup) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Questiongroup findById(java.lang.Integer id) {
		log.debug("getting Questiongroup instance with id: " + id);
		try {
			Questiongroup instance = (Questiongroup) sessionFactory
					.getCurrentSession().get("Questiongroup", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Questiongroup instance) {
		log.debug("finding Questiongroup instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("Questiongroup")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	
	public List<Questiongroup> findQuestionGroupsBySumId(Integer sumId){
		log.debug("findQuestionGroupsBySurveyId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findQuestionGroupsBySurveyId");
		query.setInteger("surveyId",sumId);
		List<Questiongroup> qgs = (List<Questiongroup>)query.list();
		return qgs;
	}
	
	public List<Questiongroup> findQuestionGroupsBySumIdAndGroupId(Integer sumId, Integer questionGrpId){
		log.debug("findQuestionGroupsBySurveyIdAndGroupId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findQuestionGroupsBySurveyIdAndGroupId");
		query.setInteger("surveyId",sumId);
		query.setParameter("questionSetId",questionGrpId);
		List<Questiongroup> qgs = (List<Questiongroup>)query.list();
		return qgs;
	}
	
	
	public List<Questiongroup> findQuestionGroupsByGroupId(Integer questionGrpId){
		log.debug("findQuestionGroupsByGroupId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findQuestionGroup");
		query.setParameter("questionGrpId",questionGrpId);
		List<Questiongroup> qgs = (List<Questiongroup>)query.list();
		return qgs;
	}
	
	public List<Questiongroup> findQuestionGroupsByGroupIdUser(){
		log.debug("findQuestionGroupsByGroupId starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findQuestionGroupUser");
		List<Questiongroup> qgs = (List<Questiongroup>)query.list();
		return qgs;
	}
	
}
