package com.qwebservices.data.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qwebservices.data.domain.AppUser;
import com.qwebservices.data.domain.Client;
import com.qwebservices.data.domain.Question;
import com.qwebservices.data.domain.Questiongroup;
import com.qwebservices.data.domain.Surveyusermapping;
import com.qwebservices.infra.dto.UserDTO;

@Repository("reportsDAO")
public class ReportsDAO {

	private static final Log log = LogFactory
			.getLog(ReportsDAO.class);
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<AppUser> getReportsBasedOnClientId(int clientId){
		log.debug("Reports for account starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findAnswersByClientId");
		query.setParameter("clientId", clientId);
		List<AppUser> qgs = (List<AppUser>)query.list();
		return qgs;
		
	}
	
	public List<AppUser> getReportsForRespondants(){
		log.debug("Reports for account starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findAnswers");
		List<AppUser> qgs = (List<AppUser>)query.list();
		return qgs;
		
	}
	
	
	public List<AppUser> getOverallNpsScore(){
		log.debug("Get ovrall nps score in dao");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findNpsScore");
		List<AppUser> qgs = (List<AppUser>)query.list();
		return qgs;
	}
	
	public List<AppUser> getScoreForQuestionSet(int clientId){
		log.debug("Scores for question set 1");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findAnswersByClientIdAndQuestionSets");
		query.setParameter("clientId", clientId);
	
		List<AppUser> qgsForQuestionSet = (List<AppUser>)query.list();
		return qgsForQuestionSet;
	}
	
	public List<AppUser> getScoreForQuestionSets(){
		log.debug("Scores for question set 1");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findAnswersByQuestionSets");
	
		List<AppUser> qgsForQuestionSet = (List<AppUser>)query.list();
		return qgsForQuestionSet;
	}
	
	public List<AppUser> getScoreByClientId(int clientId){
		log.debug("Scores by client Id");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("answersByClientId");
		query.setParameter("clientId", clientId);
		List<AppUser> qgsForQuestionSet = (List<AppUser>)query.list();
		return qgsForQuestionSet;
	}
	public List<AppUser> getReportstop3BenefitsForOrganization() {
		log.debug("Reports for account starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findAppUsers");
		List<AppUser> qgs = (List<AppUser>)query.list();
		return qgs;
	}
	
	
	public List<AppUser> getListOfPromoters(){
		log.debug("List of Promoters");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findAppUsers");
		List<AppUser> qgsForQuestionSet = (List<AppUser>)query.list();
		return qgsForQuestionSet;
	}
	
	
	public String getClientNameByClientId(int clientId){
		String clientName = null;
		Query query = sessionFactory.getCurrentSession().getNamedQuery("clientNameByClientId");
		query.setParameter("clientId", clientId);
		clientName = (String)query.uniqueResult();
		return clientName;
	}
	
	public List<Integer> listOfQuestionIdForQuestionGroup(){
		log.debug("List of question id for a question group");
		List<Integer> listOfQuestionIds = null;
		Query query = sessionFactory.getCurrentSession().getNamedQuery("listOfQuestionIds");
		listOfQuestionIds = query.list();
		return listOfQuestionIds;
	}
	
	public List<Questiongroup> getQuestion(Integer clientId,Integer questiongrpId){
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findQuestiongroupByClientId");
		query.setParameter("clientId",clientId);
		//query.setParameter("qId",qId);
		query.setParameter("grpId",questiongrpId);
		System.out.println("this is query output " + query);
		List<Questiongroup> qg = (List<Questiongroup>)query.list();
		System.out.println(qg);
		return qg;
		
	}
	public List<AppUser> getScoreForQuestionSet(){
		log.debug("Scores for question set 1");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findAnswersByQuestionSets");
		List<AppUser> qgsForQuestionSet = (List<AppUser>)query.list();
		return qgsForQuestionSet;
	}


	public List<Integer> getListOfQuestionIdForGrpId() {
		log.debug("List of questions");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("questionIdForQuestionGrp");
		List<Integer> questionIds = query.list();
		return questionIds;
	}
	public List<AppUser> getReportsBasedOnRespondantId(long respondantId){
		log.debug("Reports for respondants starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findAnswersByRespondantId");
		query.setParameter("respondantId", respondantId);
		List<AppUser> qgs = (List<AppUser>)query.list();
		return qgs;
		
	}
	public List<AppUser> getRespondantScoreForQuestionSet(long respondantId){
		log.debug("Scores for question set for each respondant");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findAnswersByRespondantIdAndQuestionSets");
		query.setParameter("respondantId", respondantId);
	
		List<AppUser> qgsForQuestionSet = (List<AppUser>)query.list();
		return qgsForQuestionSet;
	}
	
	public List<Integer> getQuestionIdForQuestionGroup(){
		List<Integer> questionIds = new ArrayList<Integer>();
		log.debug("question id present in question group");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("listOfQuestionIds");
		questionIds = query.list();
		return questionIds;
	}
	
	public List<AppUser> mapOfSentAndRecievedCSAT(){
		List<AppUser> mapOfCompletedAndAssignedCSAT = null;
		Query query = sessionFactory.getCurrentSession().getNamedQuery("surveySentAndRecieved");
		mapOfCompletedAndAssignedCSAT =  query.list();
		return mapOfCompletedAndAssignedCSAT;
	}
	
	public List<AppUser> getReportBasedOnEngagementManager(String engagamentManager){
		log.debug("Reports for engagement manager starts in DAO ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findAnswersByEngagementManager");
		query.setParameter("engagamentManager", engagamentManager);
		List<AppUser> qgs = (List<AppUser>)query.list();
		return qgs;
		
	}
	
	
	public List<Client> getReportsForClient(String engagamentManager){
		log.debug("Reports for engagement manager starts in DAO based on client Name ");
		Query query = sessionFactory.getCurrentSession().getNamedQuery("findAnswersByClientName");
		query.setParameter("engagamentManager", engagamentManager);
		List<Client> qgs = (List<Client>)query.list();
		return qgs;
		
	}
	public List<Client> getClient(){

		log.debug("List of question id for a question group");

		List<Client> listOfClients = null;

		Query query = sessionFactory.getCurrentSession().getNamedQuery("listOfClients");

		listOfClients = query.list();

		return listOfClients;

		}
	
	public List<Question> listOfQuestionIdForNPS(){
		List<Question> listOfQuestionId = null;
		Query query = sessionFactory.getCurrentSession().getNamedQuery("listOfQuestionIdForNPS");
		listOfQuestionId = query.list();
		return listOfQuestionId;
	}
}
