package com.qwebservices.data.domain;

// default package
// Generated Jan 11, 2017 4:23:48 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Questiontype generated by hbm2java
 */
public class Questiontype implements java.io.Serializable {

	private Integer qtypeId;
	private String qtype;
	private Date createdDate;
	private Date modifiedDate;
	private Set questions = new HashSet(0);

	public Questiontype() {
	}

	public Questiontype(String qtype, Date createdDate, Date modifiedDate) {
		this.qtype = qtype;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
	}

	public Questiontype(String qtype, Date createdDate, Date modifiedDate,
			Set questions) {
		this.qtype = qtype;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.questions = questions;
	}

	public Integer getQtypeId() {
		return this.qtypeId;
	}

	public void setQtypeId(Integer qtypeId) {
		this.qtypeId = qtypeId;
	}

	public String getQtype() {
		return this.qtype;
	}

	public void setQtype(String qtype) {
		this.qtype = qtype;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Set getQuestions() {
		return this.questions;
	}

	public void setQuestions(Set questions) {
		this.questions = questions;
	}


}
