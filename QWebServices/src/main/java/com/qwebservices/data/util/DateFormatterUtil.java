package com.qwebservices.data.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatterUtil {

	public static String formatDate(Date date)
	{
		DateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");
		return dateFormatter.format(date);
	}
	
	public static String formatDate(String date) 
	{
		DateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");
		return dateFormatter.format(new Date(date));
	}
	
	public static String formatDateTime(Date date) 
	{
		DateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
		return dateFormatter.format(date);
	}
	
	public static String formatDateForCalendar(Date date)
	{
		DateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		return dateFormatter.format(date);
	}
	
	/*public static void main(String[] args) {
		DateFormat date = new SimpleDateFormat("dd-MMM-yy");
		trfDetails.setFormattedCreatedDate(date.format(trfDetails.getCreatedDate()));
		//System.out.println(trfDetails.getFormattedCreatedDate());
	}*/
	
}
