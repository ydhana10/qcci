package com.qwebservices.security;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.qwebservices.business.service.UserService;
import com.qwebservices.infra.dto.UserDTO;



/**
 * @author Syed Usman The Class TransformAuthenticationProvider.
 */
@SuppressWarnings("all")
class QWebServicesAuthenticationProvider implements AuthenticationProvider {

	/** The login service. */
	

	/** The logger. */
	private static Logger logger = Logger
			.getLogger(QWebServicesAuthenticationProvider.class);
	
	@Autowired
	UserService userService;
	
	@Autowired
	HttpServletRequest request;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.authentication.AuthenticationProvider#
	 * authenticate(org.springframework.security.core.Authentication)
	 */
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		String userName = authentication.getName().trim();
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String password = authentication.getCredentials().toString().trim();
		Authentication auth = null;
		UserDTO userDetails = null;
		if(attr.getRequest().getParameter("surveyId")==null || ((attr.getRequest().getParameter("surveyId")).trim()).isEmpty()){
			userDetails = userService.authenticateUser(userName);
		}else{
			int surveyId = Integer.parseInt(attr.getRequest().getParameter("surveyId"));
			userDetails = userService.authenticateUserBySurveyId(userName,surveyId);
		}
		if(userDetails == null){
			return null;
		}else{
			if(passwordEncoder.matches(password, userDetails.getPassword())){
				List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
				GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(
						(userDetails.getRole()).trim());
				authorities.add(0, grantedAuthority);
				UserDetails appUser = new User(userName, password, true, true,
						true, true, authorities);
				auth = new UsernamePasswordAuthenticationToken(appUser, password,
						authorities);
				return auth;
			}else{
				return null;
			}
		}

	}

	/**
	 * Supports.
	 *
	 * @param authentication
	 *            the authentication
	 * @return true, if successful
	 */
	@Override
	public boolean supports(Class<? extends Object> authentication) {
		return (UsernamePasswordAuthenticationToken.class
				.isAssignableFrom(authentication));
	}
	

}