package com.qwebservices.business.util.mail;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class TriggerMail {

	public void service() {

		String newLine = System.getProperty("line.separator");

		//String patient = "jaiswal8086@gmail.com";// change accordingly

		// Get the session object
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.starttl+s.enable", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								"waystable@gmail.com", "at@quinnox"); // replace
																		// your
																		// password
																		// with
																		// "xxxxxxxxxx";
					}
				});

		try {
			MimeMessage message = new MimeMessage(session);

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					""));
			message.setSubject("Welcome mail from Stable Services");

			message.setText("Your Client Number is : " + " clientnumber"
					+ "newLine" + "You had Requested for :" + " "
					+ "Description of your request : ");

			// message.setText("Thanks ..!!");

			Transport.send(message);

			// System.out.println("message sent successfully");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}

}
