package com.qwebservices.business.service;

import java.util.List;

import com.qwebservices.infra.dto.ClientDTO;

public interface ClientService {

	List<ClientDTO> listClients(int verticalId);

}
