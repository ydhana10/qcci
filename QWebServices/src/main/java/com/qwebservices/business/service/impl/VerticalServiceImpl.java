package com.qwebservices.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import jxl.common.Logger;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.qwebservices.business.service.VerticalService;
import com.qwebservices.data.dao.VerticalDAO;
import com.qwebservices.data.domain.AppUser;
import com.qwebservices.data.domain.Vertical;
import com.qwebservices.infra.dto.UserDTO;
import com.qwebservices.infra.dto.VerticalDTO;

@Transactional
public class VerticalServiceImpl implements VerticalService{
	
	public final static Logger log = Logger.getLogger(VerticalServiceImpl.class);
	
	@Autowired
	VerticalDAO verticalDAO;
	
	@Override
	public List<VerticalDTO> findVerticals(){
		Mapper mapper = new DozerBeanMapper();
		
		List<VerticalDTO> verticalDTOs = null;
		List<Vertical> verticals = verticalDAO.findActiveVerticals();
		if(verticals!=null && !verticals.isEmpty()){
			verticalDTOs = new ArrayList<VerticalDTO>();
			for (Vertical vertical : verticals) {
				VerticalDTO verticalDTO = new VerticalDTO();
				verticalDTO =  
					    mapper.map(vertical, VerticalDTO.class);
				//BeanUtils.copyProperties(verticalDTO,vertical);
				verticalDTOs.add(verticalDTO);
			}
		}
		return verticalDTOs;
	}
	
	@Override
	public List<UserDTO> findRespondants(int clientId){
		System.out.println("************ CLIENT ID *************"+clientId);
		List<AppUser> appUsers = verticalDAO.findRespondantsByClientId(clientId);
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			userDTO = mapper.map(appUser,UserDTO.class);
			userDTOs.add(userDTO);
		
	}
		return userDTOs;
}
	

}
