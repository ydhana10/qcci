package com.qwebservices.business.service;

import java.util.List;

import org.json.JSONArray;

import com.qwebservices.infra.dto.UserDTO;

public interface UserService {

	UserDTO authenticateUser(String username);

	UserDTO authenticateUser(String ssoId, String password);

	boolean insertUser(UserDTO userDTO);

	boolean uploadMultipleUsers(UserDTO userDTO, String filename)
			throws Exception;

	UserDTO authenticateUserBySurveyId(String userName, int surveyId);

	void sendNotification(Integer sumId);

	void sendNotificationToMany(String sumId);
	
	List<UserDTO> listEmanager(int clientId);

}
