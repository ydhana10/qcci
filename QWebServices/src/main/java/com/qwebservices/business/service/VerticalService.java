package com.qwebservices.business.service;

import java.util.List;

import com.qwebservices.data.domain.AppUser;
import com.qwebservices.infra.dto.UserDTO;
import com.qwebservices.infra.dto.VerticalDTO;

public interface VerticalService {

	List<VerticalDTO> findVerticals();

	List<UserDTO> findRespondants(int clientId);

}
