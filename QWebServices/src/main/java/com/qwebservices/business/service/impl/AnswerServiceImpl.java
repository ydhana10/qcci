package com.qwebservices.business.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.qwebservices.business.service.AnswerService;
import com.qwebservices.data.dao.AnswerDAO;
import com.qwebservices.data.dao.AppUserDAO;
import com.qwebservices.data.dao.SurveyusermappingDAO;
import com.qwebservices.data.domain.Answer;
import com.qwebservices.data.domain.AppUser;
import com.qwebservices.data.domain.Option;
import com.qwebservices.data.domain.Question;
import com.qwebservices.data.domain.Survey;
import com.qwebservices.data.domain.Surveyusermapping;
import com.qwebservices.infra.dto.OptionDTO;
import com.qwebservices.infra.dto.QuestionDTO;
import com.qwebservices.infra.dto.QuestionGroupDTO;
import com.qwebservices.infra.dto.SurveyDTO;
import com.qwebservices.web.constants.QWebConstants;

@Transactional
public class AnswerServiceImpl implements AnswerService{
	
	private static Logger log = Logger.getLogger(AnswerServiceImpl.class);
	
	@Autowired
	AnswerDAO answerDAO;
	
	@Autowired
	SurveyusermappingDAO surveyusermappingDAO;
	
	@Autowired
	AppUserDAO appUserDAO;
	
	@Override
	public Boolean insertAnswers(SurveyDTO surveyDTO){
		List<QuestionGroupDTO> questionGroupDTOs = surveyDTO.getQuestionGroupDTOs();
		for (QuestionGroupDTO questionGroupDTO : questionGroupDTOs) {
			List<QuestionDTO> questionDTOs = questionGroupDTO.getQuestions();
			for (QuestionDTO questionDTO : questionDTOs) {
				
				if(questionDTO.getQuestiontype().getQtype().equals("radiobutton")){
					Answer answer = new Answer();
					Surveyusermapping surveyusermapping = new Surveyusermapping();
					surveyusermapping.setSumId(surveyDTO.getSumId());
					answer.setSurveyusermapping(surveyusermapping);
					Option option = new Option();
					option.setOid(questionDTO.getOid());
					answer.setOption(option);
					Question question = new Question();
					question.setQid(questionDTO.getQid());
					answer.setQuestion(question);
					answerDAO.merge(answer);
				}else if(questionDTO.getQuestiontype().getQtype().equals("checkbox")){
					List<OptionDTO> optionDTOs = questionDTO.getOptionDTOs();
					for (OptionDTO optionDTO : optionDTOs) {
						Answer answer = new Answer();
						Surveyusermapping surveyusermapping = new Surveyusermapping();
						surveyusermapping.setSumId(surveyDTO.getSumId());
						answer.setSurveyusermapping(surveyusermapping);
						if(optionDTO!=null && optionDTO.getSel()!=null &&(optionDTO.getSel() == true)){
							Option option = new Option();
							option.setOid(optionDTO.getOid());
							answer.setOption(option);
							if(questionDTO.getAtext()!=null && !((questionDTO.getAtext().trim()).equals("")) && optionDTO.getOtext().equals("Any Other")){
								answer.setAtext(questionDTO.getAtext());
							}
							Question question = new Question();
							question.setQid(questionDTO.getQid());
							answer.setQuestion(question);
							answerDAO.merge(answer);
						}
					}
				}else if(questionDTO.getQuestiontype().getQtype().equals("text")){
					Answer answer = new Answer();
					Surveyusermapping surveyusermapping = new Surveyusermapping();
					surveyusermapping.setSumId(surveyDTO.getSumId());
					answer.setSurveyusermapping(surveyusermapping);
					answer.setAtext(questionDTO.getAtext());
					Question question = new Question();
					question.setQid(questionDTO.getQid());
					answer.setQuestion(question);
					answerDAO.merge(answer);
				}
				
				}
			}
			surveyusermappingDAO.updateSurveyStatusById(QWebConstants.STATUS_VALUES.COMPLETED, surveyDTO.getSumId());
			appUserDAO.deactivetUserBySSOId(surveyDTO.getUserDTO().getSsoId(),surveyDTO.getSumId());
		return true;
	}

}
