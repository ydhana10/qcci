package com.qwebservices.business.service;

import java.util.List;

import com.qwebservices.infra.dto.ClientDTO;
import com.qwebservices.infra.dto.QuestionGroupDTO;
import com.qwebservices.infra.dto.SurveyDTO;

public interface SurveyUserMappingsService {

	List<SurveyDTO> listSurvey(String userId, String status, Integer sumId);

	List<SurveyDTO> listSurveys();

	List<SurveyDTO> listSurveyUsersMappings(Integer surveyId, Integer clientId,
			String startDate, String endDate, String status);

	List<ClientDTO> clientBySurvey(Integer surveyId);

	List<QuestionGroupDTO> listQuestionGrpsBySumId(Integer sumId);

}
