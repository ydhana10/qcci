package com.qwebservices.business.service.impl;



import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.qwebservices.business.service.SurveyService;
import com.qwebservices.business.service.SurveyUserMappingsService;
import com.qwebservices.data.dao.ClientDAO;
import com.qwebservices.data.dao.SurveyDAO;
import com.qwebservices.data.dao.SurveyusermappingDAO;
import com.qwebservices.data.domain.Survey;
import com.qwebservices.data.domain.Surveyusermapping;
import com.qwebservices.infra.dto.AnswerDTO;
import com.qwebservices.infra.dto.QuestionDTO;
import com.qwebservices.infra.dto.QuestionGroupDTO;
import com.qwebservices.infra.dto.SurveyDTO;
import com.qwebservices.infra.util.ConfigValues;
import com.qwebservices.infra.util.QWebUtil;

@Transactional
public class SurveyServiceImpl implements SurveyService{
	
	public static Logger log = Logger.getLogger(SurveyServiceImpl.class);
	
	@Autowired
	SurveyusermappingDAO surveyusermappingDAO;
	
	@Autowired
	SurveyUserMappingsService surveyUserMappingsService;
	
	@Autowired
	SurveyDAO surveyDAO;
	
	@Autowired
	ClientDAO clientDAO;
	
	@Override
	public String generateExcelReport(Integer surveyId,Integer clientId,String startDate,String endDate,String status) throws FileNotFoundException, IOException{
		if(clientId==0){
			clientId = null;
		}
		status = "COMPLETED";
		List<Surveyusermapping> sums = null;
		Workbook book = new XSSFWorkbook();
		XSSFSheet sheet = (XSSFSheet)book.createSheet("Survey Results");
		Row headerrow = sheet.createRow(0);
		
		XSSFCellStyle style1 = (XSSFCellStyle) book.createCellStyle();
		style1.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
		style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
		Font font = book.createFont();
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBold(true);
		style1.setFont(font);
		
		Cell colh1 = headerrow.createCell(0);
		colh1.setCellValue("Group Sl No");
		colh1.setCellStyle(style1);
		Cell colh2 = headerrow.createCell(1);
		colh2.setCellValue("Question Group");
		colh2.setCellStyle(style1);
		
		Cell colh3 = headerrow.createCell(2);
		colh3.setCellValue("Question");
		colh3.setCellStyle(style1);
		
		Row headerrow2 = sheet.createRow(1);
		CellStyle style = book.createCellStyle();
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
		List<SurveyDTO> surveyDTOs = surveyUserMappingsService.listSurveyUsersMappings(surveyId, clientId, startDate, endDate, status);
		if(surveyDTOs!=null && !surveyDTOs.isEmpty()){
			boolean firstIteration = false;
			int numberOfQuestions = 0;
			int clientIndex = 3;
			for (SurveyDTO surveyDTO : surveyDTOs) {
				numberOfQuestions = 0;
				int rowIndex = 2;
				//int clientIndex = 3;
				Cell colh4 = headerrow.createCell(clientIndex);
				Cell colhdummy1 = headerrow.createCell(clientIndex+1);
				Cell colhdummy2 = headerrow.createCell(clientIndex+2);
				colh4.setCellValue(surveyDTO.getUserDTO().getClientName()+" Feedback");
				colh4.setCellStyle(style1);
				Cell colh5 = headerrow2.createCell(clientIndex+1);
				colh5.setCellValue("Client Name");
				colh5.setCellStyle(style1);
				Cell colh6 = headerrow2.createCell(clientIndex+2);
				colh6.setCellValue("Completion Date");
				colh6.setCellStyle(style1);
				Cell colh7 = headerrow2.createCell(clientIndex);
				colh7.setCellValue("Feedback");
				colh7.setCellStyle(style1);
				
				//merge client headers 
				CellReference startCellRefh= new CellReference(colh4.getRowIndex(),colh4.getColumnIndex());
			    CellReference endCellRefh = new CellReference(colhdummy2.getRowIndex(),colhdummy2.getColumnIndex());
			    String cellRefernceh = startCellRefh.formatAsString()+":"+endCellRefh.formatAsString();
			    cellRefernceh = cellRefernceh.replace("$","");
			    CellRangeAddress regionh = CellRangeAddress.valueOf(cellRefernceh);
			    colh4.getRow().getSheet().addMergedRegion( regionh );
			    //
			    //CellUtil.setAlignment(colh4, book, CellStyle.ALIGN_CENTER);
			    colh4.setCellStyle(style1);
				//End
				
				
				if(firstIteration==false){
					firstIteration = true;
					List<QuestionGroupDTO> qgds = surveyDTO.getQuestionGroupDTOs();
					int qgIndex = 1;
					for (QuestionGroupDTO questionGroupDTO : qgds) {
						List<QuestionDTO> questionDTOs = questionGroupDTO.getQuestions();
						Cell startCell = null;
						Cell endCell = null;
						Cell startCell1 = null;
						Cell endCell1 = null;
						numberOfQuestions += questionDTOs.size();
						for (int i=0;i<questionDTOs.size();i++) {
							
							QuestionDTO questionDTO = questionDTOs.get(i);
							Row row = sheet.createRow(rowIndex++); 
							Cell col1 = row.createCell(0);
							col1.setCellValue(qgIndex);
							
							Cell col2 = row.createCell(1);
							col2.setCellValue(questionGroupDTO.getGrpName());
							if(i==0){
								startCell = col1;
								startCell1 = col2;
							}
							if(i==questionDTOs.size()-1){
								endCell = col1;
								endCell1 = col2;
							}
							
							Cell col3 = row.createCell(2);
							col3.setCellValue(questionDTO.getQtext());
							List<AnswerDTO> answerDTOs = questionDTO.getAnswerDTOs();
							String answer = "";
							String answerDate = "";
							for (AnswerDTO answerDTO : answerDTOs) {
								if(questionDTO.getQuestiontype().getQtype().equals("radiobutton")){
									answer = answerDTO.getOption().getOtext();
								}else if(questionDTO.getQuestiontype().getQtype().equals("checkbox")){
									if(answerDTO.getAtext()!=null && !((answerDTO.getAtext().trim()).equals(""))){
										answer += answerDTO.getOption().getOtext()+" - "+answerDTO.getAtext();
									}else{
										answer += answerDTO.getOption().getOtext()+" ";
									}
								}else if(questionDTO.getQuestiontype().getQtype().equals("text")){
									answer += answerDTO.getAtext();
								}
								answerDate = answerDTO.getCreatedDate()+"";
							}
							Cell col4 = row.createCell(clientIndex);
							col4.setCellValue(answer);
							Cell col5 = row.createCell(clientIndex+1);
							col5.setCellValue(surveyDTO.getUserDTO().getFirstName()+" "+surveyDTO.getUserDTO().getLastName());
							Cell col6 = row.createCell(clientIndex+2);
							col6.setCellValue(answerDate);
						}
						//merge grp id
					    CellReference startCellRef= new CellReference(startCell.getRowIndex(),startCell.getColumnIndex());
					    CellReference endCellRef = new CellReference(endCell.getRowIndex(),endCell.getColumnIndex());
					    String cellRefernce = startCellRef.formatAsString()+":"+endCellRef.formatAsString();
					    cellRefernce = cellRefernce.replace("$","");
					    CellRangeAddress region = CellRangeAddress.valueOf(cellRefernce);
					    startCell.getRow().getSheet().addMergedRegion( region );
					    //CellUtil.setAlignment(startCell, book, CellStyle.ALIGN_CENTER);
					    startCell.setCellStyle(style);
						//End
					  //merge grp name
					    CellReference startCellRef1= new CellReference(startCell1.getRowIndex(),startCell1.getColumnIndex());
					    CellReference endCellRef1 = new CellReference(endCell1.getRowIndex(),endCell1.getColumnIndex());
					    String cellRefernce1 = startCellRef1.formatAsString()+":"+endCellRef1.formatAsString();
					    cellRefernce1 = cellRefernce1.replace("$","");
					    CellRangeAddress region1 = CellRangeAddress.valueOf(cellRefernce1);
					    startCell1.getRow().getSheet().addMergedRegion( region1 );
					    //CellUtil.setAlignment(startCell1, book, CellStyle.ALIGN_CENTER);
					    startCell1.setCellStyle(style);
						//End
						qgIndex++;
					}
					
					
				}else{

					firstIteration = true;
					List<QuestionGroupDTO> qgds = surveyDTO.getQuestionGroupDTOs();
					int qgIndex = 1;
					for (QuestionGroupDTO questionGroupDTO : qgds) {
						List<QuestionDTO> questionDTOs = questionGroupDTO.getQuestions();
						Cell startCell = null;
						Cell endCell = null;
						Cell startCell1 = null;
						Cell endCell1 = null;
						numberOfQuestions += questionDTOs.size();
						for (int i=0;i<questionDTOs.size();i++) {
							QuestionDTO questionDTO = questionDTOs.get(i);
							Row row = sheet.getRow(rowIndex++);
							List<AnswerDTO> answerDTOs = questionDTO.getAnswerDTOs();
							String answer = "";
							
							String answerDate = "";
							for (AnswerDTO answerDTO : answerDTOs) {
								if(questionDTO.getQuestiontype().getQtype().equals("radiobutton")){
									answer = answerDTO.getOption().getOtext();
								}else if(questionDTO.getQuestiontype().getQtype().equals("checkbox")){
									answer += answerDTO.getOption().getOtext()+" ";
								}else if(questionDTO.getQuestiontype().getQtype().equals("text")){
									answer += answerDTO.getAtext();
								}
								answerDate = answerDTO.getCreatedDate()+"";
							}
							CellStyle stylewrap = book.createCellStyle(); //Create new style
				            style.setWrapText(true);
							Cell col4 = row.createCell(clientIndex);
							col4.setCellValue(answer);
							col4.setCellStyle(stylewrap);
							Cell col5 = row.createCell(clientIndex+1);
							col5.setCellValue(surveyDTO.getUserDTO().getFirstName()+" "+surveyDTO.getUserDTO().getLastName());
							Cell col6 = row.createCell(clientIndex+2);
							col6.setCellValue(answerDate);
						}
						qgIndex++;
					}
					
					
				
				}
				clientIndex = clientIndex+3;
			}
			for(int j=0;j<(surveyDTOs.size()*3)+3;j++){
				sheet.autoSizeColumn(j);
			}
			for(int j=3;j<(surveyDTOs.size()*3)+3;j=j+3){
				Cell cname = sheet.getRow(2).getCell(j+1);
				Cell cnamel = sheet.getRow(2+(numberOfQuestions-1)).getCell(j+1);
				CellReference startCellRef1= new CellReference(cname.getRowIndex(),cname.getColumnIndex());
				CellReference endCellRef1 = new CellReference(cnamel.getRowIndex(),cnamel.getColumnIndex());
				String cellRefernce1 = startCellRef1.formatAsString()+":"+endCellRef1.formatAsString();
				cellRefernce1 = cellRefernce1.replace("$","");
				CellRangeAddress region1 = CellRangeAddress.valueOf(cellRefernce1);
				cname.getRow().getSheet().addMergedRegion( region1 );
				
				cname.setCellStyle(style);
				
				Cell cdate = sheet.getRow(2).getCell(j+2);
				Cell cdatel = sheet.getRow(2+(numberOfQuestions-1)).getCell(j+2);
				
				CellReference startCellRef22= new CellReference(cdate.getRowIndex(),cdate.getColumnIndex());
				CellReference endCellRef22 = new CellReference(cdatel.getRowIndex(),cdatel.getColumnIndex());
				String cellRefernce22 = startCellRef22.formatAsString()+":"+endCellRef22.formatAsString();
				cellRefernce22 = cellRefernce22.replace("$","");
				CellRangeAddress region22 = CellRangeAddress.valueOf(cellRefernce22);
				cdate.getRow().getSheet().addMergedRegion( region22 );
				cdate.setCellStyle(style);
			}
			
		}
		String file = (QWebUtil.generateRandomPassword()+" Survey "+surveyDTOs.get(0).getSname()+"-"+startDate+" to "+endDate+".xlsx");
		book.write(new FileOutputStream(System.getProperty("catalina.base")+ConfigValues.getConfigValue("DOWNLOAD_LOC")+file));
		book.close();
		return file;
	}
	
	@Override
	public List<SurveyDTO> surveyDropdown(){
		List<SurveyDTO> surveyDTOs = null;
		List<Survey> surveys = surveyDAO.findSurveys();
		if(surveys!=null && !surveys.isEmpty()){
			surveyDTOs = new ArrayList<SurveyDTO>();
			for (Survey survey : surveys) {
				SurveyDTO surveyDTO = new SurveyDTO();
				surveyDTO.setSid(survey.getSid());
				surveyDTO.setSname(survey.getSname());
				surveyDTOs.add(surveyDTO);
			}
		}
		return surveyDTOs;
	}
	
	

}
