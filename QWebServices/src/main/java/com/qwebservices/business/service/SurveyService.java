package com.qwebservices.business.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.qwebservices.infra.dto.SurveyDTO;

public interface SurveyService {

	String generateExcelReport(Integer surveyId, Integer clientId,
			String startDate, String endDate, String status)
			throws FileNotFoundException, IOException;

	List<SurveyDTO> surveyDropdown();

}
