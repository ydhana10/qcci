package com.qwebservices.business.service;

import com.qwebservices.infra.dto.SurveyDTO;

public interface AnswerService {

	Boolean insertAnswers(SurveyDTO surveyDTO);

}
