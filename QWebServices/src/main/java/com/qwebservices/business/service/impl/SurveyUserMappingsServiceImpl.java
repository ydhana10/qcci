package com.qwebservices.business.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.qwebservices.business.service.SurveyUserMappingsService;
import com.qwebservices.data.dao.AppUserDAO;
import com.qwebservices.data.dao.QuestiongroupDAO;
import com.qwebservices.data.dao.SurveyusermappingDAO;
import com.qwebservices.data.domain.Answer;
import com.qwebservices.data.domain.AppUser;
import com.qwebservices.data.domain.Client;
import com.qwebservices.data.domain.Option;
import com.qwebservices.data.domain.Question;
import com.qwebservices.data.domain.Questiongroup;
import com.qwebservices.data.domain.Surveyusermapping;
import com.qwebservices.infra.dto.AnswerDTO;
import com.qwebservices.infra.dto.ClientDTO;
import com.qwebservices.infra.dto.OptionDTO;
import com.qwebservices.infra.dto.QuestionDTO;
import com.qwebservices.infra.dto.QuestionGroupDTO;
import com.qwebservices.infra.dto.SurveyDTO;
import com.qwebservices.infra.dto.UserDTO;

@Transactional
public class SurveyUserMappingsServiceImpl implements SurveyUserMappingsService {

	private static final Logger log = Logger.getLogger(SurveyUserMappingsServiceImpl.class);

	@Autowired
	SurveyusermappingDAO surveyusermappingDAO;

	@Autowired
	QuestiongroupDAO questiongroupDAO;

	@Autowired
	AppUserDAO appUserDAO;

	@Override
	public List<SurveyDTO> listSurvey(String userId, String status, Integer sumId) {
		List<SurveyDTO> surveyDTOs = null;
		UserDTO userDTO = new UserDTO();
		userDTO.setEmail(userId);
		userDTO.setSsoId(userId);
		Mapper mapper = new DozerBeanMapper();
		List<Surveyusermapping> sums = surveyusermappingDAO.findSurveyByStatus(status, userId);
		if (sums != null && !sums.isEmpty()) {
			surveyDTOs = new ArrayList<SurveyDTO>();
			for (Surveyusermapping sum : sums) {
				if (sum.getSumId().intValue() == sumId.intValue()) {
					SurveyDTO surveyDTO = new SurveyDTO();
					surveyDTO = mapper.map(sum, SurveyDTO.class);
					List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
					surveyDTO = mapper.map(sum.getSurvey(), SurveyDTO.class);
					surveyDTO.setUserDTO(userDTO);
					surveyDTO.setSumId(sum.getSumId());
					surveyDTO.setNotificationCount(sum.getNotificationCount());
					List<Questiongroup> qgs = questiongroupDAO.findQuestionGroupsBySumId(sum.getSumId());
					for (Questiongroup questiongroup : qgs) {
						QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
						questionGroupDTO = mapper.map(questiongroup, QuestionGroupDTO.class);
						Set<Question> qs = questiongroup.getQuestions();
						List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
						for (Question question : qs) {
							QuestionDTO questionDTO = new QuestionDTO();
							questionDTO = mapper.map(question, QuestionDTO.class);
							Set<Answer> answers = question.getAnswers();
							List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
							if (answers != null && !answers.isEmpty()) {
								for (Answer answer : answers) {
									if (answer.getSurveyusermapping().getSumId().intValue() == sum.getSumId().intValue()) {
										AnswerDTO answerDTO = new AnswerDTO();
										answerDTO = mapper.map(answer, AnswerDTO.class);
										answerDTOs.add(answerDTO);
									}
								}
							}
							questionDTO.setAnswerDTOs(answerDTOs);
							Set<Option> options = question.getOptions();
							List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
							for (Option option : options) {
								OptionDTO optionDTO = new OptionDTO();
								optionDTO = mapper.map(option, OptionDTO.class);
								optionDTO.setQid(question.getQid());
								optionDTOs.add(optionDTO);
							}
							questionDTO.setOptionDTOs(optionDTOs);
							questionDTOs.add(questionDTO);
						}
						questionGroupDTO.setQuestions(questionDTOs);
						questionGroupDTOs.add(questionGroupDTO);
					}
					surveyDTO.setQuestionGroupDTOs(questionGroupDTOs);

					AppUser appUser = appUserDAO.findUserBySSOIdAndSurveyId(userId, surveyDTO.getSid());

					if (appUser != null) {
						surveyDTO.getUserDTO().setFirstName(appUser.getFirstName());
						surveyDTO.getUserDTO().setLastName(appUser.getLastName());
						Client client = appUser.getClient();
						if (client != null) {
							surveyDTO.getUserDTO().setClientId(client.getClientId());
							surveyDTO.getUserDTO().setClientName(client.getClientName());
							surveyDTO.getUserDTO().setVerticalName(client.getVertical().getVerticalName());
						}
					}
					surveyDTOs.add(surveyDTO);
				}
			}

		}
		return surveyDTOs;

	}

	@Override
	public List<SurveyDTO> listSurveys() {
		List<SurveyDTO> surveyDTOs = null;

		Mapper mapper = new DozerBeanMapper();
		List<Surveyusermapping> sums = surveyusermappingDAO.findSurveys();
		if (sums != null && !sums.isEmpty()) {
			surveyDTOs = new ArrayList<SurveyDTO>();
			for (Surveyusermapping sum : sums) {
				UserDTO userDTO = new UserDTO();
				SurveyDTO surveyDTO = new SurveyDTO();
				surveyDTO = mapper.map(sum, SurveyDTO.class);
				surveyDTO = mapper.map(sum.getSurvey(), SurveyDTO.class);
				surveyDTO.setUserDTO(userDTO);
				surveyDTO.setSumId(sum.getSumId());
				surveyDTO.setNotificationCount(sum.getNotificationCount());
				surveyDTO.setStatus(sum.getStatus());
				

				AppUser appUser = sum.getAppUser();
				userDTO.setSsoId(appUser.getSsoId());
				userDTO.setEmail(appUser.getSsoId());
				if (appUser != null) {
					surveyDTO.getUserDTO().setFirstName(appUser.getFirstName());
					surveyDTO.getUserDTO().setLastName(appUser.getLastName());
					Client client = appUser.getClient();
					if (client != null) {
						surveyDTO.getUserDTO().setClientId(client.getClientId());
						surveyDTO.getUserDTO().setClientName(client.getClientName());
						surveyDTO.getUserDTO().setVerticalName(client.getVertical().getVerticalName());
					}
				}
				surveyDTOs.add(surveyDTO);
			}

		}
		return surveyDTOs;

	}
	
	@Override
	public List<QuestionGroupDTO> listQuestionGrpsBySumId(Integer sumId){
		Mapper mapper = new DozerBeanMapper();
		List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
		List<Questiongroup> qgs = questiongroupDAO.findQuestionGroupsBySumId(sumId);
		for (Questiongroup questiongroup : qgs) {
			QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
			questionGroupDTO = mapper.map(questiongroup, QuestionGroupDTO.class);
			Set<Question> qs = questiongroup.getQuestions();
			List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
			for (Question question : qs) {
				QuestionDTO questionDTO = new QuestionDTO();
				questionDTO = mapper.map(question, QuestionDTO.class);
				Set<Answer> answers = question.getAnswers();
				List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
				if (answers != null && !answers.isEmpty()) {
					for (Answer answer : answers) {
						if (answer.getSurveyusermapping().getSumId().intValue() == sumId.intValue()) {
							AnswerDTO answerDTO = new AnswerDTO();
							answerDTO = mapper.map(answer, AnswerDTO.class);
							answerDTOs.add(answerDTO);
						}
					}
				}
				questionDTO.setAnswerDTOs(answerDTOs);
				Set<Option> options = question.getOptions();
				List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
				for (Option option : options) {
					OptionDTO optionDTO = new OptionDTO();
					optionDTO = mapper.map(option, OptionDTO.class);
					optionDTOs.add(optionDTO);
				}
				questionDTO.setOptionDTOs(optionDTOs);
				questionDTOs.add(questionDTO);
			}
			questionGroupDTO.setQuestions(questionDTOs);
			questionGroupDTOs.add(questionGroupDTO);
		}
		return questionGroupDTOs;
	}

	@Override
	public List<SurveyDTO> listSurveyUsersMappings(Integer surveyId, Integer clientId, String startDate,
			String endDate, String status) {
		List<SurveyDTO> surveyDTOs = null;

		Mapper mapper = new DozerBeanMapper();
		List<Surveyusermapping> sums = null;
		if (clientId == null) {
			sums = surveyusermappingDAO.findSurveysByStatusAndSurveyIdAndCreatedDate(status, surveyId, startDate,
					endDate);
		} else {
			sums = surveyusermappingDAO.findSurveysByStatusAndClientIdAndSurveyIdAndCreatedDate(status, clientId,
					surveyId, startDate, endDate);

		}
		if (sums != null && !sums.isEmpty()) {
			surveyDTOs = new ArrayList<SurveyDTO>();
			for (Surveyusermapping sum : sums) {
				UserDTO userDTO = new UserDTO();
				SurveyDTO surveyDTO = new SurveyDTO();
				surveyDTO = mapper.map(sum, SurveyDTO.class);
				List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
				surveyDTO = mapper.map(sum.getSurvey(), SurveyDTO.class);
				surveyDTO.setUserDTO(userDTO);
				surveyDTO.setSumId(sum.getSumId());
				surveyDTO.setNotificationCount(sum.getNotificationCount());
				surveyDTO.setStatus(sum.getStatus());
				List<Questiongroup> qgs = questiongroupDAO.findQuestionGroupsBySumId(sum.getSumId());
				for (Questiongroup questiongroup : qgs) {
					QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
					questionGroupDTO = mapper.map(questiongroup, QuestionGroupDTO.class);
					Set<Question> qs = questiongroup.getQuestions();
					List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
					for (Question question : qs) {
						QuestionDTO questionDTO = new QuestionDTO();
						questionDTO = mapper.map(question, QuestionDTO.class);
						Set<Answer> answers = question.getAnswers();
						List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
						if (answers != null && !answers.isEmpty()) {
							for (Answer answer : answers) {
								if (answer.getSurveyusermapping().getSumId().intValue() == sum.getSumId().intValue()) {
									AnswerDTO answerDTO = new AnswerDTO();
									answerDTO = mapper.map(answer, AnswerDTO.class);
									answerDTOs.add(answerDTO);
								}
							}
						}
						questionDTO.setAnswerDTOs(answerDTOs);
						Set<Option> options = question.getOptions();
						List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
						for (Option option : options) {
							OptionDTO optionDTO = new OptionDTO();
							optionDTO = mapper.map(option, OptionDTO.class);
							optionDTOs.add(optionDTO);
						}
						questionDTO.setOptionDTOs(optionDTOs);
						questionDTOs.add(questionDTO);
					}
					questionGroupDTO.setQuestions(questionDTOs);
					questionGroupDTOs.add(questionGroupDTO);
				}
				surveyDTO.setQuestionGroupDTOs(questionGroupDTOs);

				AppUser appUser = sum.getAppUser();
				userDTO.setSsoId(appUser.getSsoId());
				userDTO.setEmail(appUser.getSsoId());
				if (appUser != null) {
					surveyDTO.getUserDTO().setFirstName(appUser.getFirstName());
					surveyDTO.getUserDTO().setLastName(appUser.getLastName());
					Client client = appUser.getClient();
					if (client != null) {
						surveyDTO.getUserDTO().setClientId(client.getClientId());
						surveyDTO.getUserDTO().setClientName(client.getClientName());
						surveyDTO.getUserDTO().setVerticalName(client.getVertical().getVerticalName());
					}
				}
				surveyDTOs.add(surveyDTO);
			}

		}
		return surveyDTOs;

	}

	@Override
	public List<ClientDTO> clientBySurvey(Integer surveyId) {
		List<Surveyusermapping> sums = surveyusermappingDAO.findClientsBySurveyId(surveyId);
		List<ClientDTO> clients = null;
		Map<String, String> map = new HashMap<String, String>();
		if (sums != null && !sums.isEmpty()) {
			clients = new ArrayList<ClientDTO>();
			for (Surveyusermapping surveyusermapping : sums) {
				ClientDTO clientDTO = new ClientDTO();
				if (!map.containsKey((surveyusermapping.getAppUser().getClient().getClientId() + ""))) {
					clientDTO.setClientId(surveyusermapping.getAppUser().getClient().getClientId());
					clientDTO.setClientName(surveyusermapping.getAppUser().getClient().getClientName());
					clients.add(clientDTO);
					map.put("" + surveyusermapping.getAppUser().getClient().getClientId(), surveyusermapping
							.getAppUser().getClient().getClientName());
				}
			}
		}
		return clients;
	}

}
