package com.qwebservices.business.service.impl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.qwebservices.business.service.ReportsService;
import com.qwebservices.data.dao.AppUserDAO;
import com.qwebservices.data.dao.QuestiongroupDAO;
import com.qwebservices.data.dao.ReportsDAO;
import com.qwebservices.data.dao.SurveyusermappingDAO;
import com.qwebservices.data.domain.Answer;
import com.qwebservices.data.domain.AppUser;
import com.qwebservices.data.domain.Client;
import com.qwebservices.data.domain.Option;
import com.qwebservices.data.domain.Question;
import com.qwebservices.data.domain.Questiongroup;
import com.qwebservices.data.domain.Surveyusermapping;
import com.qwebservices.infra.dto.AnswerDTO;
import com.qwebservices.infra.dto.ClientDTO;
import com.qwebservices.infra.dto.OptionDTO;
import com.qwebservices.infra.dto.QuestionDTO;
import com.qwebservices.infra.dto.QuestionGroupDTO;
import com.qwebservices.infra.dto.UserDTO;

@Transactional
public class ReportsServiceImpl implements ReportsService {
	private static final Logger log = Logger
			.getLogger(ReportsServiceImpl.class);

	@Autowired
	SurveyusermappingDAO surveyusermappingDAO;

	@Autowired
	QuestiongroupDAO questiongroupDAO;

	@Autowired
	AppUserDAO appUserDAO;

	@Autowired
	ReportsDAO reportsDAO;

	@Override
	public HashMap<String, Float> getOverallNpsForAccount(Integer clientId) {
		int avgNps = 0;
		int countOfDictractors = 0;
		int countOfPromoters = 0;
		int countOfNeutral = 0;
		int totalNumberOfRespondants = 0;
		float percentageOfDitractors = 0;
		float percentageOfPromoters = 0;
		float percentageOfNeutal = 0;
		HashMap<String, Float> mapOfNps = new HashMap<String, Float>();

		List<AppUser> appUsers = reportsDAO.getReportsBasedOnClientId(clientId);
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			Integer sumId = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next()).getSumId();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumId(sumId);

			for (Questiongroup questiongroup : qgs) {
				QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
				questionGroupDTO = mapper.map(questiongroup,
						QuestionGroupDTO.class);
				Set<Question> qs = questiongroup.getQuestions();
				List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
				for (Question question : qs) {
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					if (answers != null && !answers.isEmpty()) {
						for (Answer answer : answers) {
							if (answer.getSurveyusermapping().getSumId() == sumId) {
								if (questiongroup.getGrpId() == 5) {
									avgNps = Integer.parseInt(answer
											.getOption().getOtext());
									if (Integer.parseInt(answer.getOption()
											.getOtext()) > 0
											&& Integer.parseInt(answer
													.getOption().getOtext()) <= 6) {
										countOfDictractors++;
									} else if (Integer.parseInt(answer
											.getOption().getOtext()) == 7
											|| Integer.parseInt(answer
													.getOption().getOtext()) == 8) {
										countOfNeutral++;
									} else if (Integer.parseInt(answer
											.getOption().getOtext()) == 9
											|| Integer.parseInt(answer
													.getOption().getOtext()) == 10) {
										countOfPromoters++;
									}

								}
								AnswerDTO answerDTO = new AnswerDTO();
								answerDTO = mapper.map(answer, AnswerDTO.class);
								answerDTOs.add(answerDTO);
							}
						}
					}
					questionDTO.setAnswerDTOs(answerDTOs);
					Set<Option> options = question.getOptions();
					List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
					for (Option option : options) {
						OptionDTO optionDTO = new OptionDTO();
						optionDTO = mapper.map(option, OptionDTO.class);
						optionDTOs.add(optionDTO);
					}
					questionDTO.setOptionDTOs(optionDTOs);
					questionDTOs.add(questionDTO);
				}
				questionGroupDTO.setQuestions(questionDTOs);
				questionGroupDTOs.add(questionGroupDTO);
			}

			userDTO.setQuestionGroupDTOs(questionGroupDTOs);
			userDTO.setAvgNps(avgNps);
			userDTO.setCountOfDitractors(countOfDictractors);
			userDTO.setCountOfNeutral(countOfNeutral);
			userDTO.setCountOfPromoters(countOfPromoters);

			// retriving count for nps

			totalNumberOfRespondants = countOfDictractors + countOfPromoters
					+ countOfNeutral;
			if (totalNumberOfRespondants != 0) {
				percentageOfPromoters = ((countOfPromoters * 100) / totalNumberOfRespondants);
				percentageOfNeutal = ((countOfNeutral * 100) / totalNumberOfRespondants);
				percentageOfDitractors = ((countOfDictractors * 100) / totalNumberOfRespondants);

				userDTO.setCountOfPromoters(countOfPromoters);
				userDTO.setCountOfNeutral(countOfNeutral);
				userDTO.setCountOfDitractors(countOfDictractors);
				userDTO.setTotalRespondants(countOfPromoters + countOfNeutral
						+ countOfDictractors);

				percentageOfPromoters = ((countOfPromoters * 100) / totalNumberOfRespondants);
				percentageOfNeutal = ((countOfNeutral * 100) / totalNumberOfRespondants);
				percentageOfDitractors = ((countOfDictractors * 100) / totalNumberOfRespondants);

				userDTO.setDictractorsPercentage(percentageOfDitractors);
				userDTO.setNeutralPercentage(percentageOfNeutal);
				userDTO.setPromotersPercentage(percentageOfPromoters);

			}
			userDTOs.add(userDTO);

		}

		mapOfNps.put("Ditractors", percentageOfDitractors);
		mapOfNps.put("Promoters", percentageOfPromoters);
		mapOfNps.put("Neutral", percentageOfNeutal);
		mapOfNps.put("AccountNps", percentageOfPromoters
				- percentageOfDitractors);

		return mapOfNps;
	}

	@Override
	public String getClientNameByClientId(int clientId) {
		String clientName = reportsDAO.getClientNameByClientId(clientId);
		return clientName;
	}

	@Override
	public List<String> getTop3BenefitsForAccount(Integer clientId) {
		List<AppUser> appUsers = reportsDAO.getReportsBasedOnClientId(clientId);
		List<String> topThreeBenefits = new ArrayList<String>();
		List<String> finalBenefits = new ArrayList<String>();
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			Integer sumId = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next()).getSumId();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumId(sumId);
			// int avgNps = 0;
			for (Questiongroup questiongroup : qgs) {
				QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
				questionGroupDTO = mapper.map(questiongroup,
						QuestionGroupDTO.class);
				Set<Question> qs = questiongroup.getQuestions();
				List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
				for (Question question : qs) {
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					if (answers != null && !answers.isEmpty()) {
						for (Answer answer : answers) {
							if (answer.getSurveyusermapping().getSumId()
									.intValue() == sumId.intValue()) {
								if (question
										.getQuestiongroup()
										.getGrpDesc()
										.equalsIgnoreCase(
												"Quinnox's Overall Engagement")) {
									if ((question.getQname()
											.equalsIgnoreCase("Please describe the Top 3 Benefits when working with Quinnox"))) {
										String repondantBenefits = answer
												.getOption().getOtext();
										topThreeBenefits.add(repondantBenefits);
									}

								}
								AnswerDTO answerDTO = new AnswerDTO();
								answerDTO = mapper.map(answer, AnswerDTO.class);
								answerDTOs.add(answerDTO);
							}
						}
					}
					questionDTO.setAnswerDTOs(answerDTOs);
					Set<Option> options = question.getOptions();
					List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
					for (Option option : options) {
						OptionDTO optionDTO = new OptionDTO();
						optionDTO = mapper.map(option, OptionDTO.class);
						optionDTOs.add(optionDTO);
					}
					questionDTO.setOptionDTOs(optionDTOs);
					questionDTOs.add(questionDTO);
				}
				questionGroupDTO.setQuestions(questionDTOs);
				questionGroupDTOs.add(questionGroupDTO);
			}
			userDTO.setQuestionGroupDTOs(questionGroupDTOs);
			userDTO.setRespondantTopThreeBenefits(topThreeBenefits);
			userDTOs.add(userDTO);
		}
		for (int i = 0; i <= 2; i++) {
			finalBenefits.add(topThreeBenefits.get(i));
		}

		return finalBenefits;
	}

	@Override
	public List<UserDTO> getAverageScoresForEachQestionGrpForAccount(Integer clientId,
			Integer questionGrpId) {
		List<AppUser> appUsers = reportsDAO.getScoreForQuestionSet(clientId);

		Mapper mapper = new DozerBeanMapper();

		List<UserDTO> userDTOs = new ArrayList<UserDTO>();

		int counter = 0;

		int questionCounter = 0;

		int nonZeroAvg1 = 0, nonZeroAvg2 = 0, nonZeroAvg3 = 0, nonZeroAvg4 = 0, nonZeroAvg5 = 0, nonZeroAvg6 = 0;

		double questionavg1 = 0;
		// int nonZeroQuestionCounter1 = 0,nonZeroQuestionCounter2 =
		// 0,nonZeroQuestionCounter3 = 0,nonZeroQuestionCounter4 =
		// 0,nonZeroQuestionCounter5 = 0,nonZeroQuestionCounter = 0;

		double questionavg2 = 0;

		double questionavg3 = 0;

		double questionavg4 = 0;

		double questionavg5 = 0;

		double questionavg6 = 0;

		NumberFormat format = new DecimalFormat("#.##");

		UserDTO userDTO = new UserDTO();

		for (AppUser appUser : appUsers) {

			counter++;

			userDTO = mapper.map(appUser, UserDTO.class);

			Integer sumId = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next()).getSumId();

			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();

			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumIdAndGroupId(sumId, questionGrpId);

			double avgNps = 0;
			int nonZeroTotal = 0;

			double avg = 0;

			int qLength = 1;

			for (Questiongroup questiongroup : qgs) {

				QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();

				questionGroupDTO = mapper.map(questiongroup,
						QuestionGroupDTO.class);

				Set<Question> qs = questiongroup.getQuestions();

				List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();

				qLength = qs.size();

				questionCounter = 0;
				/* nonZeroQuestionCounter1 = 0; */

				for (Question question : qs) {

					question.getQid();

					questionCounter = question.getQid();

					QuestionDTO questionDTO = new QuestionDTO();

					questionDTO = mapper.map(question, QuestionDTO.class);

					Set<Answer> answers = question.getAnswers();

					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();

					AnswerDTO answerDTO = new AnswerDTO();

					if (answers != null && !answers.isEmpty()) {

						// int averageOfQuestionSet = 0;

						for (Answer answer : answers) {

							answerDTO = mapper.map(answer, AnswerDTO.class);

							if (answer.getSurveyusermapping().getSumId()
									.intValue() == sumId.intValue()) {

								if (questiongroup.getGrpId() == questionGrpId) {

									answer.getOption().getOtext();

									if (answer
											.getOption()
											.getOtext()
											.equalsIgnoreCase(
													"Extremely Dissatisfied")) {
										nonZeroTotal++;

										answerDTO.getOption()
												.setotextValue(1.2);

										avgNps += 1.2;

										avg += 1.2;

									}

									else if (answer.getOption().getOtext()
											.equalsIgnoreCase("Dissatisfied")) {
										nonZeroTotal++;
										answerDTO.getOption()
												.setotextValue(2.4);

										avgNps += 2.4;

										avg += 2.4;

									}

									else if (answer.getOption().getOtext()
											.equalsIgnoreCase("Neutral")) {
										nonZeroTotal++;
										answerDTO.getOption()
												.setotextValue(3.6);

										avgNps += 3.6;

										avg += 3.6;

									}

									else if (answer.getOption().getOtext()
											.equalsIgnoreCase("Satisfied")) {
										nonZeroTotal++;
										answerDTO.getOption()
												.setotextValue(4.8);

										avgNps += 4.8;

										avg += 4.8;

									}

									else if (answer
											.getOption()
											.getOtext()
											.equalsIgnoreCase(
													"Extremely Satisfied")) {
										nonZeroTotal++;
										answerDTO.getOption().setotextValue(6);

										avgNps += 6;

										avg += 6;

									}

									else if (answerDTO.getOption().getOtext()
											.equalsIgnoreCase("N/A")) {

										answerDTO.getOption().setotextValue(0);

										avgNps += 0;

										avg += 0;

									}

								}

								questionDTO.setAnswerDTO(answerDTO);

							}

						}

					}

					Set<Option> options = question.getOptions();

					List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();

					for (Option option : options) {

						OptionDTO optionDTO = new OptionDTO();

						optionDTO = mapper.map(option, OptionDTO.class);

						optionDTOs.add(optionDTO);

					}

					questionDTO.setOptionDTOs(optionDTOs);

					questionDTOs.add(questionDTO);

					if (questionCounter == 1 || questionCounter == 7
							|| questionCounter == 13) {

						questionavg1 += avg;
						System.out
								.println("****************************************************************************");
						System.out.println(avg);
						if (avg != 0) {
							nonZeroAvg1++;
						}
						System.out.println("-=-=-==-=-=-=-=-=-=-=-=-=-"
								+ nonZeroAvg1);

					} else if (questionCounter == 2 || questionCounter == 8
							|| questionCounter == 14) {

						questionavg2 += avg;
						if (avg != 0) {
							nonZeroAvg2++;
						}

					} else if (questionCounter == 3 || questionCounter == 9
							|| questionCounter == 15) {

						questionavg3 += avg;
						if (avg != 0) {
							nonZeroAvg3++;
						}

					} else if (questionCounter == 4 || questionCounter == 10
							|| questionCounter == 16) {

						questionavg4 += avg;
						if (avg != 0) {
							nonZeroAvg4++;
						}

					} else if (questionCounter == 5 || questionCounter == 11
							|| questionCounter == 17) {

						questionavg5 += avg;
						if (avg != 0) {
							nonZeroAvg5++;
						}

					} else if (questionCounter == 6 || questionCounter == 12) {

						questionavg6 += avg;
						if (avg != 0) {
							nonZeroAvg6++;
						}

					}
					avg = 0;
				}
				questionGroupDTO.setQuestions(questionDTOs);
				questionGroupDTOs.add(questionGroupDTO);
			}
			userDTO.setQuestionGroupDTOs(questionGroupDTOs);
			if (nonZeroTotal != 0) {
				userDTO.setAvgNps(Double.parseDouble(format.format(avgNps
						/ nonZeroTotal)));
			}
			userDTOs.add(userDTO);
		}
		userDTO.setQuestionavg1(Double.parseDouble(format.format(questionavg1
				/ nonZeroAvg1)));
		userDTO.setQuestionavg2(Double.parseDouble(format.format(questionavg2
				/ nonZeroAvg2)));
		userDTO.setQuestionavg3(Double.parseDouble(format.format(questionavg3
				/ nonZeroAvg3)));
		userDTO.setQuestionavg4(Double.parseDouble(format.format(questionavg4
				/ nonZeroAvg4)));
		userDTO.setQuestionavg5(Double.parseDouble(format.format(questionavg5
				/ nonZeroAvg5)));
		if (nonZeroAvg6 != 0) {
			userDTO.setQuestionavg6(Double.parseDouble(format
					.format(questionavg6 / nonZeroAvg6)));
		}
		double avgoverall = Double.parseDouble(format.format((userDTO
				.getQuestionavg1()
				+ userDTO.getQuestionavg2()
				+ userDTO.getQuestionavg3()
				+ userDTO.getQuestionavg4()
				+ userDTO.getQuestionavg5() + userDTO.getQuestionavg6()) / 6));
		double avgoverall1 = Double
				.parseDouble(format.format((userDTO.getQuestionavg1()
						+ userDTO.getQuestionavg2() + userDTO.getQuestionavg3()
						+ userDTO.getQuestionavg4() + userDTO.getQuestionavg5()) / 5));
		userDTO.setAvgOfEachUser(avgoverall);
		userDTO.setAvgNps1(avgoverall1);

		/* userDTOs.add(userDTO); */

		return userDTOs;

	}

	@Override
	public List<UserDTO> getScoresForQestionGrp4ForAccount(Integer clientId,
			Integer questionGrpId) {
		List<AppUser> appUsers = reportsDAO.getScoreForQuestionSet(clientId);
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			userDTO = mapper.map(appUser, UserDTO.class);
			Integer sumId = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next()).getSumId();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumIdAndGroupId(sumId, questionGrpId);
			double avgNps = 0;
			int qLength = 1;
			for (Questiongroup questiongroup : qgs) {
				QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
				questionGroupDTO = mapper.map(questiongroup,
						QuestionGroupDTO.class);
				Set<Question> qs = questiongroup.getQuestions();
				List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
				qLength = qs.size();
				for (Question question : qs) {
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					AnswerDTO answerDTO = new AnswerDTO();
					if (answers != null && !answers.isEmpty()) {
						// int averageOfQuestionSet = 0;

						for (Answer answer : answers) {

							answerDTO = mapper.map(answer, AnswerDTO.class);
							if (answer.getSurveyusermapping().getSumId()
									.intValue() == sumId.intValue()) {
								if (questiongroup.getGrpId() == questionGrpId) {
									answer.getAtext();
								}
								questionDTO.setAnswerDTO(answerDTO);
							}
						}
					}

					Set<Option> options = question.getOptions();
					List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
					for (Option option : options) {
						OptionDTO optionDTO = new OptionDTO();
						optionDTO = mapper.map(option, OptionDTO.class);
						optionDTOs.add(optionDTO);
					}
					questionDTO.setOptionDTOs(optionDTOs);
					questionDTOs.add(questionDTO);
				}
				questionGroupDTO.setQuestions(questionDTOs);
				questionGroupDTOs.add(questionGroupDTO);
			}
			userDTO.setQuestionGroupDTOs(questionGroupDTOs);
			userDTO.setAvgNps(avgNps / qLength);
			userDTOs.add(userDTO);
		}
		return userDTOs;
	}

	@Override
	public HashMap<String, Float> overallNpsForOrganization() {
		HashMap<String, Float> mapOfOverallNps = new HashMap<String, Float>();
		int overallPromoters = 0, overallDetractors = 0, overallNeutral = 0;
		int overallRespondants = 0;
		float percentageOfPromoters1 = 0, percentageOfDetractors1 = 0, percentageOfNeutral1 = 0;
		List<AppUser> appUsers = reportsDAO.getScoreForQuestionSets();

		int questionGrpId = 5;
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();

		for (AppUser appUser : appUsers) {
			overallRespondants++;
			UserDTO userDTO = new UserDTO();
			userDTO = mapper.map(appUser, UserDTO.class);
			Surveyusermapping sum = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next());
			Integer sumId = sum.getSumId();
			Set<Answer> anss = sum.getAnswers();
			List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumIdAndGroupId(sumId, questionGrpId);
			QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
			int i = 0;
			int countOfPromoters = 0, countOfDetractors = 0, countOfNeutral = 0;
			double percentageOfPromoters = 0, percentageOfDetractors = 0, percentageOfNeutral = 0;
			int totalRepondants = 0;
			for (Answer answer : anss) {
				if (questionGrpId == answer.getQuestion().getQuestiongroup()
						.getGrpId()) {
					if (i++ == 0)
						questionGroupDTO = mapper.map(answer.getQuestion()
								.getQuestiongroup(), QuestionGroupDTO.class);

					Question question = answer.getQuestion();
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					AnswerDTO answerDTO = new AnswerDTO();
					answerDTO = mapper.map(answer, AnswerDTO.class);

					if (answer.getSurveyusermapping().getSumId() == sumId) {
						answer.getOption().getOtext();
						if (Integer.parseInt(answer.getOption().getOtext()) > 0
								&& Integer.parseInt(answer.getOption()
										.getOtext()) <= 6) {
							countOfDetractors++;
						} else if (Integer.parseInt(answer.getOption()
								.getOtext()) == 7
								|| Integer.parseInt(answer.getOption()
										.getOtext()) == 8) {
							countOfNeutral++;
						} else if (Integer.parseInt(answer.getOption()
								.getOtext()) == 9
								|| Integer.parseInt(answer.getOption()
										.getOtext()) == 10) {
							countOfPromoters++;
						}

						questionDTO.setAnswerDTO(answerDTO);
						Set<Option> options = question.getOptions();
						List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
						for (Option option : options) {
							OptionDTO optionDTO = new OptionDTO();
							optionDTO = mapper.map(option, OptionDTO.class);
							optionDTOs.add(optionDTO);
						}
						questionDTO.setOptionDTOs(optionDTOs);
						questionDTOs.add(questionDTO);

					}

				}
			}

			totalRepondants = countOfDetractors + countOfNeutral
					+ countOfPromoters;
			percentageOfDetractors = (countOfDetractors * 100)
					/ totalRepondants;
			percentageOfNeutral = (countOfNeutral * 100) / totalRepondants;
			percentageOfPromoters = (countOfPromoters * 100) / totalRepondants;
			double npsScore = percentageOfPromoters - percentageOfDetractors;

			if (npsScore > 0) {
				userDTO.setNpsType("promoter");
				overallPromoters++;
			} else if (npsScore < 0) {
				userDTO.setNpsType("detractor");
				overallDetractors++;
			} else {
				userDTO.setNpsType("neutral");
				overallNeutral++;
			}

			questionGroupDTO.setQuestions(questionDTOs);
			questionGroupDTOs.add(questionGroupDTO);

			userDTO.setQuestionGroupDTOs(questionGroupDTOs);

			userDTOs.add(userDTO);

		}
		percentageOfPromoters1 = (overallPromoters * 100) / overallRespondants;
		percentageOfNeutral1 = (overallNeutral * 100) / overallRespondants;
		percentageOfDetractors1 = (overallDetractors * 100)
				/ overallRespondants;
		mapOfOverallNps.put("Promoters", percentageOfPromoters1);
		mapOfOverallNps.put("Ditractors", percentageOfDetractors1);
		mapOfOverallNps.put("Neutral", percentageOfNeutral1);
		return mapOfOverallNps;
	}

	@Override
	public List<UserDTO> npsOverallForEachQuestion() {
		int avgNps = 0;
		int countOfDictractors = 0;
		int countOfPromoters = 0;
		int countOfNeutral = 0;
		int totalNumberOfRespondants = 0;
		float percentageOfDitractors = 0;
		float percentageOfPromoters = 0;
		float percentageOfNeutal = 0;
		HashMap<String, Float> mapOfNps = new HashMap<String, Float>();

		List<AppUser> appUsers = reportsDAO.getOverallNpsScore();
		List<Integer> listOfQuestionIdForQuestionGrp = reportsDAO
				.listOfQuestionIdForQuestionGroup();
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			Integer sumId = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next()).getSumId();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumId(sumId);
			for (Questiongroup questiongroup : qgs) {
				QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
				questionGroupDTO = mapper.map(questiongroup,
						QuestionGroupDTO.class);
				Set<Question> qs = questiongroup.getQuestions();
				List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
				for (Question question : qs) {
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					if (answers != null && !answers.isEmpty()) {
						for (Answer answer : answers) {
							/*
							 * if (answer.getSurveyusermapping().getSumId() ==
							 * 536) {
							 */
							if (questiongroup.getGrpId() == 5) {
								/*
								 * for(int questionId :
								 * listOfQuestionIdForQuestionGrp){
								 */

								if (Integer.parseInt(answer.getOption()
										.getOtext()) > 0
										&& Integer.parseInt(answer.getOption()
												.getOtext()) <= 6) {
									((Question) (questiongroup.getQuestions()
											.iterator().next()))
											.setCountOfDetractors(countOfDictractors++);
								} else if (Integer.parseInt(answer.getOption()
										.getOtext()) == 7
										|| Integer.parseInt(answer.getOption()
												.getOtext()) == 8) {
									((Question) (questiongroup.getQuestions()
											.iterator().next()))
											.setCountOfNeutral(countOfNeutral++);
								} else if (Integer.parseInt(answer.getOption()
										.getOtext()) == 9
										|| Integer.parseInt(answer.getOption()
												.getOtext()) == 10) {
									((Question) (questiongroup.getQuestions()
											.iterator().next()))
											.setCountOfNeutral(countOfPromoters++);
								}

								/* } */

							}
							AnswerDTO answerDTO = new AnswerDTO();
							answerDTO = mapper.map(answer, AnswerDTO.class);
							answerDTOs.add(answerDTO);
							/* } */
						}
					}
					questionDTO.setAnswerDTOs(answerDTOs);
					Set<Option> options = question.getOptions();
					List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
					for (Option option : options) {
						OptionDTO optionDTO = new OptionDTO();
						optionDTO = mapper.map(option, OptionDTO.class);
						optionDTOs.add(optionDTO);
					}
					questionDTO.setOptionDTOs(optionDTOs);
					questionDTOs.add(questionDTO);
				}
				questionGroupDTO.setQuestions(questionDTOs);
				questionGroupDTOs.add(questionGroupDTO);
			}

			userDTO.setQuestionGroupDTOs(questionGroupDTOs);
			userDTO.setAvgNps(avgNps);
			userDTO.setCountOfDitractors(countOfDictractors);
			userDTO.setCountOfNeutral(countOfNeutral);
			userDTO.setCountOfPromoters(countOfPromoters);

			// retriving count for nps

			userDTO.setTotalRespondants(totalNumberOfRespondants);
			totalNumberOfRespondants = countOfDictractors + countOfPromoters
					+ countOfNeutral;
			percentageOfPromoters = ((countOfPromoters * 100) / totalNumberOfRespondants);
			percentageOfNeutal = ((countOfNeutral * 100) / totalNumberOfRespondants);
			percentageOfDitractors = ((countOfDictractors * 100) / totalNumberOfRespondants);

			userDTO.setCountOfPromoters(countOfPromoters);
			userDTO.setCountOfNeutral(countOfNeutral);
			userDTO.setCountOfDitractors(countOfDictractors);
			userDTO.setTotalRespondants(countOfPromoters + countOfNeutral
					+ countOfDictractors);

			percentageOfPromoters = ((countOfPromoters * 100) / totalNumberOfRespondants);
			percentageOfNeutal = ((countOfNeutral * 100) / totalNumberOfRespondants);
			percentageOfDitractors = ((countOfDictractors * 100) / totalNumberOfRespondants);

			userDTO.setDictractorsPercentage(percentageOfDitractors);
			userDTO.setNeutralPercentage(percentageOfNeutal);
			userDTO.setPromotersPercentage(percentageOfPromoters);

			userDTOs.add(userDTO);

		}

		mapOfNps.put("Ditractors", percentageOfDitractors);
		mapOfNps.put("Promoters", percentageOfPromoters);
		mapOfNps.put("Neutral", percentageOfNeutal);
		mapOfNps.put("AccountNps", percentageOfPromoters
				- percentageOfDitractors);

		return userDTOs;
	}

	// get list of all promoters, detractors and neutral respondants

	@Override
	public List<UserDTO> getListOfPromotersAndDetractorsAndNeutral() {
		int overallPromoters = 0, overallDetractors = 0, overallNeutral = 0;
		List<AppUser> appUsers = reportsDAO.getScoreForQuestionSets();

		int questionGrpId = 5;
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();

		for (AppUser appUser : appUsers) {
			System.out.println(appUser.getClient().getClientName());
			UserDTO userDTO = new UserDTO();
			userDTO = mapper.map(appUser, UserDTO.class);
			userDTO.setClientName(appUser.getClient().getClientName());
			Surveyusermapping sum = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next());
			Integer sumId = sum.getSumId();
			Set<Answer> anss = sum.getAnswers();
			List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumIdAndGroupId(sumId, questionGrpId);
			QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
			int i = 0;
			int countOfPromoters = 0, countOfDetractors = 0, countOfNeutral = 0;
			double percentageOfPromoters = 0, percentageOfDetractors = 0, percentageOfNeutral = 0;
			int totalRepondants = 0;
			for (Answer answer : anss) {

				if (questionGrpId == answer.getQuestion().getQuestiongroup()
						.getGrpId()) {
					if (i++ == 0)
						questionGroupDTO = mapper.map(answer.getQuestion()
								.getQuestiongroup(), QuestionGroupDTO.class);

					Question question = answer.getQuestion();
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					AnswerDTO answerDTO = new AnswerDTO();
					answerDTO = mapper.map(answer, AnswerDTO.class);

					if (answer.getSurveyusermapping().getSumId() == sumId) {

						answer.getOption().getOtext();
						if (Integer.parseInt(answer.getOption().getOtext()) > 0
								&& Integer.parseInt(answer.getOption()
										.getOtext()) <= 6) {
							countOfDetractors++;
						} else if (Integer.parseInt(answer.getOption()
								.getOtext()) == 7
								|| Integer.parseInt(answer.getOption()
										.getOtext()) == 8) {
							countOfNeutral++;
						} else if (Integer.parseInt(answer.getOption()
								.getOtext()) == 9
								|| Integer.parseInt(answer.getOption()
										.getOtext()) == 10) {
							countOfPromoters++;
						}

						questionDTO.setAnswerDTO(answerDTO);
						Set<Option> options = question.getOptions();
						List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
						for (Option option : options) {
							OptionDTO optionDTO = new OptionDTO();
							optionDTO = mapper.map(option, OptionDTO.class);
							optionDTOs.add(optionDTO);
						}
						questionDTO.setOptionDTOs(optionDTOs);
						questionDTOs.add(questionDTO);

					}

				}
			}

			totalRepondants = countOfDetractors + countOfNeutral
					+ countOfPromoters;
			percentageOfDetractors = (countOfDetractors * 100)
					/ totalRepondants;
			percentageOfNeutral = (countOfNeutral * 100) / totalRepondants;
			percentageOfPromoters = (countOfPromoters * 100) / totalRepondants;
			double npsScore = percentageOfPromoters - percentageOfDetractors;

			if (npsScore > 0) {
				userDTO.setNpsType("promoter");
				overallPromoters++;
			} else if (npsScore < 0) {
				userDTO.setNpsType("detractor");
				overallDetractors++;
			} else {
				userDTO.setNpsType("neutral");
				overallNeutral++;
			}

			questionGroupDTO.setQuestions(questionDTOs);
			questionGroupDTOs.add(questionGroupDTO);

			userDTO.setQuestionGroupDTOs(questionGroupDTOs);

			userDTOs.add(userDTO);
		}
		return userDTOs;
	}

	@Override
	public HashMap<String, Integer> overallCountOfDetractorsPromotersAndNeutral() {
		HashMap<String, Integer> mapOfOverallNps = new HashMap<String, Integer>();
		int overallPromoters = 0, overallDetractors = 0, overallNeutral = 0;
		System.out.println("Overall list of promoters,neutral, detractors");
		List<AppUser> appUsers = reportsDAO.getScoreForQuestionSets();

		int questionGrpId = 5;
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();

		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			userDTO = mapper.map(appUser, UserDTO.class);
			Surveyusermapping sum = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next());
			Integer sumId = sum.getSumId();
			Set<Answer> anss = sum.getAnswers();
			List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumIdAndGroupId(sumId, questionGrpId);
			QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
			int i = 0;
			int countOfPromoters = 0, countOfDetractors = 0, countOfNeutral = 0;
			double percentageOfPromoters = 0, percentageOfDetractors = 0, percentageOfNeutral = 0;
			int totalRepondants = 0;
			for (Answer answer : anss) {
				if (questionGrpId == answer.getQuestion().getQuestiongroup()
						.getGrpId()) {
					if (i++ == 0)
						questionGroupDTO = mapper.map(answer.getQuestion()
								.getQuestiongroup(), QuestionGroupDTO.class);

					Question question = answer.getQuestion();
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					AnswerDTO answerDTO = new AnswerDTO();
					answerDTO = mapper.map(answer, AnswerDTO.class);

					if (answer.getSurveyusermapping().getSumId() == sumId) {
						answer.getOption().getOtext();
						if (Integer.parseInt(answer.getOption().getOtext()) > 0
								&& Integer.parseInt(answer.getOption()
										.getOtext()) <= 6) {
							countOfDetractors++;
						} else if (Integer.parseInt(answer.getOption()
								.getOtext()) == 7
								|| Integer.parseInt(answer.getOption()
										.getOtext()) == 8) {
							countOfNeutral++;
						} else if (Integer.parseInt(answer.getOption()
								.getOtext()) == 9
								|| Integer.parseInt(answer.getOption()
										.getOtext()) == 10) {
							countOfPromoters++;
						}

						questionDTO.setAnswerDTO(answerDTO);
						Set<Option> options = question.getOptions();
						List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
						for (Option option : options) {
							OptionDTO optionDTO = new OptionDTO();
							optionDTO = mapper.map(option, OptionDTO.class);
							optionDTOs.add(optionDTO);
						}
						questionDTO.setOptionDTOs(optionDTOs);
						questionDTOs.add(questionDTO);

					}

				}

			}

			totalRepondants = countOfDetractors + countOfNeutral
					+ countOfPromoters;
			percentageOfDetractors = (countOfDetractors * 100)
					/ totalRepondants;
			percentageOfNeutral = (countOfNeutral * 100) / totalRepondants;
			percentageOfPromoters = (countOfPromoters * 100) / totalRepondants;
			double npsScore = percentageOfPromoters - percentageOfDetractors;

			if (npsScore > 0) {
				userDTO.setNpsType("promoter");
				overallPromoters++;
			} else if (npsScore < 0) {
				userDTO.setNpsType("detractor");
				overallDetractors++;
			} else {
				userDTO.setNpsType("neutral");
				overallNeutral++;
			}

			questionGroupDTO.setQuestions(questionDTOs);
			questionGroupDTOs.add(questionGroupDTO);

			userDTO.setQuestionGroupDTOs(questionGroupDTOs);

			userDTOs.add(userDTO);
		}

		mapOfOverallNps.put("Promoters", overallPromoters);
		mapOfOverallNps.put("Neutral", overallNeutral);
		mapOfOverallNps.put("Detractors", overallDetractors);
		return mapOfOverallNps;
	}

	@Override
	public List<String> getTop3BenefitsForOrganization() {
		List<AppUser> appUsers = reportsDAO
				.getReportstop3BenefitsForOrganization();
		List<String> topThreeBenefits = new ArrayList<String>();
		List<String> finalBenefits = new ArrayList<String>();
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			Integer sumId = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next()).getSumId();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumId(sumId);
			// int avgNps = 0;
			for (Questiongroup questiongroup : qgs) {
				QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
				questionGroupDTO = mapper.map(questiongroup,
						QuestionGroupDTO.class);
				Set<Question> qs = questiongroup.getQuestions();
				List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
				for (Question question : qs) {
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					if (answers != null && !answers.isEmpty()) {
						for (Answer answer : answers) {
							if (answer.getSurveyusermapping().getSumId()
									.intValue() == sumId.intValue()) {
								if (question
										.getQuestiongroup()
										.getGrpDesc()
										.equalsIgnoreCase(
												"Quinnox's Overall Engagement")) {
									if ((question.getQname()
											.equalsIgnoreCase("Please describe the Top 3 Benefits when working with Quinnox"))) {
										String repondantBenefits = answer
												.getOption().getOtext();
										topThreeBenefits.add(repondantBenefits);
									}

								}
								AnswerDTO answerDTO = new AnswerDTO();
								answerDTO = mapper.map(answer, AnswerDTO.class);
								answerDTOs.add(answerDTO);
							}
						}
					}
					questionDTO.setAnswerDTOs(answerDTOs);
					Set<Option> options = question.getOptions();
					List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
					for (Option option : options) {
						OptionDTO optionDTO = new OptionDTO();
						optionDTO = mapper.map(option, OptionDTO.class);
						optionDTOs.add(optionDTO);
					}
					questionDTO.setOptionDTOs(optionDTOs);
					questionDTOs.add(questionDTO);
				}
				questionGroupDTO.setQuestions(questionDTOs);
				questionGroupDTOs.add(questionGroupDTO);
			}
			userDTO.setQuestionGroupDTOs(questionGroupDTOs);
			userDTO.setRespondantTopThreeBenefits(topThreeBenefits);
			userDTOs.add(userDTO);
		}
		for (int i = 0; i <= 2; i++) {
			finalBenefits.add(topThreeBenefits.get(i));
		}

		return finalBenefits;
	}
	@Override
	public List<ClientDTO> getAverageScoresForAllClientOfOrganization() {
		List<Integer> getAllGroupId = reportsDAO.getListOfQuestionIdForGrpId();

		List<AppUser> appUsers = reportsDAO.getScoreForQuestionSet();

		Mapper mapper = new DozerBeanMapper();

		List<ClientDTO> clientDTOs = new ArrayList<ClientDTO>();

		List<Client> clientNames = reportsDAO.getClient();

		NumberFormat format = new DecimalFormat("#.##");
		int qLength = 1;
		double overAllNps1 = 0.0;
		double overAllNps2 = 0.0;
		double overAllNps3 = 0.0;
		double avgOfAll = 0.0;
		int counter = 0;

		List<Questiongroup> qgs = questiongroupDAO
				.findQuestionGroupsByGroupIdUser();

		
			
			for (AppUser appUser : appUsers) {
				int counter1 = 0;
				double avg1 = 0;
				double avg2 = 0;
				double avg3 = 0;
				double avg = 0;
				counter++;
				ClientDTO clientDTO = mapper.map(appUser.getClient(), ClientDTO.class);
					for (Integer questiongrpId : getAllGroupId) {
						avg = 0;
						Integer sumId = ((Surveyusermapping) appUser.getSurveyusermappings().iterator().next()).getSumId();
						List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
						for (Questiongroup questiongroup : qgs) {
							// count if 0 is encountered
							int nonZeroCount = 0;

							if (questiongroup.getGrpId() == questiongrpId&& questiongrpId != 4 && questiongrpId != 5) {
								if(questiongrpId==3 && appUser.getClient().getClientId()==41){
									System.out.println("hi");
								}
								double totalAnswerCount = 0;
								QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
								questionGroupDTO = mapper.map(questiongroup,QuestionGroupDTO.class);
								Set<Question> qs = questiongroup.getQuestions();
								List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
								qLength = qs.size();
								for (Question question : qs) {
									QuestionDTO questionDTO = new QuestionDTO();
									questionDTO = mapper.map(question,QuestionDTO.class);
									Set<Answer> answers = question.getAnswers();
									List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
									AnswerDTO answerDTO = new AnswerDTO();
									if (answers != null && !answers.isEmpty()) {
										for (Answer answer : answers) {
											if (questiongroup.getGrpId() == answer.getQuestion().getQuestiongroup()
													.getGrpId() && appUser.getClient().getClientId()== answer.getSurveyusermapping().getAppUser().getClient().getClientId()) {
											answerDTO = mapper.map(answer,AnswerDTO.class);
											if (answer.getSurveyusermapping().getSumId().intValue() == sumId.intValue()) {
												if (answer.getOption().getOtext().equalsIgnoreCase("Extremely Dissatisfied")) {
													answerDTO.getOption().setotextValue(1.2);
													totalAnswerCount += 1.2;
													nonZeroCount++;
												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Dissatisfied")) {
													answerDTO.getOption().setotextValue(2.4);
													totalAnswerCount += 2.4;
													nonZeroCount++;

												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Neutral")) {
													answerDTO.getOption().setotextValue(3.6);
													totalAnswerCount += 3.6;
													nonZeroCount++;

												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Satisfied")) {
													answerDTO.getOption().setotextValue(4.8);
													totalAnswerCount += 4.8;
													nonZeroCount++;
												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Extremely Satisfied")) {
													answerDTO.getOption().setotextValue(6);
													totalAnswerCount += 6;
													nonZeroCount++;
												}

												else if (answerDTO.getOption().getOtext().equalsIgnoreCase("N/A")) {
													answerDTO.getOption().setotextValue(0);
													totalAnswerCount += 0;
												}

											}
											questionDTO.setAnswerDTO(answerDTO);
										}
									}
										
										
									}

									Set<Option> options = question.getOptions();
									List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
									for (Option option : options) {
										OptionDTO optionDTO = new OptionDTO();
										optionDTO = mapper.map(option,OptionDTO.class);
										optionDTOs.add(optionDTO);
									}
									questionDTO.setOptionDTOs(optionDTOs);
									questionDTOs.add(questionDTO);
									
								}
								
								questionGroupDTO.setQuestions(questionDTOs);
								questionGroupDTOs.add(questionGroupDTO);
								if (nonZeroCount != 0) {
									avg = totalAnswerCount / nonZeroCount;
								}else{
									avg = totalAnswerCount;
								}
							
								avg = Double.parseDouble(format.format(avg));
								if (questiongroup.getGrpId() == 1 ) {
									counter1++;
									avg1 += avg;
									clientDTO.setAvggrp1(Double.parseDouble(format.format(avg1)));

								} else if (questiongroup.getGrpId() == 2) {
									avg2 += avg;
									clientDTO.setAvggrp2(Double.parseDouble(format.format(avg2)));
								} else if (questiongroup.getGrpId() == 3) {
									avg3 += avg;
									clientDTO.setAvggrp3(Double.parseDouble(format.format(avg3)));
									System.out.println("avg3="+avg3+"-"+nonZeroCount);
								}
							}
							
						}

						clientDTO.setQuestionGroupDTOs(questionGroupDTOs);

					}

				
				avgOfAll = Double.parseDouble(format.format((clientDTO.getAvggrp1()+ clientDTO.getAvggrp2() + clientDTO.getAvggrp3()) / 3));
				clientDTO.setAvgOfEachUser(avgOfAll);

				clientDTOs.add(clientDTO);
			}

			
			
		Map<Integer,List<ClientDTO>> uniqueClients = new HashMap<Integer,List<ClientDTO>>();
		for (ClientDTO clientDTO : clientDTOs) {
			if(uniqueClients.containsKey(clientDTO.getClientId())){
				List<ClientDTO> clientDTOsinMap = uniqueClients.get(clientDTO.getClientId());
				clientDTOsinMap.add(clientDTO);
				uniqueClients.put(clientDTO.getClientId(),clientDTOsinMap);
			}else{
				List<ClientDTO> dtos = new ArrayList<ClientDTO>();
				dtos.add(clientDTO);
				uniqueClients.put(clientDTO.getClientId(),dtos);
			}
		}
		List<ClientDTO> clientDTOs1 = new ArrayList<ClientDTO>();
		Iterator<Integer> iter = uniqueClients.keySet().iterator();
		while(iter.hasNext()){
			List<ClientDTO> dtos = (List<ClientDTO>)uniqueClients.get(iter.next());
			double avg1=0;
			double avg2=0;
			double avg3=0;
			double totalAvg = 0;
			int totalSize1=0;
			int totalSize2=0;
			int totalSize3=0;
			ClientDTO clientDTOToSet = new ClientDTO(); 
			for (ClientDTO clientDTO : dtos) {
				if(clientDTO.getAvggrp1()!=0){
					avg1+=clientDTO.getAvggrp1();
					totalSize1++;
				}
				if(clientDTO.getAvggrp2()!=0){
					avg2+=clientDTO.getAvggrp2();
					totalSize2++;
				}
				if(clientDTO.getAvggrp3()!=0){
					avg3+=clientDTO.getAvggrp3();
					totalSize3++;
				}
				
				clientDTOToSet = clientDTO;
			}
			avg1 = Double.parseDouble(format.format(avg1 /totalSize1));
			avg2 = Double.parseDouble(format.format(avg2 /totalSize2));
			avg3 = Double.parseDouble(format.format(avg3 /totalSize3));
			totalAvg = Double.parseDouble(format.format((avg1 + avg2 +avg3)/3));
			clientDTOToSet.setAvggrp1(avg1);
			clientDTOToSet.setAvggrp2(avg2);
			clientDTOToSet.setAvggrp3(avg3);
			clientDTOToSet.setAvgOfEachUser(totalAvg);
			clientDTOs1.add(clientDTOToSet);
		}
		return clientDTOs1;
	}
	

	
	@Override
	public HashMap<String, Double> getOverallScoreOfEachQuestiongrpForOrganization() {
		HashMap<String, Double> mapOfAvgScore = new HashMap<String, Double>();
		List<Integer> getAllGroupId = reportsDAO.getListOfQuestionIdForGrpId();

		List<AppUser> appUsers = reportsDAO.getScoreForQuestionSet();

		Mapper mapper = new DozerBeanMapper();

		List<ClientDTO> clientDTOs = new ArrayList<ClientDTO>();

		List<Client> clientNames = reportsDAO.getClient();

		NumberFormat format = new DecimalFormat("#.##");
		int qLength = 1;
		double overAllNps1 = 0.0;
		double overAllNps2 = 0.0;
		double overAllNps3 = 0.0;
		double avgOfAll = 0.0;
		int counter = 0;
		int totalClient = 0;

		double totalAvg1 = 0;
		double totalAvg2 = 0;
		double totalAvg3 = 0;
		double totalGrpAvg = 0;
		
		
		
		List<Questiongroup> qgs = questiongroupDAO
				.findQuestionGroupsByGroupIdUser();

		
			
			for (AppUser appUser : appUsers) {
				int counter1 = 0;
				double avg1 = 0;
				double avg2 = 0;
				double avg3 = 0;
				double avg = 0;
				counter++;
				ClientDTO clientDTO = mapper.map(appUser.getClient(), ClientDTO.class);
					for (Integer questiongrpId : getAllGroupId) {
						avg = 0;
						Integer sumId = ((Surveyusermapping) appUser.getSurveyusermappings().iterator().next()).getSumId();
						List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
						for (Questiongroup questiongroup : qgs) {
							// count if 0 is encountered
							int nonZeroCount = 0;

							if (questiongroup.getGrpId() == questiongrpId&& questiongrpId != 4 && questiongrpId != 5) {
								if(questiongrpId==3 && appUser.getClient().getClientId()==41){
									System.out.println("hi");
								}
								double totalAnswerCount = 0;
								QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
								questionGroupDTO = mapper.map(questiongroup,QuestionGroupDTO.class);
								Set<Question> qs = questiongroup.getQuestions();
								List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
								qLength = qs.size();
								for (Question question : qs) {
									QuestionDTO questionDTO = new QuestionDTO();
									questionDTO = mapper.map(question,QuestionDTO.class);
									Set<Answer> answers = question.getAnswers();
									List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
									AnswerDTO answerDTO = new AnswerDTO();
									if (answers != null && !answers.isEmpty()) {
										for (Answer answer : answers) {
											if (questiongroup.getGrpId() == answer.getQuestion().getQuestiongroup()
													.getGrpId() && appUser.getClient().getClientId()== answer.getSurveyusermapping().getAppUser().getClient().getClientId()) {
											answerDTO = mapper.map(answer,AnswerDTO.class);
											if (answer.getSurveyusermapping().getSumId().intValue() == sumId.intValue()) {
												if (answer.getOption().getOtext().equalsIgnoreCase("Extremely Dissatisfied")) {
													answerDTO.getOption().setotextValue(1.2);
													totalAnswerCount += 1.2;
													nonZeroCount++;
												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Dissatisfied")) {
													answerDTO.getOption().setotextValue(2.4);
													totalAnswerCount += 2.4;
													nonZeroCount++;

												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Neutral")) {
													answerDTO.getOption().setotextValue(3.6);
													totalAnswerCount += 3.6;
													nonZeroCount++;

												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Satisfied")) {
													answerDTO.getOption().setotextValue(4.8);
													totalAnswerCount += 4.8;
													nonZeroCount++;
												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Extremely Satisfied")) {
													answerDTO.getOption().setotextValue(6);
													totalAnswerCount += 6;
													nonZeroCount++;
												}

												else if (answerDTO.getOption().getOtext().equalsIgnoreCase("N/A")) {
													answerDTO.getOption().setotextValue(0);
													totalAnswerCount += 0;
												}

											}
											questionDTO.setAnswerDTO(answerDTO);
										}
									}
										
										
									}

									Set<Option> options = question.getOptions();
									List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
									for (Option option : options) {
										OptionDTO optionDTO = new OptionDTO();
										optionDTO = mapper.map(option,OptionDTO.class);
										optionDTOs.add(optionDTO);
									}
									questionDTO.setOptionDTOs(optionDTOs);
									questionDTOs.add(questionDTO);
									
								}
								
								questionGroupDTO.setQuestions(questionDTOs);
								questionGroupDTOs.add(questionGroupDTO);
								if (nonZeroCount != 0) {
									avg = totalAnswerCount / nonZeroCount;
								}else{
									avg = totalAnswerCount;
								}
							
								avg = Double.parseDouble(format.format(avg));
								if (questiongroup.getGrpId() == 1 ) {
									counter1++;
									avg1 += avg;
									clientDTO.setAvggrp1(Double.parseDouble(format.format(avg1)));

								} else if (questiongroup.getGrpId() == 2) {
									avg2 += avg;
									clientDTO.setAvggrp2(Double.parseDouble(format.format(avg2)));
								} else if (questiongroup.getGrpId() == 3) {
									avg3 += avg;
									clientDTO.setAvggrp3(Double.parseDouble(format.format(avg3)));
									
								}
							}
							
						}

						clientDTO.setQuestionGroupDTOs(questionGroupDTOs);

					}

				
				avgOfAll = Double.parseDouble(format.format((clientDTO.getAvggrp1()+ clientDTO.getAvggrp2() + clientDTO.getAvggrp3()) / 3));
				clientDTO.setAvgOfEachUser(avgOfAll);

				clientDTOs.add(clientDTO);
			}

			
			
		Map<Integer,List<ClientDTO>> uniqueClients = new HashMap<Integer,List<ClientDTO>>();
		for (ClientDTO clientDTO : clientDTOs) {
			if(uniqueClients.containsKey(clientDTO.getClientId())){
				List<ClientDTO> clientDTOsinMap = uniqueClients.get(clientDTO.getClientId());
				clientDTOsinMap.add(clientDTO);
				uniqueClients.put(clientDTO.getClientId(),clientDTOsinMap);
			}else{
				List<ClientDTO> dtos = new ArrayList<ClientDTO>();
				dtos.add(clientDTO);
				uniqueClients.put(clientDTO.getClientId(),dtos);
			}
		}
		List<ClientDTO> clientDTOs1 = new ArrayList<ClientDTO>();
		Iterator<Integer> iter = uniqueClients.keySet().iterator();
		while(iter.hasNext()){
			List<ClientDTO> dtos = (List<ClientDTO>)uniqueClients.get(iter.next());
			totalClient++;
			double avg1=0;
			double avg2=0;
			double avg3=0;
			double totalAvg = 0;
			int totalSize1=0;
			int totalSize2=0;
			int totalSize3=0;
			ClientDTO clientDTOToSet = new ClientDTO(); 
			for (ClientDTO clientDTO : dtos) {
				if(clientDTO.getAvggrp1()!=0){
					avg1+=clientDTO.getAvggrp1();
					totalSize1++;
				}
				if(clientDTO.getAvggrp2()!=0){
					avg2+=clientDTO.getAvggrp2();
					totalSize2++;
				}
				if(clientDTO.getAvggrp3()!=0){
					avg3+=clientDTO.getAvggrp3();
					totalSize3++;
				}
				
				clientDTOToSet = clientDTO;
			}
			avg1 = Double.parseDouble(format.format(avg1 /totalSize1));
			totalAvg1+=avg1;
			
			avg2 = Double.parseDouble(format.format(avg2 /totalSize2));
			totalAvg2+=avg2;
			
			avg3 = Double.parseDouble(format.format(avg3 /totalSize3));
			totalAvg3+=avg3;
			
			totalAvg = Double.parseDouble(format.format((avg1 + avg2 +avg3)/3));
			totalGrpAvg+= totalAvg;
			
			clientDTOToSet.setAvggrp1(avg1);
			clientDTOToSet.setAvggrp2(avg2);
			clientDTOToSet.setAvggrp3(avg3);
			clientDTOToSet.setAvgOfEachUser(totalAvg);
			clientDTOs1.add(clientDTOToSet);
		}
		
		
		
		
		mapOfAvgScore.put("Question1",
				Double.parseDouble(format.format(totalAvg1/totalClient)));
		mapOfAvgScore.put("Question2",
				Double.parseDouble(format.format(totalAvg2/totalClient)));
		mapOfAvgScore.put("Question3",
				Double.parseDouble(format.format(totalAvg3/totalClient)));
		mapOfAvgScore.put("overAllNps", Double.parseDouble(format
				.format((totalAvg1/totalClient + totalAvg2/totalClient + totalAvg3/totalClient) / 3)));
		
		
		return  mapOfAvgScore;
	}

	@Override
	public HashMap<String, Float> overAllNps() {
		int avgNps = 0;
		int countOfDictractors = 0;
		int countOfPromoters = 0;
		int countOfNeutral = 0;
		int totalNumberOfRespondants = 0;
		float percentageOfDitractors = 0;
		float percentageOfPromoters = 0;
		float percentageOfNeutal = 0;

		int count = 0;

		HashMap<String, Float> mapOfNps = new HashMap<String, Float>();

		List<AppUser> appUsers = reportsDAO.getScoreForQuestionSet();

		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();

		for (AppUser appUser : appUsers) {

			UserDTO userDTO = new UserDTO();
			Integer sumId = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next()).getSumId();

			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumId(sumId);

			for (Questiongroup questiongroup : qgs) {

				QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
				questionGroupDTO = mapper.map(questiongroup,
						QuestionGroupDTO.class);

				Set<Question> qs = questiongroup.getQuestions();
				// System.out.println("the number of questions: " +qs);
				List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();

				for (Question question : qs) {

					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);

					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();

					if (answers != null && !answers.isEmpty()) {
						for (Answer answer : answers) {

							if (answer.getSurveyusermapping().getSumId() == sumId) {
								if (questiongroup.getGrpId() == 5) {
									// avgNps =
									// Integer.parseInt(answer.getOption().getOtext());
									if (Integer.parseInt(answer.getOption()
											.getOtext()) > 0
											&& Integer.parseInt(answer
													.getOption().getOtext()) <= 6) {
										countOfDictractors++;
									} else if (Integer.parseInt(answer
											.getOption().getOtext()) == 7
											|| Integer.parseInt(answer
													.getOption().getOtext()) == 8) {
										countOfNeutral++;
									} else if (Integer.parseInt(answer
											.getOption().getOtext()) == 9
											|| Integer.parseInt(answer
													.getOption().getOtext()) == 10) {
										countOfPromoters++;
									}
									System.out.println("****************"
											+ answer.getOption().getOtext()
											+ "******************");
								}
								AnswerDTO answerDTO = new AnswerDTO();
								answerDTO = mapper.map(answer, AnswerDTO.class);
								answerDTOs.add(answerDTO);
							}
						}
					}

					questionDTO.setAnswerDTOs(answerDTOs);
					Set<Option> options = question.getOptions();
					List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
					for (Option option : options) {
						OptionDTO optionDTO = new OptionDTO();
						optionDTO = mapper.map(option, OptionDTO.class);
						optionDTOs.add(optionDTO);
					}
					questionDTO.setOptionDTOs(optionDTOs);
					questionDTOs.add(questionDTO);
				}
				questionGroupDTO.setQuestions(questionDTOs);
				questionGroupDTOs.add(questionGroupDTO);
			}

			userDTO.setQuestionGroupDTOs(questionGroupDTOs);
			System.out.println("Qusetion group DTO"
					+ (userDTO.getQuestionGroupDTOs()));
			userDTO.setAvgNps(avgNps);
			System.out.println("Average" + userDTO.getAvgNps());
			userDTO.setCountOfDitractors(countOfDictractors);
			System.out.println("Ditractors" + userDTO.getCountOfDitractors());
			userDTO.setCountOfNeutral(countOfNeutral);
			System.out.println("Neutral : " + userDTO.getCountOfNeutral());
			userDTO.setCountOfPromoters(countOfPromoters);
			System.out.println("Promoters : " + userDTO.getCountOfPromoters());

			// retriving count for nps

			totalNumberOfRespondants = userDTO.getCountOfDitractors()
					+ userDTO.getCountOfNeutral()
					+ userDTO.getCountOfPromoters();

			userDTO.setTotalRespondants(totalNumberOfRespondants);
			System.out.println("Total no of respondants"
					+ userDTO.getTotalRespondants());

			System.out.println("Total Number of questions: "
					+ totalNumberOfRespondants);

			userDTO.setCountOfPromoters(countOfPromoters);
			userDTO.setCountOfNeutral(countOfNeutral);
			userDTO.setCountOfDitractors(countOfDictractors);
			userDTO.setTotalRespondants(countOfPromoters + countOfNeutral
					+ countOfDictractors);

			percentageOfPromoters = ((countOfPromoters * 100) / totalNumberOfRespondants);
			percentageOfNeutal = ((countOfNeutral * 100) / totalNumberOfRespondants);
			percentageOfDitractors = ((countOfDictractors * 100) / totalNumberOfRespondants);

			userDTO.setDictractorsPercentage(percentageOfDitractors);
			userDTO.setNeutralPercentage(percentageOfNeutal);
			userDTO.setPromotersPercentage(percentageOfPromoters);

			System.out.println("Final: D " + userDTO.getDictractorsPercentage()
					+ "N: " + userDTO.getNeutralPercentage() + "P: "
					+ userDTO.getPromotersPercentage());
			userDTOs.add(userDTO);
			count++;

		}
		System.out.println("The total respondants: " + count);

		mapOfNps.put("Ditractors", percentageOfDitractors);
		mapOfNps.put("Promoters", percentageOfPromoters);
		mapOfNps.put("Neutral", percentageOfNeutal);
		mapOfNps.put("AccountNps", percentageOfPromoters
				- percentageOfDitractors);
		System.out.println("MAP: " + mapOfNps);

		return mapOfNps;

	}

	@Override
	public List<UserDTO> getListOfPromotersAndDetractorsAndNeutralForClientId(
			int clientId) {
		HashMap<String, Integer> mapOfOverallNps = new HashMap<String, Integer>();
		int overallPromoters = 0, overallDetractors = 0, overallNeutral = 0;
		List<AppUser> appUsers = reportsDAO.getScoreByClientId(clientId);

		int questionGrpId = 5;
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();

		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			userDTO = mapper.map(appUser, UserDTO.class);
			Surveyusermapping sum = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next());
			Integer sumId = sum.getSumId();
			Set<Answer> anss = sum.getAnswers();
			List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumIdAndGroupId(sumId, questionGrpId);
			QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
			int i = 0;
			int countOfPromoters = 0, countOfDetractors = 0, countOfNeutral = 0;
			double percentageOfPromoters = 0, percentageOfDetractors = 0, percentageOfNeutral = 0;
			int totalRepondants = 0;
			for (Answer answer : anss) {
				if (questionGrpId == answer.getQuestion().getQuestiongroup()
						.getGrpId()) {
					if (i++ == 0)
						questionGroupDTO = mapper.map(answer.getQuestion()
								.getQuestiongroup(), QuestionGroupDTO.class);

					Question question = answer.getQuestion();
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					AnswerDTO answerDTO = new AnswerDTO();
					answerDTO = mapper.map(answer, AnswerDTO.class);

					if (answer.getSurveyusermapping().getSumId() == sumId) {
						answer.getOption().getOtext();
						if (Integer.parseInt(answer.getOption().getOtext()) > 0
								&& Integer.parseInt(answer.getOption()
										.getOtext()) <= 6) {
							countOfDetractors++;
						} else if (Integer.parseInt(answer.getOption()
								.getOtext()) == 7
								|| Integer.parseInt(answer.getOption()
										.getOtext()) == 8) {
							countOfNeutral++;
						} else if (Integer.parseInt(answer.getOption()
								.getOtext()) == 9
								|| Integer.parseInt(answer.getOption()
										.getOtext()) == 10) {
							countOfPromoters++;
						}

						questionDTO.setAnswerDTO(answerDTO);
						Set<Option> options = question.getOptions();
						List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
						for (Option option : options) {
							OptionDTO optionDTO = new OptionDTO();
							optionDTO = mapper.map(option, OptionDTO.class);
							optionDTOs.add(optionDTO);
						}
						questionDTO.setOptionDTOs(optionDTOs);
						questionDTOs.add(questionDTO);

					}

				}

			}

			totalRepondants = countOfDetractors + countOfNeutral
					+ countOfPromoters;
			if (totalRepondants != 0) {
				percentageOfDetractors = (countOfDetractors * 100)
						/ totalRepondants;
				percentageOfNeutral = (countOfNeutral * 100) / totalRepondants;
				percentageOfPromoters = (countOfPromoters * 100)
						/ totalRepondants;
				double npsScore = percentageOfPromoters
						- percentageOfDetractors;

				if (npsScore > 0) {
					userDTO.setNpsType("promoter");
					overallPromoters++;
				} else if (npsScore < 0) {
					userDTO.setNpsType("detractor");
					overallDetractors++;
				} else {
					userDTO.setNpsType("neutral");
					overallNeutral++;
				}

			}

			questionGroupDTO.setQuestions(questionDTOs);
			questionGroupDTOs.add(questionGroupDTO);

			userDTO.setQuestionGroupDTOs(questionGroupDTOs);

			userDTOs.add(userDTO);
		}

		mapOfOverallNps.put("Promoters", overallPromoters);
		mapOfOverallNps.put("Neutral", overallNeutral);
		mapOfOverallNps.put("Detractors", overallDetractors);
		return userDTOs;
	}

	@Override
	public List<String> respondantTop3Benefits(Long respondantId) {
		List<AppUser> appUsers = reportsDAO
				.getReportsBasedOnRespondantId(respondantId);
		List<String> topThreeBenefits = new ArrayList<String>();
		List<String> finalBenefits = new ArrayList<String>();
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			Integer sumId = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next()).getSumId();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumId(sumId);
			// int avgNps = 0;
			for (Questiongroup questiongroup : qgs) {
				QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
				questionGroupDTO = mapper.map(questiongroup,
						QuestionGroupDTO.class);
				Set<Question> qs = questiongroup.getQuestions();
				List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
				for (Question question : qs) {
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					if (answers != null && !answers.isEmpty()) {
						for (Answer answer : answers) {
							if (answer.getSurveyusermapping().getSumId()
									.intValue() == sumId.intValue()) {
								if (question
										.getQuestiongroup()
										.getGrpDesc()
										.equalsIgnoreCase(
												"Quinnox's Overall Engagement")) {
									if ((question.getQname()
											.equalsIgnoreCase("Please describe the Top 3 Benefits when working with Quinnox"))) {
										String repondantBenefits = answer
												.getOption().getOtext();
										topThreeBenefits.add(repondantBenefits);
									}

								}
								AnswerDTO answerDTO = new AnswerDTO();
								answerDTO = mapper.map(answer, AnswerDTO.class);
								answerDTOs.add(answerDTO);
							}
						}
					}
					questionDTO.setAnswerDTOs(answerDTOs);
					Set<Option> options = question.getOptions();
					List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
					for (Option option : options) {
						OptionDTO optionDTO = new OptionDTO();
						optionDTO = mapper.map(option, OptionDTO.class);
						optionDTOs.add(optionDTO);
					}
					questionDTO.setOptionDTOs(optionDTOs);
					questionDTOs.add(questionDTO);
				}
				questionGroupDTO.setQuestions(questionDTOs);
				questionGroupDTOs.add(questionGroupDTO);
			}
			userDTO.setQuestionGroupDTOs(questionGroupDTOs);
			userDTO.setRespondantTopThreeBenefits(topThreeBenefits);
			userDTOs.add(userDTO);
		}
		for (int i = 0; i <= 2; i++) {
			finalBenefits.add(topThreeBenefits.get(i));
		}

		return finalBenefits;
	}

	@Override
	public List<UserDTO> respondantAvgScoresForQuestionSet(Long respondantId,
			Integer questionGrpId) {
		NumberFormat format = new DecimalFormat("#.##");
		List<AppUser> appUsers = reportsDAO
				.getReportsBasedOnRespondantId(respondantId);
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			userDTO = mapper.map(appUser, UserDTO.class);
			Integer sumId = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next()).getSumId();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumIdAndGroupId(sumId, questionGrpId);
			double avgNps = 0;
			int qLength = 1;
			int nonZeroTotal = 0;
			for (Questiongroup questiongroup : qgs) {
				QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
				questionGroupDTO = mapper.map(questiongroup,
						QuestionGroupDTO.class);
				Set<Question> qs = questiongroup.getQuestions();
				List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
				qLength = qs.size();
				for (Question question : qs) {
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					AnswerDTO answerDTO = new AnswerDTO();
					if (answers != null && !answers.isEmpty()) {
						// int averageOfQuestionSet = 0;

						for (Answer answer : answers) {

							answerDTO = mapper.map(answer, AnswerDTO.class);
							if (answer.getSurveyusermapping().getSumId()
									.intValue() == sumId.intValue()) {
								if (questiongroup.getGrpId() == questionGrpId) {
									answer.getOption().getOtext();
									if (answer
											.getOption()
											.getOtext()
											.equalsIgnoreCase(
													"Extremely Dissatisfied")) {
										answerDTO.getOption()
												.setotextValue(1.2);
										avgNps += 1.2;
										nonZeroTotal++;
									} else if (answer.getOption().getOtext()
											.equalsIgnoreCase("Dissatisfied")) {
										answerDTO.getOption()
												.setotextValue(2.4);
										avgNps += 2.4;
										nonZeroTotal++;
									} else if (answer.getOption().getOtext()
											.equalsIgnoreCase("Neutral")) {
										answerDTO.getOption()
												.setotextValue(3.6);
										avgNps += 3.6;
										nonZeroTotal++;
									} else if (answer.getOption().getOtext()
											.equalsIgnoreCase("Satisfied")) {
										answerDTO.getOption()
												.setotextValue(4.8);
										avgNps += 4.8;
										nonZeroTotal++;
									} else if (answer
											.getOption()
											.getOtext()
											.equalsIgnoreCase(
													"Extremely Satisfied")) {
										answerDTO.getOption().setotextValue(6);
										avgNps += 6;
										nonZeroTotal++;
									} else if (answerDTO.getOption().getOtext()
											.equalsIgnoreCase("N/A")) {

										answerDTO.getOption().setotextValue(0);
										avgNps += 0;
									}
								}
								questionDTO.setAnswerDTO(answerDTO);
							}
						}
					}

					Set<Option> options = question.getOptions();
					List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
					for (Option option : options) {
						OptionDTO optionDTO = new OptionDTO();
						optionDTO = mapper.map(option, OptionDTO.class);
						optionDTOs.add(optionDTO);
					}
					questionDTO.setOptionDTOs(optionDTOs);
					questionDTOs.add(questionDTO);
				}
				questionGroupDTO.setQuestions(questionDTOs);
				questionGroupDTOs.add(questionGroupDTO);
			}
			userDTO.setQuestionGroupDTOs(questionGroupDTOs);
			userDTO.setAvgNps(Double.parseDouble(format.format(avgNps
					/ nonZeroTotal)));
			userDTOs.add(userDTO);
		}
		return userDTOs;
	}

	@Override
	public List<UserDTO> getNpsScoreForRespondant(Long respondantId) {
		HashMap<String, Integer> mapOfOverallNps = new HashMap<String, Integer>();
		int overallPromoters = 0, overallDetractors = 0, overallNeutral = 0;
		List<AppUser> appUsers = reportsDAO
				.getReportsBasedOnRespondantId(respondantId);

		int questionGrpId = 5;
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();

		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			userDTO = mapper.map(appUser, UserDTO.class);
			Surveyusermapping sum = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next());
			Integer sumId = sum.getSumId();
			Set<Answer> anss = sum.getAnswers();
			List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumIdAndGroupId(sumId, questionGrpId);
			QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
			int i = 0;
			int countOfPromoters = 0, countOfDetractors = 0, countOfNeutral = 0;
			double percentageOfPromoters = 0, percentageOfDetractors = 0, percentageOfNeutral = 0;
			int totalRepondants = 0;
			for (Answer answer : anss) {
				if (questionGrpId == answer.getQuestion().getQuestiongroup()
						.getGrpId()) {
					if (i++ == 0)
						questionGroupDTO = mapper.map(answer.getQuestion()
								.getQuestiongroup(), QuestionGroupDTO.class);

					Question question = answer.getQuestion();
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					AnswerDTO answerDTO = new AnswerDTO();
					answerDTO = mapper.map(answer, AnswerDTO.class);

					if (answer.getSurveyusermapping().getSumId() == sumId) {
						answer.getOption().getOtext();
						if (Integer.parseInt(answer.getOption().getOtext()) > 0
								&& Integer.parseInt(answer.getOption()
										.getOtext()) <= 6) {
							countOfDetractors++;
						} else if (Integer.parseInt(answer.getOption()
								.getOtext()) == 7
								|| Integer.parseInt(answer.getOption()
										.getOtext()) == 8) {
							countOfNeutral++;
						} else if (Integer.parseInt(answer.getOption()
								.getOtext()) == 9
								|| Integer.parseInt(answer.getOption()
										.getOtext()) == 10) {
							countOfPromoters++;
						}

						questionDTO.setAnswerDTO(answerDTO);
						Set<Option> options = question.getOptions();
						List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
						for (Option option : options) {
							OptionDTO optionDTO = new OptionDTO();
							optionDTO = mapper.map(option, OptionDTO.class);
							optionDTOs.add(optionDTO);
						}
						questionDTO.setOptionDTOs(optionDTOs);
						questionDTOs.add(questionDTO);

					}

				}

			}

			totalRepondants = countOfDetractors + countOfNeutral
					+ countOfPromoters;
			percentageOfDetractors = (countOfDetractors * 100)
					/ totalRepondants;
			percentageOfNeutral = (countOfNeutral * 100) / totalRepondants;
			percentageOfPromoters = (countOfPromoters * 100) / totalRepondants;
			double npsScore = percentageOfPromoters - percentageOfDetractors;

			if (npsScore > 0) {
				userDTO.setNpsType("promoter");
				overallPromoters++;
			} else if (npsScore < 0) {
				userDTO.setNpsType("detractor");
				overallDetractors++;
			} else {
				userDTO.setNpsType("neutral");
				overallNeutral++;
			}

			questionGroupDTO.setQuestions(questionDTOs);
			questionGroupDTOs.add(questionGroupDTO);

			userDTO.setQuestionGroupDTOs(questionGroupDTOs);

			userDTOs.add(userDTO);
		}

		mapOfOverallNps.put("Promoters", overallPromoters);
		mapOfOverallNps.put("Neutral", overallNeutral);
		mapOfOverallNps.put("Detractors", overallDetractors);
		return userDTOs;

	}

	@Override
	public List<UserDTO> respondantAnswerForQuestionSet4(Long respondantId,
			Integer questionGrpId) {
		List<AppUser> appUsers = reportsDAO
				.getReportsBasedOnRespondantId(respondantId);
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			userDTO = mapper.map(appUser, UserDTO.class);
			Integer sumId = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next()).getSumId();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumIdAndGroupId(sumId, questionGrpId);
			double avgNps = 0;
			int qLength = 1;
			for (Questiongroup questiongroup : qgs) {
				QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
				questionGroupDTO = mapper.map(questiongroup,
						QuestionGroupDTO.class);
				Set<Question> qs = questiongroup.getQuestions();
				List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
				qLength = qs.size();
				for (Question question : qs) {
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					AnswerDTO answerDTO = new AnswerDTO();
					if (answers != null && !answers.isEmpty()) {
						// int averageOfQuestionSet = 0;

						for (Answer answer : answers) {

							answerDTO = mapper.map(answer, AnswerDTO.class);
							if (answer.getSurveyusermapping().getSumId()
									.intValue() == sumId.intValue()) {
								if (questiongroup.getGrpId() == questionGrpId) {
									answer.getAtext();
								}
								questionDTO.setAnswerDTO(answerDTO);
							}
						}
					}

					Set<Option> options = question.getOptions();
					List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
					for (Option option : options) {
						OptionDTO optionDTO = new OptionDTO();
						optionDTO = mapper.map(option, OptionDTO.class);
						optionDTOs.add(optionDTO);
					}
					questionDTO.setOptionDTOs(optionDTOs);
					questionDTOs.add(questionDTO);
				}
				questionGroupDTO.setQuestions(questionDTOs);
				questionGroupDTOs.add(questionGroupDTO);
			}
			userDTO.setQuestionGroupDTOs(questionGroupDTOs);
			userDTO.setAvgNps(avgNps / qLength);
			userDTOs.add(userDTO);
		}
		return userDTOs;
	}

	@Override
	public HashMap<String, Integer> npsValueForEachQuestion() {
		HashMap<String, Integer> mapOfOverallNps = new HashMap<String, Integer>();
		int overallPromoters = 0, overallDetractors = 0, overallNeutral = 0;
		List<AppUser> appUsers = reportsDAO.getScoreForQuestionSets();
		int countOfPromoters = 0, countOfDetractors = 0, countOfNeutral = 0, countOfDetractorsForId1 = 0, countOfPromotersForId1 = 0, countOfNeutralForId1 = 0;
		int countOfDetractorsForId2 = 0, countOfPromotersForId2 = 0, countOfNeutralForId2 = 0;
		int countOfDetractorsForId3 = 0, countOfPromotersForId3 = 0, countOfNeutralForId3 = 0;
		int countOfDetractorsForId4 = 0, countOfPromotersForId4 = 0, countOfNeutralForId4 = 0;
		double percentageOfPromoters = 0, percentageOfDetractors = 0, percentageOfNeutral = 0;
		List<Question> listOfQuestionIdForNps = reportsDAO
				.listOfQuestionIdForNPS();
		System.out.println(listOfQuestionIdForNps);

		int questionGrpId = 5;
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();

		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			userDTO = mapper.map(appUser, UserDTO.class);
			Surveyusermapping sum = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next());
			Integer sumId = sum.getSumId();
			Set<Answer> anss = sum.getAnswers();
			List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO
					.findQuestionGroupsBySumIdAndGroupId(sumId, questionGrpId);
			QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
			int i = 0;
			for (int questionId = 21; questionId <= 24; questionId++) {
				int totalRepondants = 0;
				for (Answer answer : anss) {

					// System.out.println(listOfQuestionIdForNps.get(questionGrpId).getQid());
					if (answer.getQuestion().getQid() == questionId) {

						System.out.println("******************"
								+ answer.getQuestion().getQid());
						if (questionGrpId == answer.getQuestion()
								.getQuestiongroup().getGrpId()) {
							if (i++ == 0)
								questionGroupDTO = mapper.map(answer
										.getQuestion().getQuestiongroup(),
										QuestionGroupDTO.class);

							Question question = answer.getQuestion();
							QuestionDTO questionDTO = new QuestionDTO();
							questionDTO = mapper.map(question,
									QuestionDTO.class);
							Set<Answer> answers = question.getAnswers();
							List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
							AnswerDTO answerDTO = new AnswerDTO();
							answerDTO = mapper.map(answer, AnswerDTO.class);

							if (answer.getSurveyusermapping().getSumId() == sumId) {

								answer.getOption().getOtext();
								if (Integer.parseInt(answer.getOption()
										.getOtext()) > 0
										&& Integer.parseInt(answer.getOption()
												.getOtext()) <= 6) {
									if (answer.getQuestion().getQid() == 21) {
										countOfDetractorsForId1++;
									} else if (answer.getQuestion().getQid() == 22) {
										countOfDetractorsForId2++;
									} else if (answer.getQuestion().getQid() == 23) {
										countOfDetractorsForId3++;
									} else if (answer.getQuestion().getQid() == 24) {
										countOfDetractorsForId4++;
									}
								} else if (Integer.parseInt(answer.getOption()
										.getOtext()) == 7
										|| Integer.parseInt(answer.getOption()
												.getOtext()) == 8) {
									if (answer.getQuestion().getQid() == 21) {
										countOfPromotersForId1++;
									} else if (answer.getQuestion().getQid() == 22) {
										countOfPromotersForId2++;
									} else if (answer.getQuestion().getQid() == 23) {
										countOfPromotersForId3++;
									} else if (answer.getQuestion().getQid() == 24) {
										countOfPromotersForId4++;
									}
								} else if (Integer.parseInt(answer.getOption()
										.getOtext()) == 9
										|| Integer.parseInt(answer.getOption()
												.getOtext()) == 10) {
									if (answer.getQuestion().getQid() == 21) {
										countOfNeutralForId1++;
									} else if (answer.getQuestion().getQid() == 22) {
										countOfNeutralForId2++;
									} else if (answer.getQuestion().getQid() == 23) {
										countOfNeutralForId3++;
									} else if (answer.getQuestion().getQid() == 24) {
										countOfNeutralForId4++;
									}
								}

								questionDTO.setAnswerDTO(answerDTO);
								Set<Option> options = question.getOptions();
								List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
								for (Option option : options) {
									OptionDTO optionDTO = new OptionDTO();
									optionDTO = mapper.map(option,
											OptionDTO.class);
									optionDTOs.add(optionDTO);
								}
								questionDTO.setOptionDTOs(optionDTOs);
								questionDTOs.add(questionDTO);

							}

						}

					}

				}
			}

			questionGroupDTO.setQuestions(questionDTOs);
			questionGroupDTOs.add(questionGroupDTO);

			userDTO.setQuestionGroupDTOs(questionGroupDTOs);

			userDTOs.add(userDTO);
		}

		mapOfOverallNps.put("PromotersId1", countOfPromotersForId1);
		mapOfOverallNps.put("PromotersId2", countOfPromotersForId2);
		mapOfOverallNps.put("PromotersId3", countOfPromotersForId3);
		mapOfOverallNps.put("PromotersId4", countOfPromotersForId4);

		mapOfOverallNps.put("DetractorsId1", countOfDetractorsForId1);
		mapOfOverallNps.put("DetractorsId2", countOfDetractorsForId2);
		mapOfOverallNps.put("DetractorsId3", countOfDetractorsForId3);
		mapOfOverallNps.put("DetractorsId4", countOfDetractorsForId4);

		mapOfOverallNps.put("NeutralId1", countOfNeutralForId1);
		mapOfOverallNps.put("NeutralId2", countOfNeutralForId2);
		mapOfOverallNps.put("NeutralId3", countOfNeutralForId3);
		mapOfOverallNps.put("NeutralId4", countOfNeutralForId4);
		mapOfOverallNps.put("Neutral", overallNeutral);
		mapOfOverallNps.put("Detractors", overallDetractors);
		return mapOfOverallNps;
	}

	@Override
	public List<UserDTO> getAllDetailsOfSurveysSentAndRecieved() {
		List<AppUser> csatReportsSentAndRecieved = new ArrayList<AppUser>();
		Mapper mapper = new DozerBeanMapper();
		csatReportsSentAndRecieved = reportsDAO.mapOfSentAndRecievedCSAT();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (AppUser appUser : csatReportsSentAndRecieved) {
			UserDTO userDTO = new UserDTO();
			userDTO = mapper.map(appUser, UserDTO.class);
			userDTOs.add(userDTO);
		}
		return userDTOs;
	}

	@Override
	public List<UserDTO> getAverageScoreForEachQuestionGroupsForEmwise(String engagementManager) {
		List<Integer> getAllGroupId = reportsDAO.getListOfQuestionIdForGrpId();
		List<AppUser> appUsers = reportsDAO.getReportBasedOnEngagementManager(engagementManager);
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		List<Client> clientNames = reportsDAO.getClient();
		NumberFormat format = new DecimalFormat("#.##");
		int qLength = 1;
		double overAllNps1 = 0.0;
		double overAllNps2 = 0.0;
		double overAllNps3 = 0.0;
		double avgOfAll = 0.0;
		int counter = 0;
		int overallNonZeroCount = 0;
		List<Questiongroup> qgs = questiongroupDAO.findQuestionGroupsByGroupIdUser();
		UserDTO userDTO = new UserDTO();
		
		for (Client client : clientNames) {
			for (AppUser appUser : appUsers) {	
				counter++;
				if (appUser.getClient().getClientName() == client.getClientName()) {
					
					System.out.println("the client: "+ appUser.getClient().getClientName());
				
					userDTO = mapper.map(appUser, UserDTO.class);
					int nonZeroCountForEachQuestionGroup = 0;
					for (Integer questiongrpId : getAllGroupId) {
						
						double avg = 0;
						Integer sumId = ((Surveyusermapping) appUser.getSurveyusermappings().iterator().next()).getSumId();
						List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
						for (Questiongroup questiongroup : qgs) {
							
							if (questiongroup.getGrpId() == questiongrpId && questiongrpId != 4 && questiongrpId != 5) {
								double avgNps = 0;
								QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
								questionGroupDTO = mapper.map(questiongroup,QuestionGroupDTO.class);
								Set<Question> qs = questiongroup.getQuestions();
								List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
								qLength = qs.size();
								int nonZeroCount = 0;
								for (Question question : qs) {
									QuestionDTO questionDTO = new QuestionDTO();
									questionDTO = mapper.map(question,QuestionDTO.class);
									Set<Answer> answers = question.getAnswers();
									List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
									AnswerDTO answerDTO = new AnswerDTO();
									if (answers != null && !answers.isEmpty()) {
										for (Answer answer : answers) {
											answerDTO = mapper.map(answer,AnswerDTO.class);
											if (answer.getSurveyusermapping().getSumId().intValue() == sumId.intValue()) {
												if (answer.getOption().getOtext().equalsIgnoreCase("Extremely Dissatisfied")) {
													answerDTO.getOption().setotextValue(1.2);
													avgNps += 1.2;
													nonZeroCount++;
												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Dissatisfied")) {
													answerDTO.getOption().setotextValue(2.4);
													avgNps += 2.4;
													nonZeroCount++;
												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Neutral")) {
													answerDTO.getOption().setotextValue(3.6);
													avgNps += 3.6;
													nonZeroCount++;
												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Satisfied")) {
													answerDTO.getOption().setotextValue(4.8);
													avgNps += 4.8;
													nonZeroCount++;
												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Extremely Satisfied")) {
													answerDTO.getOption().setotextValue(6);
													avgNps += 6;
													nonZeroCount++;
												}

												else if (answerDTO.getOption().getOtext().equalsIgnoreCase("N/A")) {
													answerDTO.getOption().setotextValue(0);
													avgNps += 0;
												}
											}
											questionDTO.setAnswerDTO(answerDTO);
										}
									}
									Set<Option> options = question.getOptions();
									List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
									for (Option option : options) {
										OptionDTO optionDTO = new OptionDTO();
										optionDTO = mapper.map(option,OptionDTO.class);
										optionDTOs.add(optionDTO);
									}
									questionDTO.setOptionDTOs(optionDTOs);
									questionDTOs.add(questionDTO);
								}
								questionGroupDTO.setQuestions(questionDTOs);
								questionGroupDTOs.add(questionGroupDTO);
								if (nonZeroCount != 0) {
									avg = avgNps / nonZeroCount;
								}
								avg = Double.parseDouble(format.format(avg));
								if (questiongroup.getGrpId() == 1) {
									userDTO.setAvgQg1(avg);
									if(avg!=0){
										nonZeroCountForEachQuestionGroup++;
									}
								} else if (questiongroup.getGrpId() == 2) {
									userDTO.setAvgQg2(avg);
									if(avg!=0){
										nonZeroCountForEachQuestionGroup++;
									}
								} else if (questiongroup.getGrpId() == 3) {
									userDTO.setAvgQg3(avg);
									if(avg!=0){
										nonZeroCountForEachQuestionGroup++;
									}
								}
							}
						}
						userDTO.setQuestionGroupDTOs(questionGroupDTOs);
					}
					
					System.out.println("OVERALL NON ZERO COUNT: "+nonZeroCountForEachQuestionGroup);
					if(nonZeroCountForEachQuestionGroup!=0){
					avgOfAll = Double.parseDouble(format.format((userDTO.getAvgQg1() + userDTO.getAvgQg2() + userDTO.getAvgQg3()) / nonZeroCountForEachQuestionGroup));
					}
					if(userDTO.getAvgQg1() == 0 && userDTO.getAvgQg2() == 0 && userDTO.getAvgQg2() == 0){
						System.out.println("**********************************************************************************************Hi");
						avgOfAll = 0;
					}
					userDTO.setAvgOfEachUser(Double.parseDouble(format.format(avgOfAll)));
					userDTO.setClientName(appUser.getClient().getClientName());
					userDTOs.add(userDTO);
				}
			
			}
		}
		return userDTOs;
	}

	@Override
	public HashMap<String, Double> overAllAverageNpsForRespondant(
			int repondantId) {

		HashMap<String, Double> mapOfOverallRagForRespondant = new HashMap<>();

		List<AppUser> appUsers = reportsDAO
				.getReportsBasedOnRespondantId(repondantId);

		int questionGrpId = 0;
		double avgNps = 0.0;
		double avgNps1 = 0;
		double avgNps2 = 0;
		double avgNps3 = 0;
		int qLength = 1;
		double avg = 0;

		NumberFormat format = new DecimalFormat("#.##");
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (AppUser appUser : appUsers) {

			UserDTO userDTO = new UserDTO();

			userDTO = mapper.map(appUser, UserDTO.class);

			Integer sumId = ((Surveyusermapping) appUser
					.getSurveyusermappings().iterator().next()).getSumId();

			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();

			for (int i = 1; i <= 3; i++) {

				avgNps = 0.0;

				questionGrpId = i;

				List<Questiongroup> qgs = questiongroupDAO
						.findQuestionGroupsBySumIdAndGroupId(sumId,
								questionGrpId);

				for (Questiongroup questiongroup : qgs) {

					QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();

					questionGroupDTO = mapper.map(questiongroup,
							QuestionGroupDTO.class);

					Set<Question> qs = questiongroup.getQuestions();

					List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();

					qLength = qs.size();

					for (Question question : qs) {

						QuestionDTO questionDTO = new QuestionDTO();

						questionDTO = mapper.map(question, QuestionDTO.class);

						Set<Answer> answers = question.getAnswers();

						List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();

						AnswerDTO answerDTO = new AnswerDTO();

						if (answers != null && !answers.isEmpty()) {

							// int averageOfQuestionSet = 0;

							for (Answer answer : answers) {

								answerDTO = mapper.map(answer, AnswerDTO.class);

								if (answer.getSurveyusermapping().getSumId()
										.intValue() == sumId.intValue()) {

									if (questiongroup.getGrpId() == questionGrpId) {

										answer.getOption().getOtext();

										if (answer
												.getOption()
												.getOtext()
												.equalsIgnoreCase(
														"Extremely Dissatisfied")) {

											answerDTO.getOption()
													.setotextValue(1.2);

											avgNps += 1.2;

										}

										else if (answer
												.getOption()
												.getOtext()
												.equalsIgnoreCase(
														"Dissatisfied")) {

											answerDTO.getOption()
													.setotextValue(2.4);
											avgNps += 2.4;

										}

										else if (answer.getOption().getOtext()
												.equalsIgnoreCase("Neutral")) {
											answerDTO.getOption()
													.setotextValue(3.6);
											avgNps += 3.6;

										}

										else if (answer.getOption().getOtext()
												.equalsIgnoreCase("Satisfied")) {
											answerDTO.getOption()
													.setotextValue(4.8);
											avgNps += 4.8;

										}

										else if (answer
												.getOption()
												.getOtext()
												.equalsIgnoreCase(
														"Extremely Satisfied")) {
											answerDTO.getOption()
													.setotextValue(6);
											avgNps += 6;

										}

										else if (answerDTO.getOption()
												.getOtext()
												.equalsIgnoreCase("N/A")) {
											answerDTO.getOption()
													.setotextValue(0);
											avgNps += 0;
										}
									}

									questionDTO.setAnswerDTO(answerDTO);
								}
							}
						}

						Set<Option> options = question.getOptions();
						List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
						for (Option option : options) {
							OptionDTO optionDTO = new OptionDTO();
							optionDTO = mapper.map(option, OptionDTO.class);
							optionDTOs.add(optionDTO);

						}

						questionDTO.setOptionDTOs(optionDTOs);
						questionDTOs.add(questionDTO);

					}

					questionGroupDTO.setQuestions(questionDTOs);
					questionGroupDTOs.add(questionGroupDTO);

				}

				userDTO.setQuestionGroupDTOs(questionGroupDTOs);

				if (questionGrpId == 1) {
					avgNps1 += avgNps;
					userDTO.setAvgNps1(Double.parseDouble(format.format(avgNps1
							/ qLength)));
				}

				else if (questionGrpId == 2) {
					avgNps2 += avgNps;
					userDTO.setAvgNps2(Double.parseDouble(format.format(avgNps2
							/ qLength)));
				} else {
					avgNps3 += avgNps;
					userDTO.setAvgNps3(Double.parseDouble(format.format(avgNps3
							/ qLength)));
				}
			}

			avg = (Double.parseDouble(format.format((userDTO.getAvgNps1()
					+ userDTO.getAvgNps2() + userDTO.getAvgNps3()) / 3)));
			userDTOs.add(userDTO);
		}

		mapOfOverallRagForRespondant.put("RagScore", avg);
		return mapOfOverallRagForRespondant;

	}

	@Override
	public HashMap<String, Double> getOverAllScoreForAccount(int clientId) {
		HashMap<String, Double> mapOfOverallRagForAccount= new HashMap<>();
		List<AppUser> appUsers = reportsDAO.getScoreForQuestionSet(clientId);
		int questionGrpId = 0;
		double totalOfEachAnswer = 0.0;
		double totalNpsOfQuestionGroup1 = 0;
		double totalNpsOfQuestionGroup2 = 0;
		double totalNpsOfQuestionGroup3 = 0;
		int qLength = 1;
		double avg = 0;
		int counter = 0;
		int nonZeroQuestionGroup1 = 0;
		int nonZeroQuestionGroup2 = 0;
		int nonZeroQuestionGroup3 = 0;
		NumberFormat format = new DecimalFormat("#.##");
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (AppUser appUser : appUsers) {
			counter++;
			UserDTO userDTO = new UserDTO();
			userDTO = mapper.map(appUser, UserDTO.class);
			Integer sumId = ((Surveyusermapping) appUser.getSurveyusermappings().iterator().next()).getSumId();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();

			for (int i = 1; i <= 3; i++) {
				totalOfEachAnswer = 0.0;
				avg = 0;
				questionGrpId = i;
				List<Questiongroup> qgs = questiongroupDAO.findQuestionGroupsBySumIdAndGroupId(sumId,questionGrpId);
				for (Questiongroup questiongroup : qgs) {
					QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
					questionGroupDTO = mapper.map(questiongroup,QuestionGroupDTO.class);
					Set<Question> qs = questiongroup.getQuestions();
					List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
					int nonZeroCount = 0;
					qLength = qs.size();
					for (Question question : qs) {
						QuestionDTO questionDTO = new QuestionDTO();
						questionDTO = mapper.map(question, QuestionDTO.class);
						Set<Answer> answers = question.getAnswers();
						List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
						AnswerDTO answerDTO = new AnswerDTO();
						if (answers != null && !answers.isEmpty()) {
							// int averageOfQuestionSet = 0;
							for (Answer answer : answers) {
								answerDTO = mapper.map(answer, AnswerDTO.class);
								if (answer.getSurveyusermapping().getSumId().intValue() == sumId.intValue()) {
									if (questiongroup.getGrpId() == questionGrpId) {
										answer.getOption().getOtext();
										if (answer.getOption().getOtext().equalsIgnoreCase("Extremely Dissatisfied")) {
											answerDTO.getOption().setotextValue(1.2);
											totalOfEachAnswer += 1.2;
											nonZeroCount++;
										} 
										else if (answer.getOption().getOtext().equalsIgnoreCase("Dissatisfied")) {
											answerDTO.getOption().setotextValue(2.4);
											totalOfEachAnswer += 2.4;
											nonZeroCount++;
										}
										else if (answer.getOption().getOtext().equalsIgnoreCase("Neutral")) {
											answerDTO.getOption().setotextValue(3.6);
											totalOfEachAnswer += 3.6;
											nonZeroCount++;
										}
										else if (answer.getOption().getOtext().equalsIgnoreCase("Satisfied")) {
											answerDTO.getOption().setotextValue(4.8);
											totalOfEachAnswer += 4.8;
											nonZeroCount++;
										}
										else if (answer.getOption().getOtext().equalsIgnoreCase("Extremely Satisfied")) {
											answerDTO.getOption().setotextValue(6);
											totalOfEachAnswer += 6;
											nonZeroCount++;
										}
										else if (answerDTO.getOption().getOtext().equalsIgnoreCase("N/A")) {
											answerDTO.getOption().setotextValue(0);
											totalOfEachAnswer += 0;
										}

									}
									questionDTO.setAnswerDTO(answerDTO);
								}
							}
						}
						Set<Option> options = question.getOptions();
						List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
						for (Option option : options) {
							OptionDTO optionDTO = new OptionDTO();
							optionDTO = mapper.map(option, OptionDTO.class);
							optionDTOs.add(optionDTO);
						}
						questionDTO.setOptionDTOs(optionDTOs);
						questionDTOs.add(questionDTO);
					}
					questionGroupDTO.setQuestions(questionDTOs);
					questionGroupDTOs.add(questionGroupDTO);
					if (nonZeroCount != 0) {
						avg = totalOfEachAnswer / nonZeroCount;
					}
				}
				userDTO.setQuestionGroupDTOs(questionGroupDTOs);
				if (questionGrpId == 1) {
					if(avg!=0){
						totalNpsOfQuestionGroup1 += avg;
						nonZeroQuestionGroup1++;
						userDTO.setAvgNps1(Double.parseDouble(format.format(totalNpsOfQuestionGroup1/nonZeroQuestionGroup1)));
					}					
				}
				else if (questionGrpId == 2) {	
					if(avg!=0){
						totalNpsOfQuestionGroup2 += avg;
						nonZeroQuestionGroup2++;
						userDTO.setAvgNps2(Double.parseDouble(format.format(totalNpsOfQuestionGroup2/nonZeroQuestionGroup2)));
					}	
				} 
				else if(questionGrpId == 3) {
					if(avg!=0){
						totalNpsOfQuestionGroup3 += avg;
						nonZeroQuestionGroup3++;
						userDTO.setAvgNps3(Double.parseDouble(format.format(totalNpsOfQuestionGroup3/nonZeroQuestionGroup3)));
					}
				}
			}
			avg = (Double.parseDouble(format.format((userDTO.getAvgNps1() + userDTO.getAvgNps2() + userDTO.getAvgNps3()) / 3)));
			userDTOs.add(userDTO);
		}
		mapOfOverallRagForAccount.put("RagScore",Double.parseDouble(format.format(avg)));
		//System.out.println("RAG: " + avg);
		return mapOfOverallRagForAccount;
	}

	@Override
	public List<UserDTO> getNpsPointsForEmWise(String engagementManager) {
		HashMap<String, Integer> mapOfOverallNps = new HashMap<String, Integer>();
		int overallPromoters = 0, overallDetractors = 0, overallNeutral = 0;
		List<AppUser> appUsers = reportsDAO.getReportBasedOnEngagementManager(engagementManager);
		List<Client> clients = reportsDAO.getReportsForClient(engagementManager);
		int questionGrpId = 5;
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		
		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			userDTO = mapper.map(appUser, UserDTO.class);
			userDTO.setClientName(appUser.getClient().getClientName());
			Surveyusermapping sum = ((Surveyusermapping) appUser.getSurveyusermappings().iterator().next());
			Integer sumId = sum.getSumId();
			Set<Answer> anss = sum.getAnswers();
			List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO.findQuestionGroupsBySumIdAndGroupId(sumId, questionGrpId);
			QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
			int i = 0;
			int countOfPromoters = 0, countOfDetractors = 0, countOfNeutral = 0;
			double percentageOfPromoters = 0, percentageOfDetractors = 0, percentageOfNeutral = 0;
			int totalRepondants = 0;
			for (Answer answer : anss) {
				answer.getSurveyusermapping().getAppUser().getClient().getClientName();
				if (questionGrpId == answer.getQuestion().getQuestiongroup().getGrpId()) {
					if (i++ == 0)
						questionGroupDTO = mapper.map(answer.getQuestion().getQuestiongroup(), QuestionGroupDTO.class);
					    Question question = answer.getQuestion();
					    QuestionDTO questionDTO = new QuestionDTO();
					    questionDTO = mapper.map(question, QuestionDTO.class);
					    Set<Answer> answers = question.getAnswers();
					    List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					    AnswerDTO answerDTO = new AnswerDTO();
					    answerDTO = mapper.map(answer, AnswerDTO.class);
					if (answer.getSurveyusermapping().getSumId() == sumId) {
						System.out.println("----------------"+ answer.getSurveyusermapping().getAppUser().getClient().getClientName()+ "----------------");
						answer.getOption().getOtext();
						if (Integer.parseInt(answer.getOption().getOtext()) > 0&& Integer.parseInt(answer.getOption().getOtext()) <= 6) {
							countOfDetractors++;
						} else if (Integer.parseInt(answer.getOption().getOtext()) == 7 || Integer.parseInt(answer.getOption().getOtext()) == 8) {
							countOfNeutral++;
						} else if (Integer.parseInt(answer.getOption().getOtext()) == 9 || Integer.parseInt(answer.getOption().getOtext()) == 10) {
							countOfPromoters++;
						}
						questionDTO.setAnswerDTO(answerDTO);
						Set<Option> options = question.getOptions();
						List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
						for (Option option : options) {
							OptionDTO optionDTO = new OptionDTO();
							optionDTO = mapper.map(option, OptionDTO.class);
							optionDTOs.add(optionDTO);
						}
						questionDTO.setOptionDTOs(optionDTOs);
						questionDTOs.add(questionDTO);
					}
				}
			}
			totalRepondants = countOfDetractors + countOfNeutral + countOfPromoters;
			if (totalRepondants != 0) {
				percentageOfDetractors = (countOfDetractors * 100) / totalRepondants;
				percentageOfNeutral = (countOfNeutral * 100) / totalRepondants;
				percentageOfPromoters = (countOfPromoters * 100) / totalRepondants;
				double npsScore = percentageOfPromoters - percentageOfDetractors;
				if (npsScore > 0) {
					userDTO.setNpsType("promoter");
					overallPromoters++;
				} else if (npsScore < 0) {
					userDTO.setNpsType("detractor");
					overallDetractors++;
				} else {
					userDTO.setNpsType("neutral");
					overallNeutral++;
				}
			}
			questionGroupDTO.setQuestions(questionDTOs);
			questionGroupDTOs.add(questionGroupDTO);
			userDTO.setQuestionGroupDTOs(questionGroupDTOs);
			userDTOs.add(userDTO);
		}
		mapOfOverallNps.put("Promoters", overallPromoters);
		mapOfOverallNps.put("Neutral", overallNeutral);
		mapOfOverallNps.put("Detractors", overallDetractors);
		return userDTOs;
	}

	@Override
	public HashMap<String, Double> getOverallScoreForEmWise(
			String engagementManager) {
		HashMap<String, Double> mapOfAvgScore = new HashMap<String, Double>();
		List<Integer> getAllGroupId = reportsDAO.getListOfQuestionIdForGrpId();
		List<AppUser> appUsers = reportsDAO.getReportBasedOnEngagementManager(engagementManager);
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		List<Client> clientNames = reportsDAO.getClient();
		NumberFormat format = new DecimalFormat("#.##");
		int qLength = 1;
		double overAllNps1 = 0.0;
		double overAllNps2 = 0.0;
		double overAllNps3 = 0.0;
		double avgOfAll = 0.0;
		int counter = 0;
		int overallNonZeroCount = 0;
		double overallavgNps = 0;
		int nonZeroForQuestionGroup1 = 0, nonZeroForQuestionGroup2 = 0, nonZeroForQuestionGroup3 = 0;
		List<Questiongroup> qgs = questiongroupDAO.findQuestionGroupsByGroupIdUser();
		UserDTO userDTO = new UserDTO();
		for (Client client : clientNames) {
			for (AppUser appUser : appUsers) {
				if (appUser.getClient().getClientName() == client.getClientName()) {
					userDTO = mapper.map(appUser, UserDTO.class);
					int nonZeroCountForEachQuestionGroup = 0;
					for (Integer questiongrpId : getAllGroupId) {
						double avg = 0;
						Integer sumId = ((Surveyusermapping) appUser.getSurveyusermappings().iterator().next()).getSumId();
						List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
						for (Questiongroup questiongroup : qgs) {
							if (questiongroup.getGrpId() == questiongrpId && questiongrpId != 4 && questiongrpId != 5) {
								double totalOfEachAnswer = 0;
								QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
								questionGroupDTO = mapper.map(questiongroup,QuestionGroupDTO.class);
								Set<Question> qs = questiongroup.getQuestions();
								List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
								qLength = qs.size();
								int nonZeroCount = 0;
								for (Question question : qs) {
									QuestionDTO questionDTO = new QuestionDTO();
									questionDTO = mapper.map(question,QuestionDTO.class);
									Set<Answer> answers = question.getAnswers();
									List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
									AnswerDTO answerDTO = new AnswerDTO();
									if (answers != null && !answers.isEmpty()) {
										for (Answer answer : answers) {
											answerDTO = mapper.map(answer,AnswerDTO.class);
											if (answer.getSurveyusermapping().getSumId().intValue() == sumId.intValue()) {
												if (answer.getOption().getOtext().equalsIgnoreCase("Extremely Dissatisfied")) {
													answerDTO.getOption().setotextValue(1.2);
													totalOfEachAnswer += 1.2;
													nonZeroCount++;
												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Dissatisfied")) {
													answerDTO.getOption().setotextValue(2.4);
													totalOfEachAnswer += 2.4;
													nonZeroCount++;
												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Neutral")) {
													answerDTO.getOption().setotextValue(3.6);
													totalOfEachAnswer += 3.6;
													nonZeroCount++;
												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Satisfied")) {
													answerDTO.getOption().setotextValue(4.8);
													totalOfEachAnswer += 4.8;
													nonZeroCount++;
												}

												else if (answer.getOption().getOtext().equalsIgnoreCase("Extremely Satisfied")) {
													answerDTO.getOption().setotextValue(6);
													totalOfEachAnswer += 6;
													nonZeroCount++;
												}

												else if (answerDTO.getOption().getOtext().equalsIgnoreCase("N/A")) {
													answerDTO.getOption().setotextValue(0);
													totalOfEachAnswer += 0;
												}
											}
											questionDTO.setAnswerDTO(answerDTO);
										}
									}
									Set<Option> options = question.getOptions();
									List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
									for (Option option : options) {
										OptionDTO optionDTO = new OptionDTO();
										optionDTO = mapper.map(option,OptionDTO.class);
										optionDTOs.add(optionDTO);
									}
									questionDTO.setOptionDTOs(optionDTOs);
									questionDTOs.add(questionDTO);
								}
								questionGroupDTO.setQuestions(questionDTOs);
								questionGroupDTOs.add(questionGroupDTO);
								if (nonZeroCount != 0) {
									avg = totalOfEachAnswer / nonZeroCount;
								}
								avg = Double.parseDouble(format.format(avg));
								if (questiongroup.getGrpId() == 1) {
									userDTO.setAvgQg1(avg);
									overAllNps1 += userDTO.getAvgQg1();
									System.out.println("The overall nps of  question group 1: " +overAllNps1);
									if(avg!=0){
										nonZeroForQuestionGroup1++;
										nonZeroCountForEachQuestionGroup++;
									}
									
								} else if (questiongroup.getGrpId() == 2) {
									userDTO.setAvgQg2(avg);
									overAllNps2 += userDTO.getAvgQg2();
									System.out.println("The overall nps of  question group 2: " +overAllNps2);
									if(avg!=0){
										nonZeroForQuestionGroup2++;
										nonZeroCountForEachQuestionGroup++;
									}
								} else if (questiongroup.getGrpId() == 3) {
									userDTO.setAvgQg3(avg);
									overAllNps3 += userDTO.getAvgQg3();
									System.out.println("The overall nps of  question group 3: " +overAllNps3);
									if(avg!=0){
										nonZeroForQuestionGroup3++;
										nonZeroCountForEachQuestionGroup++;
									}
								}
							}
						}
						userDTO.setQuestionGroupDTOs(questionGroupDTOs);
					}
					
					if(nonZeroCountForEachQuestionGroup!=0){
						counter++;
						avgOfAll = Double.parseDouble(format.format((userDTO.getAvgQg1() + userDTO.getAvgQg2() + userDTO.getAvgQg3()) / nonZeroCountForEachQuestionGroup));
						System.out.println("The avg of all : " +avgOfAll);
						overallavgNps+=avgOfAll;
					}
					if(userDTO.getAvgQg1() == 0 && userDTO.getAvgQg2() == 0 && userDTO.getAvgQg2() == 0){
						avgOfAll = 0;
					}
					userDTO.setAvgOfEachUser(Double.parseDouble(format.format(avgOfAll)));
					userDTOs.add(userDTO);
				}
			}
		}		
		mapOfAvgScore.put("Question1",Double.parseDouble(format.format(overAllNps1/nonZeroForQuestionGroup1)));
		mapOfAvgScore.put("Question2",Double.parseDouble(format.format(overAllNps2/nonZeroForQuestionGroup2)));
		mapOfAvgScore.put("Question3",Double.parseDouble(format.format(overAllNps3/nonZeroForQuestionGroup3)));
		mapOfAvgScore.put("overAllNps", Double.parseDouble(format.format(overallavgNps / counter)));

		return mapOfAvgScore;
	}

	@Override
	public HashMap<String, Double> getOverAllNpsScoreForEmWise(
			String engagementManager) {
		HashMap<String, Double> mapOfOverallNps = new HashMap<String, Double>();
		int overallPromoters = 0, overallDetractors = 0, overallNeutral = 0;
		List<AppUser> appUsers = reportsDAO.getReportBasedOnEngagementManager(engagementManager);
		List<Client> clients = reportsDAO.getReportsForClient(engagementManager);
		double percentageOfPromoters = 0, percentageOfDetractors = 0, percentageOfNeutral = 0;
		double percentageOfPromoters1 = 0, percentageOfDetractors1 = 0, percentageOfNeutral1 = 0;
		NumberFormat format = new DecimalFormat("#");
		int questionGrpId = 5;
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		
		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			userDTO = mapper.map(appUser, UserDTO.class);
			userDTO.setClientName(appUser.getClient().getClientName());
			Surveyusermapping sum = ((Surveyusermapping) appUser.getSurveyusermappings().iterator().next());
			Integer sumId = sum.getSumId();
			Set<Answer> anss = sum.getAnswers();
			List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO.findQuestionGroupsBySumIdAndGroupId(sumId, questionGrpId);
			QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
			
			int i = 0;
			int countOfPromoters = 0, countOfDetractors = 0, countOfNeutral = 0;
			int totalRepondants = 0;
			for (Answer answer : anss) {
				answer.getSurveyusermapping().getAppUser().getClient().getClientName();
				if (questionGrpId == answer.getQuestion().getQuestiongroup().getGrpId()) {
					if (i++ == 0)
						questionGroupDTO = mapper.map(answer.getQuestion().getQuestiongroup(), QuestionGroupDTO.class);
					Question question = answer.getQuestion();
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					AnswerDTO answerDTO = new AnswerDTO();
					answerDTO = mapper.map(answer, AnswerDTO.class);

					if (answer.getSurveyusermapping().getSumId() == sumId) {
						System.out.println("----------------" + answer.getSurveyusermapping().getAppUser().getClient().getClientName()+ "----------------");
						answer.getOption().getOtext();
						if (Integer.parseInt(answer.getOption().getOtext()) > 0 && Integer.parseInt(answer.getOption().getOtext()) <= 6) {
							countOfDetractors++;
						} else if (Integer.parseInt(answer.getOption().getOtext()) == 7 || Integer.parseInt(answer.getOption().getOtext()) == 8) {
							countOfNeutral++;
						} else if (Integer.parseInt(answer.getOption().getOtext()) == 9 || Integer.parseInt(answer.getOption().getOtext()) == 10) {
							countOfPromoters++;
						}
						questionDTO.setAnswerDTO(answerDTO);
						Set<Option> options = question.getOptions();
						List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
						for (Option option : options) {
							OptionDTO optionDTO = new OptionDTO();
							optionDTO = mapper.map(option, OptionDTO.class);
							optionDTOs.add(optionDTO);
						}
						questionDTO.setOptionDTOs(optionDTOs);
						questionDTOs.add(questionDTO);
					}
				}
			}
			totalRepondants = countOfDetractors + countOfNeutral + countOfPromoters;
			System.out.println("the total respondent: " + totalRepondants);
			if (totalRepondants != 0) {
				percentageOfDetractors = (countOfDetractors * 100) / totalRepondants;
				System.out.println("the total Detractors: " + countOfDetractors);
				percentageOfNeutral = (countOfNeutral * 100) / totalRepondants;
				System.out.println("the total Neutral: " + countOfNeutral);
				percentageOfPromoters = (countOfPromoters * 100)/ totalRepondants;
				System.out.println("the total Promoters: " + countOfPromoters);
				double npsScore = percentageOfPromoters - percentageOfDetractors;
				if (npsScore > 0) {
					userDTO.setNpsType("promoter");
					overallPromoters++;
				} else if (npsScore < 0) {
					userDTO.setNpsType("detractor");
					overallDetractors++;
				} else {
					userDTO.setNpsType("neutral");
					overallNeutral++;
				}
			}
			double totalRepondants1 = overallPromoters + overallDetractors + overallNeutral;
			if (totalRepondants1 != 0) {
				percentageOfDetractors1 = (overallDetractors * 100) / totalRepondants1;
				percentageOfNeutral1 = (overallNeutral * 100) / totalRepondants1;
				percentageOfPromoters1 = (overallPromoters * 100) / totalRepondants1;
			}
			questionGroupDTO.setQuestions(questionDTOs);
			questionGroupDTOs.add(questionGroupDTO);
			userDTO.setQuestionGroupDTOs(questionGroupDTOs);
			userDTOs.add(userDTO);
		}
		mapOfOverallNps.put("Promoters",Double.parseDouble(format.format(percentageOfPromoters1)));
		mapOfOverallNps.put("Neutral",Double.parseDouble(format.format(percentageOfNeutral1)));
		mapOfOverallNps.put("Detractors",Double.parseDouble(format.format(percentageOfDetractors1)));
		return mapOfOverallNps;

	}

	@Override
	public List<String> getTop3BenefitsForEmWise(String engagementManager) {
		List<AppUser> appUsers = reportsDAO.getReportBasedOnEngagementManager(engagementManager);
		List<String> topThreeBenefits = new ArrayList<String>();
		List<String> finalBenefits = new ArrayList<String>();
		Mapper mapper = new DozerBeanMapper();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		for (AppUser appUser : appUsers) {
			UserDTO userDTO = new UserDTO();
			Integer sumId = ((Surveyusermapping) appUser.getSurveyusermappings().iterator().next()).getSumId();
			List<QuestionGroupDTO> questionGroupDTOs = new ArrayList<QuestionGroupDTO>();
			List<Questiongroup> qgs = questiongroupDAO.findQuestionGroupsBySumId(sumId);
			// int avgNps = 0;
			for (Questiongroup questiongroup : qgs) {
				QuestionGroupDTO questionGroupDTO = new QuestionGroupDTO();
				questionGroupDTO = mapper.map(questiongroup,QuestionGroupDTO.class);
				Set<Question> qs = questiongroup.getQuestions();
				List<QuestionDTO> questionDTOs = new ArrayList<QuestionDTO>();
				for (Question question : qs) {
					QuestionDTO questionDTO = new QuestionDTO();
					questionDTO = mapper.map(question, QuestionDTO.class);
					Set<Answer> answers = question.getAnswers();
					List<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
					if (answers != null && !answers.isEmpty()) {
						for (Answer answer : answers) {
							if (answer.getSurveyusermapping().getSumId().intValue() == sumId.intValue()) {
								if (question.getQuestiongroup().getGrpDesc().equalsIgnoreCase("Quinnox's Overall Engagement")) {
									if ((question.getQname().equalsIgnoreCase("Please describe the Top 3 Benefits when working with Quinnox"))) {
										String repondantBenefits = answer.getOption().getOtext();
										topThreeBenefits.add(repondantBenefits);
									}
								}
								AnswerDTO answerDTO = new AnswerDTO();
								answerDTO = mapper.map(answer, AnswerDTO.class);
								answerDTOs.add(answerDTO);
							}
						}
					}
					questionDTO.setAnswerDTOs(answerDTOs);
					Set<Option> options = question.getOptions();
					List<OptionDTO> optionDTOs = new ArrayList<OptionDTO>();
					for (Option option : options) {
						OptionDTO optionDTO = new OptionDTO();
						optionDTO = mapper.map(option, OptionDTO.class);
						optionDTOs.add(optionDTO);
					}
					questionDTO.setOptionDTOs(optionDTOs);
					questionDTOs.add(questionDTO);
				}
				questionGroupDTO.setQuestions(questionDTOs);
				questionGroupDTOs.add(questionGroupDTO);
			}
			userDTO.setQuestionGroupDTOs(questionGroupDTOs);
			userDTO.setRespondantTopThreeBenefits(topThreeBenefits);
			userDTOs.add(userDTO);
		}
		for (int i = 0; i <= 2; i++) {
			finalBenefits.add(topThreeBenefits.get(i));
		}

		return finalBenefits;
	}

}
