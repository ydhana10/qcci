package com.qwebservices.business.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import jxl.biff.formula.ParseContext;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.qwebservices.business.service.UserService;
import com.qwebservices.data.dao.AppUserDAO;
import com.qwebservices.data.dao.ClientDAO;
import com.qwebservices.data.dao.SurveyusermappingDAO;
import com.qwebservices.data.dao.VerticalDAO;
import com.qwebservices.data.domain.AppUser;
import com.qwebservices.data.domain.Client;
import com.qwebservices.data.domain.Role;
import com.qwebservices.data.domain.Survey;
import com.qwebservices.data.domain.Surveyusermapping;
import com.qwebservices.data.domain.Vertical;
import com.qwebservices.infra.dto.UserDTO;
import com.qwebservices.infra.util.ConfigValues;
import com.qwebservices.infra.util.MailUtility;
import com.qwebservices.infra.util.QWebUtil;
import com.qwebservices.web.constants.QWebConstants;

@Transactional
public class UserServiceImpl implements UserService {
	public static final Logger log = Logger.getLogger(UserServiceImpl.class);

	@Autowired
	AppUserDAO appUserDAO;

	@Autowired
	ClientDAO clientDAO;

	@Autowired
	VerticalDAO verticalDAO;

	UserService userService;
	@Autowired
	private ApplicationContext applicationContext;

	@PostConstruct
	public void postContruct() {
		userService = applicationContext.getBean(UserService.class);
	}

	@Autowired
	SurveyusermappingDAO surveyusermappingDAO;

	@Override
	public UserDTO authenticateUser(String username) {
		AppUser appUser = appUserDAO.findUserBySSOId(username);
		UserDTO userDTO = null;
		if (appUser != null) {
			userDTO = new UserDTO();
			userDTO.setEmail(appUser.getEmail());
			userDTO.setFirstName(appUser.getFirstName());
			userDTO.setLastName(appUser.getLastName());
			userDTO.setId(appUser.getId());
			userDTO.setRole(appUser.getRole().getType());
			userDTO.setSsoId(username);
			userDTO.setState(appUser.getState());
			userDTO.setPassword(appUser.getPassword());
			/*Surveyusermapping surveyusermapping = surveyusermappingDAO.findById(appUser.getId().intValue());
			if(surveyusermapping != null)
				userDTO.setSumId(surveyusermapping.getSumId());*/
			return userDTO;
		} else {
			return null;
		}
	}

	/*@Override
	public UserDTO authenticateUser(String ssoId, String password) {
		AppUser appUser = appUserDAO.findUserBySSOId(ssoId);
		UserDTO userDTO = null;
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		if (appUser != null && passwordEncoder.matches(password, appUser.getPassword())) {
			userDTO = new UserDTO();
			userDTO.setEmail(appUser.getEmail());
			userDTO.setFirstName(appUser.getFirstName());
			userDTO.setLastName(appUser.getLastName());
			userDTO.setId(appUser.getId());
			userDTO.setRole(appUser.getRole().getType());
			userDTO.setSsoId(ssoId);
			userDTO.setState(appUser.getState());
			userDTO.setPassword(appUser.getPassword());
			return userDTO;
		} else {
			return null;
		}
	}*/
	@Override
	public UserDTO authenticateUser(String ssoId, String password) {
		AppUser appUser2 = appUserDAO.findUserBySSOId(ssoId);
		AppUser appUser = new AppUser();
		UserDTO userDTO = null;
		
			userDTO = new UserDTO();
			userDTO.setEmail(ssoId);
			userDTO.setFirstName(password);
			userDTO.setState("active");
			userDTO.setRole("2");
			userDTO.setLastName("");
			userDTO.setSsoId(ssoId);
			userDTO.setEngagementManager("");
			userDTO.setSurveyId(1);
			
			userDTO.setClientId(1);
			AppUser appUser1 = appUserDAO.findUserBySSOIdWithoutState(userDTO.getEmail());
			if (appUser1 == null) {
				userDTO = insertUser1(userDTO);
			} else {
				if(appUser1.getState().equalsIgnoreCase("active")){
					if(appUser1.getRole().getId() == 1){
						userDTO.setRole("1");
						userDTO.setPassword(appUser1.getPassword());
						return userDTO;
					}
					else{
						Surveyusermapping sumapping= (Surveyusermapping) appUser1.getSurveyusermappings().iterator().next();
						userDTO.setSumId(sumapping.getSumId());
						userDTO.setPassword(appUser1.getPassword());
						return userDTO;
					}
				}else{
					return null;
				}
			}

			return userDTO;
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public boolean insertUser(UserDTO userDTO) {
		boolean isSuccess = true;
		try {
			String password = QWebUtil.generateRandomPassword();
			System.out.println(password);
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String passwordE = passwordEncoder.encode(userDTO.getFirstName());
			userDTO.setPassword(passwordE);
			userDTO.setSsoId(userDTO.getEmail());
			Mapper mapper = new DozerBeanMapper();
			AppUser appuser = new AppUser();
			appuser = mapper.map(userDTO, AppUser.class);
			Role role = new Role();
			role.setId(2);
			appuser.setRole(role);
			Client client = new Client();
			client.setClientId(userDTO.getClientId());
			appuser.setClient(client);
			appuser.setState("active");

			String appLink = ConfigValues.getConfigValue("APP_URL");
			byte[] encodedBytes = Base64.encodeBase64((userDTO.getEmail() + "").getBytes());
			appLink = appLink.replaceAll("un_1", new String(encodedBytes));
			encodedBytes = Base64.encodeBase64((password + "@123").getBytes());
			appLink = appLink.replaceAll("pd_2", new String(encodedBytes));
			
			Surveyusermapping surveyusermapping = new Surveyusermapping();
			surveyusermapping.setStatus(QWebConstants.STATUS_VALUES.ASSIGNED);
			Survey survey = new Survey();
			survey.setSid(userDTO.getSurveyId());
			surveyusermapping.setSurvey(survey);
			surveyusermapping.setAppUser(appuser);
			surveyusermapping.setSurveyLink(appLink);
			surveyusermapping.setNotificationCount(0);
			Set<Surveyusermapping> set = new HashSet<Surveyusermapping>();
			set.add(surveyusermapping);
			appuser.setSurveyusermappings(set);
			appUserDAO.persist(appuser);
			
			Surveyusermapping surveyusermapping2 = (Surveyusermapping) appuser.getSurveyusermappings().iterator()
					.next();
			userDTO.setPassword(password);
			userDTO.setSumId(surveyusermapping2.getSumId());
			appLink = appLink.replaceAll("sid", new String(Base64.encodeBase64((userDTO.getSumId() + "").getBytes())));
			surveyusermapping.setSurveyLink(appLink);
			
			appUserDAO.merge(appuser);
			// new
			// MailUtility().sendMailIdCreationNotification(userDTO,appLink);
		} catch (MappingException e) {
			isSuccess = false;
		}
		return isSuccess;

	}
	
	public UserDTO insertUser1(UserDTO userDTO) {
		boolean isSuccess = true;
		try {
			String password = QWebUtil.generateRandomPassword();
			System.out.println(password);
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String passwordE = passwordEncoder.encode(userDTO.getFirstName());
			userDTO.setPassword(passwordE);
			userDTO.setSsoId(userDTO.getEmail());
			Mapper mapper = new DozerBeanMapper();
			AppUser appuser = new AppUser();
			appuser = mapper.map(userDTO, AppUser.class);
			Role role = new Role();
			role.setId(2);
			appuser.setRole(role);
			Client client = new Client();
			client.setClientId(userDTO.getClientId());
			appuser.setClient(client);
			appuser.setState("active");

			String appLink = ConfigValues.getConfigValue("APP_URL");
			byte[] encodedBytes = Base64.encodeBase64((userDTO.getEmail() + "").getBytes());
			appLink = appLink.replaceAll("un_1", new String(encodedBytes));
			encodedBytes = Base64.encodeBase64((password + "@123").getBytes());
			appLink = appLink.replaceAll("pd_2", new String(encodedBytes));
			Surveyusermapping surveyusermapping = new Surveyusermapping();
			surveyusermapping.setStatus(QWebConstants.STATUS_VALUES.ASSIGNED);
			Survey survey = new Survey();
			survey.setSid(userDTO.getSurveyId());
			surveyusermapping.setSurvey(survey);
			surveyusermapping.setAppUser(appuser);
			surveyusermapping.setSurveyLink(appLink);
			surveyusermapping.setNotificationCount(0);
			Set<Surveyusermapping> set = new HashSet<Surveyusermapping>();
			set.add(surveyusermapping);
			appuser.setSurveyusermappings(set);
			appUserDAO.persist(appuser);
			
			Surveyusermapping surveyusermapping2 = (Surveyusermapping) appuser.getSurveyusermappings().iterator()
					.next();
			userDTO.setPassword(password);
			userDTO.setSumId(surveyusermapping2.getSumId());
			appLink = appLink.replaceAll("sid", new String(Base64.encodeBase64((userDTO.getSumId() + "").getBytes())));
			surveyusermapping.setSurveyLink(appLink);
			
			appUserDAO.merge(appuser);
			//appUserDAO.merge(appuser);
			// new
			// MailUtility().sendMailIdCreationNotification(userDTO,appLink);
		} catch (MappingException e) {
			isSuccess = false;
		}
		return userDTO;

	}

	@Override
	public boolean uploadMultipleUsers(UserDTO userDTO, String filename) throws Exception {
		int COLUMN_LENGTH = 8;
		XSSFWorkbook myExcelBook = null;
		try {
			myExcelBook = new XSSFWorkbook(new FileInputStream(new File(filename)));
		} catch (IOException e) {
			log.error("Exception Occurred", e);
			new MailUtility().sendErrorNotification(userDTO, e);
		}
		XSSFSheet myExcelSheet = myExcelBook.getSheet("Sheet1");
		FormulaEvaluator evaluator = myExcelBook.getCreationHelper().createFormulaEvaluator();
		evaluator.evaluateAll();
		XSSFRow header = myExcelSheet.getRow(0);
		int numberOfRows = myExcelSheet.getLastRowNum() + 1;

		if (header.getLastCellNum() == COLUMN_LENGTH) {
			// Read Headers
			for (int i = 0; i < COLUMN_LENGTH; i++) {
				System.out.println(header.getCell(i));
			}
			for (int i = 1; i < numberOfRows; i++) {
				XSSFRow row = myExcelSheet.getRow(i);
				UserDTO user1DTO = new UserDTO();
				user1DTO.setSurveyId(userDTO.getSurveyId());
				if (row == null || (row.getCell(0) == null || row.getCell(0).equals(""))) {
					break;
				}
				try {
					for (int j = 0; j < COLUMN_LENGTH; j++) {
						if (row.getCell(j) == null || row.getCell(j).toString() == "") {
							log.info("Row " + i + 1 + " Column " + j + 1
									+ " does not contain a valid value. Skipping this row.");
							log.debug("Row " + i + 1 + " Column " + j + 1
									+ " does not contain a valid value. Skipping this row.");
							log.error("Row " + i + 1 + " Column " + j + 1
									+ " does not contain a valid value. Skipping this row.");
							Exception ee = new Exception("Row " + i + 1 + " Column " + j + 1
									+ " does not contain a valid value. Skipping this row.");
							new MailUtility().sendErrorNotification(userDTO, ee);
							throw ee;
						} else {

							switch (j) {
							case 0:
								user1DTO.setVerticalName(row.getCell(j).toString().trim());
								Vertical ver = verticalDAO.findActiveVerticalByVerticalName(user1DTO.getVerticalName());
								if (ver == null) {
									log.warn("Vertical not found by Vertical Name, checking by vertical id");
									// new
									// MailUtility().sendErrorNotification(userDTO,
									// new Exception(
									// "Vertical not found by Vertical Name"));
								} else {
									user1DTO.setVerticalId(ver.getVerticalId());
								}
								break;
							case 1:
								if (user1DTO.getVerticalId() == null) {
									Integer verticalId = null;
									try {
										if (row.getCell(j).getCellType() == Cell.CELL_TYPE_FORMULA) {
											Cell cell = evaluator.evaluateInCell(row.getCell(j));
											Double dval = Double.parseDouble(cell.getNumericCellValue() + "");
											verticalId = dval.intValue();
										}

									} catch (NumberFormatException nfe) {
										new MailUtility().sendErrorNotification(userDTO, nfe);
									}
									if (verticalId != null) {
										Vertical vertical = verticalDAO.findActiveVerticalByVerticalId(verticalId);
										if (vertical != null) {
											user1DTO.setVerticalId(verticalId);
										}
									} else {
										// create new Vertical
										Vertical newVertical = new Vertical();
										newVertical.setActiveStatus("Yes");
										newVertical.setVerticalName(user1DTO.getVerticalName());
										user1DTO.setVerticalId(verticalDAO.save(newVertical));
										log.warn("Vertical not found by Vertical Name and id, Created a new vertical");
										new MailUtility().sendErrorNotification(userDTO, new Exception(
												"Vertical not found by Vertical Name and id, Created a new vertical"));
									}
								}
								break;
							case 2:
								user1DTO.setClientName(row.getCell(j).toString().trim());
								Client client = clientDAO.findClientByClientName(user1DTO.getClientName());
								if (client != null) {
									user1DTO.setClientId(client.getClientId());
								}
								break;
							case 3:
								if (user1DTO.getClientName() == null) {
									Integer clientId = null;
									try {
										Cell cell1 = evaluator.evaluateInCell(row.getCell(j));
										Double cval = Double.parseDouble(cell1.getNumericCellValue() + "");
										clientId = cval.intValue();
									} catch (NumberFormatException nfe) {
										new MailUtility().sendErrorNotification(userDTO, nfe);
										break;
									}
									Client client1 = clientDAO.findClientByClientId(clientId);
									if (client1 != null) {
										user1DTO.setClientId(clientId);
									} else {
										// Insert New Client
										Client newClient = new Client();
										newClient.setActiveStatus("Yes");
										newClient.setClientName(user1DTO.getClientName());
										newClient.getVertical().setVerticalId(user1DTO.getVerticalId());
										Integer newClientId = clientDAO.save(newClient);
										user1DTO.setClientId(newClientId);
										log.warn("Client/Account not found by Client Name and id, Created a new vertical");
										new MailUtility()
												.sendErrorNotification(
														userDTO,
														new Exception(
																"Client/Account not found by Client Name and id, Created a new vertical"));
									}
								}
								break;
							case 4:
								user1DTO.setFirstName(row.getCell(j).toString().trim());
								break;
							case 5:
								user1DTO.setLastName(row.getCell(j).toString().trim());
								break;
							case 6:
								user1DTO.setEmail(row.getCell(j).toString().trim());
								user1DTO.setSsoId(row.getCell(j).toString().trim());
								break;
							case 7:
								user1DTO.setEngagementManager(row.getCell(j).toString().trim());
								break;
							}
						}
						System.out.println(row.getCell(j));
					}
				} catch (Exception e) {
					log.error("Exception Occurred: ",e);
					log.error("There was problem while processing row " + i
							+ " in upload. Continuing with other rows. Please check and correct the same."
							+ userDTO.toString());
					new MailUtility().sendErrorNotification(userDTO, new Exception(
							"There was problem while processing row " + i
									+ " in upload. Please check and correct the same." + userDTO.toString()));
					continue;

				}
				userService.insertUser(user1DTO);

			}
		} else {
			Exception ee = new Exception(
					"There should be 8 columns in excel file which you uploaded, please check and upload it again.");
			new MailUtility().sendErrorNotification(userDTO, ee);
			throw ee;
		}

		return false;
	}

	@Override
	public UserDTO authenticateUserBySurveyId(String userName, int surveyId) {
		AppUser appUser = appUserDAO.findUserBySSOIdAndSurveyId(userName, surveyId);
		UserDTO userDTO = null;
		if (appUser != null) {
			userDTO = new UserDTO();
			userDTO.setEmail(appUser.getEmail());
			userDTO.setFirstName(appUser.getFirstName());
			userDTO.setLastName(appUser.getLastName());
			userDTO.setId(appUser.getId());
			userDTO.setRole(appUser.getRole().getType());
			userDTO.setSsoId(userName);
			userDTO.setState(appUser.getState());
			userDTO.setPassword(appUser.getPassword());
			return userDTO;
		} else {
			return null;
		}
	}

	@Override
	public void sendNotification(Integer sumId) {
		Surveyusermapping surveyusermapping = surveyusermappingDAO.findSurveyUMBySumId(sumId);
		UserDTO userDTO = new UserDTO();
		if (surveyusermapping != null) {
			AppUser appUser = surveyusermapping.getAppUser();
			if (appUser != null) {
				userDTO = new UserDTO();
				userDTO.setEmail(appUser.getEmail());
				userDTO.setFirstName(appUser.getFirstName());
				userDTO.setLastName(appUser.getLastName());
				userDTO.setId(appUser.getId());
				userDTO.setRole(appUser.getRole().getType());
				userDTO.setSsoId(appUser.getSsoId());
				userDTO.setState(appUser.getState());
				userDTO.setPassword(appUser.getPassword());
				String appLink = surveyusermapping.getSurveyLink();
				int notificationCount = surveyusermapping.getNotificationCount();
				if (notificationCount == 0) {
					new MailUtility().sendMailIdCreationNotification(userDTO, appLink);
					surveyusermapping.setNotificationCount(notificationCount + 1);
					appUserDAO.merge(appUser);
				} else {
					new MailUtility().sendNotification(userDTO, appLink);
					surveyusermapping.setNotificationCount(notificationCount + 1);
					appUserDAO.merge(appUser);
				}
			}
		}
	}

	@Override
	public void sendNotificationToMany(String sumIds) {
		String[] sumIdA = sumIds.split(",");
		for (String sumIdStr : sumIdA) {
			Integer sumId = (Integer.parseInt(sumIdStr));
			Surveyusermapping surveyusermapping = surveyusermappingDAO.findSurveyUMBySumId(sumId);
			UserDTO userDTO = new UserDTO();
			if (surveyusermapping != null) {
				AppUser appUser = surveyusermapping.getAppUser();
				if (appUser != null) {
					userDTO = new UserDTO();
					userDTO.setEmail(appUser.getEmail());
					userDTO.setFirstName(appUser.getFirstName());
					userDTO.setLastName(appUser.getLastName());
					userDTO.setId(appUser.getId());
					userDTO.setRole(appUser.getRole().getType());
					userDTO.setSsoId(appUser.getSsoId());
					userDTO.setState(appUser.getState());
					userDTO.setPassword(appUser.getPassword());
					String appLink = surveyusermapping.getSurveyLink();
					int notificationCount = surveyusermapping.getNotificationCount();
					if (notificationCount == 0) {
						new MailUtility().sendMailIdCreationNotification(userDTO, appLink);
						surveyusermapping.setNotificationCount(notificationCount + 1);
						appUserDAO.merge(appUser);
					} else {
						new MailUtility().sendNotification(userDTO, appLink);
						surveyusermapping.setNotificationCount(notificationCount + 1);
						appUserDAO.merge(appUser);
					}
				}
			}
		}
	}
	
	public static void main(String[] args) {
		BCryptPasswordEncoder enc = new BCryptPasswordEncoder();
		System.out.println(enc.encode("Password"));
		
	}
	/*@Override

	public List<UserDTO> listEmanager(int clientId) {
	Mapper mapper = new DozerBeanMapper();
	List<UserDTO> userDTOs = null;
	List<AppUser> emanager = appUserDAO.findEmanagersByClientId(clientId);
	if(emanager!=null && !emanager.isEmpty()){
	userDTOs = new ArrayList<UserDTO>();
	for (AppUser appUsers : emanager) {
	UserDTO userDTO = new UserDTO();
	userDTO = mapper.map(appUsers, UserDTO.class);
	userDTOs.add(userDTO);
	}
}
	return userDTOs;

	 

	}
*/
	@Override

	public List<UserDTO> listEmanager(int verticalId) {
	Mapper mapper = new DozerBeanMapper();
	List<UserDTO> userDTOs = null;
	List<AppUser> emanager = appUserDAO.findEmanagersByVerticalId(verticalId);
	if(emanager!=null && !emanager.isEmpty()){
	userDTOs = new ArrayList<UserDTO>();
	for (AppUser appUsers : emanager) {
	UserDTO userDTO = new UserDTO();
	userDTO = mapper.map(appUsers, UserDTO.class);
	userDTOs.add(userDTO);
	}
   }
	return userDTOs;

	 

	}
}
