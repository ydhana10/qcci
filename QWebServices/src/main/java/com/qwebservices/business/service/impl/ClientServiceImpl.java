package com.qwebservices.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import jxl.common.Logger;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.qwebservices.business.service.ClientService;
import com.qwebservices.data.dao.ClientDAO;
import com.qwebservices.data.domain.Client;
import com.qwebservices.infra.dto.ClientDTO;
import com.qwebservices.infra.dto.VerticalDTO;

@Transactional
public class ClientServiceImpl implements ClientService{
	
	public static final Logger log = Logger.getLogger(ClientServiceImpl.class);
	
	@Autowired
	ClientDAO clientDAO;
	
	@Override
	public List<ClientDTO> listClients(int verticalId){
		Mapper mapper = new DozerBeanMapper();
		List<ClientDTO> clientDTOs = null;
		List<Client> clients = clientDAO.findClientsByVerticalId(verticalId);
		if(clients!=null && !clients.isEmpty()){
			clientDTOs = new ArrayList<ClientDTO>();
			for (Client client : clients) {
				ClientDTO clientDTO = new ClientDTO();
				clientDTO = mapper.map(client, ClientDTO.class);
				clientDTOs.add(clientDTO);
			}
		}
		return clientDTOs;
	}
}
