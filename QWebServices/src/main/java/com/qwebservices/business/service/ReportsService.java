package com.qwebservices.business.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.qwebservices.data.domain.AppUser;
import com.qwebservices.data.domain.Surveyusermapping;
import com.qwebservices.infra.dto.ClientDTO;
import com.qwebservices.infra.dto.QuestionGroupDTO;
import com.qwebservices.infra.dto.SurveyDTO;
import com.qwebservices.infra.dto.UserDTO;



public interface ReportsService {

	/*List<QuestionGroupDTO> accountWiseReports(int clientId);*/


	HashMap<String, Float> getOverallNpsForAccount(Integer clientId);


	List<UserDTO> getAverageScoresForEachQestionGrpForAccount(Integer clientId,
			Integer questionGrpId);


	List<String> getTop3BenefitsForAccount(Integer clientId);


	String getClientNameByClientId(int clientId);


	List<UserDTO> getScoresForQestionGrp4ForAccount(Integer clientId,
			Integer questionGrpId);


	/*HashMap<String, Float> npsPromoters(Integer clientId);*/


	HashMap<String, Float> overallNpsForOrganization();


	List<UserDTO> npsOverallForEachQuestion();


	List<UserDTO> getListOfPromotersAndDetractorsAndNeutral();

	List<String> getTop3BenefitsForOrganization();
	//HashMap<String, Float> npsScoreQuestionWise(Integer clientId);


	HashMap<String, Integer> overallCountOfDetractorsPromotersAndNeutral();
	
	List<ClientDTO> getAverageScoresForAllClientOfOrganization();


	List<UserDTO> getListOfPromotersAndDetractorsAndNeutralForClientId(int clientId);


	List<String> respondantTop3Benefits(Long respondantId);


	List<UserDTO> respondantAvgScoresForQuestionSet(Long respondantId,Integer questionGrpId);


	List<UserDTO> getNpsScoreForRespondant(Long respondantId);


	List<UserDTO> respondantAnswerForQuestionSet4(Long respondantId,
			Integer questionGrpId);


	HashMap<String, Integer> npsValueForEachQuestion();


	List<UserDTO> getAllDetailsOfSurveysSentAndRecieved();


	HashMap<String, Double> getOverallScoreOfEachQuestiongrpForOrganization();


	HashMap<String, Float> overAllNps();


	HashMap<String, Double> overAllAverageNpsForRespondant(int repondantId);


	HashMap<String, Double> getOverAllScoreForAccount(int clientId);


	List<UserDTO> getAverageScoreForEachQuestionGroupsForEmwise(String engagementManager);


	List<UserDTO> getNpsPointsForEmWise(String engagementManager);


	HashMap<String, Double> getOverallScoreForEmWise(String engagementManager);


	HashMap<String, Double> getOverAllNpsScoreForEmWise(String engagementManager);


	List<String> getTop3BenefitsForEmWise(String engagementManager);
	
	

}