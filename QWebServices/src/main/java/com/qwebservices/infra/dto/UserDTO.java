package com.qwebservices.infra.dto;




import java.io.Serializable;

import java.util.List;




public class UserDTO implements Serializable{

 

/**

* 

*/

private static final long serialVersionUID = 1640873759953203424L;

private Long id;

private String ssoId;

private String role;

private String firstName;

private String lastName;

private String email;

private String state;

private String password;

private Integer clientId;

private Integer surveyId;

private Integer sumId;

private String clientName;

private String verticalName;

private String projectName;

private String engagementManager;

private Long fileoriginalsize;

private String contenttype;

private String base64;

private Integer verticalId;

private List<QuestionGroupDTO> questionGroupDTOs;

double avgNps;

double avgQg1;

double avgQg2;

double avgQg3;

double overallavgQg1;

double overallavgQg2;

double overallavgQg3;

double AvgNps1;

double AvgNps2;

double AvgNps3;

 

double avgOfEachUser;

 

double questionavg1;

double questionavg2;

double questionavg3;

double questionavg4;

double questionavg5;

double questionavg6;

 

 

 

 

public double getQuestionavg1() {

return questionavg1;

}

public void setQuestionavg1(double questionavg1) {

this.questionavg1 = questionavg1;

}

public double getQuestionavg2() {

return questionavg2;

}

public void setQuestionavg2(double questionavg2) {

this.questionavg2 = questionavg2;

}

public double getQuestionavg3() {

return questionavg3;

}

public void setQuestionavg3(double questionavg3) {

this.questionavg3 = questionavg3;

}

public double getQuestionavg4() {

return questionavg4;

}

public void setQuestionavg4(double questionavg4) {

this.questionavg4 = questionavg4;

}

public double getQuestionavg5() {

return questionavg5;

}

public void setQuestionavg5(double questionavg5) {

this.questionavg5 = questionavg5;

}

public double getQuestionavg6() {

return questionavg6;

}

public void setQuestionavg6(double questionavg6) {

this.questionavg6 = questionavg6;

}

public double getAvgOfEachUser() {

return avgOfEachUser;

}

public void setAvgOfEachUser(double avgOfEachUser) {

this.avgOfEachUser = avgOfEachUser;

}

public double getAvgNps1() {

return AvgNps1;

}

public void setAvgNps1(double avgNps1) {

AvgNps1 = avgNps1;

}

public double getAvgNps2() {

return AvgNps2;

}

public void setAvgNps2(double avgNps2) {

AvgNps2 = avgNps2;

}

public double getAvgNps3() {

return AvgNps3;

}

public void setAvgNps3(double avgNps3) {

AvgNps3 = avgNps3;

}

public double getOverallavgQg1() {

return overallavgQg1;

}

public void setOverallavgQg1(double overallavgQg1) {

this.overallavgQg1 = overallavgQg1;

}

public double getOverallavgQg2() {

return overallavgQg2;

}

public void setOverallavgQg2(double overallavgQg2) {

this.overallavgQg2 = overallavgQg2;

}

public double getOverallavgQg3() {

return overallavgQg3;

}

public void setOverallavgQg3(double overallavgQg3) {

this.overallavgQg3 = overallavgQg3;

}

public double getAvgQg1() {

return avgQg1;

}

public void setAvgQg1(double avgQg1) {

this.avgQg1 = avgQg1;

}

public double getAvgQg2() {

return avgQg2;

}

public void setAvgQg2(double avgQg2) {

this.avgQg2 = avgQg2;

}

public double getAvgQg3() {

return avgQg3;

}

public void setAvgQg3(double avgQg3) {

this.avgQg3 = avgQg3;

}

int countOfDitractors;

int countOfPromoters;

int countOfNeutral;

int totalRespondants;

 

double dictractorsPercentage;

double neutralPercentage;

double promotersPercentage;

 

public String getNpsType() {

return npsType;

}

public void setNpsType(String npsType) {

this.npsType = npsType;

}

private String npsType;

public double getDictractorsPercentage() {

return dictractorsPercentage;

}

public void setDictractorsPercentage(double dictractorsPercentage) {

this.dictractorsPercentage = dictractorsPercentage;

}

public double getNeutralPercentage() {

return neutralPercentage;

}

public void setNeutralPercentage(double neutralPercentage) {

this.neutralPercentage = neutralPercentage;

}

public double getPromotersPercentage() {

return promotersPercentage;

}

public void setPromotersPercentage(double promotersPercentage) {

this.promotersPercentage = promotersPercentage;

}

List<String> respondantTopThreeBenefits;

public List<String> getRespondantTopThreeBenefits() {

return respondantTopThreeBenefits;

}

public int getCountOfDitractors() {

return countOfDitractors;

}

public void setCountOfDitractors(int countOfDitractors) {

this.countOfDitractors = countOfDitractors;

}

public int getCountOfPromoters() {

return countOfPromoters;

}

public void setCountOfPromoters(int countOfPromoters) {

this.countOfPromoters = countOfPromoters;

}

public int getCountOfNeutral() {

return countOfNeutral;

}

public void setCountOfNeutral(int countOfNeutral) {

this.countOfNeutral = countOfNeutral;

}

public int getTotalRespondants() {

return totalRespondants;

}

public void setTotalRespondants(int totalRespondants) {

this.totalRespondants = totalRespondants;

}

public void setRespondantTopThreeBenefits(

List<String> respondantTopThreeBenefits) {

this.respondantTopThreeBenefits = respondantTopThreeBenefits;

}

public Long getId() {

return id;

}

public void setId(Long id) {

this.id = id;

}

public String getSsoId() {

return ssoId;

}

public void setSsoId(String ssoId) {

this.ssoId = ssoId;

}

public String getRole() {

return role;

}

public void setRole(String role) {

this.role = role;

}

public String getFirstName() {

return firstName;

}

public void setFirstName(String firstName) {

this.firstName = firstName;

}

public String getLastName() {

return lastName;

}

public void setLastName(String lastName) {

this.lastName = lastName;

}

public String getEmail() {

return email;

}

public void setEmail(String email) {

this.email = email;

}

public String getState() {

return state;

}

public void setState(String state) {

this.state = state;

}

public String getPassword() {

return password;

}

public void setPassword(String password) {

this.password = password;

}

public Integer getClientId() {

return clientId;

}

public void setClientId(Integer prId) {

this.clientId = prId;

}

public Integer getSurveyId() {

return surveyId;

}

public void setSurveyId(Integer surveyId) {

this.surveyId = surveyId;

}

public String getClientName() {

return clientName;

}

public void setClientName(String clientName) {

this.clientName = clientName;

}

public String getVerticalName() {

return verticalName;

}

public void setVerticalName(String verticalName) {

this.verticalName = verticalName;

}

public String getProjectName() {

return projectName;

}

public void setProjectName(String projectName) {

this.projectName = projectName;

}

public String getEngagementManager() {

return engagementManager;

}

public void setEngagementManager(String engagementManager) {

this.engagementManager = engagementManager;

}

public Long getFileoriginalsize() {

return fileoriginalsize;

}

public void setFileoriginalsize(Long fileoriginalsize) {

this.fileoriginalsize = fileoriginalsize;

}

public String getContenttype() {

return contenttype;

}

public void setContenttype(String contenttype) {

this.contenttype = contenttype;

}

public String getBase64() {

return base64;

}

public void setBase64(String base64) {

this.base64 = base64;

}

public Integer getVerticalId() {

return verticalId;

}

public void setVerticalId(Integer verticalId) {

this.verticalId = verticalId;

}

 

@Override

public String toString() {

return "UserDTO [id=" + id + ", ssoId=" + ssoId + ", role=" + role

+ ", firstName=" + firstName + ", lastName=" + lastName

+ ", email=" + email + ", state=" + state + ", password="

+ password + ", clientId=" + clientId + ", surveyId="

+ surveyId + ", sumId=" + sumId + ", clientName=" + clientName

+ ", verticalName=" + verticalName + ", projectName="

+ projectName + ", engagementManager=" + engagementManager

+ ", fileoriginalsize=" + fileoriginalsize + ", contenttype="

+ contenttype + ", base64=" + base64 + ", verticalId="

+ verticalId + ", questionGroupDTOs=" + questionGroupDTOs

+ ", avgNps=" + avgNps + ", countOfDitractors="

+ countOfDitractors + ", countOfPromoters=" + countOfPromoters

+ ", countOfNeutral=" + countOfNeutral + ", totalRespondants="

+ totalRespondants + ", dictractorsPercentage="

+ dictractorsPercentage + ", neutralPercentage="

+ neutralPercentage + ", promotersPercentage="

+ promotersPercentage + ", npsType=" + npsType

+ ", respondantTopThreeBenefits=" + respondantTopThreeBenefits

+ "]";

}

public Integer getSumId() {

return sumId;

}

public void setSumId(Integer sumId) {

this.sumId = sumId;

}

public List<QuestionGroupDTO> getQuestionGroupDTOs() {

return questionGroupDTOs;

}

public void setQuestionGroupDTOs(List<QuestionGroupDTO> questionGroupDTOs) {

this.questionGroupDTOs = questionGroupDTOs;

}

 

 

public double getAvgNps() {

return avgNps;

}

public void setAvgNps(double avgNps) {

this.avgNps = avgNps;

}

 

 

 




}