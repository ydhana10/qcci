package com.qwebservices.infra.dto;

import java.util.List;

public class SurveyDTO {
	private Integer sid;
	private String sname;
	private String sdesc;
	private String sinstructions;
	private UserDTO userDTO;
	private List<QuestionGroupDTO> questionGroupDTOs;
	private Integer sumId;
	private String status;
	private Integer notificationCount;
	public UserDTO getUserDTO() {
		return userDTO;
	}
	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}
	public List<QuestionGroupDTO> getQuestionGroupDTOs() {
		return questionGroupDTOs;
	}
	public void setQuestionGroupDTOs(List<QuestionGroupDTO> questionGroupDTOs) {
		this.questionGroupDTOs = questionGroupDTOs;
	}
	public Integer getSid() {
		return sid;
	}
	public void setSid(Integer sid) {
		this.sid = sid;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getSdesc() {
		return sdesc;
	}
	public void setSdesc(String sdesc) {
		this.sdesc = sdesc;
	}
	public String getSinstructions() {
		return sinstructions;
	}
	public void setSinstructions(String sinstructions) {
		this.sinstructions = sinstructions;
	}
	public Integer getSumId() {
		return sumId;
	}
	public void setSumId(Integer sumId) {
		this.sumId = sumId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getNotificationCount() {
		return notificationCount;
	}
	public void setNotificationCount(Integer notificationCount) {
		this.notificationCount = notificationCount;
	}
	
	

}
