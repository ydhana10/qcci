package com.qwebservices.infra.dto;


public class IdeaDTO {
	
	private String ideaId;
	private String categoryName;
	private String ideaName;
	private String ideaDesc;
	private String teamMembers;
	private String cf1;
	private String cf2;
	private String cf3;
	private boolean isCurrent;
	private String location;
	private int numberOfQuestions;
	private boolean acceptQuestions;
	
	
	public String getIdeaId() {
		return ideaId;
	}
	public void setIdeaId(String ideaId) {
		this.ideaId = ideaId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getIdeaName() {
		return ideaName;
	}
	public void setIdeaName(String ideaName) {
		this.ideaName = ideaName;
	}
	public String getIdeaDesc() {
		return ideaDesc;
	}
	public void setIdeaDesc(String ideaDesc) {
		this.ideaDesc = ideaDesc;
	}
	public String getTeamMembers() {
		return teamMembers;
	}
	public void setTeamMembers(String teamMembers) {
		this.teamMembers = teamMembers;
	}
	public String getCf1() {
		return cf1;
	}
	public void setCf1(String cf1) {
		this.cf1 = cf1;
	}
	public String getCf2() {
		return cf2;
	}
	public void setCf2(String cf2) {
		this.cf2 = cf2;
	}
	public String getCf3() {
		return cf3;
	}
	public void setCf3(String cf3) {
		this.cf3 = cf3;
	}
	public boolean isCurrent() {
		return isCurrent;
	}
	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getNumberOfQuestions() {
		return numberOfQuestions;
	}
	public void setNumberOfQuestions(int numberOfQuestions) {
		this.numberOfQuestions = numberOfQuestions;
	}
	public boolean isAcceptQuestions() {
	    return acceptQuestions;
	}
	public void setAcceptQuestions(boolean acceptQuestions) {
	    this.acceptQuestions = acceptQuestions;
	}
	
	

}
