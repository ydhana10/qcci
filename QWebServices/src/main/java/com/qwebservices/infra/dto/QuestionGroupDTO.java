package com.qwebservices.infra.dto;


import java.util.List;

public class QuestionGroupDTO implements java.io.Serializable {

	private Integer grpId;
	private String grpName;
	private String grpDesc;
	private List<QuestionDTO> questions;
	private int avgNps;
	

	public Integer getGrpId() {
		return this.grpId;
	}

	public void setGrpId(Integer grpId) {
		this.grpId = grpId;
	}

	public String getGrpName() {
		return this.grpName;
	}

	public void setGrpName(String grpName) {
		this.grpName = grpName;
	}

	public String getGrpDesc() {
		return this.grpDesc;
	}

	public void setGrpDesc(String grpDesc) {
		this.grpDesc = grpDesc;
	}

	public List<QuestionDTO> getQuestions() {
		return this.questions;
	}

	public void setQuestions(List<QuestionDTO> questions) {
		this.questions = questions;
	}

	public int getAvgNps() {
		return avgNps;
	}

	public void setAvgNps(int avgNps) {
		this.avgNps = avgNps;
	}

}
