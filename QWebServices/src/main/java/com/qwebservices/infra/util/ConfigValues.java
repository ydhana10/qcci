package com.qwebservices.infra.util;



import java.io.File;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

public class ConfigValues {

	private static Properties properties;
	private static Properties templates;
	private static URL inputStream;

	static {
		// Load Configuration Values from Properties File to Memory
		ConfigValues configValues = new ConfigValues();
	properties = configValues.loadProperties("config.properties");
	templates = configValues.loadProperties("mail-templates.properties");
	inputStream = configValues.loadHtmlTemplate("/mailtemplates/errorhandler.html");
	}

	public static String getConfigValue(String key) {
		String value = "";
		synchronized (properties) {
			String appEnv = properties.getProperty("APP_ENV");
			value = properties.getProperty(key + "." + appEnv.toUpperCase());
		}
		return value;
	}
	public static File getErrHtmlTemplate() throws URISyntaxException {
		File file = null;
		synchronized (inputStream) {
			file = new File(inputStream.toURI());
		}
		return file;
		
	}

	public static String getTemplateValue(String key) {
		String value = "";
		synchronized (templates) {
			value = templates.getProperty(key);
		}
		return value;
	}

	private Properties loadProperties(String configFilePath) {
		Properties prop = new Properties();
		InputStream inputstream = null;
		try {
			inputstream = getClass().getResourceAsStream(configFilePath);
			prop.load(inputstream);
		} catch (Exception e) {

		} finally {
			try {
				if (inputstream != null) {
					inputstream.close();
				}
			} catch (Exception ex) {
			}
		}
		return prop;
	}
	
	private URL loadHtmlTemplate(String configFilePath) {
		URL url = null;
		try {
			url = this.getClass().getResource("/mailtemplates/errorhandler.html");
		} catch (Exception e) {
			
		}
		return url;
	}

	
}
