package com.qwebservices.infra.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.codec.binary.Base64;

import com.qwebservices.infra.dto.UserDTO;

public class MailUtility {

	// create TRF
	public String sendMailIdCreationNotification(UserDTO userDTO,String appLink) {

		try {
			String mailTo = userDTO.getEmail();
			String mailCc = ConfigValues.getConfigValue("ADMIN_TEAM_MAIL_ID");
			
			
						
			String mailSubject = "Welcome to the 2016-2017 Quinnox Client Satisfaction Survey!";
			String mailContent = "";
			BufferedReader br = null;
			br = new BufferedReader(new InputStreamReader(getClass()
					.getResourceAsStream("/mailtemplates/mtemplate.html")));
			StringBuilder sb1 = new StringBuilder();
			String line;
			try {
				while ((line = br.readLine()) != null) {
					sb1.append(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
            
			mailContent = new String(sb1.toString());
			
			
			
			
			mailContent = mailContent.replaceAll("rlink", appLink);
			mailContent = mailContent.replaceAll("fn_nm",userDTO.getFirstName());
			mailContent = mailContent.replaceAll("ln_nm",userDTO.getLastName());
			
			sendMail(mailTo, mailContent, mailSubject,mailCc);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "Success";

	}
	
	
	public String sendErrorNotification(UserDTO userDTO,Exception exception) {

		try {
			String mailTo = userDTO.getEmail();
			String mailCc = ConfigValues.getConfigValue("SUPPORT_GROUP_MAIL_ID");
			
			
						
			String mailSubject = "CSAT: Error";
			String mailContent = "";
			BufferedReader br = null;
			br = new BufferedReader(new InputStreamReader(getClass()
					.getResourceAsStream("/mailtemplates/errormtemplate.html")));
			StringBuilder sb1 = new StringBuilder();
			String line;
			try {
				while ((line = br.readLine()) != null) {
					sb1.append(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
            
			mailContent = new String(sb1.toString());
		
			mailContent = mailContent.replace("unqnx",userDTO.getSsoId());
			mailContent = mailContent.replace("enqnx",exception.getMessage());

			
			sendMail(mailTo, mailContent, mailSubject,mailCc);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "Success";

	}
	
	public String sendNotification(UserDTO userDTO,String appLink) {

		try {
			String mailTo = userDTO.getEmail();
			String mailCc = ConfigValues.getConfigValue("SUPPORT_GROUP_MAIL_ID");
			
			
						
			String mailSubject = "Reminder: 2016-2017 Quinnox Client Satisfaction Survey!";
			String mailContent = "";
			BufferedReader br = null;
			br = new BufferedReader(new InputStreamReader(getClass()
					.getResourceAsStream("/mailtemplates/mtemplate.html")));
			StringBuilder sb1 = new StringBuilder();
			String line;
			try {
				while ((line = br.readLine()) != null) {
					sb1.append(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
            
			mailContent = new String(sb1.toString());
			mailContent = mailContent.replaceAll("rlink", appLink);
			mailContent = mailContent.replaceAll("fn_nm",userDTO.getFirstName());
			mailContent = mailContent.replaceAll("ln_nm",userDTO.getLastName());

			
			sendMail(mailTo, mailContent, mailSubject,mailCc);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "Success";

	}

	public void sendMail(String mailTo, String mailContent, String mailSubject, String mailCc)
			throws Exception {
		final String mailFrom = ConfigValues.getConfigValue("MAIL_SMTP_USER");
		final String password = ConfigValues
				.getConfigValue("MAIL_SMTP_PASSWORD");
		Properties properties = new Properties();
		properties.put("mail.smtp.host",
				ConfigValues.getConfigValue("MAIL_SMTP_SERVER"));
		properties.put("mail.smtp.port",
				ConfigValues.getConfigValue("MAIL_SMTP_PORT"));
		properties.put("mail.smtp.auth",
				ConfigValues.getConfigValue("MAIL_SMTP_AUTH"));
		properties.put("mail.smtp.starttls.enable",
				ConfigValues.getConfigValue("MAIL_SMTP_STARTTLS_ENABLE"));

		// creates a new session with an authenticator
		Authenticator auth = new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(mailFrom, password);
			}
		};

		try {
			Session session = Session.getInstance(properties, auth);
			MimeMessage message = new MimeMessage(session);
			message.addHeaderLine("method=REQUEST");
			message.addHeaderLine("charset=UTF-8");
			message.addHeaderLine("component=VEVENT");
			message.setFrom(new InternetAddress(ConfigValues.getConfigValue("MAIL_ID_WITH_DISPLAY_NAME")));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					mailTo));
			message.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(mailCc));

			Multipart multipart = new MimeMultipart();

			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(mailContent, "text/html");
			multipart.addBodyPart(htmlPart);
			message.setSubject(mailSubject);
			message.setContent(multipart);
			Transport.send(message);
		} catch (Exception e) {
			throw e;
		}
	}
	
	
}
