package com.qwebservices.infra.constants;

	public interface TRFCommonConstants {
		
		public static final String LOGIN_SUCCESS_STATUS = "success";
		public static final String LOGIN_FAILURE_STATUS = "failure";
		public static final String LOGIN_CUSTOM_MESSAGE="User is not registered in Transform Please contact ESG Team";
		public static final String LOGIN_FAILURE_ERROR="Username/Password is invalid";
		public static final String LOGIN_SERVER_ISSUE="Something went wrong please try again";
		
		//Role constants
		public static final String SUPER_ADMIN="Super Admin";
		public static final String RMG_TEAM="RMG Team";
		public static final String SDMPM="SDMPM";
		public static final String RMG_ADMIN="RMG Admin";
		public static final String TAG_TEAM="TAG Team";
		public static final String TAG_ADMIN="TAG Admin";
		public static final String INTERVIEWER_ROLE="INTERVIEWER";
		public static final String VDH_APPROVAL_ROLE="VDH";
		public static final String VBH_APPROVAL_ROLE="VBH";
		public static final String EXECUTIVE="Executive";
		
		//TRF Creation Statuses
		public static final String TRF_CREATION_FAILURE_MESSAGE = "Some Problem In Creating TRF.Please try again.";
		public static final String TRF_CREATION_SUCCESS_STATUS = "Success";
		public static final String TRF_CREATION_FAILURE_STATUS = "Failure";
		
		//Hire Status
		public static final String DEFAULT_HIRE_STATUS = "Open";
		public static final String EXTERNAL_HIRE_STATUS = "External Hire";
		public static final String INTERNAL_HIRE_STATUS = "Internal Hire";
		
		
		//TRF Status
		public static final String DEFAULT_TRF_STATUS = "Submitted";
		public static final String CHANGED_TRF_STATUS = "Changed";
		public static final String ACCEPTED_TRF_STATUS = "Accepted";
		public static final String REJECTED_TRF_STATUS = "Rejected";
		public static final String CLOSED_TRF_STATUS = "Closed";
		public static final String APPROVED_TRF_STATUS = "Approved";
		public static final String RECRUITING_TRF_STATUS = "Recruiting";
		public static final String RECRUITED_TRF_STATUS = "Recruited";
		public static final String MIGRATED_TRF_STATUS = "Migrated";
		public static final String REJECTED_BY_APPROVER_TRF_STATUS ="Rejected-Approver";
		public static final String ABORTED_NO_RESPONSE_PM_TRF_STATUS ="Aborted - No response from PM";
		public static final String ABORTED_REQUESTED_BY_PM_TRF_STATUS ="Aborted - Requested by PM";
		
		
		
		//Approval Status
		public static final String APPROVER_REJECTED_COMMENTS = "One of the approver rejected the request";
		public static final String CANCELLED_APPROVER_STATUS_IH_COMMENTS = "Mapped resource Internally";
		public static final String CANCELLED_APPROVER_STATUS_RMG_REJECT_COMMENTS ="Request Cancelled as the TRF was Rejected";
		public static final String CANCELLED_APPROVER_STATUS_ABORTED_COMMENTS ="Request Cancelled as the TRF was aborted";
		public static final String PENDING_APPROVAL_STATUS = "Pending";
		public static final String APPROVAL_STATUS_UPDATE_SUCCESS = "Success";
		public static final String APPROVAL_STATUS_UPDATE_FAILURE = "Failure";
		public static final String APPROVAL_STATUS_CREATOR_UPDATE_FAILURE = "SDMPM Notification Failure";
		public static final String APPROVED_APPROVAL_STATUS = "Approved";
		public static final String REJECTED_APPROVAL_STATUS = "Rejected";
		public static final String CANCELLED_APPROVAL_STATUS = "Cancelled";
		public static final String ABORTED_TRF_STATUS = "Aborted";
		//Action for Mails
		public static final String NEW_TRF = "NEW";
		
		//TRF Recruitment Status
		public static final String OPEN_RECRUITMENT_STATUS = "Open";
		public static final String CLOSED_RECRUITMENT_STATUS = "Closed";
		public static final String CANCELLED_RECRUITMENT_STATUS = "Cancelled";
				
		//TRF JD Status
		public static final String PENDING_JD_STATUS = "Pending";
		public static final String SUBMITTED_JD_STATUS = "Completed";
		public static final String NA_JD_STATUS = "NA";
		public static final String DEFAULT_JD_STATUS = "";

		//Interview  Status
		public static final String SCHEDULED_INTERVIEW_STATUS = "Scheduled";
		public static final String RESCHEDULED_INTERVIEW_STATUS = "Rescheduled";
		public static final String CANCELLED_INTERVIEW_STATUS = "Cancelled";
		public static final String COMPLETED_INTERVIEW_STATUS = "Completed";

		public static final String DEFAULT_INTERVIEW_TAG_STATUS = "Pending";
		
		public static final String SUCCESS_STATUS = "SUCCESS";
		
		public static final String FAILURE_STATUS = "FAILURE";
		
		//Candidate Status
		public static final String ENROLLED_CANDIDATE_STATUS = "Enrolled";
		public static final String INTERVIEWING_CANDIDATE_STATUS = "Interviewing";
		public static final String RECRUITED_CANDIDATE_STATUS = "Recruited";
		public static final String OFFERED_CANDIDATE_STATUS = "Offered";
		public static final String DROPPED_CANDIDATE_STATUS = "Dropped";
		public static final String SUBMITTED_TRF_STATUS = "Submitted";
		
		
		//Active and Inactive Status
		public static final String ACTIVE_STATUS = "Yes";
		
		//Max Tries for Deadlock
		public static final int DEADLOCK_RETRY_COUNT = 5;
		
		public String ENCRYPT_PASSCODE = "0123"; // This has to be 16 characters
		
		public String ENCRYPT_KEY = "Replace";

		
		
	}


